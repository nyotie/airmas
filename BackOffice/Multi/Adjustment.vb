﻿Imports DevExpress.Utils

Public Class Adjustment
    Dim selected_cell As Integer

    Private Sub PengirimanPartner_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("GetTheID")
        xSet.Tables.Remove("mt_adjustment")
        xSet.Tables.Remove("dt_adjustment")
        xSet.Tables.Remove("DaftarStock")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub PengirimanPartner_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglAdjustment.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub PengirimanPartner_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = 0 Then
            TglAdjustment.EditValue = Now
        Else
            SQLquery = "SELECT tgl_adjustment, cat_adjustment FROM mt_adjustment WHERE id_adjustment=" & selected_id
            ExDb.ExecQuery(SQLquery, xSet, "mt_adjustment")

            TglAdjustment.EditValue = xSet.Tables("mt_adjustment").Rows(0).Item("tgl_adjustment")
            Catatan.EditValue = xSet.Tables("mt_adjustment").Rows(0).Item("cat_adjustment")
        End If
    End Sub

    Private Sub DaftarBarang_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarBarang.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell = Repository.EditValue
    End Sub

    Private Sub ViewAdjustment_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewAdjustment.CellValueChanged
        Try
            Dim drow As DataRow = xSet.Tables("DaftarStock").Select(String.Format("ID='{0}'", selected_cell))(0)
            If e.Column.FieldName = "Barang" Then
                For row As Integer = 0 To ViewAdjustment.RowCount - 1
                    If ViewAdjustment.GetRowCellValue(row, "Barang") = selected_cell And Not ViewAdjustment.FocusedRowHandle = row Then
                        ViewAdjustment.DeleteRow(ViewAdjustment.FocusedRowHandle)
                        ViewAdjustment.FocusedRowHandle = row
                        Return
                    End If
                Next

                ViewAdjustment.SetFocusedRowCellValue("ID", drow.Item("ID"))
                ViewAdjustment.SetFocusedRowCellValue("Jumlah", IIf(drow.Item("Stok") <= 0, 0, 1))
                ViewAdjustment.SetFocusedRowCellValue("Keterangan", "")
                ViewAdjustment.FocusedRowHandle = ViewAdjustment.RowCount
            End If
            If IsDBNull(ViewAdjustment.GetFocusedRowCellValue("ID")) = True Then Throw New Exception
        Catch ex As Exception
            If IsDBNull(ViewAdjustment.GetFocusedRowCellValue("Barang")) = True Then
                ViewAdjustment.DeleteRow(ViewAdjustment.FocusedRowHandle)
                xSet.Tables("dt_adjustment").AcceptChanges()
            End If
        End Try
    End Sub

    Private Sub ViewAdjustment_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewAdjustment.FocusedRowChanged
        Try
            If e.FocusedRowHandle >= 0 Then
                selected_cell = ViewAdjustment.GetRowCellValue(e.FocusedRowHandle, "Barang")
            End If
        Catch ex As Exception
            ViewAdjustment.DeleteRow(ViewAdjustment.FocusedRowHandle)
        End Try
    End Sub

    Private Sub GridAdjustment_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridAdjustment.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewAdjustment.DeleteRow(ViewAdjustment.FocusedRowHandle)
            xSet.Tables("dt_adjustment").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()
        Try
            If selected_id = 0 Then
                SQLquery = String.Format("EXEC adjustment_mt_insert '{0}', '{1}', '{2}', '{3}'",
                                            Format(TglAdjustment.EditValue, "yyyy-MM-dd HH:mm"), Catatan.Text, staff_id)
                ExDb.ExecQuery(SQLquery, xSet, "GetTheID")
                selected_id = xSet.Tables("GetTheID").Rows(0).Item(0)

                SQLquery = "SET QUOTED_IDENTIFIER OFF; EXEC adjustment_dt_Insert """
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC adjustment_update '{0}', '{1}', '{2}', '{3}', """,
                                         selected_id, Format(TglAdjustment.EditValue, "yyyy-MM-dd HH:mm"), Catatan.Text, staff_id)
            End If
            For Each drow As DataRow In xSet.Tables("dt_adjustment").Rows
                SQLquery += String.Format("('{0}', '{1}', '{2}', '{3}'), ",
                                          selected_id, drow.Item("ID"), drow.Item("Jumlah"), drow.Item("Keterangan"))
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserAdjustment.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("dt_adjustment").Clear()
        xSet.Tables("dt_adjustment").AcceptChanges()

        TglAdjustment.EditValue = Now
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------Daftar Stock 
        If Not xSet.Tables("DaftarStock") Is Nothing Then xSet.Tables("DaftarStock").Clear()

        'x, a, b, y, z | x=idcbg, a=id_jnstrans, b=id_trans, y=bkn(0)/retur(1), z=bkn(0)/titipan(1)/all(2)
        SQLquery = String.Format("EXEC Get_Stok_Lokal {0}, {1}, {2}, {3}, {4}, 0", id_jenistrans, selected_id, 0, 2)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarStock")
        DaftarBarang.DataSource = xSet.Tables("DaftarStock").DefaultView

        DaftarBarang.NullText = ""
        DaftarBarang.ValueMember = "ID"
        DaftarBarang.DisplayMember = "Barang"
        DaftarBarang.ShowClearButton = False
        DaftarBarang.PopulateViewColumns()

        ViewDaftarBarang.Columns("ID").Visible = False
        ViewDaftarBarang.Columns("HPP").Visible = False
        ViewDaftarBarang.Columns("Harga").Visible = False
        ViewDaftarBarang.Columns("Stok").DisplayFormat.FormatType = FormatType.Numeric
        ViewDaftarBarang.Columns("Stok").DisplayFormat.FormatString = "n0"
        'ViewDaftarBarang.Columns("HPP").DisplayFormat.FormatType = FormatType.Numeric
        'ViewDaftarBarang.Columns("HPP").DisplayFormat.FormatString = "n0"
        'ViewDaftarBarang.Columns("Harga").DisplayFormat.FormatType = FormatType.Numeric
        'ViewDaftarBarang.Columns("Harga").DisplayFormat.FormatString = "n0"

        For Each coll As DataColumn In xSet.Tables("DaftarStock").Columns
            ViewDaftarBarang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------dt_adjustment
        SQLquery = String.Format("SELECT a.idBrg ID, a.idBrg Barang, qtt Jumlah, ket Keterangan FROM dt_adjustment a INNER JOIN m_barang b ON a.idBrg=b.idBrg WHERE a.id_adjustment={0}", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "dt_adjustment")
        GridAdjustment.DataSource = xSet.Tables("dt_adjustment").DefaultView

        ViewAdjustment.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom

        For Each coll As DataColumn In xSet.Tables("dt_adjustment").Columns
            ViewAdjustment.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewAdjustment.Columns("ID").Visible = False
        ViewAdjustment.Columns("ID").OptionsColumn.AllowEdit = False

        ViewAdjustment.Columns("Barang").ColumnEdit = DaftarBarang

        ViewAdjustment.Columns("Jumlah").DisplayFormat.FormatType = FormatType.Numeric
        ViewAdjustment.Columns("Jumlah").DisplayFormat.FormatString = "n0"

        ViewAdjustment.Columns("Barang").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewAdjustment.Columns("Barang").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewAdjustment.Columns("Jumlah").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewAdjustment.Columns("Jumlah").SummaryItem.DisplayFormat = "Total:{0:n0}"

        ViewAdjustment.Columns("Barang").Width = GridAdjustment.Width * 0.3
        ViewAdjustment.Columns("Jumlah").Width = GridAdjustment.Width * 0.2
        ViewAdjustment.Columns("Keterangan").Width = GridAdjustment.Width * 0.5
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("dt_adjustment").Rows.Count < 1 Then
            Return False
        End If

        If TglAdjustment.EditValue = Nothing Then
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        If Catatan.Text.Contains("'") Then
            msgboxWarning("Huruf (') tidak dapat diterima")
            Return False
        End If

        'For Each drow As DataRow In xSet.Tables("dt_adjustment").Rows
        '    If drow.Item("Jumlah") < 1 Then Return False
        'Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class