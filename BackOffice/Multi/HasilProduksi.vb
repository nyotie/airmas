﻿Imports DevExpress.Utils

Public Class HasilProduksi
    Dim selected_cell As Integer

    Private Sub HasilProduksi_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("GetTheID")
        xSet.Tables.Remove("mt_hslproduksi")
        xSet.Tables.Remove("dt_hslproduksi")
        xSet.Tables.Remove("DaftarBarang")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub HasilProduksi_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        JamProduksi.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub HasilProduksi_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = 0 Then
            JamProduksi.EditValue = Now
        Else
            SQLquery = "SELECT tgl_hslproduksi, cat_hslproduksi FROM mt_hslproduksi WHERE id_hslproduksi=" & selected_id
            ExDb.ExecQuery(SQLquery, xSet, "mt_hslproduksi")

            JamProduksi.EditValue = xSet.Tables("mt_hslproduksi").Rows(0).Item("tgl_hslproduksi")
            Catatan.EditValue = xSet.Tables("mt_hslproduksi").Rows(0).Item("cat_hslproduksi")
        End If
    End Sub

    Private Sub DaftarBarang_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarBarang.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell = Repository.EditValue
    End Sub

    Private Sub ViewProdPusat_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewProdPusat.CellValueChanged
        Try
            Dim drow As DataRow = xSet.Tables("DaftarBarang").Select(String.Format("ID='{0}'", selected_cell))(0)

            If e.Column.FieldName = "Barang" Then
                For row As Integer = 0 To ViewProdPusat.RowCount - 1
                    If ViewProdPusat.GetRowCellValue(row, "Barang") = selected_cell And Not ViewProdPusat.FocusedRowHandle = row Then
                        ViewProdPusat.DeleteRow(ViewProdPusat.FocusedRowHandle)
                        ViewProdPusat.FocusedRowHandle = row
                        Return
                    End If
                Next

                ViewProdPusat.SetFocusedRowCellValue("ID", drow.Item("ID"))
                ViewProdPusat.SetFocusedRowCellValue("no_batch", "")
                ViewProdPusat.SetFocusedRowCellValue("Jumlah", 1)
                ViewProdPusat.FocusedRowHandle = ViewProdPusat.RowCount
            End If
            If IsDBNull(ViewProdPusat.GetFocusedRowCellValue("ID")) = True Then Throw New Exception

            If e.Column.FieldName = "Jumlah" And ViewProdPusat.FocusedColumn.FieldName = "Jumlah" Then
                If ViewProdPusat.GetRowCellValue(ViewProdPusat.FocusedRowHandle, "Jumlah") < 0 Then
                    ViewProdPusat.SetRowCellValue(ViewProdPusat.FocusedRowHandle, "Jumlah", 0)
                End If
            End If
        Catch ex As Exception
            If IsDBNull(ViewProdPusat.GetFocusedRowCellValue("Barang")) = True Then
                ViewProdPusat.DeleteRow(ViewProdPusat.FocusedRowHandle)
                xSet.Tables("dt_hslproduksi").AcceptChanges()
            End If
        End Try
    End Sub

    Private Sub ViewProdPusat_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewProdPusat.FocusedRowChanged
        Try
            If e.FocusedRowHandle >= 0 Then
                selected_cell = ViewProdPusat.GetRowCellValue(e.FocusedRowHandle, "Barang")
            End If
        Catch ex As Exception
            ViewProdPusat.DeleteRow(ViewProdPusat.FocusedRowHandle)
        End Try
    End Sub

    Private Sub Gridhslproduksi_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridProdPusat.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewProdPusat.DeleteRow(ViewProdPusat.FocusedRowHandle)
            xSet.Tables("dt_hslproduksi").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()
        Try
            If selected_id = 0 Then
                SQLquery = String.Format("EXEC hslproduksi_mt_insert '{0}', '{1}', '{2}', '{3}'",
                                            Format(JamProduksi.EditValue, "yyyy-MM-dd HH:mm"), Catatan.Text, staff_id)
                ExDb.ExecQuery(SQLquery, xSet, "GetTheID")
                selected_id = xSet.Tables("GetTheID").Rows(0).Item(0)

                SQLquery = "SET QUOTED_IDENTIFIER OFF; EXEC hslproduksi_dt_Insert """
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC hslproduksi_update '{0}', '{1}', '{2}', '{3}', """,
                                         selected_id, Format(JamProduksi.EditValue, "yyyy-MM-dd HH:mm"), Catatan.Text, staff_id)
            End If
            For Each drow As DataRow In xSet.Tables("dt_hslproduksi").Rows
                SQLquery += String.Format("('{0}', '{1}', '{2}', '{3}'), ",
                                          selected_id, drow.Item("ID"), drow.Item("no_batch"), drow.Item("Jumlah"))
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserHasilProduksi.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()


            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("dt_hslproduksi").Clear()
        xSet.Tables("dt_hslproduksi").AcceptChanges()

        JamProduksi.EditValue = Now
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------Daftar Barang 
        If Not xSet.Tables("DaftarBarang") Is Nothing Then xSet.Tables("DaftarBarang").Clear()

        SQLquery = "SELECT idBrg ID, b.namaKategoriBrg Kategori, namaBrg Barang, price Harga FROM m_barang a INNER JOIN m_kategoribrg b ON a.idKategoriBrg=b.idKategoriBrg WHERE a.inactive=1 AND Titipan=0"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarBarang")
        DaftarBarang.DataSource = xSet.Tables("DaftarBarang").DefaultView

        DaftarBarang.NullText = ""
        DaftarBarang.ValueMember = "ID"
        DaftarBarang.DisplayMember = "Barang"
        DaftarBarang.ShowClearButton = False
        DaftarBarang.PopulateViewColumns()

        ViewDaftarBarang.Columns("ID").Visible = False
        ViewDaftarBarang.Columns("Harga").Visible = False
        'ViewDaftarBarang.Columns("Harga").DisplayFormat.FormatType = FormatType.Numeric
        'ViewDaftarBarang.Columns("Harga").DisplayFormat.FormatString = "n0"

        For Each coll As DataColumn In xSet.Tables("DaftarBarang").Columns
            ViewDaftarBarang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------dt_hslproduksi
        SQLquery = String.Format("SELECT a.idBrg ID, a.idBrg Barang, no_batch, qtt Jumlah FROM dt_hslproduksi a INNER JOIN m_barang b ON a.idBrg=b.idBrg WHERE a.id_hslproduksi={0}", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "dt_hslproduksi")
        GridProdPusat.DataSource = xSet.Tables("dt_hslproduksi").DefaultView

        ViewProdPusat.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom

        For Each coll As DataColumn In xSet.Tables("dt_hslproduksi").Columns
            ViewProdPusat.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewProdPusat.Columns("ID").Visible = False
        ViewProdPusat.Columns("ID").OptionsColumn.AllowEdit = False

        ViewProdPusat.Columns("no_batch").Caption = "No. Batch"

        ViewProdPusat.Columns("Barang").ColumnEdit = DaftarBarang

        ViewProdPusat.Columns("Jumlah").DisplayFormat.FormatType = FormatType.Numeric
        ViewProdPusat.Columns("Jumlah").DisplayFormat.FormatString = "n0"

        ViewProdPusat.Columns("Barang").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewProdPusat.Columns("Barang").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewProdPusat.Columns("Jumlah").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewProdPusat.Columns("Jumlah").SummaryItem.DisplayFormat = "Total:{0:n0}"

        ViewProdPusat.Columns("Barang").Width = GridProdPusat.Width * 0.5
        ViewProdPusat.Columns("no_batch").Width = GridProdPusat.Width * 0.3
        ViewProdPusat.Columns("Jumlah").Width = GridProdPusat.Width * 0.2
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("dt_hslproduksi").Rows.Count < 1 Then
            Return False
        End If

        If JamProduksi.EditValue = Nothing Then
            msgboxWarning("Jam produksi tidak boleh kosong")
            Return False
        End If

        If Catatan.Text.Contains("'") Then
            msgboxWarning("Huruf (') tidak dapat diterima")
            Return False
        End If

        For Each drow As DataRow In xSet.Tables("dt_hslproduksi").Rows
            If drow.Item("Jumlah") < 1 Then Return False
            If Trim(drow.Item("no_batch")) = "" Then Return False
        Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class