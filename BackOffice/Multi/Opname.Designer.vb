﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Opname
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GridOpnameBahan = New DevExpress.XtraGrid.GridControl()
        Me.ViewOpnameBahan = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView()
        Me.BahanBanded = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.StokBanded = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.TotalBanded = New DevExpress.XtraGrid.Views.BandedGrid.GridBand()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GridOpnameBahan, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewOpnameBahan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridOpnameBahan
        '
        Me.GridOpnameBahan.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridOpnameBahan.Location = New System.Drawing.Point(12, 12)
        Me.GridOpnameBahan.MainView = Me.ViewOpnameBahan
        Me.GridOpnameBahan.Name = "GridOpnameBahan"
        Me.GridOpnameBahan.Size = New System.Drawing.Size(720, 303)
        Me.GridOpnameBahan.TabIndex = 39
        Me.GridOpnameBahan.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewOpnameBahan})
        '
        'ViewOpnameBahan
        '
        Me.ViewOpnameBahan.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.ViewOpnameBahan.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewOpnameBahan.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.ViewOpnameBahan.Appearance.Row.Options.UseFont = True
        Me.ViewOpnameBahan.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.BahanBanded, Me.StokBanded, Me.TotalBanded})
        Me.ViewOpnameBahan.GridControl = Me.GridOpnameBahan
        Me.ViewOpnameBahan.Name = "ViewOpnameBahan"
        Me.ViewOpnameBahan.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewOpnameBahan.OptionsCustomization.AllowBandMoving = False
        Me.ViewOpnameBahan.OptionsCustomization.AllowBandResizing = False
        Me.ViewOpnameBahan.OptionsCustomization.AllowColumnMoving = False
        Me.ViewOpnameBahan.OptionsCustomization.AllowColumnResizing = False
        Me.ViewOpnameBahan.OptionsCustomization.AllowFilter = False
        Me.ViewOpnameBahan.OptionsCustomization.AllowGroup = False
        Me.ViewOpnameBahan.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewOpnameBahan.OptionsCustomization.AllowSort = False
        Me.ViewOpnameBahan.OptionsCustomization.ShowBandsInCustomizationForm = False
        Me.ViewOpnameBahan.OptionsView.ColumnAutoWidth = True
        Me.ViewOpnameBahan.OptionsView.ShowGroupPanel = False
        '
        'BahanBanded
        '
        Me.BahanBanded.AppearanceHeader.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BahanBanded.AppearanceHeader.Options.UseFont = True
        Me.BahanBanded.Caption = "Bahan"
        Me.BahanBanded.MinWidth = 20
        Me.BahanBanded.Name = "BahanBanded"
        Me.BahanBanded.Width = 75
        '
        'StokBanded
        '
        Me.StokBanded.AppearanceHeader.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.StokBanded.AppearanceHeader.Options.UseFont = True
        Me.StokBanded.AppearanceHeader.Options.UseTextOptions = True
        Me.StokBanded.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.StokBanded.Caption = "Stok"
        Me.StokBanded.Name = "StokBanded"
        Me.StokBanded.Width = 107
        '
        'TotalBanded
        '
        Me.TotalBanded.AppearanceHeader.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TotalBanded.AppearanceHeader.Options.UseFont = True
        Me.TotalBanded.AppearanceHeader.Options.UseTextOptions = True
        Me.TotalBanded.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.TotalBanded.Caption = "Hasil Opname"
        Me.TotalBanded.Name = "TotalBanded"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Location = New System.Drawing.Point(105, 321)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(87, 39)
        Me.ButBatal.TabIndex = 42
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Location = New System.Drawing.Point(12, 321)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(87, 39)
        Me.ButSimpan.TabIndex = 40
        Me.ButSimpan.Text = "&Simpan"
        '
        'Opname
        '
        Me.Appearance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(744, 372)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.GridOpnameBahan)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Opname"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Opname"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GridOpnameBahan, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewOpnameBahan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridOpnameBahan As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewOpnameBahan As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents BahanBanded As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents StokBanded As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents TotalBanded As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
End Class
