﻿Imports DevExpress.Utils

Public Class Opname

    Private Sub InvenOpname_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("StokOpname")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub InvenOpname_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        GridOpnameBahan.Focus()
    End Sub

    Private Sub InvenOpname_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        SQLquery = String.Format("EXEC Get_Data_Stok_Opname {0}, 0, 2, {1}", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "StokOpname")
        GridOpnameBahan.DataSource = xSet.Tables("StokOpname").DefaultView

        ViewOpnameBahan.Columns("ID").Visible = False
        ViewOpnameBahan.Columns("ID").OptionsColumn.AllowEdit = False
        ViewOpnameBahan.Columns("Kategori").OptionsColumn.AllowEdit = False
        ViewOpnameBahan.Columns("Barang").OptionsColumn.AllowEdit = False
        ViewOpnameBahan.Columns("stok_before").OptionsColumn.AllowEdit = False
        ViewOpnameBahan.Columns("adj").OptionsColumn.AllowEdit = False

        ViewOpnameBahan.Columns("Kategori").Caption = "Kategori"
        ViewOpnameBahan.Columns("Barang").Caption = "Nama Barang"
        ViewOpnameBahan.Columns("stok_before").Caption = "Stok"
        ViewOpnameBahan.Columns("adj").Caption = "Penyesuaian"
        ViewOpnameBahan.Columns("stok_after").Caption = "Fisik"

        ViewOpnameBahan.Columns("ID").OwnerBand = BahanBanded
        ViewOpnameBahan.Columns("Kategori").OwnerBand = BahanBanded
        ViewOpnameBahan.Columns("Barang").OwnerBand = BahanBanded
        ViewOpnameBahan.Columns("stok_before").OwnerBand = StokBanded
        ViewOpnameBahan.Columns("adj").OwnerBand = StokBanded
        ViewOpnameBahan.Columns("stok_after").OwnerBand = TotalBanded

        ViewOpnameBahan.SetColumnPosition(ViewOpnameBahan.Columns("Kategori"), 0, 0)
        ViewOpnameBahan.SetColumnPosition(ViewOpnameBahan.Columns("Barang"), 0, 1)
        ViewOpnameBahan.SetColumnPosition(ViewOpnameBahan.Columns("stok_before"), 0, 0)
        ViewOpnameBahan.SetColumnPosition(ViewOpnameBahan.Columns("adj"), 1, 0)
        ViewOpnameBahan.SetColumnPosition(ViewOpnameBahan.Columns("stok_after"), 0, 0)

        ViewOpnameBahan.Columns("ID").AutoFillDown = True
        ViewOpnameBahan.Columns("Kategori").AutoFillDown = True
        ViewOpnameBahan.Columns("Barang").AutoFillDown = True
        ViewOpnameBahan.Columns("stok_after").AutoFillDown = True

        ViewOpnameBahan.Columns("stok_after").AppearanceCell.BackColor = Color.LightGray

        ViewOpnameBahan.Columns("stok_before").AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        ViewOpnameBahan.Columns("adj").AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        ViewOpnameBahan.Columns("stok_after").AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        ViewOpnameBahan.Columns("stok_before").AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
        ViewOpnameBahan.Columns("adj").AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
        ViewOpnameBahan.Columns("stok_after").AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center

        ViewOpnameBahan.Columns("Kategori").GroupIndex = 0
        ViewOpnameBahan.ExpandAllGroups()
    End Sub

    Private Sub ViewOpnameBahan_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewOpnameBahan.CellValueChanged
        If e.Column.FieldName = "stok_after" Then
            ViewOpnameBahan.SetFocusedRowCellValue("adj", ViewOpnameBahan.GetFocusedRowCellValue("stok_after") - ViewOpnameBahan.GetFocusedRowCellValue("stok_before"))
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()
        Try
            If selected_id = 0 Then
                SQLquery = String.Format("EXEC opname_mt_insert '{0}', '{1}'", staff_id)
                ExDb.ExecQuery(SQLquery, xSet, "GetTheID")

                selected_id = xSet.Tables("GetTheID").Rows(0).Item(0)
                SQLquery = "SET QUOTED_IDENTIFIER OFF; EXEC opname_dt_Insert """
                For Each drow As DataRow In xSet.Tables("StokOpname").Rows
                    SQLquery += String.Format("('{0}', '{1}', '{2}', '{3}'), ", selected_id,
                                              drow.Item("ID"), drow.Item("stok_before"), drow.Item("adj"))
                Next
                SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """; "
                ExDb.ExecData(SQLquery)
            End If

            '----------------UPLOAD DATA STOK
          

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserOpname.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            Close()
        End Try
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class