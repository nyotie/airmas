﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Adjustment
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.Catatan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TglAdjustment = New DevExpress.XtraEditors.DateEdit()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButClear = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.GridAdjustment = New DevExpress.XtraGrid.GridControl()
        Me.ViewAdjustment = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DaftarBarang = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.ViewDaftarBarang = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglAdjustment.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglAdjustment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridAdjustment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewAdjustment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarBarang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarBarang, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(137, 13)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl3.TabIndex = 19
        Me.LabelControl3.Text = "Catatan :"
        '
        'Catatan
        '
        Me.Catatan.Location = New System.Drawing.Point(137, 31)
        Me.Catatan.Name = "Catatan"
        Me.Catatan.Properties.MaxLength = 200
        Me.Catatan.Size = New System.Drawing.Size(423, 20)
        Me.Catatan.TabIndex = 2
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl1.TabIndex = 20
        Me.LabelControl1.Text = "Tanggal :"
        '
        'TglAdjustment
        '
        Me.TglAdjustment.EditValue = Nothing
        Me.TglAdjustment.Location = New System.Drawing.Point(12, 31)
        Me.TglAdjustment.Name = "TglAdjustment"
        Me.TglAdjustment.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TglAdjustment.Properties.Mask.EditMask = "dd MMM yyyy, HH:mm"
        Me.TglAdjustment.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TglAdjustment.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TglAdjustment.Size = New System.Drawing.Size(119, 20)
        Me.TglAdjustment.TabIndex = 1
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Location = New System.Drawing.Point(198, 334)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(87, 29)
        Me.ButBatal.TabIndex = 6
        Me.ButBatal.Text = "&Batal"
        '
        'ButClear
        '
        Me.ButClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButClear.Appearance.Options.UseFont = True
        Me.ButClear.Location = New System.Drawing.Point(105, 334)
        Me.ButClear.Name = "ButClear"
        Me.ButClear.Size = New System.Drawing.Size(87, 29)
        Me.ButClear.TabIndex = 5
        Me.ButClear.Text = "&Clear"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Location = New System.Drawing.Point(12, 334)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(87, 29)
        Me.ButSimpan.TabIndex = 4
        Me.ButSimpan.Text = "&Simpan"
        '
        'GridAdjustment
        '
        Me.GridAdjustment.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridAdjustment.Location = New System.Drawing.Point(12, 57)
        Me.GridAdjustment.MainView = Me.ViewAdjustment
        Me.GridAdjustment.Name = "GridAdjustment"
        Me.GridAdjustment.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.DaftarBarang})
        Me.GridAdjustment.Size = New System.Drawing.Size(760, 258)
        Me.GridAdjustment.TabIndex = 3
        Me.GridAdjustment.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewAdjustment})
        '
        'ViewAdjustment
        '
        Me.ViewAdjustment.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ViewAdjustment.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewAdjustment.ColumnPanelRowHeight = 40
        Me.ViewAdjustment.GridControl = Me.GridAdjustment
        Me.ViewAdjustment.Name = "ViewAdjustment"
        Me.ViewAdjustment.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewAdjustment.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewAdjustment.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewAdjustment.OptionsCustomization.AllowColumnMoving = False
        Me.ViewAdjustment.OptionsCustomization.AllowColumnResizing = False
        Me.ViewAdjustment.OptionsCustomization.AllowFilter = False
        Me.ViewAdjustment.OptionsCustomization.AllowGroup = False
        Me.ViewAdjustment.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewAdjustment.OptionsCustomization.AllowSort = False
        Me.ViewAdjustment.OptionsView.ShowFooter = True
        Me.ViewAdjustment.OptionsView.ShowGroupPanel = False
        '
        'DaftarBarang
        '
        Me.DaftarBarang.AutoHeight = False
        Me.DaftarBarang.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarBarang.Name = "DaftarBarang"
        Me.DaftarBarang.PopupFormSize = New System.Drawing.Size(400, 300)
        Me.DaftarBarang.View = Me.ViewDaftarBarang
        '
        'ViewDaftarBarang
        '
        Me.ViewDaftarBarang.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarBarang.Name = "ViewDaftarBarang"
        Me.ViewDaftarBarang.OptionsCustomization.AllowColumnMoving = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowColumnResizing = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowFilter = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowGroup = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowSort = False
        Me.ViewDaftarBarang.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarBarang.OptionsView.ShowGroupPanel = False
        '
        'LineShape1
        '
        Me.LineShape1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 12
        Me.LineShape1.X2 = 770
        Me.LineShape1.Y1 = 324
        Me.LineShape1.Y2 = 324
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(784, 375)
        Me.ShapeContainer1.TabIndex = 25
        Me.ShapeContainer1.TabStop = False
        '
        'Adjustment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 375)
        Me.Controls.Add(Me.GridAdjustment)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButClear)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.Catatan)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.TglAdjustment)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.Name = "Adjustment"
        Me.Text = "Adjustment"
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglAdjustment.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglAdjustment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridAdjustment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewAdjustment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarBarang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarBarang, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Catatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TglAdjustment As DevExpress.XtraEditors.DateEdit
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridAdjustment As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewAdjustment As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DaftarBarang As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents ViewDaftarBarang As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
End Class
