﻿Imports DevExpress.Utils

Public Class BrowserOpname
    Public isChange As Boolean

    Private Sub BrowserOpname_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarOpname")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisBrowser_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        reset_buttons()

        butBaru.Focus()
    End Sub

    Private Sub BrowserOpname_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        isChange = True
        DateEdit1.EditValue = DateAdd(DateInterval.Day, -1, Today.Date)
        DateEdit2.EditValue = Today.Date

        refreshingGrid()
        AddHandler GridView1.DoubleClick, AddressOf butKoreksi_Click
    End Sub

    '--Refresh
    Private Sub butRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butRefresh.Click
        isChange = True
        refreshingGrid()
    End Sub

    '--New
    Private Sub butBaru_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBaru.Click
        selected_id = 0
        Get_idtrans("Opname")

        openTheButton()
        refreshingGrid()
    End Sub

    '--Edit
    Private Sub butKoreksi_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butKoreksi.Click
        If butKoreksi.Enabled = False Or butKoreksi.Visible = False Then Return
        If GridView1.FocusedRowHandle < 0 Then Return

        If GridView1.GetFocusedRowCellValue("Status") = "Void" Then
            msgboxInformation("Transaksi sudah dibatalkan, tidak dapat dikoreksi")
            Return
        ElseIf GridView1.GetFocusedRowCellValue("Status") = "Close" Then
            msgboxInformation("Transaksi sudah di close, tidak dapat dikoreksi")
            Return
        End If

        selected_id = GridView1.GetFocusedRowCellValue("ID")
        Get_idtrans("Opname")

        openTheButton()
        refreshingGrid()
    End Sub

    '--Void
    Private Sub butVoid_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butVoid.Click
        If Not GridView1.RowCount > 0 Then Exit Sub
        If GridView1.GetFocusedRowCellValue("Status") = "Close" Then
            msgboxInformation("Transaksi sudah tidak dapat dibatalkan")
            Return
        ElseIf GridView1.GetFocusedRowCellValue("Status") = "Void" Then
            msgboxInformation("Transaksi sudah dalam status Void")
            Return
        End If

        selected_id = GridView1.GetFocusedRowCellValue("ID")
        Get_idtrans("Opname")

        ModVoid.nama_trans = "Opname"
        selected_id = GridView1.GetFocusedRowCellValue("ID")
        selected_no = GridView1.GetFocusedRowCellValue("Nomor")

        ShowModule(ModVoid, "Void Opname")
        If ModVoid.ReallyVoid Then isChange = True

        ModVoid = Nothing
        refreshingGrid()
    End Sub

    '--Print
    Private Sub butView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butView.Click
        If GridView1.FocusedRowHandle < 0 Then Exit Sub
        selected_id = GridView1.GetFocusedRowCellValue("ID")
        Get_idtrans("Opname")

    End Sub

    '--Properties
    Private Sub butProperties_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butProperties.Click
        If GridView1.FocusedRowHandle < 0 Then Return
        selected_id = GridView1.GetFocusedRowCellValue("ID")
        Get_idtrans("Opname")

        ShowModule(ModProperties, "Properties")
        ModProperties = Nothing
    End Sub

    '--Close Form
    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub reset_buttons()
        butBaru.Visible = True
        butKoreksi.Visible = False
        butVoid.Visible = False
        butView.Visible = GlobalPrintVisibility

        butBaru.Text = "Baru"
        butKoreksi.Text = "Koreksi"
        butVoid.Text = "Pembatalan"
        butView.Text = "Cetak"

        '-----start setting user permissions
        Get_idmodule(Text)

        Dim drow As DataRow = xSet.Tables("User_ProgPermisisons").Select("id_module = " & id_module)(0)
        butView.Enabled = drow.Item("AllowPrint")
        '-----end of setting user permissions

        butCheckShow.Enabled = True
    End Sub

    Private Sub openTheButton()
        ShowModule(Opname, "Opname")
        Opname = Nothing
    End Sub

    Private Sub refreshingGrid()
        If isChange = False Then Exit Sub Else isChange = False

        Dim frow As Integer = GridView1.FocusedRowHandle
        If Not xSet.Tables("DaftarOpname") Is Nothing Then xSet.Tables.Remove("DaftarOpname")
        reset_buttons()
        SQLquery = "SELECT id_opname ID, tgl_opname Tanggal, no_opname Nomor, " & _
                   "CASE a.status WHEN 'O' THEN 'Open' WHEN 'V' THEN 'Void' ELSE 'Close' END AS Status " & _
                   "FROM mt_opname a WHERE a.id_cabang="
        If butCheckShow.Checked = False Then SQLquery += " AND a.Status IN ('O','C') "
        SQLquery += String.Format(" AND CAST(a.tgl_opname AS DATE) BETWEEN '{0}' AND '{1}' ORDER BY a.tgl_opname", Format(DateEdit1.EditValue, "yyyy/MM/dd"), Format(DateEdit2.EditValue, "yyyy/MM/dd"))
        ExDb.ExecQuery(SQLquery, xSet, "DaftarOpname")

        GridControl1.DataSource = xSet.Tables("DaftarOpname").DefaultView

        For Each coll As DataColumn In xSet.Tables("DaftarOpname").Columns
            With GridView1.Columns(coll.ColumnName)
                If coll.ColumnName = "ID" Then
                    .Visible = False
                ElseIf coll.ColumnName = "Tanggal" Then
                    .DisplayFormat.FormatType = FormatType.DateTime
                    .DisplayFormat.FormatString = "dd/MMM/yyyy HH:mm"
                End If
                .AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
                .AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            End With
        Next
        GridView1.BestFitColumns()
        GridView1.FocusedRowHandle = frow
    End Sub
End Class