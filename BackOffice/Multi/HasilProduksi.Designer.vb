﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HasilProduksi
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GridProdPusat = New DevExpress.XtraGrid.GridControl()
        Me.ViewProdPusat = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DaftarBarang = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.ViewDaftarBarang = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.Catatan = New DevExpress.XtraEditors.TextEdit()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButClear = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.JamProduksi = New DevExpress.XtraEditors.DateEdit()
        CType(Me.GridProdPusat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewProdPusat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarBarang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarBarang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JamProduksi.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JamProduksi.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridProdPusat
        '
        Me.GridProdPusat.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridProdPusat.Location = New System.Drawing.Point(12, 56)
        Me.GridProdPusat.MainView = Me.ViewProdPusat
        Me.GridProdPusat.Name = "GridProdPusat"
        Me.GridProdPusat.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.DaftarBarang})
        Me.GridProdPusat.Size = New System.Drawing.Size(770, 258)
        Me.GridProdPusat.TabIndex = 3
        Me.GridProdPusat.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewProdPusat})
        '
        'ViewProdPusat
        '
        Me.ViewProdPusat.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ViewProdPusat.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewProdPusat.ColumnPanelRowHeight = 40
        Me.ViewProdPusat.GridControl = Me.GridProdPusat
        Me.ViewProdPusat.Name = "ViewProdPusat"
        Me.ViewProdPusat.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewProdPusat.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewProdPusat.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewProdPusat.OptionsCustomization.AllowColumnMoving = False
        Me.ViewProdPusat.OptionsCustomization.AllowColumnResizing = False
        Me.ViewProdPusat.OptionsCustomization.AllowFilter = False
        Me.ViewProdPusat.OptionsCustomization.AllowGroup = False
        Me.ViewProdPusat.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewProdPusat.OptionsCustomization.AllowSort = False
        Me.ViewProdPusat.OptionsView.ShowFooter = True
        Me.ViewProdPusat.OptionsView.ShowGroupPanel = False
        '
        'DaftarBarang
        '
        Me.DaftarBarang.AutoHeight = False
        Me.DaftarBarang.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarBarang.Name = "DaftarBarang"
        Me.DaftarBarang.PopupFormSize = New System.Drawing.Size(400, 300)
        Me.DaftarBarang.View = Me.ViewDaftarBarang
        '
        'ViewDaftarBarang
        '
        Me.ViewDaftarBarang.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarBarang.Name = "ViewDaftarBarang"
        Me.ViewDaftarBarang.OptionsCustomization.AllowColumnMoving = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowColumnResizing = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowFilter = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowGroup = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowSort = False
        Me.ViewDaftarBarang.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarBarang.OptionsView.ShowGroupPanel = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 11)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl1.TabIndex = 9
        Me.LabelControl1.Text = "Jam Produksi"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(137, 11)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl2.TabIndex = 9
        Me.LabelControl2.Text = "Catatan :"
        '
        'Catatan
        '
        Me.Catatan.Location = New System.Drawing.Point(137, 30)
        Me.Catatan.Name = "Catatan"
        Me.Catatan.Properties.MaxLength = 200
        Me.Catatan.Size = New System.Drawing.Size(423, 20)
        Me.Catatan.TabIndex = 2
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Location = New System.Drawing.Point(198, 334)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(87, 29)
        Me.ButBatal.TabIndex = 6
        Me.ButBatal.Text = "&Batal"
        '
        'ButClear
        '
        Me.ButClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButClear.Appearance.Options.UseFont = True
        Me.ButClear.Location = New System.Drawing.Point(105, 334)
        Me.ButClear.Name = "ButClear"
        Me.ButClear.Size = New System.Drawing.Size(87, 29)
        Me.ButClear.TabIndex = 5
        Me.ButClear.Text = "&Clear"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Location = New System.Drawing.Point(12, 334)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(87, 29)
        Me.ButSimpan.TabIndex = 4
        Me.ButSimpan.Text = "&Simpan"
        '
        'LineShape1
        '
        Me.LineShape1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 12
        Me.LineShape1.X2 = 780
        Me.LineShape1.Y1 = 324
        Me.LineShape1.Y2 = 324
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(794, 375)
        Me.ShapeContainer1.TabIndex = 16
        Me.ShapeContainer1.TabStop = False
        '
        'JamProduksi
        '
        Me.JamProduksi.EditValue = "00:00"
        Me.JamProduksi.Location = New System.Drawing.Point(12, 30)
        Me.JamProduksi.Name = "JamProduksi"
        Me.JamProduksi.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.JamProduksi.Properties.Mask.EditMask = "HH:mm"
        Me.JamProduksi.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.JamProduksi.Properties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never
        Me.JamProduksi.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.JamProduksi.Size = New System.Drawing.Size(119, 20)
        Me.JamProduksi.TabIndex = 1
        '
        'HasilProduksi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(794, 375)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButClear)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.Catatan)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.GridProdPusat)
        Me.Controls.Add(Me.JamProduksi)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.Name = "HasilProduksi"
        Me.ShowIcon = False
        Me.Text = "HasilProduksi"
        CType(Me.GridProdPusat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewProdPusat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarBarang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarBarang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JamProduksi.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JamProduksi.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GridProdPusat As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewProdPusat As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DaftarBarang As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents ViewDaftarBarang As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Catatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents JamProduksi As DevExpress.XtraEditors.DateEdit
End Class
