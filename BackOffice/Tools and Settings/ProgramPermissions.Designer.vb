﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProgramPermissions
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GridProgPermissions = New DevExpress.XtraGrid.GridControl()
        Me.ViewProgPermissions = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepoCheck = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.GroupList = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.butBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.butSimpan = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GridProgPermissions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewProgPermissions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepoCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupList.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridProgPermissions
        '
        Me.GridProgPermissions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridProgPermissions.Location = New System.Drawing.Point(12, 40)
        Me.GridProgPermissions.MainView = Me.ViewProgPermissions
        Me.GridProgPermissions.Name = "GridProgPermissions"
        Me.GridProgPermissions.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepoCheck})
        Me.GridProgPermissions.Size = New System.Drawing.Size(570, 274)
        Me.GridProgPermissions.TabIndex = 2
        Me.GridProgPermissions.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewProgPermissions})
        '
        'ViewProgPermissions
        '
        Me.ViewProgPermissions.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 11.25!)
        Me.ViewProgPermissions.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewProgPermissions.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.ViewProgPermissions.Appearance.Row.Options.UseFont = True
        Me.ViewProgPermissions.GridControl = Me.GridProgPermissions
        Me.ViewProgPermissions.Name = "ViewProgPermissions"
        Me.ViewProgPermissions.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewProgPermissions.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewProgPermissions.OptionsCustomization.AllowColumnMoving = False
        Me.ViewProgPermissions.OptionsCustomization.AllowColumnResizing = False
        Me.ViewProgPermissions.OptionsCustomization.AllowFilter = False
        Me.ViewProgPermissions.OptionsCustomization.AllowGroup = False
        Me.ViewProgPermissions.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewProgPermissions.OptionsCustomization.AllowSort = False
        Me.ViewProgPermissions.OptionsView.EnableAppearanceEvenRow = True
        Me.ViewProgPermissions.OptionsView.ShowGroupPanel = False
        '
        'RepoCheck
        '
        Me.RepoCheck.AutoHeight = False
        Me.RepoCheck.Name = "RepoCheck"
        '
        'GroupList
        '
        Me.GroupList.Location = New System.Drawing.Point(58, 12)
        Me.GroupList.Name = "GroupList"
        Me.GroupList.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.GroupList.Properties.Appearance.Options.UseFont = True
        Me.GroupList.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GroupList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.GroupList.Size = New System.Drawing.Size(279, 22)
        Me.GroupList.TabIndex = 3
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(40, 16)
        Me.LabelControl1.TabIndex = 4
        Me.LabelControl1.Text = "User :"
        '
        'butBatal
        '
        Me.butBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBatal.Appearance.Options.UseFont = True
        Me.butBatal.Location = New System.Drawing.Point(497, 320)
        Me.butBatal.Name = "butBatal"
        Me.butBatal.Size = New System.Drawing.Size(85, 40)
        Me.butBatal.TabIndex = 14
        Me.butBatal.Text = "Batal"
        '
        'butSimpan
        '
        Me.butSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSimpan.Appearance.Options.UseFont = True
        Me.butSimpan.Location = New System.Drawing.Point(406, 320)
        Me.butSimpan.Name = "butSimpan"
        Me.butSimpan.Size = New System.Drawing.Size(85, 40)
        Me.butSimpan.TabIndex = 13
        Me.butSimpan.Text = "Simpan"
        '
        'ProgramPermissions
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(594, 372)
        Me.Controls.Add(Me.butBatal)
        Me.Controls.Add(Me.butSimpan)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.GroupList)
        Me.Controls.Add(Me.GridProgPermissions)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ProgramPermissions"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Program Permissions"
        CType(Me.GridProgPermissions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewProgPermissions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepoCheck, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupList.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GridProgPermissions As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewProgPermissions As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupList As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents butBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents RepoCheck As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
End Class
