﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReportPermissions
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupList = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.butBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.butSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.GridRepPermissions = New DevExpress.XtraGrid.GridControl()
        Me.ViewRepPermissions = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepoCheck = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.GroupList.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridRepPermissions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewRepPermissions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepoCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(109, 16)
        Me.LabelControl1.TabIndex = 6
        Me.LabelControl1.Text = "Security Group :"
        '
        'GroupList
        '
        Me.GroupList.Location = New System.Drawing.Point(127, 9)
        Me.GroupList.Name = "GroupList"
        Me.GroupList.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.GroupList.Properties.Appearance.Options.UseFont = True
        Me.GroupList.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GroupList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.GroupList.Size = New System.Drawing.Size(279, 22)
        Me.GroupList.TabIndex = 5
        '
        'butBatal
        '
        Me.butBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBatal.Appearance.Options.UseFont = True
        Me.butBatal.Location = New System.Drawing.Point(497, 320)
        Me.butBatal.Name = "butBatal"
        Me.butBatal.Size = New System.Drawing.Size(85, 40)
        Me.butBatal.TabIndex = 16
        Me.butBatal.Text = "&Batal"
        '
        'butSimpan
        '
        Me.butSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSimpan.Appearance.Options.UseFont = True
        Me.butSimpan.Location = New System.Drawing.Point(406, 320)
        Me.butSimpan.Name = "butSimpan"
        Me.butSimpan.Size = New System.Drawing.Size(85, 40)
        Me.butSimpan.TabIndex = 15
        Me.butSimpan.Text = "&Simpan"
        '
        'GridRepPermissions
        '
        Me.GridRepPermissions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridRepPermissions.Location = New System.Drawing.Point(12, 37)
        Me.GridRepPermissions.MainView = Me.ViewRepPermissions
        Me.GridRepPermissions.Name = "GridRepPermissions"
        Me.GridRepPermissions.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepoCheck})
        Me.GridRepPermissions.Size = New System.Drawing.Size(570, 277)
        Me.GridRepPermissions.TabIndex = 17
        Me.GridRepPermissions.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewRepPermissions})
        '
        'ViewRepPermissions
        '
        Me.ViewRepPermissions.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 11.25!)
        Me.ViewRepPermissions.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewRepPermissions.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.ViewRepPermissions.Appearance.Row.Options.UseFont = True
        Me.ViewRepPermissions.GridControl = Me.GridRepPermissions
        Me.ViewRepPermissions.Name = "ViewRepPermissions"
        Me.ViewRepPermissions.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewRepPermissions.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewRepPermissions.OptionsCustomization.AllowColumnMoving = False
        Me.ViewRepPermissions.OptionsCustomization.AllowColumnResizing = False
        Me.ViewRepPermissions.OptionsCustomization.AllowFilter = False
        Me.ViewRepPermissions.OptionsCustomization.AllowGroup = False
        Me.ViewRepPermissions.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewRepPermissions.OptionsCustomization.AllowSort = False
        Me.ViewRepPermissions.OptionsView.EnableAppearanceEvenRow = True
        Me.ViewRepPermissions.OptionsView.ShowGroupPanel = False
        '
        'RepoCheck
        '
        Me.RepoCheck.AutoHeight = False
        Me.RepoCheck.Name = "RepoCheck"
        '
        'ReportPermissions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(594, 372)
        Me.Controls.Add(Me.GridRepPermissions)
        Me.Controls.Add(Me.butBatal)
        Me.Controls.Add(Me.butSimpan)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.GroupList)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ReportPermissions"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Report Permissions"
        CType(Me.GroupList.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridRepPermissions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewRepPermissions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepoCheck, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupList As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents butBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridRepPermissions As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewRepPermissions As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepoCheck As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
End Class
