﻿Public Class CompanyProfile

    Private Sub CompanyProfile_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub CompanyProfile_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        NamaPerusahaan.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub CompanyProfile_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        If Not selmaster_id = 0 Then
            NamaPerusahaan.EditValue = xSet.Tables("DataCompany").Rows(0).Item("NAMA")
            CompanyAddress.EditValue = xSet.Tables("DataCompany").Rows(0).Item("ALAMAT")
            CompanyCity.EditValue = xSet.Tables("DataCompany").Rows(0).Item("KOTA")
            CompanyTelp.EditValue = xSet.Tables("DataCompany").Rows(0).Item("TELP")
            CompanyFax.EditValue = xSet.Tables("DataCompany").Rows(0).Item("FAX")
            CompanyPostal.EditValue = xSet.Tables("DataCompany").Rows(0).Item("KODEPOS")
            CompanyEmail.EditValue = xSet.Tables("DataCompany").Rows(0).Item("EMAIL")
            CompanyNPWP.EditValue = xSet.Tables("DataCompany").Rows(0).Item("NPWP")
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()

            If selmaster_id = "0" Then
                SQLquery = String.Format("INSERT INTO MCOMPANY(NAMA,ALAMAT,KOTA,TELP,FAX,KODEPOS,EMAIL,NPWP,INACTIVE,CREATEDATE) " & _
                                         "VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}',GETDATE())",
                            NamaPerusahaan.Text.Replace("'", "''"), CompanyAddress.Text.Replace("'", "''"), CompanyCity.Text.Replace("'", "''"), CompanyTelp.Text.Replace("'", "''"),
                            CompanyFax.Text.Replace("'", "''"), CompanyPostal.Text.Replace("'", "''"), CompanyEmail.Text.Replace("'", "''"), CompanyNPWP.Text.Replace("'", "''"), 0)
            Else
                SQLquery = String.Format("UPDATE MCOMPANY SET NAMA='{0}', ALAMAT='{1}', KOTA='{2}', TELP='{3}', FAX='{4}', KODEPOS='{5}', EMAIL='{6}', NPWP='{7}', LASTUPDATE=GETDATE() WHERE IDCOMPANY=1",
                            NamaPerusahaan.Text.Replace("'", "''"), CompanyAddress.Text.Replace("'", "''"), CompanyCity.Text.Replace("'", "''"), CompanyTelp.Text.Replace("'", "''"),
                            CompanyFax.Text.Replace("'", "''"), CompanyPostal.Text.Replace("'", "''"), CompanyEmail.Text.Replace("'", "''"), CompanyNPWP.Text.Replace("'", "''"))
            End If
            ExDb.ExecData(SQLquery)
            LoadSprite.CloseLoading()

            Get_DataCompanyAndBranch()

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            msgboxErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If NamaPerusahaan.Text.Length < 1 Then
            msgboxWarning("Nama perusahaan tidak boleh kosong")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class