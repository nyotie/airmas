﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CompanyProfile
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.NamaPerusahaan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.CompanyEmail = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.CompanyNPWP = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.CompanyPostal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.CompanyFax = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.CompanyTelp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.CompanyCity = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.CompanyAddress = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.NamaPerusahaan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CompanyEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CompanyNPWP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.CompanyPostal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CompanyFax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CompanyTelp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CompanyCity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CompanyAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(15, 15)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(116, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Nama Perusahaan"
        '
        'NamaPerusahaan
        '
        Me.NamaPerusahaan.Location = New System.Drawing.Point(137, 12)
        Me.NamaPerusahaan.Name = "NamaPerusahaan"
        Me.NamaPerusahaan.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NamaPerusahaan.Properties.Appearance.Options.UseFont = True
        Me.NamaPerusahaan.Properties.MaxLength = 50
        Me.NamaPerusahaan.Size = New System.Drawing.Size(203, 20)
        Me.NamaPerusahaan.TabIndex = 0
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl8.Location = New System.Drawing.Point(27, 177)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl8.TabIndex = 0
        Me.LabelControl8.Text = "Email"
        '
        'CompanyEmail
        '
        Me.CompanyEmail.Location = New System.Drawing.Point(137, 174)
        Me.CompanyEmail.Name = "CompanyEmail"
        Me.CompanyEmail.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompanyEmail.Properties.Appearance.Options.UseFont = True
        Me.CompanyEmail.Properties.MaxLength = 50
        Me.CompanyEmail.Size = New System.Drawing.Size(150, 20)
        Me.CompanyEmail.TabIndex = 6
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl9.Location = New System.Drawing.Point(27, 204)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(38, 14)
        Me.LabelControl9.TabIndex = 0
        Me.LabelControl9.Text = "NPWP"
        '
        'CompanyNPWP
        '
        Me.CompanyNPWP.Location = New System.Drawing.Point(137, 201)
        Me.CompanyNPWP.Name = "CompanyNPWP"
        Me.CompanyNPWP.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompanyNPWP.Properties.Appearance.Options.UseFont = True
        Me.CompanyNPWP.Properties.MaxLength = 50
        Me.CompanyNPWP.Size = New System.Drawing.Size(150, 20)
        Me.CompanyNPWP.TabIndex = 7
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.NamaPerusahaan)
        Me.PanelControl1.Controls.Add(Me.CompanyNPWP)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.CompanyPostal)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.CompanyFax)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.CompanyTelp)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.CompanyCity)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.CompanyAddress)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.CompanyEmail)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(356, 233)
        Me.PanelControl1.TabIndex = 0
        '
        'CompanyPostal
        '
        Me.CompanyPostal.Location = New System.Drawing.Point(137, 147)
        Me.CompanyPostal.Name = "CompanyPostal"
        Me.CompanyPostal.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompanyPostal.Properties.Appearance.Options.UseFont = True
        Me.CompanyPostal.Properties.MaxLength = 50
        Me.CompanyPostal.Size = New System.Drawing.Size(150, 20)
        Me.CompanyPostal.TabIndex = 5
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl6.Location = New System.Drawing.Point(27, 150)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(59, 14)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "Kode Pos"
        '
        'CompanyFax
        '
        Me.CompanyFax.Location = New System.Drawing.Point(137, 120)
        Me.CompanyFax.Name = "CompanyFax"
        Me.CompanyFax.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompanyFax.Properties.Appearance.Options.UseFont = True
        Me.CompanyFax.Properties.MaxLength = 50
        Me.CompanyFax.Size = New System.Drawing.Size(150, 20)
        Me.CompanyFax.TabIndex = 4
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(27, 123)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(22, 14)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Fax"
        '
        'CompanyTelp
        '
        Me.CompanyTelp.Location = New System.Drawing.Point(137, 93)
        Me.CompanyTelp.Name = "CompanyTelp"
        Me.CompanyTelp.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompanyTelp.Properties.Appearance.Options.UseFont = True
        Me.CompanyTelp.Properties.MaxLength = 50
        Me.CompanyTelp.Size = New System.Drawing.Size(150, 20)
        Me.CompanyTelp.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(27, 96)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(26, 14)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Telp"
        '
        'CompanyCity
        '
        Me.CompanyCity.Location = New System.Drawing.Point(137, 66)
        Me.CompanyCity.Name = "CompanyCity"
        Me.CompanyCity.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompanyCity.Properties.Appearance.Options.UseFont = True
        Me.CompanyCity.Properties.MaxLength = 50
        Me.CompanyCity.Size = New System.Drawing.Size(150, 20)
        Me.CompanyCity.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.LabelControl3.Location = New System.Drawing.Point(27, 69)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(29, 14)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Kota"
        '
        'CompanyAddress
        '
        Me.CompanyAddress.Location = New System.Drawing.Point(137, 39)
        Me.CompanyAddress.Name = "CompanyAddress"
        Me.CompanyAddress.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CompanyAddress.Properties.Appearance.Options.UseFont = True
        Me.CompanyAddress.Properties.MaxLength = 150
        Me.CompanyAddress.Size = New System.Drawing.Size(203, 20)
        Me.CompanyAddress.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(27, 42)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(43, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Alamat"
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Location = New System.Drawing.Point(299, 251)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(69, 29)
        Me.ButBatal.TabIndex = 2
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Location = New System.Drawing.Point(224, 251)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(69, 29)
        Me.ButSimpan.TabIndex = 1
        Me.ButSimpan.Text = "&Simpan"
        '
        'CompanyProfile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(380, 292)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "CompanyProfile"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Company Profile"
        CType(Me.NamaPerusahaan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CompanyEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CompanyNPWP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.CompanyPostal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CompanyFax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CompanyTelp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CompanyCity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CompanyAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NamaPerusahaan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CompanyEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CompanyNPWP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CompanyPostal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CompanyFax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CompanyTelp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CompanyCity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CompanyAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
End Class
