﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GantiPassword
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OldPassword = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.butCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.NewPassword = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.staffName = New DevExpress.XtraEditors.LabelControl()
        Me.butSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.RetypePassword = New DevExpress.XtraEditors.TextEdit()
        CType(Me.OldPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RetypePassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OldPassword
        '
        Me.OldPassword.Location = New System.Drawing.Point(93, 49)
        Me.OldPassword.Name = "OldPassword"
        Me.OldPassword.Properties.MaxLength = 10
        Me.OldPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.OldPassword.Size = New System.Drawing.Size(147, 20)
        Me.OldPassword.TabIndex = 21
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(9, 52)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl5.TabIndex = 20
        Me.LabelControl5.Text = "Password lama :"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl4.Location = New System.Drawing.Point(93, 30)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(125, 13)
        Me.LabelControl4.TabIndex = 19
        Me.LabelControl4.Text = "password baru tidak sama"
        Me.LabelControl4.Visible = False
        '
        'butCancel
        '
        Me.butCancel.Location = New System.Drawing.Point(169, 127)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(71, 23)
        Me.butCancel.TabIndex = 18
        Me.butCancel.Text = "Batal"
        '
        'NewPassword
        '
        Me.NewPassword.Location = New System.Drawing.Point(93, 75)
        Me.NewPassword.Name = "NewPassword"
        Me.NewPassword.Properties.MaxLength = 10
        Me.NewPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.NewPassword.Size = New System.Drawing.Size(147, 20)
        Me.NewPassword.TabIndex = 15
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(53, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl1.TabIndex = 11
        Me.LabelControl1.Text = "Nama :"
        '
        'staffName
        '
        Me.staffName.Location = New System.Drawing.Point(93, 12)
        Me.staffName.Name = "staffName"
        Me.staffName.Size = New System.Drawing.Size(66, 13)
        Me.staffName.TabIndex = 14
        Me.staffName.Text = "LabelControl4"
        '
        'butSimpan
        '
        Me.butSimpan.Location = New System.Drawing.Point(93, 127)
        Me.butSimpan.Name = "butSimpan"
        Me.butSimpan.Size = New System.Drawing.Size(71, 23)
        Me.butSimpan.TabIndex = 17
        Me.butSimpan.Text = "OK"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(51, 104)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl3.TabIndex = 13
        Me.LabelControl3.Text = "Ulangi :"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(9, 78)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 12
        Me.LabelControl2.Text = "Password baru :"
        '
        'RetypePassword
        '
        Me.RetypePassword.Location = New System.Drawing.Point(93, 101)
        Me.RetypePassword.Name = "RetypePassword"
        Me.RetypePassword.Properties.MaxLength = 10
        Me.RetypePassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.RetypePassword.Size = New System.Drawing.Size(147, 20)
        Me.RetypePassword.TabIndex = 16
        '
        'GantiPassword
        '
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(258, 164)
        Me.Controls.Add(Me.OldPassword)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.NewPassword)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.staffName)
        Me.Controls.Add(Me.butSimpan)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.RetypePassword)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "GantiPassword"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "GantiPassword"
        CType(Me.OldPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RetypePassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OldPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents butCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents NewPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents staffName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents butSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RetypePassword As DevExpress.XtraEditors.TextEdit
End Class
