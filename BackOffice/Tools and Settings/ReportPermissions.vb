﻿Public Class ReportPermissions
    Dim id_securitygroup As Integer

    Private Sub ReportPermissions_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("GroupList")
        xSet.Tables.Remove("RepPermissionsDefault")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ReportPermissions_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        GroupList.Focus()
    End Sub

    Private Sub ReportPermissions_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        If Not xSet.Tables("GroupList") Is Nothing Then xSet.Tables("GroupList").Clear()
        SQLquery = "SELECT id_securitygroup, nama_securitygroup, isAllowEdit FROM m_securitygroup where inactive = 1 ORDER BY id_securitygroup"
        ExDb.ExecQuery(SQLquery, xSet, "GroupList")

        GroupList.Properties.Items.Clear()
        GroupList.Properties.Items.Add("Pilih Group")

        For Each row As DataRow In xSet.Tables("GroupList").Rows
            GroupList.Properties.Items.Add(row.Item("nama_securitygroup"))
        Next
        GroupList.SelectedIndex = 0
    End Sub

    Private Sub GroupList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GroupList.SelectedIndexChanged
        If Not xSet.Tables("RepPermissionsDefault") Is Nothing Then xSet.Tables("RepPermissionsDefault").Clear()

        If GroupList.SelectedIndex > 0 Then
            id_securitygroup = xSet.Tables("GroupList").Select("nama_securitygroup = '" & GroupList.SelectedItem & "'")(0).Item("id_securitygroup")
            SQLquery = String.Format("EXEC Get_Data_Permissions_Report {0};", id_securitygroup)
            ExDb.ExecQuery(SQLquery, xSet, "RepPermissionsDefault")

            RepoCheck.ValueChecked = "True"
            RepoCheck.ValueUnchecked = "False"
            RepoCheck.ValueGrayed = "False"

            GridRepPermissions.DataSource = xSet.Tables("RepPermissionsDefault").DefaultView
            GridRepPermissions.RepositoryItems.Add(RepoCheck)

            With ViewRepPermissions
                .Columns("idReport").OptionsColumn.AllowEdit = False
                .Columns("idReport").Visible = False
                .Columns("JudulReport").Caption = "Nama Report"
                .Columns("AllowRead").Caption = "Lihat Report"
                .Columns("JudulReport").Width = GridRepPermissions.Width * 0.7
            End With

            For Each coll As DataColumn In xSet.Tables("RepPermissionsDefault").Columns
                ViewRepPermissions.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Next

            ViewRepPermissions.ExpandAllGroups()

            If id_securitygroup = 1 Then
                ViewRepPermissions.OptionsBehavior.Editable = False
            Else
                ViewRepPermissions.OptionsBehavior.Editable = True
            End If
        Else
            GridRepPermissions.DataSource = Nothing
        End If
    End Sub

    Private Sub butSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butSimpan.Click
        If ViewRepPermissions.OptionsBehavior.Editable = False Then Return
        If ViewRepPermissions.RowCount < 1 Then Return
        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return
        End If

        SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC sp_permissions_report_insert {0}, """, id_securitygroup)
        For Each drow As DataRow In xSet.Tables("RepPermissionsDefault").Rows
            SQLquery += String.Format("({0}, {1}, '{2}'), ", id_securitygroup, drow.Item("idReport"), drow.Item("AllowRead"))
        Next
        SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
        ExDb.ExecData(SQLquery)

        msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        GroupList.SelectedIndex = 0
    End Sub

    Private Sub butBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBatal.Click
        Close()
    End Sub
End Class