﻿Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.SqlServer.Management.Common

Public Class FrmSettingKoneksi
    Private srv As Server
    Private conn As ServerConnection

    Private Sub FrmSettingKoneksi_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        MainSetting()
    End Sub

    Private Sub MainSetting()
        Using dt As DataTable = SmoApplication.EnumAvailableSqlServers(False)
            LocalOpt1.Checked = True

            LocalServer.Items.Add("- Select -")
            LocalDB.Items.Add("- Select -")

            LocalServer.SelectedIndex = 0
            LocalDB.SelectedIndex = 0

            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    LocalServer.Items.Add(dr("Name").ToString())
                Next
            End If
        End Using

        LocalUID.Enabled = False
        LocalPWD.Enabled = False
        LocalDB.Enabled = True
    End Sub

    Private Sub LocalOpt1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles LocalOpt1.CheckedChanged
        If LocalOpt2.Checked = True Then
            LocalUID.Enabled = True
            LocalPWD.Enabled = True
            LocalDB.Enabled = True
        Else
            LocalUID.Enabled = False
            LocalPWD.Enabled = False
            LocalDB.Enabled = True
            LocalUID.Text = ""
            LocalPWD.Text = ""
            'CbDatabase.SelectedIndex = 0
        End If
    End Sub

    Private Sub LocalOpt2_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles LocalOpt2.CheckedChanged
        If LocalOpt2.Checked = True Then
            LocalUID.Enabled = True
            LocalPWD.Enabled = True
            LocalDB.Enabled = True
        Else
            LocalUID.Enabled = False
            LocalPWD.Enabled = False
            LocalDB.Enabled = False
            LocalUID.Text = ""
            LocalPWD.Text = ""
            ConnectDatabase("Local Database")
            'CbDatabase.SelectedIndex = 0
        End If
    End Sub

    Private Sub LocalPWD_LostFocus(ByVal sender As Object, ByVal e As EventArgs) Handles LocalPWD.LostFocus
        ConnectDatabase("Local Database")
    End Sub

    Private Sub LocalServer_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles LocalServer.SelectedIndexChanged
        If LocalServer.Text <> "- Select -" Then
            ConnectDatabase("Local Database")
        End If
    End Sub

    Private Sub CmdSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CmdSimpan.Click
       
        If LocalServer.Text = "" Then
            msgboxWarning("Server kosong")
            Return
        End If
        If LocalOpt2.Checked = True And (LocalUID.Text = "" Or LocalPWD.Text = "") Then
            msgboxWarning("Parameter login kosong")
            Return
        End If
        If LocalDB.Text = "" Then
            msgboxWarning("Database belum dipilih")
            Return
        End If

        SettingsWritter("Local Database")
        If ConnectionChecker("Local Database", conn_string_local) Then
            Close()
        Else
            msgboxWarning("Failed to initialized Local Connection")
        End If
    End Sub

    Private Sub CmdKeluar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CmdKeluar.Click
        Close()
    End Sub

    Private Sub ConnectDatabase(ByVal LineConnection As String)
        Try
            Dim sqlSErverInstance As String
            If LineConnection = "Local Database" Then
                LocalDB.Items.Clear()
                sqlSErverInstance = LocalServer.Text

                If LocalOpt1.Checked = True Then
                    conn = New ServerConnection() With {.ServerInstance = sqlSErverInstance}
                Else
                    conn = New ServerConnection(sqlSErverInstance, LocalUID.Text, LocalPWD.Text)
                End If
                srv = New Server(conn)

                For Each db As Database In srv.Databases
                    LocalDB.Items.Add(db.Name)
                Next
            End If
        Catch err As Exception
            MessageBox.Show(err.Message)
        End Try
    End Sub

    Private Sub SettingsWritter(ByVal LineConnection As String)
        FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "Host", LocalServer.Text)
        FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "User", LocalUID.Text)
        FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "Password", Encryptor.EncryptData(LocalPWD.Text))
        FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "DatabaseName", LocalDB.Text)
        FileSettingsHelper.WriteSettings(Application.StartupPath & "\setting.ini", LineConnection, "SQL", IIf(LocalOpt1.Checked = True, 1, 2))
    End Sub
End Class