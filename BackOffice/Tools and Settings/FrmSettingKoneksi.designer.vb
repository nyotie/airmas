﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSettingKoneksi
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSettingKoneksi))
        Me.CmdKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelLocal = New DevExpress.XtraEditors.GroupControl()
        Me.LocalOpt2 = New System.Windows.Forms.RadioButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LocalOpt1 = New System.Windows.Forms.RadioButton()
        Me.LocalServer = New System.Windows.Forms.ComboBox()
        Me.LocalDB = New System.Windows.Forms.ComboBox()
        Me.LocalPWD = New DevExpress.XtraEditors.TextEdit()
        Me.LocalUID = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        CType(Me.PanelLocal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelLocal.SuspendLayout()
        CType(Me.LocalPWD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LocalUID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CmdKeluar
        '
        Me.CmdKeluar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdKeluar.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmdKeluar.Appearance.Options.UseFont = True
        Me.CmdKeluar.Image = CType(resources.GetObject("CmdKeluar.Image"), System.Drawing.Image)
        Me.CmdKeluar.Location = New System.Drawing.Point(279, 259)
        Me.CmdKeluar.Name = "CmdKeluar"
        Me.CmdKeluar.Size = New System.Drawing.Size(76, 27)
        Me.CmdKeluar.TabIndex = 9
        Me.CmdKeluar.Text = "Cancel"
        '
        'CmdSimpan
        '
        Me.CmdSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdSimpan.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmdSimpan.Appearance.Options.UseFont = True
        Me.CmdSimpan.Image = CType(resources.GetObject("CmdSimpan.Image"), System.Drawing.Image)
        Me.CmdSimpan.Location = New System.Drawing.Point(197, 259)
        Me.CmdSimpan.Name = "CmdSimpan"
        Me.CmdSimpan.Size = New System.Drawing.Size(76, 27)
        Me.CmdSimpan.TabIndex = 8
        Me.CmdSimpan.Text = "Connect"
        '
        'PanelLocal
        '
        Me.PanelLocal.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelLocal.AppearanceCaption.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelLocal.AppearanceCaption.Options.UseFont = True
        Me.PanelLocal.Controls.Add(Me.LocalOpt2)
        Me.PanelLocal.Controls.Add(Me.LabelControl3)
        Me.PanelLocal.Controls.Add(Me.LocalOpt1)
        Me.PanelLocal.Controls.Add(Me.LocalServer)
        Me.PanelLocal.Controls.Add(Me.LocalDB)
        Me.PanelLocal.Controls.Add(Me.LocalPWD)
        Me.PanelLocal.Controls.Add(Me.LocalUID)
        Me.PanelLocal.Controls.Add(Me.LabelControl9)
        Me.PanelLocal.Controls.Add(Me.LabelControl10)
        Me.PanelLocal.Controls.Add(Me.LabelControl7)
        Me.PanelLocal.Controls.Add(Me.LabelControl8)
        Me.PanelLocal.Controls.Add(Me.LabelControl1)
        Me.PanelLocal.Controls.Add(Me.LabelControl2)
        Me.PanelLocal.Controls.Add(Me.LabelControl5)
        Me.PanelLocal.Controls.Add(Me.LabelControl6)
        Me.PanelLocal.Controls.Add(Me.ShapeContainer1)
        Me.PanelLocal.Location = New System.Drawing.Point(12, 12)
        Me.PanelLocal.Name = "PanelLocal"
        Me.PanelLocal.Size = New System.Drawing.Size(343, 241)
        Me.PanelLocal.TabIndex = 7
        Me.PanelLocal.Text = "Local Connection"
        '
        'LocalOpt2
        '
        Me.LocalOpt2.AutoSize = True
        Me.LocalOpt2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocalOpt2.Location = New System.Drawing.Point(32, 122)
        Me.LocalOpt2.Name = "LocalOpt2"
        Me.LocalOpt2.Size = New System.Drawing.Size(168, 19)
        Me.LocalOpt2.TabIndex = 251
        Me.LocalOpt2.Text = "SQL Server Authentication"
        Me.LocalOpt2.UseVisualStyleBackColor = True
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(11, 76)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 15)
        Me.LabelControl3.TabIndex = 249
        Me.LabelControl3.Text = "Connect Using :"
        '
        'LocalOpt1
        '
        Me.LocalOpt1.AutoSize = True
        Me.LocalOpt1.Checked = True
        Me.LocalOpt1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocalOpt1.Location = New System.Drawing.Point(32, 97)
        Me.LocalOpt1.Name = "LocalOpt1"
        Me.LocalOpt1.Size = New System.Drawing.Size(159, 19)
        Me.LocalOpt1.TabIndex = 248
        Me.LocalOpt1.TabStop = True
        Me.LocalOpt1.Text = "Windows Authentication"
        Me.LocalOpt1.UseVisualStyleBackColor = True
        '
        'LocalServer
        '
        Me.LocalServer.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocalServer.FormattingEnabled = True
        Me.LocalServer.Location = New System.Drawing.Point(81, 30)
        Me.LocalServer.Name = "LocalServer"
        Me.LocalServer.Size = New System.Drawing.Size(249, 23)
        Me.LocalServer.TabIndex = 247
        '
        'LocalDB
        '
        Me.LocalDB.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocalDB.FormattingEnabled = True
        Me.LocalDB.Location = New System.Drawing.Point(137, 201)
        Me.LocalDB.Name = "LocalDB"
        Me.LocalDB.Size = New System.Drawing.Size(193, 23)
        Me.LocalDB.TabIndex = 246
        '
        'LocalPWD
        '
        Me.LocalPWD.Location = New System.Drawing.Point(137, 174)
        Me.LocalPWD.Name = "LocalPWD"
        Me.LocalPWD.Properties.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocalPWD.Properties.Appearance.Options.UseFont = True
        Me.LocalPWD.Properties.MaxLength = 50
        Me.LocalPWD.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.LocalPWD.Size = New System.Drawing.Size(132, 21)
        Me.LocalPWD.TabIndex = 3
        '
        'LocalUID
        '
        Me.LocalUID.Location = New System.Drawing.Point(137, 147)
        Me.LocalUID.Name = "LocalUID"
        Me.LocalUID.Properties.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LocalUID.Properties.Appearance.Options.UseFont = True
        Me.LocalUID.Properties.MaxLength = 50
        Me.LocalUID.Size = New System.Drawing.Size(132, 21)
        Me.LocalUID.TabIndex = 2
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl9.Location = New System.Drawing.Point(128, 180)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(3, 17)
        Me.LabelControl9.TabIndex = 245
        Me.LabelControl9.Text = ":"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl10.Location = New System.Drawing.Point(68, 180)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(52, 15)
        Me.LabelControl10.TabIndex = 244
        Me.LabelControl10.Text = "Password"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl7.Location = New System.Drawing.Point(127, 152)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(3, 17)
        Me.LabelControl7.TabIndex = 242
        Me.LabelControl7.Text = ":"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl8.Location = New System.Drawing.Point(68, 153)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(53, 15)
        Me.LabelControl8.TabIndex = 241
        Me.LabelControl8.Text = "Username"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(128, 203)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(3, 17)
        Me.LabelControl1.TabIndex = 236
        Me.LabelControl1.Text = ":"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(71, 204)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(50, 15)
        Me.LabelControl2.TabIndex = 235
        Me.LabelControl2.Text = "Database"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(72, 34)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(3, 17)
        Me.LabelControl5.TabIndex = 233
        Me.LabelControl5.Text = ":"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl6.Location = New System.Drawing.Point(32, 36)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(34, 15)
        Me.LabelControl6.TabIndex = 232
        Me.LabelControl6.Text = "Server"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(2, 23)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(339, 216)
        Me.ShapeContainer1.TabIndex = 250
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 9
        Me.LineShape1.X2 = 327
        Me.LineShape1.Y1 = 41
        Me.LineShape1.Y2 = 41
        '
        'FrmSettingKoneksi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(367, 298)
        Me.ControlBox = False
        Me.Controls.Add(Me.CmdKeluar)
        Me.Controls.Add(Me.CmdSimpan)
        Me.Controls.Add(Me.PanelLocal)
        Me.Name = "FrmSettingKoneksi"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Database"
        CType(Me.PanelLocal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelLocal.ResumeLayout(False)
        Me.PanelLocal.PerformLayout()
        CType(Me.LocalPWD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LocalUID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CmdKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelLocal As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LocalPWD As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LocalUID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LocalOpt1 As System.Windows.Forms.RadioButton
    Friend WithEvents LocalServer As System.Windows.Forms.ComboBox
    Friend WithEvents LocalDB As System.Windows.Forms.ComboBox
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LocalOpt2 As System.Windows.Forms.RadioButton
End Class
