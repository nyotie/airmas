﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAbout
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.UpdateNotes = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.VersionNum = New DevExpress.XtraEditors.TextEdit()
        CType(Me.UpdateNotes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VersionNum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'butClose
        '
        Me.butClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butClose.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Appearance.Options.UseFont = True
        Me.butClose.Location = New System.Drawing.Point(363, 166)
        Me.butClose.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(90, 40)
        Me.butClose.TabIndex = 16
        Me.butClose.Text = "Tutup"
        '
        'UpdateNotes
        '
        Me.UpdateNotes.EditValue = ""
        Me.UpdateNotes.Location = New System.Drawing.Point(136, 52)
        Me.UpdateNotes.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.UpdateNotes.Name = "UpdateNotes"
        Me.UpdateNotes.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.UpdateNotes.Properties.Appearance.Options.UseFont = True
        Me.UpdateNotes.Properties.ReadOnly = True
        Me.UpdateNotes.Size = New System.Drawing.Size(317, 103)
        Me.UpdateNotes.TabIndex = 15
        Me.UpdateNotes.TabStop = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(32, 55)
        Me.LabelControl3.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl3.TabIndex = 14
        Me.LabelControl3.Text = "Update notes"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.LabelControl1.Location = New System.Drawing.Point(32, 28)
        Me.LabelControl1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl1.TabIndex = 12
        Me.LabelControl1.Text = "Version"
        '
        'VersionNum
        '
        Me.VersionNum.Location = New System.Drawing.Point(136, 25)
        Me.VersionNum.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.VersionNum.Name = "VersionNum"
        Me.VersionNum.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!)
        Me.VersionNum.Properties.Appearance.Options.UseFont = True
        Me.VersionNum.Properties.ReadOnly = True
        Me.VersionNum.Size = New System.Drawing.Size(317, 21)
        Me.VersionNum.TabIndex = 11
        Me.VersionNum.TabStop = False
        '
        'FrmAbout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 231)
        Me.ControlBox = False
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.UpdateNotes)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.VersionNum)
        Me.Name = "FrmAbout"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmAbout"
        CType(Me.UpdateNotes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VersionNum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents UpdateNotes As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents VersionNum As DevExpress.XtraEditors.TextEdit
End Class
