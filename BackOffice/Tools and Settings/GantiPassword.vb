﻿Public Class GantiPassword

    Private Sub GantiPassword_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        OldPassword.Focus()
    End Sub

    Private Sub GantiPassword_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("try_change_password")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub GantiPassword_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        staffName.Text = staff_name
        LabelControl4.Visible = False
        OldPassword.Text = ""
        NewPassword.Text = ""
        RetypePassword.Text = ""
    End Sub

    Private Sub simpanButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butSimpan.Click
        If Not beforeSave() Then Return
        If Not NewPassword.EditValue = RetypePassword.EditValue Then
            LabelControl4.Visible = True
            Return
        End If

        Try
            SQLquery = String.Format("EXEC try_change_password '{0}','{1}','{2}'",
                                     Encryptor.EncryptData(OldPassword.EditValue.ToString.Replace("'", "''")), Encryptor.EncryptData(RetypePassword.EditValue.ToString.Replace("'", "''")), staff_id)
            ExDb.ExecQuery(SQLquery, xSet, "try_change_password")
            LabelControl4.Visible = False

            If xSet.Tables("try_change_password").Rows(0).Item("HASIL") Then
                msgboxInformation("Password berhasil di ganti")
                Close()
            Else
                msgboxInformation("Password lama salah!")
            End If
        Catch ex As Exception
            msgboxErrorDev()
            Close()
        End Try
    End Sub

    Private Sub cancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butCancel.Click
        Close()
    End Sub

    Private Function beforeSave()
        If NewPassword.Text.Length < 3 Then
            msgboxInformation("Password minimal 3 digit")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class