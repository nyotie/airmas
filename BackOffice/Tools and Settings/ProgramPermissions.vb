﻿Imports DevExpress.Data
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid

Public Class ProgramPermissions
    Dim IDUSER As Integer

    Private Sub ProgramPermissions_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        GroupList.Focus()
    End Sub

    Private Sub ProgramPermissions_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("UserList")
        xSet.Tables.Remove("ProgPermissionsDefault")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ProgramPermissions_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        If Not xSet.Tables("UserList") Is Nothing Then xSet.Tables("UserList").Clear()
        SQLquery = "SELECT IDUSER, NAMA FROM MUSER where INACTIVE=0"
        ExDb.ExecQuery(SQLquery, xSet, "UserList")

        GroupList.Properties.Items.Clear()
        GroupList.Properties.Items.Add("Pilih User")

        For Each row As DataRow In xSet.Tables("UserList").Rows
            GroupList.Properties.Items.Add(row.Item("NAMA"))
        Next
        GroupList.SelectedIndex = 0
    End Sub

    Private Sub GroupList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GroupList.SelectedIndexChanged
        Try
            If GroupList.SelectedIndex > 0 And xSet.Tables("ProgPermissionsDefault") Is Nothing Then
                IDUSER = xSet.Tables("UserList").Select(String.Format("NAMA = '{0}'", GroupList.SelectedItem))(0).Item("IDUSER")
                SQLquery = String.Format("EXEC Get_Data_Permissions_Program {0};", IDUSER)
                ExDb.ExecQuery(SQLquery, xSet, "ProgPermissionsDefault")

                RepoCheck.ValueChecked = "True"
                RepoCheck.ValueUnchecked = "False"
                RepoCheck.ValueGrayed = "False"

                GridProgPermissions.DataSource = xSet.Tables("ProgPermissionsDefault").DefaultView
                GridProgPermissions.RepositoryItems.Add(RepoCheck)

                With ViewProgPermissions
                    .Columns("IDFORM").OptionsColumn.AllowEdit = False
                    .Columns("NAMA").OptionsColumn.AllowEdit = False

                    .Columns("IDFORM").Visible = False

                    .Columns("BACA").Caption = "Baca"
                    .Columns("NEW").Caption = "Tambah"
                    .Columns("EDIT").Caption = "Koreksi"
                    .Columns("HAPUS").Caption = "Hapus"
                    .Columns("ACC").Caption = "ACC"
                    .Columns("VOID").Caption = "Void"

                    .Columns("NAMA").Caption = "Module"
                    .Columns("NAMA").Width = 300
                End With

                For Each coll As DataColumn In xSet.Tables("ProgPermissionsDefault").Columns
                    ViewProgPermissions.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
                Next
            ElseIf GroupList.SelectedIndex > 0 And Not xSet.Tables("ProgPermissionsDefault") Is Nothing Then
                xSet.Tables("ProgPermissionsDefault").Clear()
                IDUSER = xSet.Tables("UserList").Select(String.Format("NAMA = '{0}'", GroupList.SelectedItem))(0).Item("IDUSER")
                SQLquery = String.Format("EXEC Get_Data_Permissions_Program {0};", IDUSER)
                ExDb.ExecQuery(SQLquery, xSet, "ProgPermissionsDefault")

                GridProgPermissions.DataSource = xSet.Tables("ProgPermissionsDefault").DefaultView
            Else
                GridProgPermissions.DataSource = Nothing
            End If

        Catch ex As Exception
            msgboxErrorDev()
        End Try
    End Sub

    'Private Sub ViewProgPermissions_RowCellStyle(ByVal sender As Object, ByVal e As RowCellStyleEventArgs) Handles ViewProgPermissions.RowCellStyle
    '    Dim View As GridView = sender

    '    If e.Column.FieldName = "AllowRead" Then
    '        If View.GetRowCellValue(e.RowHandle, View.Columns("EnableRead")) = False Then
    '            e.Appearance.BackColor = Color.Gray
    '        End If
    '    ElseIf e.Column.FieldName = "AllowCreate" Then
    '        If View.GetRowCellValue(e.RowHandle, View.Columns("EnableCreate")) = False Then
    '            e.Appearance.BackColor = Color.Gray
    '        End If
    '    ElseIf e.Column.FieldName = "AllowEdit" Then
    '        If View.GetRowCellValue(e.RowHandle, View.Columns("EnableEdit")) = False Then
    '            e.Appearance.BackColor = Color.Gray
    '        End If
    '    ElseIf e.Column.FieldName = "AllowDelete" Then
    '        If View.GetRowCellValue(e.RowHandle, View.Columns("EnableDelete")) = False Then
    '            e.Appearance.BackColor = Color.Gray
    '        End If
    '    ElseIf e.Column.FieldName = "AllowForceClose" Then
    '        If View.GetRowCellValue(e.RowHandle, View.Columns("EnableForceClose")) = False Then
    '            e.Appearance.BackColor = Color.Gray
    '        End If
    '    ElseIf e.Column.FieldName = "AllowPrint" Then
    '        If View.GetRowCellValue(e.RowHandle, View.Columns("EnablePrint")) = False Then
    '            e.Appearance.BackColor = Color.Gray
    '        End If
    '    End If
    'End Sub

    'Private Sub ViewProgPermissions_ShowingEditor(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ViewProgPermissions.ShowingEditor
    '    Dim View As GridView = sender

    '    If View.FocusedColumn.FieldName = "AllowRead" Then
    '        If View.GetFocusedRowCellValue("EnableRead") = False Then
    '            e.Cancel = True
    '        End If
    '    ElseIf View.FocusedColumn.FieldName = "AllowCreate" Then
    '        If View.GetFocusedRowCellValue("EnableCreate") = False Then
    '            e.Cancel = True
    '        End If
    '    ElseIf View.FocusedColumn.FieldName = "AllowEdit" Then
    '        If View.GetFocusedRowCellValue("EnableEdit") = False Then
    '            e.Cancel = True
    '        End If
    '    ElseIf View.FocusedColumn.FieldName = "AllowDelete" Then
    '        If View.GetFocusedRowCellValue("EnableDelete") = False Then
    '            e.Cancel = True
    '        End If
    '    ElseIf View.FocusedColumn.FieldName = "AllowForceClose" Then
    '        If View.GetFocusedRowCellValue("EnableForceClose") = False Then
    '            e.Cancel = True
    '        End If
    '    ElseIf View.FocusedColumn.FieldName = "AllowPrint" Then
    '        If View.GetFocusedRowCellValue("EnablePrint") = False Then
    '            e.Cancel = True
    '        End If
    '    End If
    'End Sub

    Private Sub butSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butSimpan.Click
        If ViewProgPermissions.OptionsBehavior.Editable = False Then Return
        If ViewProgPermissions.RowCount < 1 Then Return
        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return
        End If

        SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC sp_permissions_program_insert {0}, """, IDUSER)
        For Each drow As DataRow In xSet.Tables("ProgPermissionsDefault").Rows
            SQLquery += String.Format("({0}, {1},'{2}','{3}','{4}','{5}','{6}','{7}'), ", IDUSER,
                                      drow.Item("IDFORM"), drow.Item("BACA"),
                                      drow.Item("NEW"), drow.Item("EDIT"),
                                      drow.Item("HAPUS"), drow.Item("ACC"),
                                      drow.Item("VOID"))
        Next
        SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
        ExDb.ExecData(SQLquery)

        msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        GroupList.SelectedIndex = 0
    End Sub

    Private Sub butBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBatal.Click
        Close()
    End Sub
End Class