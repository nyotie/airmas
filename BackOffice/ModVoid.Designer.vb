﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ModVoid
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.NamaTrans = New DevExpress.XtraEditors.TextEdit()
        Me.KetVoid = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.NoTrans = New DevExpress.XtraEditors.TextEdit()
        Me.ButOK = New DevExpress.XtraEditors.SimpleButton()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.NamaTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KetVoid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NoTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 40)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(75, 15)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "No. Transaksi"
        '
        'NamaTrans
        '
        Me.NamaTrans.Location = New System.Drawing.Point(106, 9)
        Me.NamaTrans.Name = "NamaTrans"
        Me.NamaTrans.Properties.Appearance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NamaTrans.Properties.Appearance.Options.UseFont = True
        Me.NamaTrans.Properties.ReadOnly = True
        Me.NamaTrans.Size = New System.Drawing.Size(156, 22)
        Me.NamaTrans.TabIndex = 1
        Me.NamaTrans.TabStop = False
        '
        'KetVoid
        '
        Me.KetVoid.Location = New System.Drawing.Point(106, 65)
        Me.KetVoid.Name = "KetVoid"
        Me.KetVoid.Properties.Appearance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KetVoid.Properties.Appearance.Options.UseFont = True
        Me.KetVoid.Properties.MaxLength = 200
        Me.KetVoid.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.KetVoid.Size = New System.Drawing.Size(321, 88)
        Me.KetVoid.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(12, 68)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(62, 15)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Keterangan"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(54, 15)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Transaksi"
        '
        'NoTrans
        '
        Me.NoTrans.Location = New System.Drawing.Point(106, 37)
        Me.NoTrans.Name = "NoTrans"
        Me.NoTrans.Properties.Appearance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoTrans.Properties.Appearance.Options.UseFont = True
        Me.NoTrans.Properties.ReadOnly = True
        Me.NoTrans.Size = New System.Drawing.Size(75, 22)
        Me.NoTrans.TabIndex = 2
        Me.NoTrans.TabStop = False
        '
        'ButOK
        '
        Me.ButOK.Appearance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButOK.Appearance.Options.UseFont = True
        Me.ButOK.Location = New System.Drawing.Point(106, 159)
        Me.ButOK.Name = "ButOK"
        Me.ButOK.Size = New System.Drawing.Size(75, 29)
        Me.ButOK.TabIndex = 4
        Me.ButOK.Text = "&OK"
        '
        'ButBatal
        '
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Location = New System.Drawing.Point(187, 159)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(75, 29)
        Me.ButBatal.TabIndex = 5
        Me.ButBatal.Text = "&Batal"
        '
        'ModVoid
        '
        Me.Appearance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(439, 200)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButOK)
        Me.Controls.Add(Me.KetVoid)
        Me.Controls.Add(Me.NoTrans)
        Me.Controls.Add(Me.NamaTrans)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Name = "ModVoid"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Void"
        CType(Me.NamaTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KetVoid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NoTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NamaTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents KetVoid As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NoTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ButOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
End Class
