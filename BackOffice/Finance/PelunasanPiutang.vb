﻿Imports DevExpress.Utils

Public Class PelunasanPiutang
    Dim selected_cell As String

    Private Sub ThisForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_PLP")
        xSet.Tables.Remove("TD_PLP")
        xSet.Tables.Remove("DaftarPiutang")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub ThisForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now
        Else
            SQLquery = String.Format("SELECT NOPLP, TANGGAL, CARA_BAYAR, MEMO, ACC FROM TM_PLP WHERE NOPLH='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_PLP")

            NoTrans.Text = xSet.Tables("TM_PLP").Rows(0).Item("NOPLH")
            TglTrans.EditValue = xSet.Tables("TM_PLP").Rows(0).Item("TANGGAL")
            CaraBayarPiutang.SelectedIndex = xSet.Tables("TM_PLP").Rows(0).Item("CARA_BAYAR")
            Catatan.EditValue = xSet.Tables("TM_PLP").Rows(0).Item("MEMO")
            ACCcheck.Checked = IIf(xSet.Tables("TM_PLP").Rows(0).Item("ACC") = 0, False, True)
        End If
    End Sub

    Private Sub DaftarPiutang_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarPiutang.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell = Repository.EditValue
    End Sub

    Private Sub ViewTrans_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans.CellValueChanged
        Try
            Dim drow As DataRow = xSet.Tables("DaftarPiutang").Select(String.Format("Nota='{0}'", selected_cell))(0)

            If e.Column.FieldName = "NOTA" Then
                For row As Integer = 0 To ViewTrans.RowCount - 1
                    If ViewTrans.GetRowCellValue(row, "NOTA") = selected_cell And Not ViewTrans.FocusedRowHandle = row Then
                        ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                        ViewTrans.FocusedRowHandle = row
                        Return
                    End If
                Next

                ViewTrans.SetFocusedRowCellValue("IDPARTNER", drow.Item("ID_PARTNER"))
                ViewTrans.SetFocusedRowCellValue("Customer", drow.Item("Customer"))
                ViewTrans.SetFocusedRowCellValue("Piutang", drow.Item("Piutang"))
                ViewTrans.SetFocusedRowCellValue("Retur", drow.Item("Retur"))
                ViewTrans.SetFocusedRowCellValue("Sisa", drow.Item("SisaPiutang"))
                ViewTrans.SetFocusedRowCellValue("Pelunasan", 0)
                ViewTrans.SetFocusedRowCellValue("Lainnya", 0)
                ViewTrans.SetFocusedRowCellValue("BG", "")
                ViewTrans.SetFocusedRowCellValue("TanggalBG", Today.Date)
                ViewTrans.SetFocusedRowCellValue("Catatan", "")
                ViewTrans.FocusedRowHandle = ViewTrans.RowCount
            End If
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("NOTA")) = True Then Throw New Exception

            If e.Column.FieldName = "Pelunasan" And ViewTrans.FocusedColumn.FieldName = "Pelunasan" Then
                If ViewTrans.GetFocusedRowCellValue("Pelunasan") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("Pelunasan", 0)
                End If
            End If
        Catch ex As Exception
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("NOTA")) = True Then
                ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                xSet.Tables("TD_PLH").AcceptChanges()
            End If
        End Try
    End Sub

    Private Sub ViewTrans_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewTrans.FocusedRowChanged
        Try
            If e.FocusedRowHandle >= 0 Then
                selected_cell = ViewTrans.GetRowCellValue(e.FocusedRowHandle, "NOTA")
            End If
        Catch ex As Exception
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
        End Try
    End Sub

    Private Sub GridTrans_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
            xSet.Tables("TD_PLH").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()
        ViewTrans.UpdateSummary()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = "SET QUOTED_IDENTIFIER OFF; EXEC PLP_INS "
            Else
                SQLquery = "SET QUOTED_IDENTIFIER OFF; EXEC PLP_UPD "
            End If
            SQLquery += String.Format("'{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', """,
                        NoTrans.Text, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), CaraBayarPiutang.EditValue,
                        Catatan.Text.Replace("'", "''"), ViewTrans.Columns("Pelunasan").SummaryItem.SummaryValue, ViewTrans.Columns("Lainnya").SummaryItem.SummaryValue,
                        IIf(ACCcheck.Checked, 1, 0), staff_id)

            Dim nourut As Integer = 1
            For Each drow As DataRow In xSet.Tables("TD_PLP").Rows
                SQLquery += String.Format("('{0}', {1}, '{2}', '{3}', {4}, {5}, {6}, '{7}', '{8}', '{9}'), ",
                                          NoTrans.Text, nourut, drow.Item("IDPARTNER"),
                                          drow.Item("NOTA"), drow.Item("Sisa"), drow.Item("Pelunasan"), drow.Item("Lainnya"),
                                          drow.Item("BG"), Format(drow.Item("TanggalBG"), "yyyy-MM-dd"), drow.Item("Catatan"))
                nourut += 1
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserPelunasanPiutang.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()
            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_PLP").Clear()
        xSet.Tables("TD_PLP").AcceptChanges()

        TglTrans.EditValue = Now
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------DaftarPiutang 
        If Not xSet.Tables("DaftarPiutang") Is Nothing Then xSet.Tables("DaftarPiutang").Clear()
        SQLquery = String.Format("SELECT TM.ID_PARTNER, MS.NAMA Customer, NOTRX Nota, Tanggal, NILAITRANS Piutang, NILAIRETUR Retur, NILAITRANS-NILAIRETUR-PELUNASAN SisaPiutang " & _
                                 "FROM T_SALDO TM INNER JOIN MCUSTOMER MS ON TM.ID_PARTNER=MS.IDCUSTOMER " & _
                                 "WHERE (NILAITRANS-NILAIRETUR>PELUNASAN AND VOID=0 AND JNS_TRX=0) OR (TM.NOTRX IN (SELECT NOTA FROM TD_PLP WHERE NOPLP='{0}')) ORDER BY TANGGAL", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarPiutang")
        DaftarPiutang.DataSource = xSet.Tables("DaftarPiutang").DefaultView

        DaftarPiutang.NullText = ""
        DaftarPiutang.ValueMember = "Nota"
        DaftarPiutang.DisplayMember = "Nota"
        DaftarPiutang.ShowClearButton = False
        DaftarPiutang.PopulateViewColumns()

        ViewDaftarPiutang.Columns("ID_PARTNER").Visible = False

        ViewDaftarPiutang.Columns("Tanggal").DisplayFormat.FormatType = FormatType.DateTime
        ViewDaftarPiutang.Columns("Tanggal").DisplayFormat.FormatString = "dd MMM yyyy"
        ViewDaftarPiutang.Columns("Piutang").DisplayFormat.FormatType = FormatType.Numeric
        ViewDaftarPiutang.Columns("Piutang").DisplayFormat.FormatString = "n0"
        ViewDaftarPiutang.Columns("Retur").DisplayFormat.FormatType = FormatType.Numeric
        ViewDaftarPiutang.Columns("Retur").DisplayFormat.FormatString = "n0"
        ViewDaftarPiutang.Columns("SisaPiutang").DisplayFormat.FormatType = FormatType.Numeric
        ViewDaftarPiutang.Columns("SisaPiutang").DisplayFormat.FormatString = "n0"

        For Each coll As DataColumn In xSet.Tables("DaftarPiutang").Columns
            ViewDaftarPiutang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------TD_PLP
        SQLquery = String.Format("SELECT TD.NOTA, TD.IDPARTNER, MS.NAMA Customer, TS.NILAITRANS Piutang, TS.NILAIRETUR Retur, SISA Sisa, BAYAR Pelunasan, Lainnya, BG, TGL_BG TanggalBG, CAT Catatan FROM TD_PLP TD " & _
                                 "INNER JOIN T_SALDO TS ON TS.NOTRX=TD.NOTA INNER JOIN MCUSTOMER MS ON TS.ID_PARTNER=MS.IDCUSTOMER " & _
                                 "WHERE TD.NOPLP='{0}' ORDER BY TD.NO_URUT ", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_PLP")
        GridTrans.DataSource = xSet.Tables("TD_PLP").DefaultView

        ViewTrans.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        For Each coll As DataColumn In xSet.Tables("TD_PLP").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("NOTA").ColumnEdit = DaftarPiutang

        ViewTrans.Columns("IDPARTNER").Visible = False
        ViewTrans.Columns("Customer").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("Piutang").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("Retur").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("Sisa").OptionsColumn.AllowEdit = False

        ViewTrans.Columns("BG").Caption = "T/BG/KU"

        ViewTrans.Columns("Piutang").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Piutang").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("Retur").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Retur").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("Sisa").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Sisa").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("Pelunasan").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Pelunasan").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("Lainnya").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Lainnya").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("TanggalBG").DisplayFormat.FormatType = FormatType.DateTime
        ViewTrans.Columns("TanggalBG").DisplayFormat.FormatString = "dd MMM yyyy"

        ViewTrans.Columns("NOTA").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("NOTA").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewTrans.Columns("Sisa").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("Sisa").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("Pelunasan").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("Pelunasan").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("Lainnya").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("Lainnya").SummaryItem.DisplayFormat = "Total:{0:n0}"

        ViewTrans.Columns("NOTA").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Customer").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("Piutang").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Retur").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Sisa").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Pelunasan").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("BG").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Catatan").Width = GridTrans.Width * 0.2
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_PLP").Rows.Count < 1 Then
            Return False
        End If

        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        For Each drow As DataRow In xSet.Tables("TD_PLP").Rows
            If IsDBNull(drow.Item("Pelunasan")) Then
                msgboxWarning("Jumlah yang dibayarkan tidak boleh kosong atau kurang dari 0")
                Return False
            ElseIf drow.Item("Pelunasan") <= 0 Then
                msgboxWarning("Jumlah yang dibayarkan tidak boleh kosong atau kurang dari 0")
                Return False
            ElseIf IsDBNull(drow.Item("TanggalBG")) Then
                msgboxWarning("Tanggal BG tidak boleh kosong")
                Return False
            End If
        Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "PLP", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NOPLP, 3)) AS KODE FROM TM_PLP WHERE LEFT(NOPLP, 10) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        Dim tgl As Date

        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        tgl = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function
End Class