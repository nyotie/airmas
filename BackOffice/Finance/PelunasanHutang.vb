﻿Imports DevExpress.Utils

Public Class PelunasanHutang
    Dim selected_cell As String

    Private Sub ThisForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_PLH")
        xSet.Tables.Remove("TD_PLH")
        xSet.Tables.Remove("DaftarHutang")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub ThisForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now
        Else
            SQLquery = String.Format("SELECT NOPLH, TANGGAL, CARA_BAYAR, MEMO, ACC FROM TM_PLH WHERE NOPLH='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_PLH")

            NoTrans.Text = xSet.Tables("TM_PLH").Rows(0).Item("NOPLH")
            TglTrans.EditValue = xSet.Tables("TM_PLH").Rows(0).Item("TANGGAL")
            CaraBayarHutang.SelectedIndex = xSet.Tables("TM_PLH").Rows(0).Item("CARA_BAYAR")
            Catatan.EditValue = xSet.Tables("TM_PLH").Rows(0).Item("MEMO").ToString
            ACCcheck.Checked = IIf(xSet.Tables("TM_PLH").Rows(0).Item("ACC") = 0, False, True)
        End If
    End Sub

    Private Sub DaftarHutang_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarHutang.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell = Repository.EditValue
    End Sub

    Private Sub ViewTrans_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans.CellValueChanged
        Try
            Dim drow As DataRow = xSet.Tables("DaftarHutang").Select(String.Format("Nota='{0}'", selected_cell))(0)

            If e.Column.FieldName = "NOTA" Then
                For row As Integer = 0 To ViewTrans.RowCount - 1
                    If ViewTrans.GetRowCellValue(row, "NOTA") = selected_cell And Not ViewTrans.FocusedRowHandle = row Then
                        ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                        ViewTrans.FocusedRowHandle = row
                        Return
                    End If
                Next

                ViewTrans.SetFocusedRowCellValue("IDPARTNER", drow.Item("ID_PARTNER"))
                ViewTrans.SetFocusedRowCellValue("Supplier", drow.Item("Supplier"))
                ViewTrans.SetFocusedRowCellValue("Hutang", drow.Item("Hutang"))
                ViewTrans.SetFocusedRowCellValue("Retur", drow.Item("Retur"))
                ViewTrans.SetFocusedRowCellValue("Sisa", drow.Item("SisaHutang"))
                ViewTrans.SetFocusedRowCellValue("Pelunasan", 0)
                ViewTrans.SetFocusedRowCellValue("Lainnya", 0)
                ViewTrans.SetFocusedRowCellValue("BG", "")
                ViewTrans.SetFocusedRowCellValue("TanggalBG", Today.Date)
                ViewTrans.SetFocusedRowCellValue("Catatan", "")
                ViewTrans.FocusedRowHandle = ViewTrans.RowCount
            End If
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("NOTA")) = True Then Throw New Exception

            If e.Column.FieldName = "Pelunasan" And ViewTrans.FocusedColumn.FieldName = "Pelunasan" Then
                If ViewTrans.GetFocusedRowCellValue("Pelunasan") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("Pelunasan", 0)
                End If
            End If
        Catch ex As Exception
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("NOTA")) = True Then
                ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                xSet.Tables("TD_PLH").AcceptChanges()
            End If
        End Try
    End Sub

    Private Sub ViewTrans_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewTrans.FocusedRowChanged
        Try
            If e.FocusedRowHandle >= 0 Then
                selected_cell = ViewTrans.GetRowCellValue(e.FocusedRowHandle, "NOTA")
            End If
        Catch ex As Exception
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
        End Try
    End Sub

    Private Sub GridTrans_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
            xSet.Tables("TD_PLH").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()
        ViewTrans.UpdateSummary()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = "SET QUOTED_IDENTIFIER OFF; EXEC PLH_INS "
            Else
                SQLquery = "SET QUOTED_IDENTIFIER OFF; EXEC PLH_UPD "
            End If
            SQLquery += String.Format("'{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', """,
                        NoTrans.Text, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), CaraBayarHutang.EditValue,
                        Catatan.Text.Replace("'", "''"), ViewTrans.Columns("Pelunasan").SummaryItem.SummaryValue, ViewTrans.Columns("Lainnya").SummaryItem.SummaryValue,
                        IIf(ACCcheck.Checked, 1, 0), staff_id)

            Dim nourut As Integer = 1
            For Each drow As DataRow In xSet.Tables("TD_PLH").Rows
                SQLquery += String.Format("('{0}', {1}, '{2}', '{3}', {4}, {5}, {6}, '{7}', '{8}', '{9}'), ",
                                          NoTrans.Text, nourut, drow.Item("IDPARTNER"),
                                          drow.Item("NOTA"), drow.Item("Sisa"), drow.Item("Pelunasan"), drow.Item("Lainnya"),
                                          drow.Item("BG"), Format(drow.Item("TanggalBG"), "yyyy-MM-dd"), drow.Item("Catatan"))
                nourut += 1
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserPelunasanHutang.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()
            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_PLH").Clear()
        xSet.Tables("TD_PLH").AcceptChanges()

        TglTrans.EditValue = Now
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------DaftarHutang 
        If Not xSet.Tables("DaftarHutang") Is Nothing Then xSet.Tables("DaftarHutang").Clear()
        SQLquery = String.Format("SELECT TM.ID_PARTNER, MS.NAMA Supplier, NOTRX Nota, Tanggal, NILAITRANS Hutang, NILAIRETUR Retur, NILAITRANS-NILAIRETUR-PELUNASAN SisaHutang " & _
                                 "FROM T_SALDO TM INNER JOIN MSUPPLIER MS ON TM.ID_PARTNER=MS.IDSUPPLIER " & _
                                 "WHERE (NILAITRANS-NILAIRETUR>PELUNASAN AND VOID=0 AND JNS_TRX=0) OR (TM.NOTRX IN (SELECT NOTA FROM TD_PLH WHERE NOPLH='{0}')) ORDER BY TANGGAL", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarHutang")
        DaftarHutang.DataSource = xSet.Tables("DaftarHutang").DefaultView

        DaftarHutang.NullText = ""
        DaftarHutang.ValueMember = "Nota"
        DaftarHutang.DisplayMember = "Nota"
        DaftarHutang.ShowClearButton = False
        DaftarHutang.PopulateViewColumns()

        ViewDaftarHutang.Columns("ID_PARTNER").Visible = False

        ViewDaftarHutang.Columns("Tanggal").DisplayFormat.FormatType = FormatType.DateTime
        ViewDaftarHutang.Columns("Tanggal").DisplayFormat.FormatString = "dd MMM yyyy"
        ViewDaftarHutang.Columns("Hutang").DisplayFormat.FormatType = FormatType.Numeric
        ViewDaftarHutang.Columns("Hutang").DisplayFormat.FormatString = "n0"
        ViewDaftarHutang.Columns("Retur").DisplayFormat.FormatType = FormatType.Numeric
        ViewDaftarHutang.Columns("Retur").DisplayFormat.FormatString = "n0"
        ViewDaftarHutang.Columns("SisaHutang").DisplayFormat.FormatType = FormatType.Numeric
        ViewDaftarHutang.Columns("SisaHutang").DisplayFormat.FormatString = "n0"

        For Each coll As DataColumn In xSet.Tables("DaftarHutang").Columns
            ViewDaftarHutang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------TD_PLH
        SQLquery = String.Format("SELECT TD.NOTA, TD.IDPARTNER, MS.NAMA Supplier, TS.NILAITRANS Hutang, TS.NILAIRETUR Retur, SISA Sisa, BAYAR Pelunasan, Lainnya, BG, TGL_BG TanggalBG, CAT Catatan FROM TD_PLH TD " & _
                                 "INNER JOIN T_SALDO TS ON TS.NOTRX=TD.NOTA INNER JOIN MSUPPLIER MS ON TD.IDPARTNER=MS.IDSUPPLIER " & _
                                 "WHERE TD.NOPLH='{0}' ORDER BY TD.NO_URUT ", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_PLH")
        GridTrans.DataSource = xSet.Tables("TD_PLH").DefaultView

        ViewTrans.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        For Each coll As DataColumn In xSet.Tables("TD_PLH").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("NOTA").ColumnEdit = DaftarHutang

        ViewTrans.Columns("IDPARTNER").Visible = False
        ViewTrans.Columns("Supplier").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("Hutang").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("Retur").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("Sisa").OptionsColumn.AllowEdit = False

        ViewTrans.Columns("BG").Caption = "T/BG/KU"

        ViewTrans.Columns("Hutang").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Hutang").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("Retur").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Retur").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("Sisa").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Sisa").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("Pelunasan").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Pelunasan").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("Lainnya").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Lainnya").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("TanggalBG").DisplayFormat.FormatType = FormatType.DateTime
        ViewTrans.Columns("TanggalBG").DisplayFormat.FormatString = "dd MMM yyyy"

        ViewTrans.Columns("NOTA").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("NOTA").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewTrans.Columns("Sisa").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("Sisa").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("Pelunasan").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("Pelunasan").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("Lainnya").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("Lainnya").SummaryItem.DisplayFormat = "Total:{0:n0}"

        ViewTrans.Columns("NOTA").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Supplier").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("Hutang").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Retur").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Sisa").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Pelunasan").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("BG").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Catatan").Width = GridTrans.Width * 0.2
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_PLH").Rows.Count < 1 Then
            Return False
        End If
        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        For Each drow As DataRow In xSet.Tables("TD_PLH").Rows
            If IsDBNull(drow.Item("Pelunasan")) Then
                msgboxWarning("Jumlah yang dibayarkan tidak boleh kosong atau kurang dari 0")
                Return False
            ElseIf drow.Item("Pelunasan") <= 0 Then
                msgboxWarning("Jumlah yang dibayarkan tidak boleh kosong atau kurang dari 0")
                Return False
            ElseIf IsDBNull(drow.Item("TanggalBG")) Then
                msgboxWarning("Tanggal BG tidak boleh kosong")
                Return False
            End If
        Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "PLH", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NOPLH, 3)) AS KODE FROM TM_PLH WHERE LEFT(NOPLH, 10) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        Dim tgl As Date

        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        tgl = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function
End Class