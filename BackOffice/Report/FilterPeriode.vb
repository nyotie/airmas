﻿Public Class FilterPeriode
    Public idReport As Integer
    Public startdate, enddate As DateTime

    Private Sub FilterPeriode_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        If idReport = 0 Then Close()
        Tanggal1.EditValue = Now
        Tanggal2.EditValue = Now

    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Tanggal1.EditValue = Nothing Then Return
        If Tanggal2.EditValue = Nothing Then Return

        startdate = Tanggal1.EditValue
        enddate = Tanggal2.EditValue

        Dim tool As DevExpress.XtraReports.UI.ReportPrintTool
        Select Case idReport
            Case 1
                Using thisReport As New LapPemb1
                    tool = New DevExpress.XtraReports.UI.ReportPrintTool(thisReport)
                    tool.ShowRibbonPreviewDialog()
                End Using
            Case 2
                Using thisReport As New LapPenj1
                    tool = New DevExpress.XtraReports.UI.ReportPrintTool(thisReport)
                    tool.ShowRibbonPreviewDialog()
                End Using
            Case 3
                Using thisReport As New LapPemb2
                    tool = New DevExpress.XtraReports.UI.ReportPrintTool(thisReport)
                    tool.ShowRibbonPreviewDialog()
                End Using
        End Select
    End Sub

    Private Sub ButCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButCancel.Click
        Close()
    End Sub
End Class