﻿Public Class LapPemb1

    Private Sub ThisReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        LabelPeriode.Text = String.Format("Periode : {0} sampai {1}", Format(param1.Value, "d MMM yyyy"), Format(param2.Value, "d MMM yyyy"))
    End Sub

    Private Sub ThisReport_ParametersRequestBeforeShow(sender As Object, e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs) Handles Me.ParametersRequestBeforeShow
        Dim Info As DevExpress.XtraReports.Parameters.ParameterInfo

        For Each Info In e.ParametersInformation
            Dim Tanggalan As New DevExpress.XtraEditors.DateEdit
            Tanggalan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
            Tanggalan.Properties.Mask.EditMask = "dd MMM yyyy"
            Tanggalan.Properties.Mask.UseMaskAsDisplayFormat = True
            Tanggalan.EditValue = Today.Date

            Info.Editor = Tanggalan
        Next Info

        param1.Value = Today.Date
        param2.Value = Today.Date
    End Sub

    Private Sub ThisReport_ParametersRequestSubmit(sender As Object, e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs) Handles Me.ParametersRequestSubmit
        View_Lap1.ClearBeforeFill = True
        View_Lap1.Connection.ConnectionString = conn_string_local
        View_Lap1.Fill(Dataset_Laporan1.dtLap1, Format(param1.Value, "yyyy-MM-dd"), Format(param2.Value, "yyyy-MM-dd"))
    End Sub
End Class