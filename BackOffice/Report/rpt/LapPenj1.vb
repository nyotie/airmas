﻿Imports DevExpress.XtraEditors
Imports DevExpress.Utils

Public Class LapPenj1

    Private Sub ThisReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        LabelParamInfo.Text = String.Format("Periode : {0} - {1} | Jns Trans : {2} | Wilayah : {3}",
                                            Format(param1.Value, "d MMM yyyy"), Format(param2.Value, "d MMM yyyy"),
                                            IIf(param3.Value = True, "Kredit", "Cash"),
                                            IIf(param4.Value = "", "-", param4.Value))
    End Sub

    Private Sub ThisReport_ParametersRequestBeforeShow(sender As Object, e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs) Handles Me.ParametersRequestBeforeShow
        Dim Info As DevExpress.XtraReports.Parameters.ParameterInfo

        For Each Info In e.ParametersInformation
            If Info.Parameter.Name = "param1" Or Info.Parameter.Name = "param2" Then
                Dim Tanggalan As New DevExpress.XtraEditors.DateEdit
                Tanggalan.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
                Tanggalan.Properties.Mask.EditMask = "dd MMM yyyy"
                Tanggalan.Properties.Mask.UseMaskAsDisplayFormat = True
                Tanggalan.EditValue = Today.Date

                Info.Editor = Tanggalan
            ElseIf Info.Parameter.Name = "param3" Then
                Dim JenisTransaksi As New DevExpress.XtraEditors.ComboBoxEdit
                JenisTransaksi.Properties.Items.Clear()
                JenisTransaksi.Properties.Items.Add("Kredit")
                JenisTransaksi.Properties.Items.Add("Cash")
                JenisTransaksi.SelectedIndex = 0

                Info.Editor = JenisTransaksi
            ElseIf Info.Parameter.Name = "param4" Then
                SQLquery = "SELECT IDWILAYAH ID, Nama, Grup FROM MWILAYAH WHERE INACTIVE=0;"
                ExDb.ExecQuery(SQLquery, xSet, "DaftarWilayahAktif")

                Dim DaftarWilayahAktif As New SearchLookUpEdit()
                DaftarWilayahAktif.Properties.DataSource = xSet.Tables("DaftarWilayahAktif").DefaultView
                DaftarWilayahAktif.Properties.NullText = ""
                DaftarWilayahAktif.Properties.ValueMember = "ID"
                DaftarWilayahAktif.Properties.DisplayMember = "Nama"
                DaftarWilayahAktif.Properties.ShowClearButton = True
                DaftarWilayahAktif.Properties.PopulateViewColumns()

                DaftarWilayahAktif.Properties.View.Columns("ID").Visible = False
                For Each coll As DataColumn In xSet.Tables("DaftarWilayahAktif").Columns
                    DaftarWilayahAktif.Properties.View.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
                Next
                Info.Editor = DaftarWilayahAktif
            End If
        Next Info

        param1.Value = Today.Date
        param2.Value = Today.Date
    End Sub

    Private Sub ThisReport_ParametersRequestSubmit(sender As Object, e As DevExpress.XtraReports.Parameters.ParametersRequestEventArgs) Handles Me.ParametersRequestSubmit
        If param3.Value = "" Then
            msgboxWarning("Parameter jenis transaksi tidak boleh kosong!")
            Return
        End If
        Dim paramKe4 As String
        If param4.Value = "" Then paramKe4 = "%%" Else paramKe4 = param4.Value

        View_Lap2.ClearBeforeFill = True
        View_Lap2.Connection.ConnectionString = conn_string_local
        View_Lap2.Fill(Dataset_Laporan1.dtLap2, Format(param1.Value, "yyyy-MM-dd"), Format(param2.Value, "yyyy-MM-dd"), IIf(param3.Value = "Kredit", 0, 1), paramKe4)
    End Sub
End Class