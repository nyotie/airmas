﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrowserReport
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DaftarReport = New DevExpress.XtraGrid.GridControl()
        Me.ViewDaftarReport = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButPreview = New DevExpress.XtraEditors.SimpleButton()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        CType(Me.DaftarReport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DaftarReport
        '
        Me.DaftarReport.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DaftarReport.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.DaftarReport.Location = New System.Drawing.Point(11, 11)
        Me.DaftarReport.MainView = Me.ViewDaftarReport
        Me.DaftarReport.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.DaftarReport.Name = "DaftarReport"
        Me.DaftarReport.Size = New System.Drawing.Size(446, 316)
        Me.DaftarReport.TabIndex = 14
        Me.DaftarReport.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewDaftarReport})
        '
        'ViewDaftarReport
        '
        Me.ViewDaftarReport.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 11.25!)
        Me.ViewDaftarReport.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewDaftarReport.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.ViewDaftarReport.Appearance.Row.Options.UseFont = True
        Me.ViewDaftarReport.GridControl = Me.DaftarReport
        Me.ViewDaftarReport.Name = "ViewDaftarReport"
        Me.ViewDaftarReport.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDaftarReport.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewDaftarReport.OptionsBehavior.Editable = False
        Me.ViewDaftarReport.OptionsCustomization.AllowColumnMoving = False
        Me.ViewDaftarReport.OptionsCustomization.AllowColumnResizing = False
        Me.ViewDaftarReport.OptionsCustomization.AllowGroup = False
        Me.ViewDaftarReport.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewDaftarReport.OptionsView.EnableAppearanceEvenRow = True
        Me.ViewDaftarReport.OptionsView.ShowAutoFilterRow = True
        Me.ViewDaftarReport.OptionsView.ShowGroupPanel = False
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Location = New System.Drawing.Point(78, 349)
        Me.ButBatal.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(63, 35)
        Me.ButBatal.TabIndex = 16
        Me.ButBatal.Text = "&Cancel"
        '
        'ButPreview
        '
        Me.ButPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButPreview.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButPreview.Appearance.Options.UseFont = True
        Me.ButPreview.Location = New System.Drawing.Point(11, 349)
        Me.ButPreview.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.ButPreview.Name = "ButPreview"
        Me.ButPreview.Size = New System.Drawing.Size(63, 35)
        Me.ButPreview.TabIndex = 15
        Me.ButPreview.Text = "&Preview"
        '
        'LineShape1
        '
        Me.LineShape1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 11
        Me.LineShape1.X2 = 455
        Me.LineShape1.Y1 = 337
        Me.LineShape1.Y2 = 337
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(466, 396)
        Me.ShapeContainer1.TabIndex = 18
        Me.ShapeContainer1.TabStop = False
        '
        'BrowserReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(466, 396)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButPreview)
        Me.Controls.Add(Me.DaftarReport)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "BrowserReport"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Daftar Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DaftarReport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DaftarReport As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewDaftarReport As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButPreview As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
End Class
