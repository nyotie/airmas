﻿Imports DevExpress.XtraReports.UI
Imports DevExpress.Utils

Public Class BrowserReport
    Dim T As Type

    Private Sub BrowserReport_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarReport")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub BrowserReport_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        DaftarReport.Focus()

        LoadSprite.CloseLoading()
    End Sub

    Private Sub BrowserReport_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        SQLquery = "SELECT IDREPORT ID, KATEGORI, NAMA, FILEREPORT FROM MREPORT WHERE INACTIVE=0 ORDER BY KATEGORI, NO_URUT;"
        'SQLquery = String.Format("SELECT a.idreport ID, namareport Nama, judulreport Report, ISNULL(b.AllowRead,0) AllowRead " & _
        '            "FROM m_report a LEFT JOIN PermissionsReport b ON a.idReport=b.idReport AND b.id_securitygroup={0} " & _
        '            "WHERE a.status=1 ", staff_group)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarReport")

        DaftarReport.DataSource = xSet.Tables("DaftarReport").DefaultView

        ViewDaftarReport.Columns("ID").Visible = False
        ViewDaftarReport.Columns("FILEREPORT").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarReport").Columns
            ViewDaftarReport.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Next
    End Sub

    Private Sub ButPreview_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButPreview.Click
        'If ViewDaftarReport.GetFocusedRowCellValue("AllowRead") = False Then
        '    msgboxWarning("Security Group anda tidak memiliki hak untuk mengakses report ini")
        'Else

        T = Type.GetType(Replace(Reflection.Assembly.GetExecutingAssembly.GetName.Name & "." & ViewDaftarReport.GetFocusedRowCellValue("FILEREPORT"), " ", "_"))
        Dim f = DirectCast(Activator.CreateInstance(T), XtraReport)
        Try
            Dim thisReport As ReportPrintTool = New ReportPrintTool(f)
            thisReport.ShowRibbonPreviewDialog()
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            f.Dispose()
        End Try

        'End If
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub
End Class