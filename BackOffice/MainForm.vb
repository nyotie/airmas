﻿Public Class MainForm

    Private Sub MainForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        Application.Exit()
    End Sub

    Private Sub MainForm_GotFocus(ByVal sender As Object, ByVal e As EventArgs) Handles Me.GotFocus
        If status_login = False Then
            Close()
        Else
            If Not LocalStatus Then Close()

            ConnStatus.Caption = IIf(LocalStatus = True, "Lokal", "Offline")
            ConnStatus.ImageIndex = IIf(LocalStatus = True, 0, 1)
        End If
    End Sub

    Private Sub MainForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        For Each ctl As Control In Controls
            Dim ctlMDI As MdiClient = TryCast(ctl, MdiClient)
            If ctlMDI Is Nothing Then
                Continue For
            End If
            ctlMDI.BackColor = BackColor
        Next
    End Sub

    Private Sub MainForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        loginForm.ShowDialog()
    End Sub

    Private Sub RibbonControl_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles RibbonControl.ItemClick
        If e.Item.Caption = "Logout" Then
            PageMaster.Visible = False
            PageTransaksi.Visible = False
            PageReport.Visible = False
            PageTools.Visible = False

            LoadSprite.CloseLoading()
            For i As Integer = MdiChildren.Length - 1 To 0 Step -1
                MdiChildren(i).Close()
            Next
            loginForm.ShowDialog()
        ElseIf e.Item.Caption = "Ganti Password" Then
            GantiPassword.ShowDialog()
            GantiPassword = Nothing

            '--------------Master------------------------------------------------------------------------------------------------------------------------
        ElseIf e.Item.Caption = "User" Then
            ShowLookUpModule(BrowserMasterUser, Me, "Master User")
        ElseIf e.Item.Caption = "Kategori Barang" Then
            ShowLookUpModule(BrowserMasterKategori, Me, "Master Kategori Barang")
        ElseIf e.Item.Caption = "Sub Kategori Barang" Then
            ShowLookUpModule(BrowserMasterSubKategori, Me, "Master Sub Kategori Barang")
        ElseIf e.Item.Caption = "Barang" Then
            ShowLookUpModule(BrowserMasterBarang, Me, "Master Barang")
        ElseIf e.Item.Caption = "Kendaraan" Then
            ShowLookUpModule(BrowserMasterKendaraan, Me, "Master Kendaraan")
        ElseIf e.Item.Caption = "Wilayah" Then
            ShowLookUpModule(BrowserMasterWilayah, Me, "Master Wilayah")
        ElseIf e.Item.Caption = "Supplier" Then
            ShowLookUpModule(BrowserMasterSupplier, Me, "Master Supplier")
        ElseIf e.Item.Caption = "Gudang" Then
            ShowLookUpModule(BrowserMasterGudang, Me, "Master Gudang")
        ElseIf e.Item.Caption = "Sales" Then
            ShowLookUpModule(BrowserMasterSales, Me, "Master Sales")
        ElseIf e.Item.Caption = "Customer" Then
            ShowLookUpModule(BrowserMasterCustomer, Me, "Master Customer")

            '--------------Pembelian---------------------------------------------------------------------------------------------------------------------
        ElseIf e.Item.Caption = "Purchase Order" Then
            ShowLookUpModule(BrowserPurchaseOrder, Me, "Purchase Order")
        ElseIf e.Item.Caption = "Penerimaan Barang" Then
            ShowLookUpModule(BrowserPenerimaanBarang, Me, "Penerimaan Barang")
        ElseIf e.Item.Caption = "Retur Beli" Then
            ShowLookUpModule(BrowserReturBarang, Me, "Retur Beli")

            '--------------Penjualan---------------------------------------------------------------------------------------------------------------------
        ElseIf e.Item.Caption = "Sales Order" Then
            ShowLookUpModule(BrowserSalesOrder, Me, "Sales Order")
        ElseIf e.Item.Caption = "Sales Cash" Then
            ShowLookUpModule(BrowserSalesCash1, Me, "Sales Cash")
        ElseIf e.Item.Caption = "Penjualan" Then
            ShowLookUpModule(BrowserPenjualan, Me, "Penjualan")
        ElseIf e.Item.Caption = "Loading" Then
            ShowLookUpModule(BrowserDeliveryOrder, Me, "Loading")

            '--------------Produksi----------------------------------------------------------------------------------------------------------------------
        ElseIf e.Item.Caption = "Permintaan Bahan" Then
            ShowLookUpModule(BrowserProOUT, Me, "Permintaan Bahan")
        ElseIf e.Item.Caption = "Hasil Produksi" Then
            ShowLookUpModule(BrowserProIN, Me, "Hasil Produksi")

            '--------------Gudang---------------------------------------------------------------------------------------------------------------------
        ElseIf e.Item.Caption = "Kartu Stok" Then
            ShowLookUpModule(KartuStok, Me, "Kartu Stok")

            '--------------Finance----------------------------------------------------------------------------------------------------------------------
        ElseIf e.Item.Caption = "Pelunasan Hutang" Then
            ShowLookUpModule(BrowserPelunasanHutang, Me, "Pelunasan Hutang")
        ElseIf e.Item.Caption = "Pelunasan Piutang" Then
            ShowLookUpModule(BrowserPelunasanPiutang, Me, "Pelunasan Piutang")

            '--------------Tools & Settings--------------------------------------------------------------------------------------------------------------
        ElseIf e.Item.Caption = "Profile Perusahaan" Then
            selmaster_id = 1
            ShowModule(CompanyProfile, "Profile Perusahaan")
            CompanyProfile = Nothing
        ElseIf e.Item.Caption = "Program Permissions" Then
            ShowLookUpModule(ProgramPermissions, Me, "Program Permissions")
        ElseIf e.Item.Caption = "Report Permissions" Then
            ShowLookUpModule(ReportPermissions, Me, "Report Permissions")
        ElseIf e.Item.Caption = "Koneksi" Then
            ShowModule(FrmSettingKoneksi, "Setting Koneksi Database")
            FrmSettingKoneksi = Nothing

            '--------------Report------------------------------------------------------------------------------------------------------------------------
        ElseIf e.Item.Caption = "Laporan" Then
            ShowLookUpModule(BrowserReport, Me, "Daftar Report")

            '--------------About------------------------------------------------------------------------------------------------------------------------
        ElseIf e.Item.Caption = "About" Then
            MsgBox("Versi applikasi :" & My.Application.Info.Version.ToString, MsgBoxStyle.OkOnly, "About")

        End If
    End Sub
End Class