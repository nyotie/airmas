﻿Public Class MasterKendaraan

    Private Sub MasterKategoriBarang_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("KoreksiMaster")
        xSet.Tables.Remove("GetMAXNO")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterKategoriBarang_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        KendaraanNama.Focus()
        If selmaster_id = "0" Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
            KendaraanTgl.EditValue = Today.Date
        End If
    End Sub

    Private Sub MasterKategoriBarang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        If Not selmaster_id = "0" Then
            SQLquery = String.Format("SELECT IDKENDARAAN, NAMA, TAHUN, INACTIVE FROM MKENDARAAN WHERE IDKENDARAAN='{0}'", selmaster_id)
            ExDb.ExecQuery(SQLquery, xSet, "KoreksiMaster")

            KendaraanID.Properties.ReadOnly = True
            KendaraanID.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("IDKENDARAAN").ToString
            KendaraanNama.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NAMA").ToString
            KendaraanTgl.EditValue = xSet.Tables("KoreksiMaster").Rows(0).Item("TAHUN")
            CekInaktif.Checked = xSet.Tables("KoreksiMaster").Rows(0).Item("INACTIVE")
            nameCheck = KendaraanNama.Text
        Else
            KendaraanID.Properties.ReadOnly = False
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO MKENDARAAN(IDKENDARAAN,NAMA,TAHUN,INACTIVE,CREATEDATE,IDUSER) "
                param_Query = String.Format("VALUES('{0}', '{1}', '{2}', 0, GETDATE(), {3}); ", KendaraanID.Text.Replace("'", "''"), KendaraanNama.Text.Replace("'", "''"), Format(KendaraanTgl.EditValue, "yyyy-MM-dd"), staff_id)
            Else
                sp_Query = String.Format("UPDATE MKENDARAAN SET NAMA='{0}', TAHUN='{1}', INACTIVE='{2}', IDUSER={3}, LASTUPDATE=GETDATE() ", KendaraanNama.Text.Replace("'", "''"), Format(KendaraanTgl.EditValue, "yyyy-MM-dd"), CekInaktif.Checked, staff_id)
                param_Query = String.Format("WHERE IDKENDARAAN='{0}'", selmaster_id)
            End If

            ExDb.ExecData(sp_Query & param_Query)
            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterKendaraan.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If KendaraanNama.Text.Length < 2 Or KendaraanID.Text.Length < 2 Then
            msgboxWarning("Kolom inputan tidak boleh kosong")
            Return False
        End If

        If KendaraanNama.Text.Contains("'") Then
            msgboxWarning("Huruf (') tidak diterima sebagai inputan")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If selmaster_id = "0" Then
            If Not isDoubleData(String.Format("SELECT IDKENDARAAN ID FROM MKENDARAAN WHERE IDKENDARAAN='{0}'", KendaraanID.Text)) Then
                msgboxWarning("NOPOL yang di inputkan sudah ada dalam daftar, silahkan pilih NOPOL lain")
                Return False
            End If
        End If
        If KendaraanNama.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT IDKENDARAAN ID FROM MKENDARAAN WHERE NAMA='{0}'", KendaraanNama.Text)) Then
                msgboxWarning("Nama yang di inputkan sudah ada dalam daftar, silahkan pilih nama yang lain")
                Return False
            End If
        End If

        Return True
    End Function
End Class