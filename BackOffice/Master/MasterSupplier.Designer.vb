﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterSupplier
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SaldoAwal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.AlamatNPWP = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.NamaNPWP = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.NoNPWP = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.CPSupp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.EmailSupp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.FaxSupp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TelpSupp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.PostalSupp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.KotaSupp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.AlamatSupp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.NamaSupp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.CekInaktif = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.SaldoAwal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AlamatNPWP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NamaNPWP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NoNPWP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CPSupp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmailSupp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FaxSupp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TelpSupp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PostalSupp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KotaSupp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AlamatSupp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NamaSupp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Location = New System.Drawing.Point(524, 272)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(69, 29)
        Me.ButBatal.TabIndex = 2
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Location = New System.Drawing.Point(449, 272)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(69, 29)
        Me.ButSimpan.TabIndex = 1
        Me.ButSimpan.Text = "&Simpan"
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.SaldoAwal)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.AlamatNPWP)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.NamaNPWP)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.NoNPWP)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.CPSupp)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.EmailSupp)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.FaxSupp)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.TelpSupp)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.PostalSupp)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.KotaSupp)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.AlamatSupp)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.NamaSupp)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.ShapeContainer1)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 37)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(581, 229)
        Me.PanelControl1.TabIndex = 10
        '
        'SaldoAwal
        '
        Me.SaldoAwal.EditValue = "0"
        Me.SaldoAwal.Location = New System.Drawing.Point(394, 93)
        Me.SaldoAwal.Name = "SaldoAwal"
        Me.SaldoAwal.Properties.Mask.EditMask = "n2"
        Me.SaldoAwal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.SaldoAwal.Size = New System.Drawing.Size(75, 20)
        Me.SaldoAwal.TabIndex = 12
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(318, 96)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl12.TabIndex = 2
        Me.LabelControl12.Text = "Saldo"
        '
        'AlamatNPWP
        '
        Me.AlamatNPWP.Location = New System.Drawing.Point(394, 67)
        Me.AlamatNPWP.Name = "AlamatNPWP"
        Me.AlamatNPWP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.AlamatNPWP.Properties.MaxLength = 150
        Me.AlamatNPWP.Size = New System.Drawing.Size(182, 20)
        Me.AlamatNPWP.TabIndex = 11
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(318, 70)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl11.TabIndex = 2
        Me.LabelControl11.Text = "Alamat NPWP"
        '
        'NamaNPWP
        '
        Me.NamaNPWP.Location = New System.Drawing.Point(394, 41)
        Me.NamaNPWP.Name = "NamaNPWP"
        Me.NamaNPWP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.NamaNPWP.Properties.MaxLength = 150
        Me.NamaNPWP.Size = New System.Drawing.Size(182, 20)
        Me.NamaNPWP.TabIndex = 10
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(318, 44)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl10.TabIndex = 2
        Me.LabelControl10.Text = "Nama NPWP"
        '
        'NoNPWP
        '
        Me.NoNPWP.Location = New System.Drawing.Point(394, 15)
        Me.NoNPWP.Name = "NoNPWP"
        Me.NoNPWP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.NoNPWP.Properties.MaxLength = 50
        Me.NoNPWP.Size = New System.Drawing.Size(182, 20)
        Me.NoNPWP.TabIndex = 9
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(318, 18)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl9.TabIndex = 2
        Me.LabelControl9.Text = "NPWP"
        '
        'CPSupp
        '
        Me.CPSupp.Location = New System.Drawing.Point(92, 198)
        Me.CPSupp.Name = "CPSupp"
        Me.CPSupp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.CPSupp.Properties.MaxLength = 50
        Me.CPSupp.Size = New System.Drawing.Size(197, 20)
        Me.CPSupp.TabIndex = 8
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(16, 201)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl8.TabIndex = 2
        Me.LabelControl8.Text = "CP"
        '
        'EmailSupp
        '
        Me.EmailSupp.Location = New System.Drawing.Point(92, 172)
        Me.EmailSupp.Name = "EmailSupp"
        Me.EmailSupp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.EmailSupp.Properties.MaxLength = 50
        Me.EmailSupp.Size = New System.Drawing.Size(122, 20)
        Me.EmailSupp.TabIndex = 7
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(16, 175)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl7.TabIndex = 2
        Me.LabelControl7.Text = "Email"
        '
        'FaxSupp
        '
        Me.FaxSupp.Location = New System.Drawing.Point(92, 146)
        Me.FaxSupp.Name = "FaxSupp"
        Me.FaxSupp.Properties.Mask.EditMask = "[A-Z 0-9.,]+"
        Me.FaxSupp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.FaxSupp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.FaxSupp.Properties.MaxLength = 30
        Me.FaxSupp.Size = New System.Drawing.Size(122, 20)
        Me.FaxSupp.TabIndex = 6
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(16, 149)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(18, 13)
        Me.LabelControl6.TabIndex = 2
        Me.LabelControl6.Text = "Fax"
        '
        'TelpSupp
        '
        Me.TelpSupp.Location = New System.Drawing.Point(92, 120)
        Me.TelpSupp.Name = "TelpSupp"
        Me.TelpSupp.Properties.Mask.EditMask = "[A-Z 0-9.,]+"
        Me.TelpSupp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TelpSupp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TelpSupp.Properties.MaxLength = 30
        Me.TelpSupp.Size = New System.Drawing.Size(122, 20)
        Me.TelpSupp.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(16, 123)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(20, 13)
        Me.LabelControl5.TabIndex = 2
        Me.LabelControl5.Text = "Telp"
        '
        'PostalSupp
        '
        Me.PostalSupp.Location = New System.Drawing.Point(92, 94)
        Me.PostalSupp.Name = "PostalSupp"
        Me.PostalSupp.Properties.Mask.EditMask = "[0-9]+"
        Me.PostalSupp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.PostalSupp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.PostalSupp.Properties.MaxLength = 5
        Me.PostalSupp.Size = New System.Drawing.Size(122, 20)
        Me.PostalSupp.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(16, 97)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Kode Pos"
        '
        'KotaSupp
        '
        Me.KotaSupp.Location = New System.Drawing.Point(92, 68)
        Me.KotaSupp.Name = "KotaSupp"
        Me.KotaSupp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.KotaSupp.Properties.MaxLength = 30
        Me.KotaSupp.Size = New System.Drawing.Size(122, 20)
        Me.KotaSupp.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(16, 71)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Kota"
        '
        'AlamatSupp
        '
        Me.AlamatSupp.Location = New System.Drawing.Point(92, 42)
        Me.AlamatSupp.Name = "AlamatSupp"
        Me.AlamatSupp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.AlamatSupp.Properties.MaxLength = 150
        Me.AlamatSupp.Size = New System.Drawing.Size(197, 20)
        Me.AlamatSupp.TabIndex = 2
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(16, 45)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Alamat"
        '
        'NamaSupp
        '
        Me.NamaSupp.Location = New System.Drawing.Point(92, 16)
        Me.NamaSupp.Name = "NamaSupp"
        Me.NamaSupp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.NamaSupp.Properties.MaxLength = 150
        Me.NamaSupp.Size = New System.Drawing.Size(197, 20)
        Me.NamaSupp.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(16, 19)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Nama"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(2, 2)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(577, 225)
        Me.ShapeContainer1.TabIndex = 0
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 300
        Me.LineShape1.X2 = 300
        Me.LineShape1.Y1 = 19
        Me.LineShape1.Y2 = 211
        '
        'CekInaktif
        '
        Me.CekInaktif.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CekInaktif.Location = New System.Drawing.Point(516, 12)
        Me.CekInaktif.Name = "CekInaktif"
        Me.CekInaktif.Properties.Caption = "Tidak aktif"
        Me.CekInaktif.Size = New System.Drawing.Size(77, 19)
        Me.CekInaktif.TabIndex = 0
        '
        'MasterSupplier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(605, 313)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.CekInaktif)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MasterSupplier"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.SaldoAwal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AlamatNPWP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NamaNPWP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NoNPWP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CPSupp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmailSupp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FaxSupp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TelpSupp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PostalSupp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KotaSupp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AlamatSupp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NamaSupp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents NamaSupp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CekInaktif As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SaldoAwal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AlamatNPWP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NamaNPWP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents NoNPWP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CPSupp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents EmailSupp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents FaxSupp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TelpSupp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PostalSupp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents KotaSupp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AlamatSupp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
End Class
