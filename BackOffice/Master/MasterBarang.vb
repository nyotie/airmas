﻿
Public Class MasterBarang
    Dim selected_cell As String

    Private Sub MasterBarang_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("KoreksiMaster")
        xSet.Tables.Remove("DataMasterKategoriAktif")
        xSet.Tables.Remove("DataMasterSubKategoriAktif")
        xSet.Tables.Remove("DataMasterBarangAktif")
        xSet.Tables.Remove("GetMAXNO")
        xSet.Tables.Remove("DATA_DHARGA")
        xSet.Tables.Remove("DATA_DPRODUKSI")
        xSet.Tables.Remove("DaftarGudang")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterBarang_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        KategoriBrg.Focus()
        If selmaster_id = "0" Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub MasterBarang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_DataMaster()

        If Not selmaster_id = "0" Then
            SQLquery = String.Format("SELECT IDKATEGORI,IDSUBKATEGORI,NAMA,ALIAS,SATUAN1,SATUAN2,QTY_AWAL1,QTY_AWAL2,KONVERSI,MIN_QTY,HPP,IDGUDANG,INACTIVE FROM MBARANG WHERE IDBARANG='{0}'", selmaster_id)
            ExDb.ExecQuery(SQLquery, xSet, "KoreksiMaster")

            KategoriBrg.EditValue = xSet.Tables("KoreksiMaster").Rows(0).Item("IDKATEGORI")
            SubKategoriBrg.EditValue = xSet.Tables("KoreksiMaster").Rows(0).Item("IDSUBKATEGORI")
            NamaBrg.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NAMA").ToString
            AliasBrg.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("ALIAS").ToString
            Sat1.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("SATUAN1").ToString
            Sat2.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("SATUAN2").ToString
            Nama1.Text = "1 " & Sat1.Text & " ="
            Nama2.Text = Sat2.Text
            qtt_awal1.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("QTY_AWAL1")
            qtt_awal2.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("QTY_AWAL2")
            KonversiSat.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("KONVERSI")
            MinQtt.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("MIN_QTY")
            HPP.EditValue = xSet.Tables("KoreksiMaster").Rows(0).Item("HPP")
            DaftarGudang.EditValue = xSet.Tables("KoreksiMaster").Rows(0).Item("IDGUDANG")
            CekInaktif.Checked = xSet.Tables("KoreksiMaster").Rows(0).Item("Inactive")
            nameCheck = NamaBrg.Text
            SubKategoriBrg.Enabled = False
        Else
            SubKategoriBrg.Enabled = True
        End If
    End Sub

    Private Sub KategoriBrg_EditValueChanged(sender As Object, e As EventArgs) Handles KategoriBrg.EditValueChanged
        SetDataGrid(xSet.Tables("DataMasterKategoriAktif").Select("ID='" & KategoriBrg.EditValue & "'")(0).Item("DETAIL"))
    End Sub

    Private Sub DaftarBarang_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarBarang.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell = Repository.EditValue
    End Sub

    Private Sub GridView1_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        If xSet.Tables("DataMasterKategoriAktif").Select("ID='" & KategoriBrg.EditValue & "'")(0).Item("DETAIL") = 0 Then
            Try
                Dim drow As DataRow = xSet.Tables("DataMasterBarangAktif").Select(String.Format("ID='{0}'", selected_cell))(0)

                If e.Column.FieldName = "NAMA" Then
                    For row As Integer = 0 To GridView1.RowCount - 1
                        If GridView1.GetRowCellValue(row, "NAMA") = selected_cell And Not GridView1.FocusedRowHandle = row Then
                            GridView1.DeleteRow(GridView1.FocusedRowHandle)
                            GridView1.FocusedRowHandle = row
                            Return
                        End If
                    Next

                    GridView1.SetFocusedRowCellValue("KIMIA", drow.Item("ALIAS"))
                    GridView1.SetFocusedRowCellValue("QTY", 1)
                    GridView1.SetFocusedRowCellValue("SATUAN", drow.Item("SAT"))
                    GridView1.FocusedRowHandle = GridView1.RowCount
                End If
                If IsDBNull(GridView1.GetFocusedRowCellValue("NAMA")) = True Then Throw New Exception

                If e.Column.FieldName = "QTY" And GridView1.FocusedColumn.FieldName = "QTY" Then
                    If GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "QTY") < 0 Then
                        GridView1.SetRowCellValue(GridView1.FocusedRowHandle, "QTY", 0)
                    End If
                End If
            Catch ex As Exception
                If IsDBNull(GridView1.GetFocusedRowCellValue("NAMA")) = True Then
                    GridView1.DeleteRow(GridView1.FocusedRowHandle)
                    xSet.Tables("DATA_DPRODUKSI").AcceptChanges()
                End If
            End Try
        End If
    End Sub

    Private Sub GridControl1_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles GridControl1.KeyDown
        If e.KeyCode = Keys.Delete Then
            GridView1.DeleteRow(GridView1.FocusedRowHandle)
            If xSet.Tables("DataMasterKategoriAktif").Select("ID='" & KategoriBrg.EditValue & "'")(0).Item("DETAIL") = 0 Then
                xSet.Tables("DATA_DPRODUKSI").AcceptChanges()
            Else
                xSet.Tables("DATA_DHARGA").AcceptChanges()
            End If
        End If
    End Sub

    Private Sub Sat1_Validated(sender As Object, e As EventArgs) Handles Sat1.Validated
        Nama1.Text = "1 " & Sat1.Text & " ="
    End Sub

    Private Sub Sat2_Validated(sender As Object, e As EventArgs) Handles Sat2.Validated
        Nama2.Text = Sat2.Text
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()

            If selmaster_id = "0" Then
                SQLquery = String.Format("SELECT ISNULL(MAX(IDNUM),0) MAXNO FROM MBARANG WHERE IDSUBKATEGORI='{0}';", SubKategoriBrg.EditValue)
                ExDb.ExecQuery(SQLquery, xSet, "GetMAXNO")
                Dim MAXNO As Integer = 1 + xSet.Tables("GetMAXNO").Rows(0).Item(0)
                selmaster_id = String.Format("{0}-{1}", xSet.Tables("DataMasterSubKategoriAktif").Select("ID='" & SubKategoriBrg.EditValue & "'")(0).Item("Kode"), MAXNO.ToString.PadLeft(4, "0"))
                '                                   0       1         2           3      4     5    6       7       8           9       10      11      12        13  14                    15
                sp_Query = "INSERT INTO MBARANG(IDBARANG,IDNUM,IDKATEGORI,IDSUBKATEGORI,NAMA,ALIAS,SATUAN1,SATUAN2,QTY_AWAL1,QTY_AWAL2,KONVERSI,MIN_QTY,HARGABELI,HPP,IDGUDANG,CREATEDATE,IDUSER) "
                'param_Query = String.Format("VALUES('{0}' + RIGHT('0000' + CAST({1} AS VARCHAR), 4),{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}',0, GETDATE(),'{12}'); ",
                'xSet.Tables("DataMasterSubKategoriAktif").Select("ID='" & SubKategoriBrg.EditValue & "'")(0).Item("Kode"),
                param_Query = String.Format("VALUES('{0}',{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}', GETDATE(),'{15}'); ",
                                          selmaster_id, MAXNO, KategoriBrg.EditValue, SubKategoriBrg.EditValue, NamaBrg.Text.Replace("'", "''"),
                                          AliasBrg.Text.Replace("'", "''"), Sat1.Text.Replace("'", "''"), Sat2.Text.Replace("'", "''"), qtt_awal1.EditValue, qtt_awal2.EditValue,
                                          KonversiSat.EditValue, MinQtt.EditValue, HPP.EditValue, HPP.EditValue, DaftarGudang.EditValue, staff_id)
                ExDb.ExecData(sp_Query & param_Query)
            Else
                SQLquery = String.Format("UPDATE MBARANG SET IDKATEGORI='{0}',IDSUBKATEGORI='{1}',NAMA='{2}',ALIAS='{3}',SATUAN1='{4}',SATUAN2='{5}',QTY_AWAL1={6},QTY_AWAL2={7},KONVERSI={8},MIN_QTY={9}, " & _
                                         "HARGABELI='{10}',HPP='{11}',IDGUDANG='{12}',INACTIVE='{13}',LASTUPDATE=GETDATE(),IDUSER={14} WHERE IDBARANG='{15}'",
                                         KategoriBrg.EditValue, SubKategoriBrg.EditValue, NamaBrg.Text.Replace("'", "''"), AliasBrg.Text.Replace("'", "''"),
                                         Sat1.Text, Sat2.Text, qtt_awal1.EditValue, qtt_awal2.EditValue,
                                         KonversiSat.EditValue, MinQtt.EditValue, HPP.EditValue, HPP.EditValue, DaftarGudang.EditValue, CekInaktif.Checked, staff_id, selmaster_id)
                ExDb.ExecData(SQLquery)

                If xSet.Tables("DataMasterKategoriAktif").Select("ID='" & KategoriBrg.EditValue & "'")(0).Item("DETAIL") = 0 Then
                    'if brg produksi
                    SQLquery = String.Format("DELETE FROM DPRODUKSI WHERE IDBARANG='{0}';", selmaster_id)
                Else
                    'if brg jual
                    SQLquery = String.Format("DELETE FROM DHARGA WHERE IDBARANG='{0}';", selmaster_id)
                End If
            End If

            'if grid rowcount>0
            If GridView1.RowCount > 1 Then
                Dim NOURUT As Integer = 1
                If xSet.Tables("DataMasterKategoriAktif").Select("ID='" & KategoriBrg.EditValue & "'")(0).Item("DETAIL") = 0 Then
                    'if brg produksi
                    SQLquery += "INSERT INTO DPRODUKSI(IDBARANG,NO_URUT,IDBARANG1,QTY,CREATEDATE,IDUSER) VALUES"
                    For Each drow As DataRow In xSet.Tables("DATA_DPRODUKSI").Rows
                        SQLquery += String.Format("('{0}','{1}','{2}','{3}',GETDATE(),'{4}'), ",
                                                  selmaster_id, NOURUT, drow.Item("NAMA"), drow.Item("QTY"), staff_id)
                        NOURUT += 1
                    Next
                Else
                    'if brg jual
                    SQLquery += "INSERT INTO DHARGA(IDBARANG,NO_URUT,HARGA1,HARGA2,TGL_MULAI,CREATEDATE,IDUSER) VALUES"
                    For Each drow As DataRow In xSet.Tables("DATA_DHARGA").Rows
                        SQLquery += String.Format("('{0}','{1}','{2}','{3}','{4}',GETDATE(),'{5}'), ",
                                                  selmaster_id, NOURUT, drow.Item("HARGA1"), drow.Item("HARGA2"), Format(drow.Item("TGL_MULAI"), "yyyy-MM-dd"), staff_id)
                        NOURUT += 1
                    Next
                End If
                SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & ";"
                ExDb.ExecData(SQLquery)
            End If


            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterBarang.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            If Not xCon Is Nothing Then xCon.Close()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_DataMaster()
        '-----kategori
        SQLquery = "SELECT IDKATEGORI ID, NAMA Kategori, DETAIL, CASE DETAIL WHEN 0 THEN 'Barang Produksi' ELSE 'Barang Jual' END AS Jenis FROM MKATEGORI WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DataMasterKategoriAktif")
        KategoriBrg.Properties.DataSource = xSet.Tables("DataMasterKategoriAktif").DefaultView

        KategoriBrg.Properties.NullText = ""
        KategoriBrg.Properties.ValueMember = "ID"
        KategoriBrg.Properties.DisplayMember = "Kategori"
        KategoriBrg.Properties.ShowClearButton = False
        KategoriBrg.Properties.PopulateViewColumns()

        ViewKategoriBrg.Columns("DETAIL").Visible = False

        For Each coll As DataColumn In xSet.Tables("DataMasterKategoriAktif").Columns
            ViewKategoriBrg.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Next

        '-----sub kategori
        SQLquery = "SELECT IDSUBKATEGORI ID, NAMA SubKategori, KODEPREFIX Kode FROM MSUBKATEGORI WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DataMasterSubKategoriAktif")
        SubKategoriBrg.Properties.DataSource = xSet.Tables("DataMasterSubKategoriAktif").DefaultView

        SubKategoriBrg.Properties.NullText = ""
        SubKategoriBrg.Properties.ValueMember = "ID"
        SubKategoriBrg.Properties.DisplayMember = "SubKategori"
        SubKategoriBrg.Properties.ShowClearButton = False
        SubKategoriBrg.Properties.PopulateViewColumns()

        For Each coll As DataColumn In xSet.Tables("DataMasterSubKategoriAktif").Columns
            ViewSubKategoriBrg.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Next

        '-----Daftar Barang
        SQLquery = "SELECT MB.IDBARANG ID, MSK.NAMA SUBKATEGORI, MB.NAMA, MB.ALIAS, MB.SATUAN2 SAT FROM MBARANG MB INNER JOIN MKATEGORI MK ON MB.IDKATEGORI=MK.IDKATEGORI INNER JOIN MSUBKATEGORI MSK ON MB.IDSUBKATEGORI=MSK.IDSUBKATEGORI WHERE MK.DETAIL=0 AND MB.INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DataMasterBarangAktif")
        DaftarBarang.DataSource = xSet.Tables("DataMasterBarangAktif").DefaultView

        DaftarBarang.NullText = ""
        DaftarBarang.ValueMember = "ID"
        DaftarBarang.DisplayMember = "NAMA"
        DaftarBarang.ShowClearButton = False
        DaftarBarang.PopulateViewColumns()

        '-----Daftar Gudang
        If Not xSet.Tables("DaftarGudang") Is Nothing Then xSet.Tables("DaftarGudang").Clear()
        SQLquery = "SELECT IDGUDANG ID, NAMA FROM MGUDANG WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarGudang")
        DaftarGudang.Properties.DataSource = xSet.Tables("DaftarGudang").DefaultView

        DaftarGudang.Properties.NullText = ""
        DaftarGudang.Properties.ValueMember = "ID"
        DaftarGudang.Properties.DisplayMember = "NAMA"
        DaftarGudang.Properties.ShowClearButton = False
        DaftarGudang.Properties.PopulateViewColumns()

        ViewDaftarGudang.Columns("ID").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarGudang").Columns
            ViewDaftarGudang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Next
    End Sub

    Private Sub SetDataGrid(ByVal Jenis As Integer)
        If GridControl1.DataSource IsNot Nothing Then GridControl1.DataSource = Nothing
        GridView1.PopulateColumns()
        GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom

        If Jenis = 1 Then
            '1=jual
            If Not xSet.Tables("DATA_DHARGA") Is Nothing Then xSet.Tables("DATA_DHARGA").Clear()
            SQLquery = String.Format("SELECT HARGA1, HARGA2, TGL_MULAI FROM DHARGA WHERE INACTIVE=0 AND IDBARANG='{0}'", selmaster_id)
            ExDb.ExecQuery(SQLquery, xSet, "DATA_DHARGA")
            GridControl1.DataSource = xSet.Tables("DATA_DHARGA").DefaultView

            GridView1.Columns("HARGA1").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            GridView1.Columns("HARGA1").DisplayFormat.FormatString = "Rp{0:n0}"
            GridView1.Columns("HARGA2").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            GridView1.Columns("HARGA2").DisplayFormat.FormatString = "Rp{0:n0}"
            GridView1.Columns("TGL_MULAI").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            GridView1.Columns("TGL_MULAI").DisplayFormat.FormatString = "dd/MMM/yyyy"
        Else
            '0=produksi
            If Not xSet.Tables("DATA_DPRODUKSI") Is Nothing Then xSet.Tables("DATA_DPRODUKSI").Clear()
            SQLquery = String.Format("SELECT DP.IDBARANG1 NAMA, MB.ALIAS KIMIA, QTY, MB.SATUAN2 SATUAN FROM DPRODUKSI DP INNER JOIN MBARANG MB ON MB.IDBARANG=DP.IDBARANG1 WHERE DP.INACTIVE=0 AND DP.IDBARANG='{0}' ORDER BY NO_URUT", selmaster_id)
            ExDb.ExecQuery(SQLquery, xSet, "DATA_DPRODUKSI")
            GridControl1.DataSource = xSet.Tables("DATA_DPRODUKSI").DefaultView

            GridView1.Columns("NAMA").ColumnEdit = DaftarBarang

            GridView1.Columns("KIMIA").OptionsColumn.AllowEdit = False
            GridView1.Columns("SATUAN").OptionsColumn.AllowEdit = False
        End If
    End Sub

    Private Function beforeSave() As Boolean
        If KategoriBrg.EditValue = Nothing Then
            msgboxWarning("Kategori belum di pilih")
            KategoriBrg.Focus()
            Return False
        End If
        If SubKategoriBrg.EditValue = Nothing Then
            msgboxWarning("Sub Kategori belum di pilih")
            SubKategoriBrg.Focus()
            Return False
        End If
        If DaftarGudang.EditValue Is Nothing Or DaftarGudang.Text = "" Then
            msgboxWarning("Gudang belum di pilih")
            DaftarGudang.Focus()
            Return False
        End If

        If NamaBrg.Text.Length < 2 Then
            msgboxWarning("Nama barang belum di isi")
            Return False
        End If

        If qtt_awal1.EditValue < 0 Or qtt_awal1.Text = "" Then qtt_awal1.EditValue = 0
        If qtt_awal2.EditValue < 0 Or qtt_awal2.Text = "" Then qtt_awal2.EditValue = 0
        If KonversiSat.EditValue < 0 Or KonversiSat.Text = "" Then KonversiSat.EditValue = 1
        If MinQtt.EditValue < 0 Or MinQtt.Text = "" Then MinQtt.EditValue = 0
        If HPP.EditValue < 0 Or HPP.Text = "" Then HPP.EditValue = 0

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If NamaBrg.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT IDBARANG ID FROM MBARANG WHERE NAMA='{0}'", NamaBrg.Text.Replace("'", "''"))) Then
                msgboxWarning("Nama yang di inputkan sudah ada dalam daftar, silahkan pilih nama yang lain")
                Return False
            End If
        End If

        Return True
    End Function
End Class