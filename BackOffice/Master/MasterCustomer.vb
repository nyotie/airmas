﻿Public Class MasterCustomer

    Private Sub ThisForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("KoreksiMaster")
        xSet.Tables.Remove("DataMasterWilayahAktif")
        xSet.Tables.Remove("GetMAXNO")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        Nama.Focus()
        If selmaster_id = "0" Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub ThisForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Get_DataMaster()

        If Not selmaster_id = "0" Then
            SQLquery = String.Format("SELECT IDWILAYAH,NAMA,ALAMAT,KOTA,KODEPOS,TELP,FAX,EMAIL,CP,NPWP,NAMA_NPWP,ALAMAT_NPWP,SALDO, INACTIVE FROM MCUSTOMER WHERE IDCUSTOMER='{0}'", selmaster_id)
            ExDb.ExecQuery(SQLquery, xSet, "KoreksiMaster")

            DaftarWilayah.EditValue = xSet.Tables("KoreksiMaster").Rows(0).Item("IDWILAYAH")
            Nama.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NAMA").ToString
            Alamat.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("ALAMAT").ToString
            Kota.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("KOTA").ToString
            Postal.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("KODEPOS").ToString
            Telp.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("TELP").ToString
            Fax.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("FAX").ToString
            Email.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("EMAIL").ToString
            CPerson.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("CP").ToString
            NoNPWP.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NPWP").ToString
            NamaNPWP.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NAMA_NPWP").ToString
            AlamatNPWP.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("ALAMAT_NPWP").ToString
            SaldoAwal.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("SALDO")
            CekInaktif.Checked = xSet.Tables("KoreksiMaster").Rows(0).Item("INACTIVE")
            nameCheck = Nama.Text
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                SQLquery = "SELECT ISNULL(MAX(IDNUM),0) MAXNO FROM MCUSTOMER;"
                ExDb.ExecQuery(SQLquery, xSet, "GetMAXNO")
                Dim MAXNO As Integer = 1 + xSet.Tables("GetMAXNO").Rows(0).Item(0)
                selmaster_id = Nama.Text.Trim.Substring(0, 1) & "-" & MAXNO.ToString.PadLeft(4, "0")
                '                                       0       1   2    3      4    5       6    7   8    9  10   11          12          13              14
                sp_Query = "INSERT INTO MCUSTOMER(IDCUSTOMER,IDNUM,NAMA,ALAMAT,KOTA,KODEPOS,TELP,FAX,EMAIL,CP,NPWP,NAMA_NPWP,ALAMAT_NPWP,SALDO,CREATEDATE,IDUSER,IDWILAYAH) "
                param_Query = String.Format("VALUES('{0}',{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}',{13},GETDATE(),{14},'{15}'); ",
                                            selmaster_id, MAXNO, Nama.Text.Replace("'", "''"), Alamat.Text.Replace("'", "''"), Kota.Text.Replace("'", "''"), Postal.Text, Telp.Text, Fax.Text, Email.Text.Replace("'", "''"), CPerson.Text.Replace("'", "''"), NoNPWP.Text.Replace("'", "''"), NamaNPWP.Text.Replace("'", "''"), AlamatNPWP.Text.Replace("'", "''"), SaldoAwal.EditValue, staff_id, DaftarWilayah.EditValue)
            Else
                sp_Query = String.Format("UPDATE MCUSTOMER SET NAMA='{0}',ALAMAT='{1}',KOTA='{2}',KODEPOS='{3}',TELP='{4}',FAX='{5}',EMAIL='{6}',CP='{7}',NPWP='{8}',NAMA_NPWP='{9}',ALAMAT_NPWP='{10}',SALDO={11},INACTIVE='{12}',IDUSER={13},LASTUPDATE=GETDATE(),IDWILAYAH='{14}' ",
                                         Nama.Text.Replace("'", "''"), Alamat.Text.Replace("'", "''"), Kota.Text.Replace("'", "''"), Postal.Text, Telp.Text, Fax.Text, Email.Text.Replace("'", "''"), CPerson.Text.Replace("'", "''"), NoNPWP.Text.Replace("'", "''"), NamaNPWP.Text.Replace("'", "''"), AlamatNPWP.Text.Replace("'", "''"), SaldoAwal.EditValue, CekInaktif.Checked, staff_id, DaftarWilayah.EditValue)
                param_Query = String.Format("WHERE IDCUSTOMER='{0}';", selmaster_id)
            End If

            ExDb.ExecData(sp_Query & param_Query)
            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterCustomer.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If DaftarWilayah.EditValue = Nothing Then
            msgboxWarning("Wilayah tidak boleh kosong")
            DaftarWilayah.Focus()
            Return False
        End If

        If Nama.Text.Length < 1 Then
            msgboxWarning("Nama tidak boleh kosong")
            Return False
        End If

        If Nama.Text.Contains("'") Then
            msgboxWarning("Huruf (') tidak diterima sebagai inputan")
            Return False
        End If

        If SaldoAwal.EditValue < 0 Then
            SaldoAwal.EditValue = 0
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If Nama.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT IDCUSTOMER ID FROM MCUSTOMER WHERE NAMA='{0}' AND IDWILAYAH='{1}' AND ALAMAT ='{2}'", Nama.Text, DaftarWilayah.EditValue, Alamat.Text)) Then
                msgboxWarning("Nama yang di inputkan sudah ada dalam daftar, silahkan pilih nama yang lain")
                Return False
            End If
        End If

        Return True
    End Function

    Private Sub Get_DataMaster()
        '-----Wilayah
        SQLquery = "SELECT IDWILAYAH ID, NAMA, GRUP FROM MWILAYAH WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DataMasterWilayahAktif")
        DaftarWilayah.Properties.DataSource = xSet.Tables("DataMasterWilayahAktif").DefaultView

        DaftarWilayah.Properties.NullText = ""
        DaftarWilayah.Properties.ValueMember = "ID"
        DaftarWilayah.Properties.DisplayMember = "NAMA"
        DaftarWilayah.Properties.ShowClearButton = False
        DaftarWilayah.Properties.PopulateViewColumns()

        For Each coll As DataColumn In xSet.Tables("DataMasterWilayahAktif").Columns
            ViewDaftarWilayah.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Next
    End Sub
End Class