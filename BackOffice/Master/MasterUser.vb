﻿Public Class MasterUser

    Private Sub MasterUser_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("KoreksiMaster")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterUser_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        NamaUser.Focus()
        If selmaster_id = 0 Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub MasterUser_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()

        If Not selmaster_id = 0 Then
            SQLquery = String.Format("SELECT NAMA, USERNAME, PWD, INACTIVE FROM MUSER WHERE IDUSER={0}", selmaster_id)
            ExDb.ExecQuery(SQLquery, xSet, "KoreksiMaster")

            NamaUser.EditValue = xSet.Tables("KoreksiMaster").Rows(0).Item("NAMA")
            DataUsername.EditValue = xSet.Tables("KoreksiMaster").Rows(0).Item("USERNAME")
            Try
                DataPassword.EditValue = Encryptor.DecryptData(xSet.Tables("KoreksiMaster").Rows(0).Item("PWD"))
            Catch ex As Exception
                msgboxWarning("Data Password gagal di dekripsi!")
                Close()
            End Try
            VerifyPassword.EditValue = DataPassword.EditValue
            CekInaktif.Checked = xSet.Tables("KoreksiMaster").Rows(0).Item("INACTIVE")
            nameCheck = DataUsername.EditValue
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO MUSER(NAMA, USERNAME, PWD, CREATEDATE) "
                param_Query = String.Format("VALUES('{0}','{1}','{2}',GETDATE()) ",
                                            NamaUser.Text.Replace("'", "''"), DataUsername.Text.Replace("'", "''"), Encryptor.EncryptData(DataPassword.EditValue.ToString.Replace("'", "''")))
            Else
                sp_Query = String.Format("UPDATE MUSER SET NAMA='{0}', USERNAME='{1}', PWD='{2}', INACTIVE='{3}', LASTUPDATE=GETDATE() ",
                                          NamaUser.Text.Replace("'", "''"), DataUsername.Text.Replace("'", "''"), Encryptor.EncryptData(DataPassword.EditValue.ToString.Replace("'", "''")), CekInaktif.Checked)
                param_Query = String.Format("WHERE IDUSER={0}", selmaster_id)
            End If

            ExDb.ExecData(sp_Query & param_Query)
            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterUser.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If NamaUser.Text.Length < 1 Or DataUsername.Text.Length < 1 Or DataPassword.Text.Length < 3 Or VerifyPassword.Text.Length < 3 Then
            msgboxWarning("Kolom inputan tidak boleh kosong, panjang Password minimal 3 digit!")
            Return False
        End If

        If DataPassword.EditValue <> VerifyPassword.EditValue Then
            msgboxWarning("Password tidak sama dengan verifikasi!")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If DataUsername.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT IDUSER ID FROM MUSER WHERE USERNAME='{0}'", DataUsername.Text)) Then
                msgboxWarning("USERNAME yang di inputkan sudah ada dalam daftar, silahkan pilih USERNAME yang lain")
                Return False
            End If
        End If

        Return True
    End Function
End Class