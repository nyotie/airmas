﻿Public Class MasterSupplier

    Private Sub MasterKategoriBarang_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("KoreksiMaster")
        xSet.Tables.Remove("GetMAXNO")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterKategoriBarang_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        NamaSupp.Focus()
        If selmaster_id = "0" Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub MasterKategoriBarang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        If Not selmaster_id = "0" Then
            SQLquery = String.Format("SELECT NAMA, ALAMAT,KOTA,KODEPOS,TELP,FAX,EMAIL,CP,NPWP,NAMA_NPWP,ALAMAT_NPWP,SALDO, INACTIVE FROM MSUPPLIER WHERE IDSUPPLIER='{0}'", selmaster_id)
            ExDb.ExecQuery(SQLquery, xSet, "KoreksiMaster")

            NamaSupp.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NAMA").ToString
            AlamatSupp.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("ALAMAT").ToString
            KotaSupp.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("KOTA").ToString
            PostalSupp.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("KODEPOS").ToString
            TelpSupp.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("TELP").ToString
            FaxSupp.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("FAX").ToString
            EmailSupp.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("EMAIL").ToString
            CPSupp.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("CP").ToString
            NoNPWP.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NPWP").ToString
            NamaNPWP.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NAMA_NPWP").ToString
            AlamatNPWP.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("ALAMAT_NPWP").ToString
            SaldoAwal.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("SALDO")
            CekInaktif.Checked = xSet.Tables("KoreksiMaster").Rows(0).Item("INACTIVE")
            nameCheck = NamaSupp.Text
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                SQLquery = "SELECT ISNULL(MAX(IDNUM),0) MAXNO FROM MSUPPLIER;"
                ExDb.ExecQuery(SQLquery, xSet, "GetMAXNO")
                Dim MAXNO As Integer = 1 + xSet.Tables("GetMAXNO").Rows(0).Item(0)
                selmaster_id = NamaSupp.Text.Trim.Substring(0, 1) & "-" & MAXNO.ToString.PadLeft(4, "0")
                '                                       0       1   2    3      4    5       6    7   8    9  10   11          12          13              14
                sp_Query = "INSERT INTO MSUPPLIER(IDSUPPLIER,IDNUM,NAMA,ALAMAT,KOTA,KODEPOS,TELP,FAX,EMAIL,CP,NPWP,NAMA_NPWP,ALAMAT_NPWP,SALDO,CREATEDATE,IDUSER) "
                param_Query = String.Format("VALUES('{0}',{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}',{13},GETDATE(),{14}); ",
                                            selmaster_id, MAXNO, NamaSupp.Text.Replace("'", "''"), AlamatSupp.Text.Replace("'", "''"), KotaSupp.Text.Replace("'", "''"), PostalSupp.Text, TelpSupp.Text, FaxSupp.Text, EmailSupp.Text.Replace("'", "''"), CPSupp.Text.Replace("'", "''"), NoNPWP.Text.Replace("'", "''"), NamaNPWP.Text.Replace("'", "''"), AlamatNPWP.Text.Replace("'", "''"), SaldoAwal.EditValue, staff_id)
            Else
                sp_Query = String.Format("UPDATE MSUPPLIER SET NAMA='{0}',ALAMAT='{1}',KOTA='{2}',KODEPOS='{3}',TELP='{4}',FAX='{5}',EMAIL='{6}',CP='{7}',NPWP='{8}',NAMA_NPWP='{9}',ALAMAT_NPWP='{10}',SALDO={11},INACTIVE='{12}',IDUSER={13},LASTUPDATE=GETDATE() ",
                                         NamaSupp.Text.Replace("'", "''"), AlamatSupp.Text.Replace("'", "''"), KotaSupp.Text.Replace("'", "''"), PostalSupp.Text, TelpSupp.Text, FaxSupp.Text, EmailSupp.Text.Replace("'", "''"), CPSupp.Text.Replace("'", "''"), NoNPWP.Text.Replace("'", "''"), NamaNPWP.Text.Replace("'", "''"), AlamatNPWP.Text.Replace("'", "''"), SaldoAwal.EditValue, CekInaktif.Checked, staff_id)
                param_Query = String.Format("WHERE IDSUPPLIER='{0}';", selmaster_id)
            End If

            ExDb.ExecData(sp_Query & param_Query)
            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterSupplier.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If NamaSupp.Text.Length < 1 Then
            msgboxWarning("Kolom inputan tidak boleh kosong")
            Return False
        End If

        If NamaSupp.Text.Contains("'") Then
            msgboxWarning("Huruf (') tidak diterima sebagai inputan")
            Return False
        End If

        If SaldoAwal.EditValue < 0 Then
            SaldoAwal.EditValue = 0
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If NamaSupp.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT IDSUPPLIER ID FROM MSUPPLIER WHERE NAMA='{0}'", NamaSupp.Text)) Then
                msgboxWarning("Nama yang di inputkan sudah ada dalam daftar, silahkan pilih nama yang lain")
                Return False
            End If
        End If

        Return True
    End Function
End Class