﻿Public Class MasterWilayah

    Private Sub MasterKategoriBarang_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("KoreksiMaster")
        xSet.Tables.Remove("GetMAXNO")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterKategoriBarang_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        WilKode.Focus()
        If selmaster_id = "0" Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub MasterKategoriBarang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        If Not selmaster_id = "0" Then
            SQLquery = String.Format("SELECT IDWILAYAH, NAMA, GRUP, INACTIVE FROM MWILAYAH WHERE IDWILAYAH='{0}'", selmaster_id)
            ExDb.ExecQuery(SQLquery, xSet, "KoreksiMaster")

            WilKode.Properties.ReadOnly = True
            WilKode.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("IDWILAYAH").ToString
            WilNama.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NAMA").ToString
            WilGrup.EditValue = xSet.Tables("KoreksiMaster").Rows(0).Item("GRUP")
            CekInaktif.Checked = xSet.Tables("KoreksiMaster").Rows(0).Item("INACTIVE")
            nameCheck = WilNama.Text
        Else
            WilKode.Properties.ReadOnly = False
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO MWILAYAH(IDWILAYAH,NAMA,GRUP,INACTIVE,CREATEDATE,IDUSER) "
                param_Query = String.Format("VALUES('{0}', '{1}', '{2}', 0, GETDATE(), {3}); ", WilKode.Text.Replace("'", "''"), WilNama.Text.Replace("'", "''"), WilGrup.EditValue.ToString.Replace("'", "''"), staff_id)
            Else
                sp_Query = String.Format("UPDATE MWILAYAH SET NAMA='{0}', GRUP='{1}', INACTIVE='{2}', IDUSER={3}, LASTUPDATE=GETDATE() ", WilNama.Text.Replace("'", "''"), WilGrup.EditValue.ToString.Replace("'", "''"), CekInaktif.Checked, staff_id)
                param_Query = String.Format("WHERE IDWILAYAH='{0}'", selmaster_id)
            End If

            ExDb.ExecData(sp_Query & param_Query)
            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterWilayah.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If WilKode.Text.Length < 1 Or WilGrup.Text.Length < 1 Or WilNama.Text.Length < 2 Then
            msgboxWarning("Kolom inputan tidak boleh kosong")
            Return False
        End If

        If WilNama.Text.Contains("'") Then
            msgboxWarning("Huruf (') tidak diterima sebagai inputan")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If selmaster_id = "0" Then
            If Not isDoubleData(String.Format("SELECT IDWILAYAH ID FROM MWILAYAH WHERE IDWILAYAH='{0}'", WilKode.Text)) Then
                msgboxWarning("Kode wilayah yang di inputkan sudah ada dalam daftar, silahkan pilih kode lain")
                Return False
            End If
        End If
        If WilNama.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT IDWILAYAH ID FROM MWILAYAH WHERE NAMA='{0}'", WilNama.Text)) Then
                msgboxWarning("Nama yang di inputkan sudah ada dalam daftar, silahkan pilih nama yang lain")
                Return False
            End If
        End If

        Return True
    End Function
End Class