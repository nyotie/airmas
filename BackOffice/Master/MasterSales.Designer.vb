﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterSales
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.Kode = New DevExpress.XtraEditors.TextEdit()
        Me.Nama = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.Telp = New DevExpress.XtraEditors.TextEdit()
        Me.Alamat = New DevExpress.XtraEditors.TextEdit()
        Me.TglLahir = New DevExpress.XtraEditors.DateEdit()
        Me.CekInaktif = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.Kode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Nama.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Telp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alamat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglLahir.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglLahir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Location = New System.Drawing.Point(225, 199)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(69, 29)
        Me.ButBatal.TabIndex = 3
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Location = New System.Drawing.Point(150, 199)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(69, 29)
        Me.ButSimpan.TabIndex = 2
        Me.ButSimpan.Text = "&Simpan"
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.Kode)
        Me.PanelControl1.Controls.Add(Me.Nama)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.Telp)
        Me.PanelControl1.Controls.Add(Me.Alamat)
        Me.PanelControl1.Controls.Add(Me.TglLahir)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 37)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(282, 156)
        Me.PanelControl1.TabIndex = 0
        '
        'Kode
        '
        Me.Kode.Location = New System.Drawing.Point(93, 14)
        Me.Kode.Name = "Kode"
        Me.Kode.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.Kode.Properties.MaxLength = 10
        Me.Kode.Size = New System.Drawing.Size(174, 20)
        Me.Kode.TabIndex = 0
        '
        'Nama
        '
        Me.Nama.Location = New System.Drawing.Point(93, 40)
        Me.Nama.Name = "Nama"
        Me.Nama.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.Nama.Properties.MaxLength = 50
        Me.Nama.Size = New System.Drawing.Size(174, 20)
        Me.Nama.TabIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(17, 17)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Kode"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(17, 121)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl5.TabIndex = 2
        Me.LabelControl5.Text = "Tgl Lahir"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(17, 95)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(20, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Telp"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(17, 69)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Alamat"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(17, 43)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Nama"
        '
        'Telp
        '
        Me.Telp.Location = New System.Drawing.Point(93, 92)
        Me.Telp.Name = "Telp"
        Me.Telp.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.Telp.Properties.MaxLength = 30
        Me.Telp.Size = New System.Drawing.Size(174, 20)
        Me.Telp.TabIndex = 3
        '
        'Alamat
        '
        Me.Alamat.Location = New System.Drawing.Point(93, 66)
        Me.Alamat.Name = "Alamat"
        Me.Alamat.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.Alamat.Properties.MaxLength = 50
        Me.Alamat.Size = New System.Drawing.Size(174, 20)
        Me.Alamat.TabIndex = 2
        '
        'TglLahir
        '
        Me.TglLahir.EditValue = Nothing
        Me.TglLahir.Location = New System.Drawing.Point(93, 118)
        Me.TglLahir.Name = "TglLahir"
        Me.TglLahir.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TglLahir.Properties.Mask.EditMask = "dd MMM yyyy"
        Me.TglLahir.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TglLahir.Properties.MaxLength = 50
        Me.TglLahir.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TglLahir.Size = New System.Drawing.Size(174, 20)
        Me.TglLahir.TabIndex = 4
        '
        'CekInaktif
        '
        Me.CekInaktif.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CekInaktif.Location = New System.Drawing.Point(217, 12)
        Me.CekInaktif.Name = "CekInaktif"
        Me.CekInaktif.Properties.Caption = "Tidak aktif"
        Me.CekInaktif.Size = New System.Drawing.Size(77, 19)
        Me.CekInaktif.TabIndex = 1
        '
        'MasterSales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(306, 240)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.CekInaktif)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MasterSales"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.Kode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Nama.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Telp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alamat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglLahir.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglLahir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Nama As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CekInaktif As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Kode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Alamat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Telp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TglLahir As DevExpress.XtraEditors.DateEdit
End Class
