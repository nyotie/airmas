﻿Imports DevExpress.Utils

Public Class BrowserMasterSales
    Public isChange As Boolean

    Private Sub BrowserMaster_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarBrowserMasterSales")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub BrowserMaster_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        butBaru.Focus()
    End Sub

    Private Sub BrowserMaster_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        isChange = True
        refreshingGrid()

        AddHandler GridView1.DoubleClick, AddressOf butKoreksi_Click
    End Sub

    Private Sub butBaru_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBaru.Click
        selmaster_id = "0"
        openTheButton()
        refreshingGrid()
    End Sub

    Private Sub butKoreksi_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butKoreksi.Click
        If Not GridView1.RowCount > 0 Then Return

        selmaster_id = GridView1.GetFocusedRowCellValue("ID")
        If selmaster_id = Nothing Then Exit Sub
        If selmaster_id = "0" Then Return
        openTheButton()
        refreshingGrid()
    End Sub

    Private Sub butVoid_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butVoid.Click
        If Not GridView1.RowCount > 0 Then Return

        selmaster_id = GridView1.GetFocusedRowCellValue("ID")
        If selmaster_id = Nothing Then Exit Sub
        Dim NextStat As Integer = IIf(GridView1.GetFocusedRowCellValue("Status"), 0, 1)

        If selmaster_id = "0" Then Return
        SQLquery = String.Format("UPDATE MSALES SET INACTIVE={1}, IDUSER={2}, LASTUPDATE=GETDATE() WHERE IDSALES='{0}';", selmaster_id, NextStat, staff_id)
        ExDb.ExecData(SQLquery)

        msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
        isChange = True
        refreshingGrid()
    End Sub

    Private Sub butRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butRefresh.Click
        isChange = True
        refreshingGrid()
    End Sub

    Private Sub ButExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButExport.Click
        PopupMenu1.ShowPopup(Control.MousePosition)
    End Sub

    Private Sub ExportPDF_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles ExportPDF.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "PDF Files|*.pdf", .Title = "Save a PDF File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToPdf(saveFileDialog1.FileName)
                msgboxInformation("Export file success")
            End If
        End Using
    End Sub

    Private Sub ExportXLS_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles ExportXLS.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "XLS Files|*.xls", .Title = "Save a XLS File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToXls(saveFileDialog1.FileName)
                msgboxInformation("Export file success")
            End If
        End Using
    End Sub

    Private Sub ExportXLSX_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles ExportXLSX.ItemClick
        Using saveFileDialog1 As New SaveFileDialog() With {.Filter = "XLSX Files|*.xlsx", .Title = "Save a XLSX File"}
            saveFileDialog1.ShowDialog()
            If saveFileDialog1.FileName <> "" Then
                GridView1.OptionsPrint.ExpandAllDetails = True
                GridView1.ExportToXlsx(saveFileDialog1.FileName)
                msgboxInformation("Export file success")
            End If
        End Using
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub reset_buttons()
        butBaru.Text = "Baru"
        butKoreksi.Text = "Koreksi"
        butVoid.Text = "Non/Aktifkan"

        butBaru.Enabled = True
        butKoreksi.Enabled = True
        butVoid.Enabled = True
    End Sub

    Private Sub refreshingGrid()
        reset_buttons()
        Dim idxRow As Integer = GridView1.FocusedRowHandle

        If isChange = True Then isChange = False Else Return

        If Not xSet.Tables("DaftarBrowserMasterSales") Is Nothing Then xSet.Tables("DaftarBrowserMasterSales").Clear()

        SQLquery = "SELECT IDSALES ID, NAMA, ALAMAT, TELP, INACTIVE Status FROM MSALES "
        If butCheckShow.Checked = False Then SQLquery += "WHERE INACTIVE=0 "
        SQLquery += "ORDER BY NAMA"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarBrowserMasterSales")

        GridControl1.DataSource = xSet.Tables("DaftarBrowserMasterSales").DefaultView
        Default_GridSettings()
        GridView1.FocusedRowHandle = idxRow
    End Sub

    Private Sub openTheButton()
        MasterSales.ShowDialog()
        MasterSales = Nothing
    End Sub

    Private Sub Default_GridSettings()
        For Each coll As DataColumn In xSet.Tables("DaftarBrowserMasterSales").Columns
            GridView1.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        GridView1.Columns("NAMA").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        GridView1.Columns("NAMA").SummaryItem.DisplayFormat = "{0:n0} item(s)"

        GridView1.BestFitColumns()
    End Sub

End Class