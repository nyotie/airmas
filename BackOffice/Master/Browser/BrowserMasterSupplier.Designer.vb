﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrowserMasterSupplier
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.butCheckShow = New DevExpress.XtraEditors.CheckEdit()
        Me.butBaru = New DevExpress.XtraEditors.SimpleButton()
        Me.butKoreksi = New DevExpress.XtraEditors.SimpleButton()
        Me.ButExport = New DevExpress.XtraEditors.SimpleButton()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.butVoid = New DevExpress.XtraEditors.SimpleButton()
        Me.butRefresh = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu(Me.components)
        Me.ExportPDF = New DevExpress.XtraBars.BarButtonItem()
        Me.ExportXLS = New DevExpress.XtraBars.BarButtonItem()
        Me.ExportXLSX = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.SettingHargaClient = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.butCheckShow.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.butCheckShow)
        Me.PanelControl1.Controls.Add(Me.butBaru)
        Me.PanelControl1.Controls.Add(Me.butKoreksi)
        Me.PanelControl1.Controls.Add(Me.ButExport)
        Me.PanelControl1.Controls.Add(Me.butClose)
        Me.PanelControl1.Controls.Add(Me.butVoid)
        Me.PanelControl1.Controls.Add(Me.butRefresh)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 347)
        Me.PanelControl1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(818, 53)
        Me.PanelControl1.TabIndex = 9
        '
        'butCheckShow
        '
        Me.butCheckShow.Location = New System.Drawing.Point(438, 17)
        Me.butCheckShow.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butCheckShow.Name = "butCheckShow"
        Me.butCheckShow.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCheckShow.Properties.Appearance.Options.UseFont = True
        Me.butCheckShow.Properties.Caption = "Show inactive records"
        Me.butCheckShow.Size = New System.Drawing.Size(170, 19)
        Me.butCheckShow.TabIndex = 5
        '
        'butBaru
        '
        Me.butBaru.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBaru.Appearance.Options.UseFont = True
        Me.butBaru.Location = New System.Drawing.Point(6, 6)
        Me.butBaru.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butBaru.Name = "butBaru"
        Me.butBaru.Size = New System.Drawing.Size(100, 41)
        Me.butBaru.TabIndex = 1
        Me.butBaru.Text = "Baru"
        '
        'butKoreksi
        '
        Me.butKoreksi.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butKoreksi.Appearance.Options.UseFont = True
        Me.butKoreksi.Location = New System.Drawing.Point(114, 5)
        Me.butKoreksi.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butKoreksi.Name = "butKoreksi"
        Me.butKoreksi.Size = New System.Drawing.Size(100, 41)
        Me.butKoreksi.TabIndex = 2
        Me.butKoreksi.Text = "Koreksi"
        '
        'ButExport
        '
        Me.ButExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButExport.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButExport.Appearance.Options.UseFont = True
        Me.ButExport.Location = New System.Drawing.Point(624, 6)
        Me.ButExport.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ButExport.Name = "ButExport"
        Me.BarManager1.SetPopupContextMenu(Me.ButExport, Me.PopupMenu1)
        Me.ButExport.Size = New System.Drawing.Size(90, 40)
        Me.ButExport.TabIndex = 6
        Me.ButExport.Text = "Export"
        '
        'butClose
        '
        Me.butClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butClose.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Appearance.Options.UseFont = True
        Me.butClose.Location = New System.Drawing.Point(722, 6)
        Me.butClose.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(90, 40)
        Me.butClose.TabIndex = 6
        Me.butClose.Text = "Tutup"
        '
        'butVoid
        '
        Me.butVoid.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butVoid.Appearance.Options.UseFont = True
        Me.butVoid.Location = New System.Drawing.Point(222, 6)
        Me.butVoid.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butVoid.Name = "butVoid"
        Me.butVoid.Size = New System.Drawing.Size(100, 41)
        Me.butVoid.TabIndex = 3
        Me.butVoid.Text = "Non/Aktifkan"
        '
        'butRefresh
        '
        Me.butRefresh.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butRefresh.Appearance.Options.UseFont = True
        Me.butRefresh.Location = New System.Drawing.Point(330, 5)
        Me.butRefresh.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.butRefresh.Name = "butRefresh"
        Me.butRefresh.Size = New System.Drawing.Size(100, 41)
        Me.butRefresh.TabIndex = 4
        Me.butRefresh.Text = "Refresh"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(12, 12)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(819, 329)
        Me.GridControl1.TabIndex = 10
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.HeaderPanel.Options.UseFont = True
        Me.GridView1.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.Row.Options.UseFont = True
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'BarManager1
        '
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.ExportPDF, Me.ExportXLS, Me.ExportXLSX, Me.SettingHargaClient})
        Me.BarManager1.MaxItemId = 4
        '
        'PopupMenu1
        '
        Me.PopupMenu1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.ExportPDF), New DevExpress.XtraBars.LinkPersistInfo(Me.ExportXLS), New DevExpress.XtraBars.LinkPersistInfo(Me.ExportXLSX)})
        Me.PopupMenu1.Manager = Me.BarManager1
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'ExportPDF
        '
        Me.ExportPDF.Caption = "Export to PDF"
        Me.ExportPDF.Id = 0
        Me.ExportPDF.Name = "ExportPDF"
        '
        'ExportXLS
        '
        Me.ExportXLS.Caption = "Export to XLS"
        Me.ExportXLS.Id = 1
        Me.ExportXLS.Name = "ExportXLS"
        '
        'ExportXLSX
        '
        Me.ExportXLSX.Caption = "Export to XLSX"
        Me.ExportXLSX.Id = 2
        Me.ExportXLSX.Name = "ExportXLSX"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(843, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 412)
        Me.barDockControlBottom.Size = New System.Drawing.Size(843, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 412)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(843, 0)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 412)
        '
        'SettingHargaClient
        '
        Me.SettingHargaClient.Caption = "Set Harga"
        Me.SettingHargaClient.Id = 3
        Me.SettingHargaClient.Name = "SettingHargaClient"
        '
        'BrowserMaster
        '
        Me.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(843, 412)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "BrowserMaster"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Master"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.butCheckShow.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butCheckShow As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents butBaru As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butKoreksi As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butVoid As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butRefresh As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButExport As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents ExportPDF As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ExportXLS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ExportXLSX As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents SettingHargaClient As DevExpress.XtraBars.BarButtonItem
End Class
