﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MasterBarang
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.KategoriBrg = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewKategoriBrg = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.NamaBrg = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.CekInaktif = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.DaftarGudang = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewDaftarGudang = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.HPP = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DaftarBarang = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.ViewDaftarBarang = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.SubKategoriBrg = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewSubKategoriBrg = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.MinQtt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.KonversiSat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.Sat2 = New DevExpress.XtraEditors.TextEdit()
        Me.qtt_awal2 = New DevExpress.XtraEditors.TextEdit()
        Me.qtt_awal1 = New DevExpress.XtraEditors.TextEdit()
        Me.Nama2 = New DevExpress.XtraEditors.LabelControl()
        Me.Nama1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.Sat1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.AliasBrg = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.KategoriBrg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewKategoriBrg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NamaBrg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.DaftarGudang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarGudang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HPP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarBarang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarBarang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SubKategoriBrg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewSubKategoriBrg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MinQtt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.KonversiSat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sat2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.qtt_awal2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.qtt_awal1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sat1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AliasBrg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'KategoriBrg
        '
        Me.KategoriBrg.Location = New System.Drawing.Point(99, 13)
        Me.KategoriBrg.Name = "KategoriBrg"
        Me.KategoriBrg.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.KategoriBrg.Properties.NullText = ""
        Me.KategoriBrg.Properties.View = Me.ViewKategoriBrg
        Me.KategoriBrg.Size = New System.Drawing.Size(174, 20)
        Me.KategoriBrg.TabIndex = 1
        '
        'ViewKategoriBrg
        '
        Me.ViewKategoriBrg.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewKategoriBrg.Name = "ViewKategoriBrg"
        Me.ViewKategoriBrg.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewKategoriBrg.OptionsView.ShowGroupPanel = False
        '
        'NamaBrg
        '
        Me.NamaBrg.Location = New System.Drawing.Point(99, 65)
        Me.NamaBrg.Name = "NamaBrg"
        Me.NamaBrg.Properties.MaxLength = 200
        Me.NamaBrg.Size = New System.Drawing.Size(174, 20)
        Me.NamaBrg.TabIndex = 3
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(15, 16)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Kategori"
        '
        'CekInaktif
        '
        Me.CekInaktif.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CekInaktif.Location = New System.Drawing.Point(610, 12)
        Me.CekInaktif.Name = "CekInaktif"
        Me.CekInaktif.Properties.Caption = "Tidak aktif"
        Me.CekInaktif.Size = New System.Drawing.Size(77, 19)
        Me.CekInaktif.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(15, 68)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Nama"
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.DaftarGudang)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.HPP)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.GridControl1)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.SubKategoriBrg)
        Me.PanelControl1.Controls.Add(Me.KategoriBrg)
        Me.PanelControl1.Controls.Add(Me.MinQtt)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.KonversiSat)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.Sat2)
        Me.PanelControl1.Controls.Add(Me.qtt_awal2)
        Me.PanelControl1.Controls.Add(Me.qtt_awal1)
        Me.PanelControl1.Controls.Add(Me.Nama2)
        Me.PanelControl1.Controls.Add(Me.Nama1)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.Sat1)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.AliasBrg)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.NamaBrg)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.ShapeContainer2)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 38)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(675, 277)
        Me.PanelControl1.TabIndex = 0
        '
        'DaftarGudang
        '
        Me.DaftarGudang.Location = New System.Drawing.Point(99, 247)
        Me.DaftarGudang.Name = "DaftarGudang"
        Me.DaftarGudang.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarGudang.Properties.NullText = ""
        Me.DaftarGudang.Properties.View = Me.ViewDaftarGudang
        Me.DaftarGudang.Size = New System.Drawing.Size(174, 20)
        Me.DaftarGudang.TabIndex = 12
        '
        'ViewDaftarGudang
        '
        Me.ViewDaftarGudang.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarGudang.Name = "ViewDaftarGudang"
        Me.ViewDaftarGudang.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarGudang.OptionsView.ShowGroupPanel = False
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(15, 250)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl12.TabIndex = 2
        Me.LabelControl12.Text = "Gudang"
        '
        'HPP
        '
        Me.HPP.EditValue = "0"
        Me.HPP.Location = New System.Drawing.Point(99, 221)
        Me.HPP.Name = "HPP"
        Me.HPP.Properties.Mask.EditMask = "n3"
        Me.HPP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.HPP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.HPP.Properties.MaxLength = 200
        Me.HPP.Properties.NullText = "0.000"
        Me.HPP.Properties.NullValuePrompt = "0.000"
        Me.HPP.Size = New System.Drawing.Size(115, 20)
        Me.HPP.TabIndex = 11
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(15, 224)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl11.TabIndex = 2
        Me.LabelControl11.Text = "HPP"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(302, 13)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.DaftarBarang})
        Me.GridControl1.Size = New System.Drawing.Size(368, 254)
        Me.GridControl1.TabIndex = 11
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsCustomization.AllowColumnMoving = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsCustomization.AllowQuickHideColumns = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'DaftarBarang
        '
        Me.DaftarBarang.AutoHeight = False
        Me.DaftarBarang.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarBarang.Name = "DaftarBarang"
        Me.DaftarBarang.PopupFormSize = New System.Drawing.Size(400, 300)
        Me.DaftarBarang.View = Me.ViewDaftarBarang
        '
        'ViewDaftarBarang
        '
        Me.ViewDaftarBarang.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarBarang.Name = "ViewDaftarBarang"
        Me.ViewDaftarBarang.OptionsCustomization.AllowColumnMoving = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowFilter = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowGroup = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowSort = False
        Me.ViewDaftarBarang.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarBarang.OptionsView.ShowGroupPanel = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(15, 42)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Sub Kategori"
        '
        'SubKategoriBrg
        '
        Me.SubKategoriBrg.Location = New System.Drawing.Point(99, 39)
        Me.SubKategoriBrg.Name = "SubKategoriBrg"
        Me.SubKategoriBrg.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.SubKategoriBrg.Properties.NullText = ""
        Me.SubKategoriBrg.Properties.View = Me.ViewSubKategoriBrg
        Me.SubKategoriBrg.Size = New System.Drawing.Size(174, 20)
        Me.SubKategoriBrg.TabIndex = 2
        '
        'ViewSubKategoriBrg
        '
        Me.ViewSubKategoriBrg.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewSubKategoriBrg.Name = "ViewSubKategoriBrg"
        Me.ViewSubKategoriBrg.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewSubKategoriBrg.OptionsView.ShowGroupPanel = False
        '
        'MinQtt
        '
        Me.MinQtt.EditValue = "0"
        Me.MinQtt.Location = New System.Drawing.Point(99, 195)
        Me.MinQtt.Name = "MinQtt"
        Me.MinQtt.Properties.Mask.EditMask = "n3"
        Me.MinQtt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.MinQtt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.MinQtt.Properties.MaxLength = 200
        Me.MinQtt.Properties.NullText = "0.000"
        Me.MinQtt.Properties.NullValuePrompt = "0.000"
        Me.MinQtt.Size = New System.Drawing.Size(115, 20)
        Me.MinQtt.TabIndex = 10
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(15, 198)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl10.TabIndex = 2
        Me.LabelControl10.Text = "Stok Minimum"
        '
        'KonversiSat
        '
        Me.KonversiSat.Location = New System.Drawing.Point(168, 169)
        Me.KonversiSat.Name = "KonversiSat"
        Me.KonversiSat.Properties.Mask.EditMask = "n0"
        Me.KonversiSat.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.KonversiSat.Properties.NullText = "0"
        Me.KonversiSat.Properties.NullValuePrompt = "0"
        Me.KonversiSat.Size = New System.Drawing.Size(46, 20)
        Me.KonversiSat.TabIndex = 9
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(15, 172)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl9.TabIndex = 2
        Me.LabelControl9.Text = "Konversi"
        '
        'Sat2
        '
        Me.Sat2.EditValue = "PCS"
        Me.Sat2.Location = New System.Drawing.Point(99, 143)
        Me.Sat2.Name = "Sat2"
        Me.Sat2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.Sat2.Properties.MaxLength = 10
        Me.Sat2.Size = New System.Drawing.Size(47, 20)
        Me.Sat2.TabIndex = 7
        '
        'qtt_awal2
        '
        Me.qtt_awal2.EditValue = "0"
        Me.qtt_awal2.Location = New System.Drawing.Point(204, 143)
        Me.qtt_awal2.Name = "qtt_awal2"
        Me.qtt_awal2.Properties.Mask.EditMask = "n3"
        Me.qtt_awal2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.qtt_awal2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.qtt_awal2.Properties.NullText = "0.000"
        Me.qtt_awal2.Properties.NullValuePrompt = "0.000"
        Me.qtt_awal2.Size = New System.Drawing.Size(69, 20)
        Me.qtt_awal2.TabIndex = 8
        '
        'qtt_awal1
        '
        Me.qtt_awal1.EditValue = "0"
        Me.qtt_awal1.Location = New System.Drawing.Point(204, 117)
        Me.qtt_awal1.Name = "qtt_awal1"
        Me.qtt_awal1.Properties.Mask.EditMask = "n3"
        Me.qtt_awal1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.qtt_awal1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.qtt_awal1.Properties.NullText = "0.000"
        Me.qtt_awal1.Properties.NullValuePrompt = "0.000"
        Me.qtt_awal1.Size = New System.Drawing.Size(69, 20)
        Me.qtt_awal1.TabIndex = 6
        '
        'Nama2
        '
        Me.Nama2.Location = New System.Drawing.Point(220, 172)
        Me.Nama2.Name = "Nama2"
        Me.Nama2.Size = New System.Drawing.Size(19, 13)
        Me.Nama2.TabIndex = 2
        Me.Nama2.Text = "PCS"
        '
        'Nama1
        '
        Me.Nama1.Location = New System.Drawing.Point(99, 172)
        Me.Nama1.Name = "Nama1"
        Me.Nama1.Size = New System.Drawing.Size(46, 13)
        Me.Nama1.TabIndex = 2
        Me.Nama1.Text = "1 PACK ="
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(15, 146)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl6.TabIndex = 2
        Me.LabelControl6.Text = "Satuan utama"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(152, 146)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl8.TabIndex = 2
        Me.LabelControl8.Text = "Stok awal"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(152, 120)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl7.TabIndex = 2
        Me.LabelControl7.Text = "Stok awal"
        '
        'Sat1
        '
        Me.Sat1.EditValue = "PACK"
        Me.Sat1.Location = New System.Drawing.Point(99, 117)
        Me.Sat1.Name = "Sat1"
        Me.Sat1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.Sat1.Properties.MaxLength = 10
        Me.Sat1.Size = New System.Drawing.Size(47, 20)
        Me.Sat1.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(15, 120)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl5.TabIndex = 2
        Me.LabelControl5.Text = "Satuan terbesar"
        '
        'AliasBrg
        '
        Me.AliasBrg.Location = New System.Drawing.Point(99, 91)
        Me.AliasBrg.Name = "AliasBrg"
        Me.AliasBrg.Properties.MaxLength = 200
        Me.AliasBrg.Size = New System.Drawing.Size(174, 20)
        Me.AliasBrg.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(15, 94)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Alias"
        '
        'ShapeContainer2
        '
        Me.ShapeContainer2.Location = New System.Drawing.Point(2, 2)
        Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer2.Name = "ShapeContainer2"
        Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer2.Size = New System.Drawing.Size(671, 273)
        Me.ShapeContainer2.TabIndex = 0
        Me.ShapeContainer2.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ButtonShadow
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 284
        Me.LineShape1.X2 = 284
        Me.LineShape1.Y1 = 15
        Me.LineShape1.Y2 = 260
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Location = New System.Drawing.Point(618, 321)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(69, 29)
        Me.ButBatal.TabIndex = 3
        Me.ButBatal.Text = "&Batal"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Location = New System.Drawing.Point(543, 321)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(69, 29)
        Me.ButSimpan.TabIndex = 2
        Me.ButSimpan.Text = "&Simpan"
        '
        'MasterBarang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(699, 362)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.CekInaktif)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MasterBarang"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Master Barang"
        CType(Me.KategoriBrg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewKategoriBrg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NamaBrg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CekInaktif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.DaftarGudang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarGudang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HPP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarBarang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarBarang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SubKategoriBrg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewSubKategoriBrg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MinQtt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.KonversiSat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sat2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.qtt_awal2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.qtt_awal1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sat1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AliasBrg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents KategoriBrg As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewKategoriBrg As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents NamaBrg As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CekInaktif As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SubKategoriBrg As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewSubKategoriBrg As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents MinQtt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents KonversiSat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Sat2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents qtt_awal2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents qtt_awal1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Nama2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Nama1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Sat1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents AliasBrg As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ShapeContainer2 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents DaftarBarang As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents ViewDaftarBarang As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DaftarGudang As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewDaftarGudang As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents HPP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
End Class
