﻿Public Class MasterSales

    Private Sub Master_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("KoreksiMaster")
        xSet.Tables.Remove("GetMAXNO")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub Master_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        Kode.Focus()
        If selmaster_id = "0" Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub Master_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        If Not selmaster_id = "0" Then
            SQLquery = String.Format("SELECT IDSALES, NAMA, ALAMAT, TELP, TGL_LAHIR, INACTIVE FROM MSALES WHERE IDSALES='{0}'", selmaster_id)
            ExDb.ExecQuery(SQLquery, xSet, "KoreksiMaster")

            Kode.Properties.ReadOnly = True
            Kode.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("IDSALES").ToString
            Nama.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NAMA").ToString
            Alamat.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("ALAMAT").ToString
            Telp.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("TELP").ToString
            TglLahir.EditValue = xSet.Tables("KoreksiMaster").Rows(0).Item("TGL_LAHIR")
            CekInaktif.Checked = xSet.Tables("KoreksiMaster").Rows(0).Item("INACTIVE")
            nameCheck = Nama.Text
        Else
            Kode.Properties.ReadOnly = False
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                sp_Query = "INSERT INTO MSALES(IDSALES,NAMA,ALAMAT,TELP,TGL_LAHIR,INACTIVE,CREATEDATE,IDUSER) "
                param_Query = String.Format("VALUES('{0}', '{1}', '{2}', '{3}', '{4}', 0, GETDATE(), {5}); ", Kode.Text.Replace("'", "''"), Nama.Text.Replace("'", "''"), Alamat.Text.Replace("'", "''"), Telp.Text.Replace("'", "''"), Format(TglLahir.EditValue, "yyyy-MM-dd"), staff_id)
            Else
                sp_Query = String.Format("UPDATE MSALES SET NAMA='{0}', ALAMAT='{1}', TELP='{2}', TGL_LAHIR='{3}', INACTIVE='{4}', IDUSER={5}, LASTUPDATE=GETDATE() ", Nama.Text.Replace("'", "''"), Alamat.Text.Replace("'", "''"), Telp.Text.Replace("'", "''"), Format(TglLahir.EditValue, "yyyy-MM-dd"), CekInaktif.Checked, staff_id)
                param_Query = String.Format("WHERE IDSALES='{0}'", selmaster_id)
            End If

            ExDb.ExecData(sp_Query & param_Query)
            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterSales.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If Kode.Text.Length < 1 Or Nama.Text.Length < 2 Then
            msgboxWarning("Kolom Kode dan Nama tidak boleh kosong")
            Return False
        End If
        If TglLahir.EditValue = Nothing Then
            msgboxWarning("Tanggal lahir tidak boleh kosong")
            TglLahir.Focus()
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If selmaster_id = "0" Then
            If Not isDoubleData(String.Format("SELECT IDSALES ID FROM MSALES WHERE IDSALES='{0}'", Kode.Text)) Then
                msgboxWarning("Kode yang di inputkan sudah ada dalam daftar, silahkan pilih kode lain")
                Return False
            End If
        End If
        If Nama.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT IDSALES ID FROM MSALES WHERE NAMA='{0}'", Nama.Text)) Then
                msgboxWarning("Nama yang di inputkan sudah ada dalam daftar, silahkan pilih nama yang lain")
                Return False
            End If
        End If

        Return True
    End Function
End Class