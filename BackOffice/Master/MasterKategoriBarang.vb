﻿Public Class MasterKategoriBarang

    Private Sub MasterKategoriBarang_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("KoreksiMaster")
        xSet.Tables.Remove("GetMAXNO")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub MasterKategoriBarang_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        LoadSprite.CloseLoading()
        NamaKategori.Focus()
        If selmaster_id = "0" Then
            CekInaktif.Properties.ReadOnly = True
            CekInaktif.Checked = False
        End If
    End Sub

    Private Sub MasterKategoriBarang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        If Not selmaster_id = "0" Then
            SQLquery = String.Format("SELECT NAMA, DETAIL, INACTIVE FROM MKATEGORI WHERE IDKATEGORI='{0}'", selmaster_id)
            ExDb.ExecQuery(SQLquery, xSet, "KoreksiMaster")

            NamaKategori.Text = xSet.Tables("KoreksiMaster").Rows(0).Item("NAMA").ToString
            JenisKategori.SelectedIndex = xSet.Tables("KoreksiMaster").Rows(0).Item("DETAIL")
            CekInaktif.Checked = xSet.Tables("KoreksiMaster").Rows(0).Item("INACTIVE")
            nameCheck = NamaKategori.Text
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        Try
            LoadSprite.ShowLoading()
            If selmaster_id = "0" Then
                SQLquery = "SELECT ISNULL(MAX(IDNUM),0) MAXNO FROM MKATEGORI;"
                ExDb.ExecQuery(SQLquery, xSet, "GetMAXNO")
                Dim MAXNO As Integer = 1 + xSet.Tables("GetMAXNO").Rows(0).Item(0)
                selmaster_id = "K-" & MAXNO.ToString.PadLeft(3, "0")

                sp_Query = "INSERT INTO MKATEGORI(IDKATEGORI,IDNUM,NAMA,DETAIL,INACTIVE,CREATEDATE,IDUSER) "
                param_Query = String.Format("VALUES('{0}',{1},'{2}',{3},0,GETDATE(),{4}); ", selmaster_id, MAXNO, NamaKategori.Text.Replace("'", "''"), JenisKategori.SelectedIndex, staff_id)
            Else
                sp_Query = String.Format("UPDATE MKATEGORI SET NAMA='{0}', DETAIL={1}, INACTIVE='{2}', IDUSER={3}, LASTUPDATE=GETDATE() ", NamaKategori.Text.Replace("'", "''"), JenisKategori.SelectedIndex, CekInaktif.Checked, staff_id)
                param_Query = String.Format("WHERE IDKATEGORI='{0}'", selmaster_id)
            End If

            ExDb.ExecData(sp_Query & param_Query)
            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserMasterKategori.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        End Try
        LoadSprite.CloseLoading()
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If NamaKategori.Text.Length < 1 Then
            msgboxWarning("Kolom inputan tidak boleh kosong")
            Return False
        End If

        If NamaKategori.Text.Contains("'") Then
            msgboxWarning("Huruf (') tidak diterima sebagai inputan")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        If NamaKategori.Text <> nameCheck Then
            If Not isDoubleData(String.Format("SELECT IDKATEGORI ID FROM MKATEGORI WHERE NAMA='{0}'", NamaKategori.Text)) Then
                msgboxWarning("Nama yang di inputkan sudah ada dalam daftar, silahkan pilih nama yang lain")
                Return False
            End If
        End If

        Return True
    End Function
End Class