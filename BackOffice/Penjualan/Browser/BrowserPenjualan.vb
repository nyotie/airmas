﻿Imports DevExpress.Utils

Public Class BrowserPenjualan
    Public isChange As Boolean

    Private Sub ThisBrowser_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarBrowserFakturPenjualan")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisBrowser_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        reset_buttons()

        butBaru.Focus()
    End Sub

    Private Sub ThisBrowser_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        isChange = True
        DateEdit1.EditValue = DateAdd(DateInterval.Day, -1, Today.Date)
        DateEdit2.EditValue = Today.Date

        refreshingGrid()
        AddHandler GridView1.DoubleClick, AddressOf butKoreksi_Click
    End Sub

    '--Refresh
    Private Sub butRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butRefresh.Click
        isChange = True
        refreshingGrid()
    End Sub

    '--New
    Private Sub butBaru_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBaru.Click
        selected_id = "0"

        openTheButton()
        refreshingGrid()
    End Sub

    '--Edit
    Private Sub butKoreksi_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butKoreksi.Click
        If butKoreksi.Enabled = False Or butKoreksi.Visible = False Then Return
        If GridView1.FocusedRowHandle < 0 Then Return

        If GridView1.GetFocusedRowCellValue("VOID") = True Then
            msgboxInformation("Transaksi sudah dibatalkan, tidak dapat dikoreksi")
            Return
        ElseIf GridView1.GetFocusedRowCellValue("ACC") <> "OPEN" Then
            msgboxInformation("Transaksi sudah di ACC, tidak dapat dikoreksi")
            Return
        End If

        selected_id = GridView1.GetFocusedRowCellValue("KODE")

        openTheButton()
        refreshingGrid()
    End Sub

    '--Void
    Private Sub butVoid_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butVoid.Click
        If Not GridView1.RowCount > 0 Then Exit Sub

        If GridView1.GetFocusedRowCellValue("VOID") = True Then
            msgboxInformation("Transaksi sudah dibatalkan")
            Return
        ElseIf GridView1.GetFocusedRowCellValue("ACCANGKA") <> 2 Then
            'BISA DI HAPUS
            If MsgBox("Nomor transaksi ini bisa di HAPUS, yakin ingin menghapus?", MsgBoxStyle.YesNo, "Konfirmasi") = vbYes Then
                SQLquery = String.Format("UPDATE TM_SO SET ACC=1 WHERE NOSO='{1}'; DELETE FROM TM_PJ WHERE NOPJ='{0}'; ", GridView1.GetFocusedRowCellValue("KODE"), GridView1.GetFocusedRowCellValue("NOSO"))
                ExDb.ExecData(SQLquery)

                isChange = True
                refreshingGrid()
            End If
        Else
            'TIDAK BISA DI HAPUS
            If MsgBox("Nomor transaksi ini hanya bisa di BATALKAN, yakin ingin membatalkan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbYes Then
                SQLquery = String.Format("UPDATE TM_PJ SET VOID=1, IDUSER={1}, LASTUPDATE=GETDATE() WHERE NOPJ='{0}'; ", GridView1.GetFocusedRowCellValue("KODE"), staff_id)
                ExDb.ExecData(SQLquery)

                isChange = True
                refreshingGrid()
            End If
        End If
    End Sub

    '--Print
    Private Sub butView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butView.Click
        If GridView1.FocusedRowHandle < 0 Then Exit Sub
        selected_id = GridView1.GetFocusedRowCellValue("KODE")

        Try
            Using printout As New xrPenjualanNota
                printout.param_notrans.Value = selected_id
                Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                    tool.ShowRibbonPreviewDialog()
                End Using
            End Using

        Catch ex As Exception
            msgboxErrorDev()
        End Try
    End Sub

    Private Sub PrintFaktur_Click(sender As Object, e As EventArgs) Handles PrintFaktur.Click
        If GridView1.FocusedRowHandle < 0 Then Exit Sub
        selected_id = GridView1.GetFocusedRowCellValue("KODE")

        Try
            Using printout As New xrPenjualanFaktur
                printout.param_notrans.Value = selected_id
                Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                    tool.ShowRibbonPreviewDialog()
                End Using
            End Using
            refreshingGrid()
        Catch ex As Exception
            msgboxErrorDev()
        End Try
    End Sub

    '--Close Form
    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub reset_buttons()
        butBaru.Visible = True
        butKoreksi.Visible = True
        butView.Visible = True

        butBaru.Text = "Baru"
        butKoreksi.Text = "Koreksi"

        butBaru.Enabled = True
        butKoreksi.Enabled = True
        butView.Enabled = True
    End Sub

    Private Sub openTheButton()
        ShowModule(Penjualan, "Penjualan")
        Penjualan = Nothing
    End Sub

    Private Sub refreshingGrid()
        If isChange = False Then Exit Sub Else isChange = False

        Dim frow As Integer = GridView1.FocusedRowHandle
        If Not xSet.Tables("DaftarBrowserFakturPenjualan") Is Nothing Then xSet.Tables.Remove("DaftarBrowserFakturPenjualan")
        reset_buttons()
        SQLquery = "SELECT TM.NOPJ KODE, SO.TANGGAL AS 'TGL SO', SO.NOTA NOSO, TM.TANGGAL AS 'TGL NOTA', TM.NOTA, TM.NOPJK,MC.NAMA AS CUSTOMER, MW.GRUP WILAYAH, MS.IDSALES SALES, TM.ACC ACCANGKA,  " & _
            "CASE TM.ACC WHEN 0 THEN 'OPEN' WHEN 1 THEN 'ACC' ELSE 'CLOSE' END AS ACC, TM.VOID FROM TM_PJ TM " & _
            "INNER JOIN TM_SO SO ON TM.NOSO=SO.NOSO INNER JOIN MSALES MS ON SO.IDSALES=MS.IDSALES INNER JOIN MWILAYAH MW ON SO.IDWILAYAH=MW.IDWILAYAH INNER JOIN MCUSTOMER MC ON SO.IDCUSTOMER = MC.IDCUSTOMER WHERE"
        If butCheckShow.Checked = False Then SQLquery += " TM.VOID=0 AND "
        SQLquery += String.Format(" CAST(TM.TANGGAL AS DATE) BETWEEN '{0}' AND '{1}' ORDER BY TM.TANGGAL DESC", Format(DateEdit1.EditValue, "yyyy/MM/dd"), Format(DateEdit2.EditValue, "yyyy/MM/dd"))
        ExDb.ExecQuery(SQLquery, xSet, "DaftarBrowserFakturPenjualan")

        GridControl1.DataSource = xSet.Tables("DaftarBrowserFakturPenjualan").DefaultView

        For Each coll As DataColumn In xSet.Tables("DaftarBrowserFakturPenjualan").Columns
            With GridView1.Columns(coll.ColumnName)
                .AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
                .AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            End With
        Next

        GridView1.Columns("KODE").Visible = False
        GridView1.Columns("ACCANGKA").Visible = False

        GridView1.Columns("TGL SO").DisplayFormat.FormatType = FormatType.DateTime
        GridView1.Columns("TGL SO").DisplayFormat.FormatString = "dd/MMM/yyyy HH:mm"

        GridView1.Columns("TGL NOTA").DisplayFormat.FormatType = FormatType.DateTime
        GridView1.Columns("TGL NOTA").DisplayFormat.FormatString = "dd/MMM/yyyy HH:mm"

        GridView1.BestFitColumns()
        GridView1.FocusedRowHandle = frow
    End Sub
End Class