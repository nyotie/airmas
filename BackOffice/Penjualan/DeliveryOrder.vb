﻿Imports DevExpress.Utils

'save to online only
Public Class DeliveryOrder
    Dim selected_cell As String

    Private Sub PengirimanCabang_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_DO")
        xSet.Tables.Remove("TD_DO")
        xSet.Tables.Remove("DaftarPenjualan")
        xSet.Tables.Remove("DaftarKendaraan")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub PengirimanCabang_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub PengirimanCabang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now
        Else
            SQLquery = String.Format("SELECT NODO,IDKENDARAAN,TANGGAL,SUPIR,MEMO,GRUP,ACC FROM TM_DO WHERE NODO='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_DO")

            NoTrans.Text = xSet.Tables("TM_DO").Rows(0).Item("NODO")
            TglTrans.EditValue = xSet.Tables("TM_DO").Rows(0).Item("TANGGAL")
            DaftarKendaraan.EditValue = xSet.Tables("TM_DO").Rows(0).Item("IDKENDARAAN")
            NamaSupir.EditValue = xSet.Tables("TM_DO").Rows(0).Item("SUPIR")
            GrupKota.EditValue = xSet.Tables("TM_DO").Rows(0).Item("GRUP")
            Catatan.EditValue = xSet.Tables("TM_DO").Rows(0).Item("MEMO")
            ACCcheck.Checked = IIf(xSet.Tables("TM_DO").Rows(0).Item("ACC") = 0, False, True)
        End If
    End Sub

    Private Sub DaftarBarang_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarPenjualan.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell = Repository.EditValue
    End Sub

    Private Sub ViewTrans_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans.CellValueChanged
        Try
            Dim drow As DataRow = xSet.Tables("DaftarPenjualan").Select(String.Format("KODE='{0}'", selected_cell))(0)

            If e.Column.FieldName = "NOTA" Then
                For row As Integer = 0 To ViewTrans.RowCount - 1
                    If ViewTrans.GetRowCellValue(row, "NOTA") = selected_cell And Not ViewTrans.FocusedRowHandle = row Then
                        ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                        ViewTrans.FocusedRowHandle = row
                        Return
                    End If
                Next

                ViewTrans.SetFocusedRowCellValue("CUSTOMER", drow.Item("CUSTOMER"))
                ViewTrans.SetFocusedRowCellValue("WILAYAH", drow.Item("WILAYAH"))
                ViewTrans.SetFocusedRowCellValue("ALAMAT", drow.Item("ALAMAT"))
                ViewTrans.FocusedRowHandle = ViewTrans.RowCount
            End If
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("NOTA")) = True Then Throw New Exception
        Catch ex As Exception
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("NOTA")) = True Then
                ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                xSet.Tables("TD_DO").AcceptChanges()
            End If
        End Try
    End Sub

    Private Sub ViewTrans_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewTrans.FocusedRowChanged
        Try
            If e.FocusedRowHandle >= 0 Then
                selected_cell = ViewTrans.GetRowCellValue(e.FocusedRowHandle, "NOTA")
            End If
        Catch ex As Exception
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
        End Try
    End Sub

    Private Sub GridTransKeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
            xSet.Tables("TD_DO").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC DO_INS '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', """,
                                            NoTrans.Text, DaftarKendaraan.EditValue, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"),
                                            NamaSupir.Text.Replace("'", "''"), GrupKota.Text.Replace("'", "''"), Catatan.Text.Replace("'", "''"), IIf(ACCcheck.Checked, 1, 0), staff_id)
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC DO_UPD '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', """,
                                            NoTrans.Text, DaftarKendaraan.EditValue, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"),
                                            NamaSupir.Text.Replace("'", "''"), GrupKota.Text.Replace("'", "''"), Catatan.Text.Replace("'", "''"), IIf(ACCcheck.Checked, 1, 0), staff_id)
            End If

            Dim nourut As Integer = 1
            For Each drow As DataRow In xSet.Tables("TD_DO").Rows
                SQLquery += String.Format("('{0}', '{1}', '{2}'), ", NoTrans.Text, nourut, drow.Item("NOTA"))
                nourut += 1
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserDeliveryOrder.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            'Using printout As New xrPengirimanCabang
            '    printout.ShowRibbonPreviewDialog()
            'End Using

            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_DO").Clear()
        xSet.Tables("TD_DO").AcceptChanges()

        TglTrans.EditValue = Now
        DaftarKendaraan.EditValue = Nothing
        NamaSupir.Text = ""
        GrupKota.Text = ""
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------Daftar Kendaraan
        If Not xSet.Tables("DaftarKendaraan") Is Nothing Then xSet.Tables("DaftarKendaraan").Clear()
        SQLquery = "SELECT IDKENDARAAN ID, NAMA FROM MKENDARAAN WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarKendaraan")
        DaftarKendaraan.Properties.DataSource = xSet.Tables("DaftarKendaraan").DefaultView

        DaftarKendaraan.Properties.NullText = ""
        DaftarKendaraan.Properties.ValueMember = "ID"
        DaftarKendaraan.Properties.DisplayMember = "NAMA"
        DaftarKendaraan.Properties.ShowClearButton = False
        DaftarKendaraan.Properties.PopulateViewColumns()

        For Each coll As DataColumn In xSet.Tables("DaftarKendaraan").Columns
            ViewDaftarKendaraan.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar Penjualan 
        If Not xSet.Tables("DaftarPenjualan") Is Nothing Then xSet.Tables("DaftarPenjualan").Clear()
        SQLquery = String.Format("SELECT PJ.NOPJ KODE, PJ.NOTA, PJ.TANGGAL, MC.NAMA CUSTOMER, MC.IDWILAYAH WILAYAH, MC.ALAMAT FROM TM_PJ PJ INNER JOIN TM_SO SO ON PJ.NOSO=SO.NOSO  " & _
                                 "INNER JOIN MCUSTOMER MC ON SO.IDCUSTOMER=MC.IDCUSTOMER  " & _
                                 "WHERE PJ.ACC=1 AND PJ.VOID=0 OR NOPJ IN (SELECT NOPJ FROM TM_DO WHERE NODO='{0}');", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarPenjualan")
        DaftarPenjualan.DataSource = xSet.Tables("DaftarPenjualan").DefaultView

        DaftarPenjualan.NullText = ""
        DaftarPenjualan.ValueMember = "KODE"
        DaftarPenjualan.DisplayMember = "NOTA"
        DaftarPenjualan.ShowClearButton = False
        DaftarPenjualan.PopulateViewColumns()

        ViewDaftarPenjualan.Columns("KODE").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarPenjualan").Columns
            ViewDaftarPenjualan.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------TD_DO
        SQLquery = String.Format("SELECT PJ.NOPJ NOTA, MC.NAMA CUSTOMER, MC.IDWILAYAH WILAYAH, MC.ALAMAT FROM TD_DO DO INNER JOIN TM_PJ PJ ON DO.NOPJ=PJ.NOPJ INNER JOIN TM_SO SO ON SO.NOSO=PJ.NOSO " & _
                                 "INNER JOIN MCUSTOMER MC ON SO.IDCUSTOMER=MC.IDCUSTOMER WHERE DO.NODO='{0}' ORDER BY DO.NO_URUT", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_DO")
        GridTrans.DataSource = xSet.Tables("TD_DO").DefaultView

        ViewTrans.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top

        For Each coll As DataColumn In xSet.Tables("TD_DO").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("CUSTOMER").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("NOTA").ColumnEdit = DaftarPenjualan

        ViewTrans.Columns("NOTA").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("NOTA").SummaryItem.DisplayFormat = "{0:n0} item(s)"

        ViewTrans.Columns("NOTA").Width = GridTrans.Width * 0.4
        ViewTrans.Columns("CUSTOMER").Width = GridTrans.Width * 0.6
        ViewTrans.Columns("WILAYAH").Width = GridTrans.Width * 0.8
        ViewTrans.Columns("ALAMAT").Width = GridTrans.Width * 0.9
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_DO").Rows.Count < 1 Then
            Return False
        End If

        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        If DaftarKendaraan.EditValue = Nothing Then
            DaftarKendaraan.Focus()
            msgboxWarning("Kendaraan tidak boleh kosong")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "DO", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NODO, 3)) AS KODE FROM TM_DO WHERE LEFT(NODO, 9) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        Dim tgl As Date = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function
End Class