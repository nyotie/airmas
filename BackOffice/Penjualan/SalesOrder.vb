﻿Imports DevExpress.Utils

Public Class SalesOrder
    Dim isChange As Boolean = False

    Private Sub ThisForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_SO")
        xSet.Tables.Remove("TD_SO")
        xSet.Tables.Remove("DaftarWilayah")
        xSet.Tables.Remove("DaftarCustomer")
        xSet.Tables.Remove("DaftarSales")
        xSet.Tables.Remove("DaftarBarang2Satuan")

        AddNewItemSO = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub ThisForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now
        Else
            SQLquery = String.Format("SELECT NOSO, IDCUSTOMER, IDSALES, IDWILAYAH, NOTA, TANGGAL, MEMO, JNS_TRX, JTH_TEMPO, ACC FROM TM_SO WHERE NOSO='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_SO")

            NoTrans.Text = xSet.Tables("TM_SO").Rows(0).Item("NOSO")
            TglTrans.EditValue = xSet.Tables("TM_SO").Rows(0).Item("TANGGAL")
            DaftarWilayah.EditValue = xSet.Tables("TM_SO").Rows(0).Item("IDWILAYAH")
            DaftarCustomer.EditValue = xSet.Tables("TM_SO").Rows(0).Item("IDCUSTOMER")
            DaftarSales.EditValue = xSet.Tables("TM_SO").Rows(0).Item("IDSALES")
            NoTransManual.Text = xSet.Tables("TM_SO").Rows(0).Item("NOTA")
            CASHCheck.Checked = IIf(xSet.Tables("TM_SO").Rows(0).Item("JNS_TRX") = 0, False, True)
            Catatan.EditValue = xSet.Tables("TM_SO").Rows(0).Item("MEMO")
            ACCcheck.Checked = xSet.Tables("TM_SO").Rows(0).Item("ACC")
        End If

        DaftarCustomer.Properties.ShowClearButton = True
        DaftarCustomer.Properties.ShowAddNewButton = True
    End Sub

    Private Sub CASHCheck_CheckedChanged(sender As Object, e As EventArgs) Handles CASHCheck.CheckedChanged
        If CASHCheck.Checked Then
            HariJthTempo.Enabled = True
        Else
            HariJthTempo.Enabled = False
        End If
    End Sub

    Private Sub DaftarCustomer_AddNewValue(sender As Object, e As DevExpress.XtraEditors.Controls.AddNewValueEventArgs) Handles DaftarCustomer.AddNewValue
        selmaster_id = "0"
        MasterCustomer.ShowDialog()
        MasterCustomer = Nothing

        If Not xSet.Tables("DaftarCustomer") Is Nothing Then xSet.Tables("DaftarCustomer").Clear()
        SQLquery = String.Format("SELECT IDCUSTOMER, NAMA, ALAMAT, KOTA, CP FROM MCUSTOMER WHERE IDWILAYAH='{0}' AND INACTIVE=0", DaftarWilayah.EditValue)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarCustomer")
    End Sub

    Private Sub DaftarWilayah_EditValueChanged(sender As Object, e As EventArgs) Handles DaftarWilayah.EditValueChanged
        If Not xSet.Tables("DaftarCustomer") Is Nothing Then xSet.Tables("DaftarCustomer").Clear()
        SQLquery = String.Format("SELECT IDCUSTOMER, NAMA, ALAMAT, KOTA, CP FROM MCUSTOMER WHERE IDWILAYAH='{0}' AND INACTIVE=0", DaftarWilayah.EditValue)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarCustomer")
    End Sub

    Private Sub TambahBarang_Click(sender As Object, e As EventArgs) Handles TambahBarang.Click
        ShowModule(AddNewItemSO, "Tambah barang")
    End Sub

    Private Sub ViewTrans_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans.CellValueChanged
        Try
            'TD.QTY1, TD.HARGA1, TD.QTY2, TD.HARGA2, DISC_PROSEN, DISC_RP, TOTAL
            If e.Column.FieldName = "QTY1" And ViewTrans.FocusedColumn.FieldName = "QTY1" Then
                If ViewTrans.GetFocusedRowCellValue("QTY1") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("QTY1", 0)
                ElseIf ViewTrans.GetFocusedRowCellValue("QTY1") > 0 Then
                    ViewTrans.SetFocusedRowCellValue("QTY2", 0)
                    isChange = True
                End If
            ElseIf e.Column.FieldName = "QTY2" And ViewTrans.FocusedColumn.FieldName = "QTY2" Then
                If ViewTrans.GetFocusedRowCellValue("QTY2") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("QTY2", 0)
                ElseIf ViewTrans.GetFocusedRowCellValue("QTY2") > 0 Then
                    ViewTrans.SetFocusedRowCellValue("QTY1", 0)
                    isChange = True
                End If
            ElseIf e.Column.FieldName = "DISC_PROSEN" And ViewTrans.FocusedColumn.FieldName = "DISC_PROSEN" Then
                If ViewTrans.GetFocusedRowCellValue("DISC_PROSEN") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("DISC_PROSEN", 0)
                ElseIf ViewTrans.GetFocusedRowCellValue("DISC_PROSEN") > 0 Then
                    ViewTrans.SetFocusedRowCellValue("DISC_RP", HitungDisc(ViewTrans.GetFocusedRowCellValue("DISC_PROSEN")))
                    isChange = True
                End If
            ElseIf e.Column.FieldName = "DISC_RP" And ViewTrans.FocusedColumn.FieldName = "DISC_RP" Then
                If ViewTrans.GetFocusedRowCellValue("DISC_RP") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("DISC_RP", 0)
                ElseIf ViewTrans.GetFocusedRowCellValue("DISC_RP") > 0 Then
                    ViewTrans.SetFocusedRowCellValue("DISC_PROSEN", 0)
                    isChange = True
                End If
            End If

            If isChange Then
                isChange = False
                ViewTrans.SetFocusedRowCellValue("TOTAL", HitungTotal(ViewTrans.GetFocusedRowCellValue("DISC_RP")))
            End If
        Catch ex As Exception
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("BARANG")) = True Then
                ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                xSet.Tables("TD_SO").AcceptChanges()
            End If
        End Try
    End Sub

    Private Function HitungDisc(ByVal discp As Double) As Double
        Dim qtt, hrg As Double
        If ViewTrans.GetFocusedRowCellValue("QTY1") = 0 Then
            qtt = ViewTrans.GetFocusedRowCellValue("QTY2")
            hrg = ViewTrans.GetFocusedRowCellValue("HARGA2")
        Else
            qtt = ViewTrans.GetFocusedRowCellValue("QTY1")
            hrg = ViewTrans.GetFocusedRowCellValue("HARGA1")
        End If

        Dim discn As Double = (qtt * hrg) * (discp / 100)
        Return discn
    End Function

    Private Function HitungTotal(ByVal discn As Integer) As Integer
        Dim qtt, hrg As Integer
        If ViewTrans.GetFocusedRowCellValue("QTY1") = 0 Then
            qtt = ViewTrans.GetFocusedRowCellValue("QTY2")
            hrg = ViewTrans.GetFocusedRowCellValue("HARGA2")
        Else
            qtt = ViewTrans.GetFocusedRowCellValue("QTY1")
            hrg = ViewTrans.GetFocusedRowCellValue("HARGA1")
        End If

        Dim subtotal As Integer = qtt * hrg
        Return subtotal - discn
    End Function

    Private Sub GridTrans_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
            xSet.Tables("TD_SO").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()
        ViewTrans.UpdateSummary()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC SO_INS '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', """,
                                            NoTrans.Text, DaftarWilayah.EditValue, DaftarCustomer.EditValue, DaftarSales.EditValue,
                                            Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), NoTransManual.Text.Replace("'", "''"),
                                            Catatan.Text.Replace("'", "''"), ViewTrans.Columns("TOTAL").SummaryItem.SummaryValue,
                                            IIf(CASHCheck.Checked, 1, 0), HariJthTempo.EditValue, IIf(ACCcheck.Checked, 1, 0), staff_id)
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC SO_UPD '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', """,
                                            NoTrans.Text, DaftarWilayah.EditValue, DaftarCustomer.EditValue, DaftarSales.EditValue,
                                            Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), NoTransManual.Text.Replace("'", "''"),
                                            Catatan.Text.Replace("'", "''"), ViewTrans.Columns("TOTAL").SummaryItem.SummaryValue,
                                            IIf(CASHCheck.Checked, 1, 0), HariJthTempo.EditValue, IIf(ACCcheck.Checked, 1, 0), staff_id)
            End If

            Dim nourut As Integer = 1
            'NOSO, NO_URUT, IDBARANG, QTY1, QTY2, HARGA1, HARGA2, DISC_PROSEN, DISC_RP, TOTAL
            For Each drow As DataRow In xSet.Tables("TD_SO").Rows
                SQLquery += String.Format("('{0}', {1}, '{2}', {3}, {4}, {5}, {6}, {7}, {8}, {9}), ",
                                          NoTrans.Text, nourut, drow.Item("IDBARANG"),
                                          drow.Item("QTY1"), drow.Item("QTY2"), drow.Item("HARGA1"), drow.Item("HARGA2"),
                                          drow.Item("DISC_PROSEN"), drow.Item("DISC_RP"), drow.Item("TOTAL"))
                nourut += 1
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserSalesOrder.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            'Using printout As New xrPengirimanCabang
            '    printout.ShowRibbonPreviewDialog()
            'End Using

            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_SO").Clear()
        xSet.Tables("TD_SO").AcceptChanges()
        DaftarCustomer.EditValue = Nothing
        DaftarWilayah.EditValue = Nothing
        DaftarSales.EditValue = Nothing

        TglTrans.EditValue = Now
        NoTransManual.EditValue = ""
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------Daftar Wilayah
        If Not xSet.Tables("DaftarWilayah") Is Nothing Then xSet.Tables("DaftarWilayah").Clear()
        SQLquery = String.Format("SELECT IDWILAYAH, GRUP, NAMA FROM MWILAYAH WHERE INACTIVE=0", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarWilayah")
        DaftarWilayah.Properties.DataSource = xSet.Tables("DaftarWilayah").DefaultView

        DaftarWilayah.Properties.NullText = ""
        DaftarWilayah.Properties.ValueMember = "IDWILAYAH"
        DaftarWilayah.Properties.DisplayMember = "GRUP"
        DaftarWilayah.Properties.ShowClearButton = False
        DaftarWilayah.Properties.PopulateViewColumns()

        ViewDaftarWilayah.Columns("IDWILAYAH").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarWilayah").Columns
            ViewDaftarWilayah.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar Customer
        If Not xSet.Tables("DaftarCustomer") Is Nothing Then xSet.Tables("DaftarCustomer").Clear()
        SQLquery = String.Format("SELECT IDCUSTOMER, NAMA, ALAMAT, KOTA, CP FROM MCUSTOMER WHERE IDWILAYAH='{0}' AND INACTIVE=0", DaftarWilayah.EditValue)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarCustomer")
        DaftarCustomer.Properties.DataSource = xSet.Tables("DaftarCustomer").DefaultView

        DaftarCustomer.Properties.NullText = ""
        DaftarCustomer.Properties.ValueMember = "IDCUSTOMER"
        DaftarCustomer.Properties.DisplayMember = "NAMA"
        DaftarCustomer.Properties.ShowClearButton = False
        DaftarCustomer.Properties.PopulateViewColumns()

        ViewDaftarCustomer.Columns("IDCUSTOMER").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarCustomer").Columns
            ViewDaftarCustomer.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar Sales
        If Not xSet.Tables("DaftarSales") Is Nothing Then xSet.Tables("DaftarSales").Clear()
        SQLquery = "SELECT IDSALES, NAMA, ALAMAT, TELP FROM MSALES WHERE INACTIVE=0"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarSales")
        DaftarSales.Properties.DataSource = xSet.Tables("DaftarSales").DefaultView

        DaftarSales.Properties.NullText = ""
        DaftarSales.Properties.ValueMember = "IDSALES"
        DaftarSales.Properties.DisplayMember = "NAMA"
        DaftarSales.Properties.ShowClearButton = False
        DaftarSales.Properties.PopulateViewColumns()

        ViewDaftarSales.Columns("IDSALES").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarSales").Columns
            ViewDaftarSales.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------TD_SO
        SQLquery = String.Format("SELECT TD.IDBARANG, MB.NAMA BARANG, TD.QTY1, MB.SATUAN1, TD.HARGA1, TD.QTY2, MB.SATUAN2, TD.HARGA2, DISC_PROSEN, DISC_RP, TOTAL FROM TD_SO TD INNER JOIN MBARANG MB ON TD.IDBARANG=MB.IDBARANG WHERE NOSO='{0}' ORDER BY TD.NO_URUT;", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_SO")
        GridTrans.DataSource = xSet.Tables("TD_SO").DefaultView

        For Each coll As DataColumn In xSet.Tables("TD_SO").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("IDBARANG").Visible = False
        ViewTrans.Columns("DISC_PROSEN").Caption = "DISC %"
        ViewTrans.Columns("DISC_RP").Caption = "DISC"

        ViewTrans.Columns("IDBARANG").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("BARANG").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("SATUAN1").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("HARGA1").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("SATUAN2").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("HARGA2").OptionsColumn.AllowEdit = False

        ViewTrans.Columns("QTY1").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("QTY1").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("QTY2").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("QTY2").DisplayFormat.FormatString = "n0"

        ViewTrans.Columns("HARGA1").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("HARGA1").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("HARGA2").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("HARGA2").DisplayFormat.FormatString = "n0"

        ViewTrans.Columns("DISC_PROSEN").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("DISC_PROSEN").DisplayFormat.FormatString = "n2"
        ViewTrans.Columns("DISC_RP").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("DISC_RP").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("TOTAL").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("TOTAL").DisplayFormat.FormatString = "n0"

        ViewTrans.Columns("BARANG").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("BARANG").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewTrans.Columns("QTY1").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("QTY1").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("QTY2").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("QTY2").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("TOTAL").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("TOTAL").SummaryItem.DisplayFormat = "Total:{0:n0}"

        ViewTrans.Columns("BARANG").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("QTY1").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("SATUAN1").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("HARGA1").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("QTY2").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("SATUAN2").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("HARGA2").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("DISC_PROSEN").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("DISC_RP").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("TOTAL").Width = GridTrans.Width * 0.125
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_SO").Rows.Count < 1 Then
            Return False
        End If

        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        If DaftarWilayah.EditValue = Nothing Then
            DaftarWilayah.Focus()
            msgboxWarning("Wilayah tidak boleh kosong")
            Return False
        End If

        If DaftarCustomer.EditValue = Nothing Then
            DaftarCustomer.Focus()
            msgboxWarning("Customer tidak boleh kosong")
            Return False
        End If

        If DaftarSales.EditValue = Nothing Then
            DaftarSales.Focus()
            msgboxWarning("Sales tidak boleh kosong")
            Return False
        End If

        If Trim(NoTransManual.Text).ToString.Length < 2 Then
            NoTransManual.Focus()
            msgboxWarning("Nomor SP tidak boleh kurang dari 2 digit")
            Return False
        End If

        If HariJthTempo.EditValue <= 0 Then
            HariJthTempo.Focus()
            msgboxWarning("Jatuh tempo tidak boleh kosong atau kurang dari 1")
            Return False
        End If

        For Each drow As DataRow In xSet.Tables("TD_SO").Rows
            If drow.Item("TOTAL") <= 0 Then
                msgboxWarning("Total harga barang tidak boleh kosong")
                Return False
            End If
        Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "SO", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NOSO, 3)) AS KODE FROM TM_SO WHERE LEFT(NOSO, 9) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        Dim tgl As Date

        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        tgl = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function
End Class