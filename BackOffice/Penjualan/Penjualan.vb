﻿Imports DevExpress.Utils

Public Class Penjualan

    Private Sub ThisForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_PJ")
        xSet.Tables.Remove("TD_PJ")
        xSet.Tables.Remove("DaftarSO")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub ThisForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now
        Else
            SQLquery = String.Format("SELECT NOPJ, NOSO, NOTA, TANGGAL, MEMO, ACC FROM TM_PJ WHERE NOPJ='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_PJ")

            NoTrans.Text = xSet.Tables("TM_PJ").Rows(0).Item("NOPJ")
            DaftarSO.Enabled = False
            DaftarSO.EditValue = xSet.Tables("TM_PJ").Rows(0).Item("NOSO")
            TglTrans.EditValue = xSet.Tables("TM_PJ").Rows(0).Item("TANGGAL")
            NoTransManual.EditValue = xSet.Tables("TM_PJ").Rows(0).Item("NOTA").ToString
            Catatan.EditValue = xSet.Tables("TM_PJ").Rows(0).Item("MEMO")
            ACCcheck.Checked = IIf(xSet.Tables("TM_PJ").Rows(0).Item("ACC") = 0, False, True)
        End If
    End Sub

    Private Sub DaftarSO_EditValueChanged(sender As Object, e As EventArgs) Handles DaftarSO.EditValueChanged
        If DaftarSO.Enabled = False Or DaftarSO.EditValue Is Nothing Then Return

        Try
            xSet.Tables("TD_PJ").Clear()
            SQLquery = String.Format("SELECT TDS.IDBARANG, MB.NAMA BARANG,ISNULL(TDS.QTY1,0) QTY1, MB.SATUAN1, HARGA1, " & _
                                    "ISNULL(TDS.QTY2,0) QTY2, MB.SATUAN2, HARGA2, TDS.DISC_PROSEN, TDS.DISC_RP, TDS.TOTAL, NULL AS Gudang " & _
                                    "FROM TD_SO TDS INNER JOIN MBARANG MB ON TDS.IDBARANG=MB.IDBARANG WHERE TDS.NOSO='{0}';",
                                    DaftarSO.EditValue)
            ExDb.ExecQuery(SQLquery, xSet, "TD_PJ")
        Catch ex As Exception
            xSet.Tables("TD_PJ").Clear()
        End Try
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()
        ViewTrans.UpdateSummary()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC PJ_INS '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', """,
                                            NoTrans.Text, DaftarSO.EditValue,
                                            Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), NoTransManual.Text.Replace("'", "''"),
                                            Catatan.Text.Replace("'", "''"), ViewTrans.Columns("TOTAL").SummaryItem.SummaryValue,
                                            IIf(ACCcheck.Checked, 1, 0), staff_id)
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC PJ_UPD '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', """,
                                            NoTrans.Text, DaftarSO.EditValue,
                                            Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), NoTransManual.Text.Replace("'", "''"),
                                            Catatan.Text.Replace("'", "''"), ViewTrans.Columns("TOTAL").SummaryItem.SummaryValue,
                                            IIf(ACCcheck.Checked, 1, 0), staff_id)
            End If

            Dim nourut As Integer = 1
            'NOPJ, NO_URUT, IDBARANG, QTY1, QTY2, HARGA1, HARGA2, DISC_PROSEN, DISC_RP, TOTAL, IDGUDANG
            For Each drow As DataRow In xSet.Tables("TD_PJ").Rows
                SQLquery += String.Format("('{0}', {1}, '{2}', {3}, {4}, {5}, {6}, {7}, {8}, {9}, '{10}'), ",
                                          NoTrans.Text, nourut, drow.Item("IDBARANG"),
                                          drow.Item("QTY1"), drow.Item("QTY2"), drow.Item("HARGA1"), drow.Item("HARGA2"),
                                          drow.Item("DISC_PROSEN"), drow.Item("DISC_RP"), drow.Item("TOTAL"), drow.Item("Gudang"))
                nourut += 1
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserPenjualan.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            'Using printout As New xrPengirimanCabang
            '    printout.ShowRibbonPreviewDialog()
            'End Using

            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_PJ").Clear()
        xSet.Tables("TD_PJ").AcceptChanges()

        DaftarSO.EditValue = Nothing
        TglTrans.EditValue = Now
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------Daftar SO
        If Not xSet.Tables("DaftarSO") Is Nothing Then xSet.Tables("DaftarSO").Clear()
        SQLquery = String.Format("SELECT NOSO, TANGGAL, SO.NOTA NOSP, MW.NAMA WILAYAH, MC.NAMA CUSTOMER, MS.NAMA SALES, CASE JNS_TRX WHEN 0 THEN 'CASH' ELSE 'KREDIT' END AS JENIS " & _
                                 "FROM TM_SO SO INNER JOIN MCUSTOMER MC ON SO.IDCUSTOMER=MC.IDCUSTOMER INNER JOIN MSALES MS ON SO.IDSALES=MS.IDSALES " & _
                                 "INNER JOIN MWILAYAH MW ON SO.IDWILAYAH=MW.IDWILAYAH WHERE SO.VOID=0 AND SO.ACC=1 OR SO.NOSO IN (SELECT NOSO FROM TM_PJ WHERE NOPJ='{0}')", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarSO")
        DaftarSO.Properties.DataSource = xSet.Tables("DaftarSO").DefaultView

        DaftarSO.Properties.NullText = ""
        DaftarSO.Properties.ValueMember = "NOSO"
        DaftarSO.Properties.DisplayMember = "NOSP"
        DaftarSO.Properties.ShowClearButton = False
        DaftarSO.Properties.PopulateViewColumns()

        ViewDaftarSO.Columns("NOSO").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarSO").Columns
            ViewDaftarSO.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar Gudang 
        If Not xSet.Tables("DaftarGudang") Is Nothing Then xSet.Tables("DaftarGudang").Clear()
        SQLquery = "SELECT IDGUDANG ID, NAMA FROM MGUDANG WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarGudang")
        DaftarGudang.DataSource = xSet.Tables("DaftarGudang").DefaultView

        DaftarGudang.NullText = ""
        DaftarGudang.ValueMember = "ID"
        DaftarGudang.DisplayMember = "NAMA"
        DaftarGudang.ShowClearButton = False
        DaftarGudang.PopulateViewColumns()

        ViewDaftarGudang.Columns("ID").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarGudang").Columns
            ViewDaftarGudang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------TD_PJ
        SQLquery = String.Format("SELECT TD.IDBARANG, MB.NAMA BARANG, TD.QTY1, MB.SATUAN1, TD.HARGA1, TD.QTY2, MB.SATUAN2, TD.HARGA2, DISC_PROSEN, DISC_RP, TOTAL, TD.IDGUDANG Gudang FROM TD_PJ TD INNER JOIN MBARANG MB ON TD.IDBARANG=MB.IDBARANG WHERE NOPJ='{0}' ORDER BY TD.NO_URUT;", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_PJ")
        GridTrans.DataSource = xSet.Tables("TD_PJ").DefaultView

        For Each coll As DataColumn In xSet.Tables("TD_PJ").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("IDBARANG").Visible = False
        ViewTrans.Columns("DISC_PROSEN").Caption = "DISC %"
        ViewTrans.Columns("DISC_RP").Caption = "DISC"
        ViewTrans.Columns("Gudang").ColumnEdit = DaftarGudang

        ViewTrans.Columns("IDBARANG").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("BARANG").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("QTY1").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("SATUAN1").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("HARGA1").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("QTY2").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("SATUAN2").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("HARGA2").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("DISC_PROSEN").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("DISC_RP").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("TOTAL").OptionsColumn.AllowEdit = False

        ViewTrans.Columns("QTY1").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("QTY1").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("QTY2").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("QTY2").DisplayFormat.FormatString = "n0"

        ViewTrans.Columns("HARGA1").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("HARGA1").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("HARGA2").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("HARGA2").DisplayFormat.FormatString = "n0"

        ViewTrans.Columns("DISC_PROSEN").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("DISC_PROSEN").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("DISC_RP").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("DISC_RP").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("TOTAL").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("TOTAL").DisplayFormat.FormatString = "n0"

        ViewTrans.Columns("BARANG").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("BARANG").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewTrans.Columns("QTY1").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("QTY1").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("QTY2").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("QTY2").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("TOTAL").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("TOTAL").SummaryItem.DisplayFormat = "Total:{0:n0}"

        ViewTrans.Columns("BARANG").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("QTY1").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("SATUAN1").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("HARGA1").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("QTY2").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("SATUAN2").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("HARGA2").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("DISC_PROSEN").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("DISC_RP").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("TOTAL").Width = GridTrans.Width * 0.125
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_PJ").Rows.Count < 1 Then
            Return False
        End If

        If DaftarSO.EditValue = Nothing Then
            DaftarSO.Focus()
            msgboxWarning("Nomor SO tidak boleh kosong")
            Return False
        End If

        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        If Trim(NoTransManual.Text).ToString.Length < 2 Then
            NoTransManual.Focus()
            msgboxWarning("Nomor PJ tidak boleh kurang dari 2 digit")
            Return False
        End If

        For Each drow As DataRow In xSet.Tables("TD_PJ").Rows
            If IsDBNull(drow.Item("Gudang")) Then
                msgboxWarning("Gudang pada daftar tidak boleh kosong")
                Return False
            End If
        Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "PJ", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NOPJ, 3)) AS KODE FROM TM_PJ WHERE LEFT(NOPJ, 9) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        Dim tgl As Date

        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        tgl = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function
End Class