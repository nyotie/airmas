﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SalesOrder
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Catatan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridTrans = New DevExpress.XtraGrid.GridControl()
        Me.ViewTrans = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButClear = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.TglTrans = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ACCcheck = New DevExpress.XtraEditors.CheckEdit()
        Me.NoTrans = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.DaftarSales = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewDaftarSales = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TambahBarang = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.DaftarWilayah = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewDaftarWilayah = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.NoTransManual = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.CASHCheck = New DevExpress.XtraEditors.CheckEdit()
        Me.DaftarCustomer = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewDaftarCustomer = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.HariJthTempo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridTrans, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewTrans, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglTrans.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ACCcheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NoTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarSales.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarSales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarWilayah.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarWilayah, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NoTransManual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CASHCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarCustomer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarCustomer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HariJthTempo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Catatan
        '
        Me.Catatan.Location = New System.Drawing.Point(12, 76)
        Me.Catatan.Name = "Catatan"
        Me.Catatan.Properties.MaxLength = 200
        Me.Catatan.Size = New System.Drawing.Size(846, 20)
        Me.Catatan.TabIndex = 9
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(663, 13)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl2.TabIndex = 12
        Me.LabelControl2.Text = "Customer :"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(163, 13)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl1.TabIndex = 13
        Me.LabelControl1.Text = "Tanggal :"
        '
        'GridTrans
        '
        Me.GridTrans.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridTrans.Location = New System.Drawing.Point(12, 102)
        Me.GridTrans.MainView = Me.ViewTrans
        Me.GridTrans.Name = "GridTrans"
        Me.GridTrans.Size = New System.Drawing.Size(960, 256)
        Me.GridTrans.TabIndex = 11
        Me.GridTrans.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewTrans})
        '
        'ViewTrans
        '
        Me.ViewTrans.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ViewTrans.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewTrans.ColumnPanelRowHeight = 40
        Me.ViewTrans.GridControl = Me.GridTrans
        Me.ViewTrans.Name = "ViewTrans"
        Me.ViewTrans.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTrans.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTrans.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTrans.OptionsCustomization.AllowColumnMoving = False
        Me.ViewTrans.OptionsCustomization.AllowColumnResizing = False
        Me.ViewTrans.OptionsCustomization.AllowFilter = False
        Me.ViewTrans.OptionsCustomization.AllowGroup = False
        Me.ViewTrans.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewTrans.OptionsCustomization.AllowSort = False
        Me.ViewTrans.OptionsView.ShowFooter = True
        Me.ViewTrans.OptionsView.ShowGroupPanel = False
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Location = New System.Drawing.Point(198, 400)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(87, 29)
        Me.ButBatal.TabIndex = 15
        Me.ButBatal.Text = "&Batal"
        '
        'ButClear
        '
        Me.ButClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButClear.Appearance.Options.UseFont = True
        Me.ButClear.Location = New System.Drawing.Point(105, 400)
        Me.ButClear.Name = "ButClear"
        Me.ButClear.Size = New System.Drawing.Size(87, 29)
        Me.ButClear.TabIndex = 14
        Me.ButClear.Text = "&Clear"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Location = New System.Drawing.Point(12, 400)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(87, 29)
        Me.ButSimpan.TabIndex = 13
        Me.ButSimpan.Text = "&Simpan"
        '
        'LineShape1
        '
        Me.LineShape1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 12
        Me.LineShape1.X2 = 970
        Me.LineShape1.Y1 = 390
        Me.LineShape1.Y2 = 390
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(984, 441)
        Me.ShapeContainer1.TabIndex = 0
        Me.ShapeContainer1.TabStop = False
        '
        'TglTrans
        '
        Me.TglTrans.EditValue = Nothing
        Me.TglTrans.Location = New System.Drawing.Point(163, 32)
        Me.TglTrans.Name = "TglTrans"
        Me.TglTrans.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TglTrans.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TglTrans.Properties.Mask.EditMask = "dd MMM yyyy, HH:mm"
        Me.TglTrans.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TglTrans.Size = New System.Drawing.Size(119, 20)
        Me.TglTrans.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(12, 57)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl3.TabIndex = 12
        Me.LabelControl3.Text = "Catatan :"
        '
        'ACCcheck
        '
        Me.ACCcheck.Location = New System.Drawing.Point(12, 364)
        Me.ACCcheck.Name = "ACCcheck"
        Me.ACCcheck.Properties.Caption = "ACC"
        Me.ACCcheck.Size = New System.Drawing.Size(47, 19)
        Me.ACCcheck.TabIndex = 12
        '
        'NoTrans
        '
        Me.NoTrans.Location = New System.Drawing.Point(12, 31)
        Me.NoTrans.Name = "NoTrans"
        Me.NoTrans.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoTrans.Properties.Appearance.Options.UseFont = True
        Me.NoTrans.Properties.ReadOnly = True
        Me.NoTrans.Size = New System.Drawing.Size(145, 20)
        Me.NoTrans.TabIndex = 1
        Me.NoTrans.TabStop = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl8.TabIndex = 12
        Me.LabelControl8.Text = "Kode transaksi :"
        '
        'DaftarSales
        '
        Me.DaftarSales.Location = New System.Drawing.Point(413, 31)
        Me.DaftarSales.Name = "DaftarSales"
        Me.DaftarSales.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarSales.Properties.NullText = ""
        Me.DaftarSales.Properties.View = Me.ViewDaftarSales
        Me.DaftarSales.Size = New System.Drawing.Size(119, 20)
        Me.DaftarSales.TabIndex = 4
        '
        'ViewDaftarSales
        '
        Me.ViewDaftarSales.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarSales.Name = "ViewDaftarSales"
        Me.ViewDaftarSales.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarSales.OptionsView.ShowGroupPanel = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(413, 12)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl4.TabIndex = 12
        Me.LabelControl4.Text = "Sales :"
        '
        'TambahBarang
        '
        Me.TambahBarang.Location = New System.Drawing.Point(897, 73)
        Me.TambahBarang.Name = "TambahBarang"
        Me.TambahBarang.Size = New System.Drawing.Size(75, 23)
        Me.TambahBarang.TabIndex = 10
        Me.TambahBarang.Text = "+Barang"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(538, 13)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl5.TabIndex = 12
        Me.LabelControl5.Text = "Wilayah :"
        '
        'DaftarWilayah
        '
        Me.DaftarWilayah.Location = New System.Drawing.Point(538, 32)
        Me.DaftarWilayah.Name = "DaftarWilayah"
        Me.DaftarWilayah.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarWilayah.Properties.NullText = ""
        Me.DaftarWilayah.Properties.View = Me.ViewDaftarWilayah
        Me.DaftarWilayah.Size = New System.Drawing.Size(119, 20)
        Me.DaftarWilayah.TabIndex = 5
        '
        'ViewDaftarWilayah
        '
        Me.ViewDaftarWilayah.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarWilayah.Name = "ViewDaftarWilayah"
        Me.ViewDaftarWilayah.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarWilayah.OptionsView.ShowGroupPanel = False
        '
        'NoTransManual
        '
        Me.NoTransManual.Location = New System.Drawing.Point(288, 32)
        Me.NoTransManual.Name = "NoTransManual"
        Me.NoTransManual.Properties.MaxLength = 15
        Me.NoTransManual.Size = New System.Drawing.Size(119, 20)
        Me.NoTransManual.TabIndex = 3
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(288, 13)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl6.TabIndex = 12
        Me.LabelControl6.Text = "Nomor SO :"
        '
        'CASHCheck
        '
        Me.CASHCheck.Location = New System.Drawing.Point(788, 32)
        Me.CASHCheck.Name = "CASHCheck"
        Me.CASHCheck.Properties.Caption = "CASH"
        Me.CASHCheck.Size = New System.Drawing.Size(47, 19)
        Me.CASHCheck.TabIndex = 7
        '
        'DaftarCustomer
        '
        Me.DaftarCustomer.Location = New System.Drawing.Point(663, 32)
        Me.DaftarCustomer.Name = "DaftarCustomer"
        Me.DaftarCustomer.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarCustomer.Properties.NullText = ""
        Me.DaftarCustomer.Properties.View = Me.ViewDaftarCustomer
        Me.DaftarCustomer.Size = New System.Drawing.Size(119, 20)
        Me.DaftarCustomer.TabIndex = 6
        '
        'ViewDaftarCustomer
        '
        Me.ViewDaftarCustomer.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarCustomer.Name = "ViewDaftarCustomer"
        Me.ViewDaftarCustomer.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarCustomer.OptionsView.ShowGroupPanel = False
        '
        'HariJthTempo
        '
        Me.HariJthTempo.EditValue = "7"
        Me.HariJthTempo.Location = New System.Drawing.Point(841, 32)
        Me.HariJthTempo.Name = "HariJthTempo"
        Me.HariJthTempo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.HariJthTempo.Properties.Mask.EditMask = "n0"
        Me.HariJthTempo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.HariJthTempo.Properties.MaxLength = 15
        Me.HariJthTempo.Size = New System.Drawing.Size(67, 20)
        Me.HariJthTempo.TabIndex = 8
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(841, 13)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl7.TabIndex = 12
        Me.LabelControl7.Text = "Jatuh tempo :"
        '
        'SalesOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 441)
        Me.Controls.Add(Me.HariJthTempo)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.DaftarCustomer)
        Me.Controls.Add(Me.CASHCheck)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.NoTransManual)
        Me.Controls.Add(Me.DaftarWilayah)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.TambahBarang)
        Me.Controls.Add(Me.DaftarSales)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.NoTrans)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.ACCcheck)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.TglTrans)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButClear)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.Catatan)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.GridTrans)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.Name = "SalesOrder"
        Me.ShowIcon = False
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridTrans, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewTrans, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglTrans.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ACCcheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NoTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarSales.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarSales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarWilayah.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarWilayah, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NoTransManual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CASHCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarCustomer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarCustomer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HariJthTempo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Catatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridTrans As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewTrans As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents TglTrans As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ACCcheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents NoTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DaftarSales As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewDaftarSales As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TambahBarang As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DaftarWilayah As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewDaftarWilayah As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents NoTransManual As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CASHCheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents DaftarCustomer As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewDaftarCustomer As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents HariJthTempo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
End Class
