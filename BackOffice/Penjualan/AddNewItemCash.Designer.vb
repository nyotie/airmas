﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddNewItemCash
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PeriodeHrg = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridBarang = New DevExpress.XtraGrid.GridControl()
        Me.ViewBarang = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GridTemp = New DevExpress.XtraGrid.GridControl()
        Me.ViewTemp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemSearchLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemSearchLookUpEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ButClear = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PeriodeHrg.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodeHrg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridBarang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewBarang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GridTemp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewTemp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSearchLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSearchLookUpEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PeriodeHrg
        '
        Me.PeriodeHrg.EditValue = Nothing
        Me.PeriodeHrg.Location = New System.Drawing.Point(100, 12)
        Me.PeriodeHrg.Name = "PeriodeHrg"
        Me.PeriodeHrg.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PeriodeHrg.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.PeriodeHrg.Properties.Mask.EditMask = "dd MMM yyyy"
        Me.PeriodeHrg.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.PeriodeHrg.Size = New System.Drawing.Size(119, 20)
        Me.PeriodeHrg.TabIndex = 21
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 15)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl1.TabIndex = 22
        Me.LabelControl1.Text = "Periode tanggal :"
        '
        'GridBarang
        '
        Me.GridBarang.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridBarang.Location = New System.Drawing.Point(0, 0)
        Me.GridBarang.MainView = Me.ViewBarang
        Me.GridBarang.Name = "GridBarang"
        Me.GridBarang.Size = New System.Drawing.Size(588, 294)
        Me.GridBarang.TabIndex = 16
        Me.GridBarang.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewBarang})
        '
        'ViewBarang
        '
        Me.ViewBarang.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ViewBarang.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewBarang.ColumnPanelRowHeight = 40
        Me.ViewBarang.GridControl = Me.GridBarang
        Me.ViewBarang.Name = "ViewBarang"
        Me.ViewBarang.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewBarang.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewBarang.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewBarang.OptionsBehavior.Editable = False
        Me.ViewBarang.OptionsCustomization.AllowColumnMoving = False
        Me.ViewBarang.OptionsCustomization.AllowFilter = False
        Me.ViewBarang.OptionsCustomization.AllowGroup = False
        Me.ViewBarang.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewBarang.OptionsFind.AlwaysVisible = True
        Me.ViewBarang.OptionsView.ShowFooter = True
        Me.ViewBarang.OptionsView.ShowGroupPanel = False
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Location = New System.Drawing.Point(12, 38)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridBarang)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.GridTemp)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(857, 294)
        Me.SplitContainerControl1.SplitterPosition = 588
        Me.SplitContainerControl1.TabIndex = 25
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GridTemp
        '
        Me.GridTemp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridTemp.Location = New System.Drawing.Point(0, 0)
        Me.GridTemp.MainView = Me.ViewTemp
        Me.GridTemp.Name = "GridTemp"
        Me.GridTemp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemSearchLookUpEdit1, Me.RepositoryItemSearchLookUpEdit2})
        Me.GridTemp.Size = New System.Drawing.Size(264, 294)
        Me.GridTemp.TabIndex = 17
        Me.GridTemp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewTemp})
        '
        'ViewTemp
        '
        Me.ViewTemp.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ViewTemp.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewTemp.ColumnPanelRowHeight = 40
        Me.ViewTemp.GridControl = Me.GridTemp
        Me.ViewTemp.Name = "ViewTemp"
        Me.ViewTemp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTemp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTemp.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTemp.OptionsBehavior.Editable = False
        Me.ViewTemp.OptionsCustomization.AllowColumnMoving = False
        Me.ViewTemp.OptionsCustomization.AllowFilter = False
        Me.ViewTemp.OptionsCustomization.AllowGroup = False
        Me.ViewTemp.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewTemp.OptionsView.ShowFooter = True
        Me.ViewTemp.OptionsView.ShowGroupPanel = False
        '
        'RepositoryItemSearchLookUpEdit1
        '
        Me.RepositoryItemSearchLookUpEdit1.AutoHeight = False
        Me.RepositoryItemSearchLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemSearchLookUpEdit1.Name = "RepositoryItemSearchLookUpEdit1"
        Me.RepositoryItemSearchLookUpEdit1.PopupFormSize = New System.Drawing.Size(700, 300)
        Me.RepositoryItemSearchLookUpEdit1.View = Me.GridView2
        '
        'GridView2
        '
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsCustomization.AllowColumnMoving = False
        Me.GridView2.OptionsCustomization.AllowColumnResizing = False
        Me.GridView2.OptionsCustomization.AllowFilter = False
        Me.GridView2.OptionsCustomization.AllowGroup = False
        Me.GridView2.OptionsCustomization.AllowQuickHideColumns = False
        Me.GridView2.OptionsCustomization.AllowSort = False
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'RepositoryItemSearchLookUpEdit2
        '
        Me.RepositoryItemSearchLookUpEdit2.AutoHeight = False
        Me.RepositoryItemSearchLookUpEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemSearchLookUpEdit2.Name = "RepositoryItemSearchLookUpEdit2"
        Me.RepositoryItemSearchLookUpEdit2.View = Me.GridView3
        '
        'GridView3
        '
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'ButClear
        '
        Me.ButClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButClear.Appearance.Options.UseFont = True
        Me.ButClear.Location = New System.Drawing.Point(782, 338)
        Me.ButClear.Name = "ButClear"
        Me.ButClear.Size = New System.Drawing.Size(87, 29)
        Me.ButClear.TabIndex = 24
        Me.ButClear.Text = "&Clear"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Location = New System.Drawing.Point(689, 338)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(87, 29)
        Me.ButSimpan.TabIndex = 23
        Me.ButSimpan.Text = "&Simpan"
        '
        'AddNewItemCash
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(881, 379)
        Me.Controls.Add(Me.PeriodeHrg)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Controls.Add(Me.ButClear)
        Me.Controls.Add(Me.ButSimpan)
        Me.Name = "AddNewItemCash"
        Me.Text = "AddNewItemCash"
        CType(Me.PeriodeHrg.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodeHrg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridBarang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewBarang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GridTemp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewTemp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSearchLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSearchLookUpEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PeriodeHrg As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridBarang As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewBarang As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents GridTemp As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewTemp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemSearchLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemSearchLookUpEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
End Class
