﻿Imports DevExpress.Utils

Public Class SalesCash
    Dim isChange As Boolean = False

    Private Sub ThisForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_CASH")
        xSet.Tables.Remove("TD_CASH")
        xSet.Tables.Remove("DaftarBarang2Satuan")

        AddNewItemSO = Nothing

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub ThisForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now
        Else
            SQLquery = String.Format("SELECT NOCASH, CUSTOMER, TANGGAL, MEMO, ACC FROM TM_CASH WHERE NOCASH='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_CASH")

            NoTrans.Text = xSet.Tables("TM_CASH").Rows(0).Item("NOCASH")
            TglTrans.EditValue = xSet.Tables("TM_CASH").Rows(0).Item("TANGGAL")
            TxtCustomer.Text = xSet.Tables("TM_CASH").Rows(0).Item("CUSTOMER")
            Catatan.EditValue = xSet.Tables("TM_CASH").Rows(0).Item("MEMO")
            ACCcheck.Checked = xSet.Tables("TM_CASH").Rows(0).Item("ACC")
        End If

    End Sub

    Private Sub TambahBarang_Click(sender As Object, e As EventArgs) Handles TambahBarang.Click
        ShowModule(AddNewItemCash, "Tambah barang")
    End Sub

    Private Sub ViewTrans_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans.CellValueChanged
        Try
            'TD.QTY1, TD.HARGA1, TD.QTY2, TD.HARGA2, DISC_PROSEN, DISC_RP, TOTAL
            If e.Column.FieldName = "QTY1" And ViewTrans.FocusedColumn.FieldName = "QTY1" Then
                If ViewTrans.GetFocusedRowCellValue("QTY1") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("QTY1", 0)
                ElseIf ViewTrans.GetFocusedRowCellValue("QTY1") > 0 Then
                    ViewTrans.SetFocusedRowCellValue("QTY2", 0)
                    isChange = True
                End If
            ElseIf e.Column.FieldName = "QTY2" And ViewTrans.FocusedColumn.FieldName = "QTY2" Then
                If ViewTrans.GetFocusedRowCellValue("QTY2") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("QTY2", 0)
                ElseIf ViewTrans.GetFocusedRowCellValue("QTY2") > 0 Then
                    ViewTrans.SetFocusedRowCellValue("QTY1", 0)
                    isChange = True
                End If
            ElseIf e.Column.FieldName = "DISC_PROSEN" And ViewTrans.FocusedColumn.FieldName = "DISC_PROSEN" Then
                If ViewTrans.GetFocusedRowCellValue("DISC_PROSEN") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("DISC_PROSEN", 0)
                ElseIf ViewTrans.GetFocusedRowCellValue("DISC_PROSEN") > 0 Then
                    ViewTrans.SetFocusedRowCellValue("DISC_RP", HitungDisc(ViewTrans.GetFocusedRowCellValue("DISC_PROSEN")))
                    isChange = True
                End If
            ElseIf e.Column.FieldName = "DISC_RP" And ViewTrans.FocusedColumn.FieldName = "DISC_RP" Then
                If ViewTrans.GetFocusedRowCellValue("DISC_RP") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("DISC_RP", 0)
                ElseIf ViewTrans.GetFocusedRowCellValue("DISC_RP") > 0 Then
                    ViewTrans.SetFocusedRowCellValue("DISC_PROSEN", 0)
                    isChange = True
                End If
            End If

            If isChange Then
                isChange = False
                ViewTrans.SetFocusedRowCellValue("TOTAL", HitungTotal(ViewTrans.GetFocusedRowCellValue("DISC_RP")))
            End If
        Catch ex As Exception
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("BARANG")) = True Then
                ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                xSet.Tables("TD_CASH").AcceptChanges()
            End If
        End Try
    End Sub

    Private Function HitungDisc(ByVal discp As Integer) As Integer
        Dim qtt, hrg As Integer
        If ViewTrans.GetFocusedRowCellValue("QTY1") = 0 Then
            qtt = ViewTrans.GetFocusedRowCellValue("QTY2")
            hrg = ViewTrans.GetFocusedRowCellValue("HARGA2")
        Else
            qtt = ViewTrans.GetFocusedRowCellValue("QTY1")
            hrg = ViewTrans.GetFocusedRowCellValue("HARGA1")
        End If

        Dim discn As Integer = (qtt * hrg) * (discp / 100)
        Return discn
    End Function

    Private Function HitungTotal(ByVal discn As Integer) As Integer
        Dim qtt, hrg As Integer
        If ViewTrans.GetFocusedRowCellValue("QTY1") = 0 Then
            qtt = ViewTrans.GetFocusedRowCellValue("QTY2")
            hrg = ViewTrans.GetFocusedRowCellValue("HARGA2")
        Else
            qtt = ViewTrans.GetFocusedRowCellValue("QTY1")
            hrg = ViewTrans.GetFocusedRowCellValue("HARGA1")
        End If

        Dim subtotal As Integer = qtt * hrg
        Return subtotal - discn
    End Function

    Private Sub GridTrans_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
            xSet.Tables("TD_CASH").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()
        ViewTrans.UpdateSummary()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC CASH_INS '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', """,
                                            NoTrans.Text, TxtCustomer.Text.Replace("'", "''"),
                                            Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"),
                                            Catatan.Text.Replace("'", "''"), ViewTrans.Columns("TOTAL").SummaryItem.SummaryValue,
                                            IIf(ACCcheck.Checked, 1, 0), staff_id)
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC CASH_UPD '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', """,
                                            NoTrans.Text, TxtCustomer.Text.Replace("'", "''"),
                                            Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"),
                                            Catatan.Text.Replace("'", "''"), ViewTrans.Columns("TOTAL").SummaryItem.SummaryValue,
                                            IIf(ACCcheck.Checked, 1, 0), staff_id)
            End If

            Dim nourut As Integer = 1
            'NOCASH, NO_URUT, IDBARANG, QTY1, QTY2, HARGA1, HARGA2, DISC_PROSEN, DISC_RP, TOTAL
            For Each drow As DataRow In xSet.Tables("TD_CASH").Rows
                SQLquery += String.Format("('{0}', {1}, '{2}', {3}, {4}, {5}, {6}, {7}, {8}, {9}), ",
                                          NoTrans.Text, nourut, drow.Item("IDBARANG"),
                                          drow.Item("QTY1"), drow.Item("QTY2"), drow.Item("HARGA1"), drow.Item("HARGA2"),
                                          drow.Item("DISC_PROSEN"), drow.Item("DISC_RP"), drow.Item("TOTAL"))
                nourut += 1
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserSalesCash1.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            'Using printout As New xrPengirimanCabang
            '    printout.ShowRibbonPreviewDialog()
            'End Using

            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_CASH").Clear()
        xSet.Tables("TD_CASH").AcceptChanges()

        TglTrans.EditValue = Now
        TxtCustomer.EditValue = ""
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------TD_CASH
        SQLquery = String.Format("SELECT TD.IDBARANG, MB.NAMA BARANG, TD.QTY1, MB.SATUAN1, TD.HARGA1, TD.QTY2, MB.SATUAN2, TD.HARGA2, DISC_PROSEN, DISC_RP, TOTAL FROM TD_CASH TD INNER JOIN MBARANG MB ON TD.IDBARANG=MB.IDBARANG WHERE NOCASH='{0}' ORDER BY TD.NO_URUT;", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_CASH")
        GridTrans.DataSource = xSet.Tables("TD_CASH").DefaultView

        For Each coll As DataColumn In xSet.Tables("TD_CASH").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("IDBARANG").Visible = False
        ViewTrans.Columns("DISC_PROSEN").Caption = "DISC %"
        ViewTrans.Columns("DISC_RP").Caption = "DISC"

        ViewTrans.Columns("IDBARANG").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("BARANG").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("SATUAN1").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("HARGA1").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("SATUAN2").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("HARGA2").OptionsColumn.AllowEdit = False

        ViewTrans.Columns("QTY1").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("QTY1").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("QTY2").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("QTY2").DisplayFormat.FormatString = "n0"

        ViewTrans.Columns("HARGA1").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("HARGA1").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("HARGA2").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("HARGA2").DisplayFormat.FormatString = "n0"

        ViewTrans.Columns("DISC_PROSEN").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("DISC_PROSEN").DisplayFormat.FormatString = "n2"
        ViewTrans.Columns("DISC_RP").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("DISC_RP").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("TOTAL").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("TOTAL").DisplayFormat.FormatString = "n0"

        ViewTrans.Columns("BARANG").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("BARANG").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewTrans.Columns("QTY1").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("QTY1").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("QTY2").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("QTY2").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("TOTAL").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("TOTAL").SummaryItem.DisplayFormat = "Total:{0:n0}"

        ViewTrans.Columns("BARANG").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("QTY1").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("SATUAN1").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("HARGA1").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("QTY2").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("SATUAN2").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("HARGA2").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("DISC_PROSEN").Width = GridTrans.Width * 0.075
        ViewTrans.Columns("DISC_RP").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("TOTAL").Width = GridTrans.Width * 0.125
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_CASH").Rows.Count < 1 Then
            Return False
        End If

        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If



        If Trim(TxtCustomer.Text).ToString.Length < 2 Then
            TxtCustomer.Focus()
            msgboxWarning("Nomor SP tidak boleh kurang dari 2 digit")
            Return False
        End If


        For Each drow As DataRow In xSet.Tables("TD_CASH").Rows
            If drow.Item("TOTAL") <= 0 Then
                msgboxWarning("Total harga barang tidak boleh kosong")
                Return False
            End If
        Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "CH", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NOCASH, 3)) AS KODE FROM TM_CASH WHERE LEFT(NOCASH, 9) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        Dim tgl As Date

        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        tgl = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function
End Class