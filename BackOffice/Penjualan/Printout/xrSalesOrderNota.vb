﻿Public Class xrSalesOrderNota

    Private Sub ThisPrintout_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        SQLquery = String.Format("SELECT TM.TANGGAL, MC.NAMA CUST, MC.ALAMAT, MC.KOTA, MS.NAMA SALES, TM.IDWILAYAH, TM.NOTA,TM.GRANDTOTAL, CASE TM.JNS_TRX WHEN 0 THEN 'CASH' ELSE 'KREDIT' END AS JNS " & _
            "FROM TM_SO TM INNER JOIN MCUSTOMER MC ON TM.IDCUSTOMER=MC.IDCUSTOMER INNER JOIN MSALES MS ON TM.IDSALES=MS.IDSALES WHERE TM.NOSO='{0}'", param_notrans.Value)
        ExDb.ExecQuery(SQLquery, xSet, "Get_DetailPrintOut")

        DATA_TGL.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("TANGGAL"), "dd-MM-yyyy HH:mm")
        DATA_NAMACUST.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("CUST")
        DATA_ALMCUST.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("ALAMAT")
        DATA_KOTACUST.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("KOTA")
        DATA_SALES.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("SALES")
        DATA_WIL.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("IDWILAYAH")
        DATA_NONOTA.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("NOTA")
        DATA_GT.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("GRANDTOTAL"), "n0")

        DA_TDSO_NOTA.Connection.ConnectionString = conn_string_local
        DA_TDSO_NOTA.Fill(Dataset_Printout1.PRT_TDSO_NOTA, param_notrans.Value)
        xSet.Tables.Remove("Get_DetailPrintOut")
    End Sub
End Class