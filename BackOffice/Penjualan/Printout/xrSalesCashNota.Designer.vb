﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class xrSalesCashNota
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.XrPivotGrid1 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.fieldNOCASH1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldCUSTOMER1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldBRG1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldSAT1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldQTT1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.Dataset_Printout1 = New BackOffice.Dataset_Printout()
        Me.param_tgl1 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.DA_TDSOCASH_NOTA1 = New BackOffice.Dataset_PrintoutTableAdapters.DA_TDSOCASH_NOTA()
        Me.param_tgl2 = New DevExpress.XtraReports.Parameters.Parameter()
        CType(Me.Dataset_Printout1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Dpi = 254.0!
        Me.Detail.HeightF = 0.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 50.00002!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 50.00002!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPivotGrid1})
        Me.PageHeader.Dpi = 254.0!
        Me.PageHeader.HeightF = 747.5833!
        Me.PageHeader.Name = "PageHeader"
        '
        'XrPivotGrid1
        '
        Me.XrPivotGrid1.Appearance.FieldHeader.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldHeader.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldValue.Font = New System.Drawing.Font("Times New Roman", 8.25!)
        Me.XrPivotGrid1.Appearance.FieldValue.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.FieldValue.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.FieldValueTotal.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.FieldValueTotal.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Appearance.TotalCell.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.XrPivotGrid1.Appearance.TotalCell.TextVerticalAlignment = DevExpress.Utils.VertAlignment.Center
        Me.XrPivotGrid1.Dpi = 254.0!
        Me.XrPivotGrid1.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.fieldNOCASH1, Me.fieldCUSTOMER1, Me.fieldBRG1, Me.fieldSAT1, Me.fieldQTT1})
        Me.XrPivotGrid1.LocationFloat = New DevExpress.Utils.PointFloat(25.00001!, 25.00001!)
        Me.XrPivotGrid1.Name = "XrPivotGrid1"
        Me.XrPivotGrid1.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid1.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid1.SizeF = New System.Drawing.SizeF(2644.0!, 722.5833!)
        '
        'fieldNOCASH1
        '
        Me.fieldNOCASH1.AreaIndex = 0
        Me.fieldNOCASH1.Caption = "NOCASH"
        Me.fieldNOCASH1.FieldName = "NOCASH"
        Me.fieldNOCASH1.Name = "fieldNOCASH1"
        Me.fieldNOCASH1.Options.ShowTotals = False
        '
        'fieldCUSTOMER1
        '
        Me.fieldCUSTOMER1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldCUSTOMER1.AreaIndex = 0
        Me.fieldCUSTOMER1.Caption = "CUSTOMER"
        Me.fieldCUSTOMER1.FieldName = "CUSTOMER"
        Me.fieldCUSTOMER1.Name = "fieldCUSTOMER1"
        Me.fieldCUSTOMER1.Options.ShowTotals = False
        Me.fieldCUSTOMER1.Width = 200
        '
        'fieldBRG1
        '
        Me.fieldBRG1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldBRG1.AreaIndex = 0
        Me.fieldBRG1.Caption = "BRG"
        Me.fieldBRG1.FieldName = "BRG"
        Me.fieldBRG1.Name = "fieldBRG1"
        Me.fieldBRG1.Options.ShowTotals = False
        '
        'fieldSAT1
        '
        Me.fieldSAT1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldSAT1.AreaIndex = 1
        Me.fieldSAT1.Caption = "SAT"
        Me.fieldSAT1.FieldName = "SAT"
        Me.fieldSAT1.Name = "fieldSAT1"
        Me.fieldSAT1.Options.ShowTotals = False
        '
        'fieldQTT1
        '
        Me.fieldQTT1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldQTT1.AreaIndex = 0
        Me.fieldQTT1.Caption = "Jml"
        Me.fieldQTT1.CellFormat.FormatString = "n0"
        Me.fieldQTT1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldQTT1.FieldName = "QTT"
        Me.fieldQTT1.Name = "fieldQTT1"
        Me.fieldQTT1.ValueFormat.FormatString = "n0"
        Me.fieldQTT1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        '
        'Dataset_Printout1
        '
        Me.Dataset_Printout1.DataSetName = "Dataset_Printout"
        Me.Dataset_Printout1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'param_tgl1
        '
        Me.param_tgl1.Description = "Tgl awal :"
        Me.param_tgl1.Name = "param_tgl1"
        Me.param_tgl1.Type = GetType(Date)
        Me.param_tgl1.Visible = False
        '
        'DA_TDSOCASH_NOTA1
        '
        Me.DA_TDSOCASH_NOTA1.ClearBeforeFill = True
        '
        'param_tgl2
        '
        Me.param_tgl2.Description = "Tgl akhir :"
        Me.param_tgl2.Name = "param_tgl2"
        Me.param_tgl2.Type = GetType(Date)
        Me.param_tgl2.Visible = False
        '
        'xrSalesCashNota
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader})
        Me.Bookmark = "Sales Cash"
        Me.DataMember = "PRT_TDSOCASH_NOTA"
        Me.DataSource = Me.Dataset_Printout1
        Me.DisplayName = "Sales Cash"
        Me.Dpi = 254.0!
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(50, 50, 50, 50)
        Me.PageHeight = 2159
        Me.PageWidth = 2794
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.param_tgl1, Me.param_tgl2})
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.SnapGridSize = 31.75!
        Me.Version = "14.2"
        CType(Me.Dataset_Printout1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrPivotGrid1 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents Dataset_Printout1 As BackOffice.Dataset_Printout
    Friend WithEvents param_tgl1 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents DA_TDSOCASH_NOTA1 As BackOffice.Dataset_PrintoutTableAdapters.DA_TDSOCASH_NOTA
    Friend WithEvents fieldNOCASH1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldCUSTOMER1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldBRG1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldSAT1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldQTT1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents param_tgl2 As DevExpress.XtraReports.Parameters.Parameter
End Class
