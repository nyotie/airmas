﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class xrPenjualanNota
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.Table1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.Dataset_Printout1 = New BackOffice.Dataset_Printout()
        Me.param_notrans = New DevExpress.XtraReports.Parameters.Parameter()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.DATA_TGLSO = New DevExpress.XtraReports.UI.XRLabel()
        Me.DATA_NONOTA = New DevExpress.XtraReports.UI.XRLabel()
        Me.DATA_WIL = New DevExpress.XtraReports.UI.XRLabel()
        Me.DATA_SALES = New DevExpress.XtraReports.UI.XRLabel()
        Me.DATA_KOTACUST = New DevExpress.XtraReports.UI.XRLabel()
        Me.DATA_ALMCUST = New DevExpress.XtraReports.UI.XRLabel()
        Me.DATA_NAMACUST = New DevExpress.XtraReports.UI.XRLabel()
        Me.DATA_TGL = New DevExpress.XtraReports.UI.XRLabel()
        Me.DATA_GT = New DevExpress.XtraReports.UI.XRLabel()
        Me.DA_TDPJ_NOTA = New BackOffice.Dataset_PrintoutTableAdapters.DA_TDPJ_NOTA()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dataset_Printout1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
        Me.Detail.Dpi = 254.0!
        Me.Detail.HeightF = 43.79166!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrTable1
        '
        Me.XrTable1.Dpi = 254.0!
        Me.XrTable1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(24.61486!, 0.0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(2019.781!, 43.79166!)
        Me.XrTable1.StylePriority.UseFont = False
        Me.XrTable1.StylePriority.UseTextAlignment = False
        Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell8, Me.XrTableCell3, Me.Table1, Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell6, Me.XrTableCell7, Me.XrTableCell10})
        Me.XrTableRow1.Dpi = 254.0!
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 0.5679012345679012R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "PRT_TDPJ_NOTA.BELI_JML", "{0:n0}")})
        Me.XrTableCell1.Dpi = 254.0!
        Me.XrTableCell1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.StylePriority.UseFont = False
        Me.XrTableCell1.StylePriority.UseTextAlignment = False
        Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell1.Weight = 0.0498592844348624R
        '
        'XrTableCell2
        '
        Me.XrTableCell2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "PRT_TDPJ_NOTA.BELI_SAT", "{0:n0}")})
        Me.XrTableCell2.Dpi = 254.0!
        Me.XrTableCell2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.StylePriority.UseFont = False
        Me.XrTableCell2.StylePriority.UseTextAlignment = False
        Me.XrTableCell2.Text = "XrTableCell2"
        Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell2.Weight = 0.06910834646686484R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Dpi = 254.0!
        Me.XrTableCell8.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.StylePriority.UseFont = False
        Me.XrTableCell8.StylePriority.UseTextAlignment = False
        Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableCell8.Weight = 0.01490540570538483R
        '
        'XrTableCell3
        '
        Me.XrTableCell3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "PRT_TDPJ_NOTA.NAMA")})
        Me.XrTableCell3.Dpi = 254.0!
        Me.XrTableCell3.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell3.Name = "XrTableCell3"
        Me.XrTableCell3.StylePriority.UseFont = False
        Me.XrTableCell3.Weight = 0.30229501006047393R
        '
        'Table1
        '
        Me.Table1.Dpi = 254.0!
        Me.Table1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Table1.Name = "Table1"
        Me.Table1.StylePriority.UseFont = False
        Me.Table1.StylePriority.UseTextAlignment = False
        Me.Table1.Text = "Table1"
        Me.Table1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.Table1.Visible = False
        Me.Table1.Weight = 0.048817447875528393R
        '
        'XrTableCell4
        '
        Me.XrTableCell4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "PRT_TDPJ_NOTA.INFO_KONV1", "@  {0:n0}")})
        Me.XrTableCell4.Dpi = 254.0!
        Me.XrTableCell4.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.StylePriority.UseFont = False
        Me.XrTableCell4.StylePriority.UseTextAlignment = False
        Me.XrTableCell4.Text = "XrTableCell4"
        Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell4.Weight = 0.094531180938653508R
        '
        'XrTableCell5
        '
        Me.XrTableCell5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "PRT_TDPJ_NOTA.INFO_KONV2")})
        Me.XrTableCell5.Dpi = 254.0!
        Me.XrTableCell5.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell5.Name = "XrTableCell5"
        Me.XrTableCell5.StylePriority.UseFont = False
        Me.XrTableCell5.StylePriority.UseTextAlignment = False
        Me.XrTableCell5.Text = "XrTableCell5"
        Me.XrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell5.Weight = 0.11490858373432755R
        '
        'XrTableCell6
        '
        Me.XrTableCell6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "PRT_TDPJ_NOTA.HRG", "{0:n2}")})
        Me.XrTableCell6.Dpi = 254.0!
        Me.XrTableCell6.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.StylePriority.UseFont = False
        Me.XrTableCell6.StylePriority.UseTextAlignment = False
        Me.XrTableCell6.Text = "XrTableCell6"
        Me.XrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell6.Weight = 0.17726242435229717R
        '
        'XrTableCell7
        '
        Me.XrTableCell7.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "PRT_TDPJ_NOTA.DISC_RP", "{0:n2}")})
        Me.XrTableCell7.Dpi = 254.0!
        Me.XrTableCell7.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.StylePriority.UseFont = False
        Me.XrTableCell7.StylePriority.UseTextAlignment = False
        Me.XrTableCell7.Text = "XrTableCell7"
        Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell7.Weight = 0.095217518848753319R
        '
        'XrTableCell10
        '
        Me.XrTableCell10.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "PRT_TDPJ_NOTA.TOTAL", "{0:n2}")})
        Me.XrTableCell10.Dpi = 254.0!
        Me.XrTableCell10.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell10.Name = "XrTableCell10"
        Me.XrTableCell10.StylePriority.UseFont = False
        Me.XrTableCell10.StylePriority.UseTextAlignment = False
        Me.XrTableCell10.Text = "XrTableCell10"
        Me.XrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell10.Weight = 0.24845909938277724R
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 30.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 35.58326!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'Dataset_Printout1
        '
        Me.Dataset_Printout1.DataSetName = "Dataset_Printout"
        Me.Dataset_Printout1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'param_notrans
        '
        Me.param_notrans.Description = "No. Transaksi"
        Me.param_notrans.Name = "param_notrans"
        Me.param_notrans.Visible = False
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.DATA_TGLSO, Me.DATA_NONOTA, Me.DATA_WIL, Me.DATA_SALES, Me.DATA_KOTACUST, Me.DATA_ALMCUST, Me.DATA_NAMACUST, Me.DATA_TGL})
        Me.PageHeader.Dpi = 254.0!
        Me.PageHeader.HeightF = 422.5209!
        Me.PageHeader.Name = "PageHeader"
        '
        'DATA_TGLSO
        '
        Me.DATA_TGLSO.Dpi = 254.0!
        Me.DATA_TGLSO.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DATA_TGLSO.LocationFloat = New DevExpress.Utils.PointFloat(162.3648!, 226.7292!)
        Me.DATA_TGLSO.Name = "DATA_TGLSO"
        Me.DATA_TGLSO.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DATA_TGLSO.SizeF = New System.Drawing.SizeF(254.0!, 50.0!)
        Me.DATA_TGLSO.StylePriority.UseFont = False
        Me.DATA_TGLSO.Text = "DATA_TGLSO"
        '
        'DATA_NONOTA
        '
        Me.DATA_NONOTA.Dpi = 254.0!
        Me.DATA_NONOTA.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DATA_NONOTA.LocationFloat = New DevExpress.Utils.PointFloat(971.8231!, 226.7292!)
        Me.DATA_NONOTA.Name = "DATA_NONOTA"
        Me.DATA_NONOTA.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DATA_NONOTA.SizeF = New System.Drawing.SizeF(254.0!, 50.0!)
        Me.DATA_NONOTA.StylePriority.UseFont = False
        Me.DATA_NONOTA.Text = "DATA_NONOTA"
        '
        'DATA_WIL
        '
        Me.DATA_WIL.Dpi = 254.0!
        Me.DATA_WIL.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DATA_WIL.LocationFloat = New DevExpress.Utils.PointFloat(773.3856!, 226.7292!)
        Me.DATA_WIL.Name = "DATA_WIL"
        Me.DATA_WIL.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DATA_WIL.SizeF = New System.Drawing.SizeF(198.4375!, 50.0!)
        Me.DATA_WIL.StylePriority.UseFont = False
        Me.DATA_WIL.StylePriority.UseTextAlignment = False
        Me.DATA_WIL.Text = "DATA_WIL"
        Me.DATA_WIL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'DATA_SALES
        '
        Me.DATA_SALES.Dpi = 254.0!
        Me.DATA_SALES.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DATA_SALES.LocationFloat = New DevExpress.Utils.PointFloat(24.61478!, 226.7292!)
        Me.DATA_SALES.Name = "DATA_SALES"
        Me.DATA_SALES.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DATA_SALES.SizeF = New System.Drawing.SizeF(137.7499!, 50.0!)
        Me.DATA_SALES.StylePriority.UseFont = False
        Me.DATA_SALES.StylePriority.UseTextAlignment = False
        Me.DATA_SALES.Text = "DATA_SALES"
        Me.DATA_SALES.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'DATA_KOTACUST
        '
        Me.DATA_KOTACUST.Dpi = 254.0!
        Me.DATA_KOTACUST.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DATA_KOTACUST.LocationFloat = New DevExpress.Utils.PointFloat(1491.417!, 213.5!)
        Me.DATA_KOTACUST.Name = "DATA_KOTACUST"
        Me.DATA_KOTACUST.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DATA_KOTACUST.SizeF = New System.Drawing.SizeF(552.9794!, 50.0!)
        Me.DATA_KOTACUST.StylePriority.UseFont = False
        Me.DATA_KOTACUST.StylePriority.UseTextAlignment = False
        Me.DATA_KOTACUST.Text = "DATA_KOTACUST"
        Me.DATA_KOTACUST.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'DATA_ALMCUST
        '
        Me.DATA_ALMCUST.CanGrow = False
        Me.DATA_ALMCUST.Dpi = 254.0!
        Me.DATA_ALMCUST.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DATA_ALMCUST.LocationFloat = New DevExpress.Utils.PointFloat(1491.417!, 126.7293!)
        Me.DATA_ALMCUST.Name = "DATA_ALMCUST"
        Me.DATA_ALMCUST.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DATA_ALMCUST.SizeF = New System.Drawing.SizeF(552.9794!, 86.77077!)
        Me.DATA_ALMCUST.StylePriority.UseFont = False
        Me.DATA_ALMCUST.StylePriority.UseTextAlignment = False
        Me.DATA_ALMCUST.Text = "DATA_ALMCUST"
        Me.DATA_ALMCUST.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'DATA_NAMACUST
        '
        Me.DATA_NAMACUST.CanGrow = False
        Me.DATA_NAMACUST.Dpi = 254.0!
        Me.DATA_NAMACUST.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DATA_NAMACUST.LocationFloat = New DevExpress.Utils.PointFloat(1491.417!, 76.72925!)
        Me.DATA_NAMACUST.Name = "DATA_NAMACUST"
        Me.DATA_NAMACUST.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DATA_NAMACUST.SizeF = New System.Drawing.SizeF(552.9794!, 50.0!)
        Me.DATA_NAMACUST.StylePriority.UseFont = False
        Me.DATA_NAMACUST.StylePriority.UseTextAlignment = False
        Me.DATA_NAMACUST.Text = "DATA_NAMACUST"
        Me.DATA_NAMACUST.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'DATA_TGL
        '
        Me.DATA_TGL.Dpi = 254.0!
        Me.DATA_TGL.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DATA_TGL.LocationFloat = New DevExpress.Utils.PointFloat(1491.417!, 0.0!)
        Me.DATA_TGL.Name = "DATA_TGL"
        Me.DATA_TGL.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DATA_TGL.SizeF = New System.Drawing.SizeF(552.9794!, 50.0!)
        Me.DATA_TGL.StylePriority.UseFont = False
        Me.DATA_TGL.Text = "DATA_TGL"
        '
        'DATA_GT
        '
        Me.DATA_GT.Dpi = 254.0!
        Me.DATA_GT.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DATA_GT.LocationFloat = New DevExpress.Utils.PointFloat(1631.718!, 25.00001!)
        Me.DATA_GT.Name = "DATA_GT"
        Me.DATA_GT.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DATA_GT.SizeF = New System.Drawing.SizeF(412.6782!, 49.99995!)
        Me.DATA_GT.StylePriority.UseFont = False
        Me.DATA_GT.StylePriority.UseTextAlignment = False
        Me.DATA_GT.Text = "DATA_GT"
        Me.DATA_GT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'DA_TDPJ_NOTA
        '
        Me.DA_TDPJ_NOTA.ClearBeforeFill = True
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.DATA_GT})
        Me.PageFooter.Dpi = 254.0!
        Me.PageFooter.HeightF = 170.7917!
        Me.PageFooter.Name = "PageFooter"
        '
        'xrPenjualanNota
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.PageFooter})
        Me.DataMember = "PRT_TDPJ_NOTA"
        Me.DataSource = Me.Dataset_Printout1
        Me.Dpi = 254.0!
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(20, 13, 30, 36)
        Me.PageHeight = 1350
        Me.PageWidth = 2150
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.param_notrans})
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.ShowPrintMarginsWarning = False
        Me.SnapGridSize = 31.75!
        Me.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.Version = "14.2"
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dataset_Printout1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents Dataset_Printout1 As BackOffice.Dataset_Printout
    Friend WithEvents param_notrans As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents DATA_NONOTA As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DATA_WIL As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DATA_SALES As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DATA_KOTACUST As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DATA_ALMCUST As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DATA_NAMACUST As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DATA_TGL As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DATA_GT As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DATA_TGLSO As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DA_TDPJ_NOTA As BackOffice.Dataset_PrintoutTableAdapters.DA_TDPJ_NOTA
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents Table1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
End Class
