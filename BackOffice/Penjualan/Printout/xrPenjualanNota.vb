﻿Public Class xrPenjualanNota

    Private Sub ThisPrintout_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        SQLquery = String.Format("SELECT TM.TANGGAL, MC.NAMA CUST, MC.ALAMAT, MC.KOTA, SO.IDSALES SALES, SO.TANGGAL TGLSO, MW.GRUP IDWILAYAH, TM.NOTA, TM.GRANDTOTAL, CASE SO.JNS_TRX WHEN 0 THEN 'CASH' ELSE 'KREDIT' END AS JNS " & _
            "FROM TM_PJ TM INNER JOIN TM_SO SO ON TM.NOSO = SO.NOSO INNER JOIN MCUSTOMER MC ON SO.IDCUSTOMER=MC.IDCUSTOMER INNER JOIN MWILAYAH MW ON SO.IDWILAYAH = MW.IDWILAYAH WHERE TM.NOPJ='{0}'", param_notrans.Value)
        ExDb.ExecQuery(SQLquery, xSet, "Get_DetailPrintOut")

        My.Application.ChangeCulture("id-ID")
        DATA_TGL.Text = UCase(Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("TANGGAL"), "dd-MMMM-yyyy"))
        DATA_NAMACUST.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("CUST")
        DATA_ALMCUST.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("ALAMAT")
        DATA_KOTACUST.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("KOTA")
        DATA_SALES.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("SALES")
        DATA_TGLSO.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("TGLSO"), "ddMMyy")
        DATA_WIL.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("IDWILAYAH")
        DATA_NONOTA.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("NOTA")
        DATA_GT.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("GRANDTOTAL"), "n2")
        Table1.Text = "@"

        DA_TDPJ_NOTA.Connection.ConnectionString = conn_string_local
        DA_TDPJ_NOTA.Fill(Dataset_Printout1.PRT_TDPJ_NOTA, param_notrans.Value)
        xSet.Tables.Remove("Get_DetailPrintOut")
        My.Application.ChangeCulture("en-US")
    End Sub

    Private Sub XrTableCell2_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrTableCell2.BeforePrint
        'MsgBox(GetCurrentColumnValue("BELI_JML"))
        'MsgBox(GetCurrentColumnValue("INFO_KONV1"))
        'iki kon serius, mbandingno jumlah ambek info konversi? iyo
        If GetCurrentColumnValue("BELI_SAT") = GetCurrentColumnValue("INFO_KONV2") Then
            XrTableCell4.Text = ""
            XrTableCell5.Text = ""
            Table1.Text = ""
        Else
            Table1.Text = "@"
        End If
    End Sub
End Class