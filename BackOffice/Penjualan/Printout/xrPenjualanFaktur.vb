﻿Public Class xrPenjualanFaktur

    Private Sub xrPenjualanFaktur_AfterPrint(sender As Object, e As EventArgs) Handles Me.AfterPrint
        If param_pajak.Value <> "" Then
            SQLquery = "UPDATE TM_PJ SET NOPJK ='" & param_pajak.Value & "' WHERE NOPJ='" & param_notrans.Value & "'"
            ExDb.ExecData(SQLquery)
        End If
    End Sub

    Private Sub ThisPrintout_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        SQLquery = String.Format("SELECT TM.TANGGAL, CASE MC.NAMA_NPWP WHEN '' THEN MC.NAMA ELSE MC.NAMA_NPWP END AS NAMA_NPWP, " & _
                                 "CASE MC.ALAMAT_NPWP WHEN '' THEN MC.ALAMAT + ', ' + MC.KOTA ELSE MC.ALAMAT_NPWP END AS ALAMAT_NPWP, " & _
                                 "CASE MC.NPWP WHEN '' THEN '00.000.000.0-000.000' ELSE MC.NPWP END AS NPWP, MC.KOTA, MS.NAMA SALES, SO.TANGGAL TGLSO, MW.GRUP IDWILAYAH, TM.NOTA, (TM.GRANDTOTAL / 1.1) GRANDTOTAL, " & _
                                 "CASE SO.JNS_TRX WHEN 0 THEN 'CASH' ELSE 'KREDIT' END AS JNS " & _
            "FROM TM_PJ TM INNER JOIN TM_SO SO ON TM.NOSO = SO.NOSO INNER JOIN MCUSTOMER MC ON SO.IDCUSTOMER=MC.IDCUSTOMER " & _
            "INNER JOIN MSALES MS ON SO.IDSALES=MS.IDSALES INNER JOIN MWILAYAH MW ON SO.IDWILAYAH = MW.IDWILAYAH " & _
            " WHERE TM.NOPJ='{0}'", param_notrans.Value)
        ExDb.ExecQuery(SQLquery, xSet, "Get_DetailPrintOut")

        My.Application.ChangeCulture("id-ID")

        LblKodePajak.Text = param_pajak.Value
        LblNama.Text = "Nama :    " & param_nama.Value

        Bendel.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("IDWILAYAH")
        NoNota.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("NOTA")
        'HRG.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("HRG")

        NamaPKP.Text = xSet.Tables("DataCompany").Rows(0).Item("NAMA")
        AlamatPKP.Text = xSet.Tables("DataCompany").Rows(0).Item("ALAMAT")
        NpwpPKP.Text = xSet.Tables("DataCompany").Rows(0).Item("NPWP")

        NamaPBKP.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("NAMA_NPWP")
        AlamatPBKP.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("ALAMAT_NPWP")
        NpwpPBKP.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("NPWP")

        GT_1.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("GRANDTOTAL"), "n2")
        GT_2.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("GRANDTOTAL"), "n2")
        PPNValue.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("GRANDTOTAL") * 0.1, "n2")
        'KotaTanggal.Text = String.Format("{0}, {1}", "Surabaya", Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("TANGGAL"), "dd MMMM yyyy"))
        KotaTanggal.Text = String.Format("{0}, {1}", "SURABAYA", UCase(Format(param_tgl.Value, "dd MMMM yyyy")))


        DA_TDPJ_NOTA_FAKTUR.Connection.ConnectionString = conn_string_local
        DA_TDPJ_NOTA_FAKTUR.Fill(Dataset_Printout1.PRT_TDPJ_NOTA_FAKTUR, param_notrans.Value)
        xSet.Tables.Remove("Get_DetailPrintOut")
        My.Application.ChangeCulture("en-US")
    End Sub

    Private Sub XrLabel6_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrLabel6.BeforePrint
       
    End Sub

    Private Sub XrLabel43_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrLabel43.BeforePrint
        'MsgBox(GetCurrentColumnValue("BELI_SAT"))
        'MsgBox(GetCurrentColumnValue("INFO_KONV2"))
        If GetCurrentColumnValue("BELI_SAT") = GetCurrentColumnValue("INFO_KONV2") Then
            XrLabel44.Text = ""
            XrLabel45.Text = ""
            XrLabel3.Text = ""
        Else
            XrLabel3.Text = "@"
        End If
    End Sub
End Class