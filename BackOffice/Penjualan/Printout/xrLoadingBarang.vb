﻿Public Class xrLoadingBarang

    Private Sub ThisPrintout_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint

        SQLquery = String.Format("SELECT TANGGAL, IDKENDARAAN, SUPIR, MEMO, GRUP FROM TM_DO WHERE NODO='{0}'", param_notrans.Value)
        ExDb.ExecQuery(SQLquery, xSet, "Get_DetailPrintOut")

        NoTrans.Text = param_notrans.Value
        TglTrans.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("TANGGAL"), "dd-MM-yyyy HH:mm")
        NopolKendaraan.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("IDKENDARAAN")
        SupirKendaraan.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("SUPIR")

        DA_LOADBRG.Connection.ConnectionString = conn_string_local
        DA_LOADBRG.Fill(Dataset_Printout1.PRT_LOADBRG, param_notrans.Value)
        xSet.Tables.Remove("Get_DetailPrintOut")
    End Sub

End Class