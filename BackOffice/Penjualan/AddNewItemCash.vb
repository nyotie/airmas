﻿Imports DevExpress.Utils
Public Class AddNewItemCash
    'Private Sub ThisBrowser_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
    '    On Error Resume Next
    '    xSet.Tables.Remove("DaftarBarang2Satuan")

    '    GC.Collect()
    '    GC.WaitForPendingFinalizers()
    'End Sub

    Private Sub ThisBrowser_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        PeriodeHrg.Focus()
    End Sub

    Private Sub ThisBrowser_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        PeriodeHrg.EditValue = Today.Date

        Try
            GridTemp.DataSource = xSet.Tables("TD_CASH").DefaultView

            ViewTemp.Columns("IDBARANG").Visible = False
            ViewTemp.Columns("QTY1").Visible = False
            ViewTemp.Columns("SATUAN1").Visible = False
            ViewTemp.Columns("QTY2").Visible = False
            ViewTemp.Columns("SATUAN2").Visible = False
            ViewTemp.Columns("DISC_PROSEN").Visible = False
            ViewTemp.Columns("DISC_RP").Visible = False
            ViewTemp.Columns("TOTAL").Visible = False
            ViewTemp.Columns("HARGA1").DisplayFormat.FormatType = FormatType.Numeric
            ViewTemp.Columns("HARGA1").DisplayFormat.FormatString = "n0"
            ViewTemp.Columns("HARGA2").DisplayFormat.FormatType = FormatType.Numeric
            ViewTemp.Columns("HARGA2").DisplayFormat.FormatString = "n0"
        Catch ex As Exception
            msgboxWarning("Grid daftar Cash belum tercipta!")
            Close()
        End Try
    End Sub

    Private Sub PeriodeHrg_EditValueChanged(sender As Object, e As EventArgs) Handles PeriodeHrg.EditValueChanged
        If PeriodeHrg.EditValue Is Nothing Or PeriodeHrg.Enabled = False Then Return
        '----------------------------------------------------Daftar Barang 2 satuan 
        If Not xSet.Tables("DaftarBarang2Satuan") Is Nothing Then xSet.Tables("DaftarBarang2Satuan").Clear()
        SQLquery = String.Format("EXEC [GET_DAFTARBARANG] '{0}'", Format(PeriodeHrg.EditValue, "yyyy-MM-dd")) 'jual
        ExDb.ExecQuery(SQLquery, xSet, "DaftarBarang2Satuan")
        GridBarang.DataSource = xSet.Tables("DaftarBarang2Satuan").DefaultView

        ViewBarang.Columns("ID").Visible = False
        ViewBarang.Columns("HARGA1").DisplayFormat.FormatType = FormatType.Numeric
        ViewBarang.Columns("HARGA1").DisplayFormat.FormatString = "n0"
        ViewBarang.Columns("HARGA2").DisplayFormat.FormatType = FormatType.Numeric
        ViewBarang.Columns("HARGA2").DisplayFormat.FormatString = "n0"

        For Each coll As DataColumn In xSet.Tables("DaftarBarang2Satuan").Columns
            ViewBarang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Sub ViewBarang_DoubleClick(sender As Object, e As EventArgs) Handles ViewBarang.DoubleClick
        If xSet.Tables("DaftarBarang2Satuan").Rows.Count > 0 And ViewBarang.FocusedRowHandle >= 0 Then
            Try
                'uncomment to prevent double item
                'If xSet.Tables("TD_CASH").Select("IDBARANG='" & ViewBarang.GetFocusedRowCellValue("ID") & "'").Length > 0 Then Return

                xSet.Tables("TD_CASH").Rows.Add(ViewBarang.GetFocusedRowCellValue("ID"), ViewBarang.GetFocusedRowCellValue("NAMA"),
                                              0, ViewBarang.GetFocusedRowCellValue("SATUAN1"), ViewBarang.GetFocusedRowCellValue("HARGA1"),
                                              0, ViewBarang.GetFocusedRowCellValue("SATUAN2"), ViewBarang.GetFocusedRowCellValue("HARGA2"),
                                              0, 0, 0)
            Catch ex As Exception
                msgboxErrorDev()
            End Try
        End If
    End Sub

    Private Sub GridTemp_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTemp.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTemp.DeleteRow(ViewTemp.FocusedRowHandle)
            xSet.Tables("TD_CASH").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(sender As Object, e As EventArgs) Handles ButSimpan.Click
        Close()
    End Sub

    Private Sub ButClear_Click(sender As Object, e As EventArgs) Handles ButClear.Click
        If xSet.Tables("TD_CASH").Rows.Count > 0 Then
            If MsgBox("Yakin ingin mengosongi tabel daftar?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return
        End If

        xSet.Tables("TD_CASH").Clear()
        xSet.Tables("TD_CASH").AcceptChanges()
    End Sub
End Class