﻿Public Class ModVoid
    Public nama_trans As String
    Public ReallyVoid As Boolean

    Private Sub ModVoid_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        NoTrans.EditValue = selected_no
        NamaTrans.Text = nama_trans

        KetVoid.Focus()
    End Sub

    Private Sub ButOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButOK.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()

        Try
            SQLquery = String.Format("EXEC module_void {0}, {1}, {2}, '{3}', {4}", id_jenistrans, selected_id, KetVoid.Text, staff_id)
            ExDb.ExecData(SQLquery)

            ReallyVoid = True
            msgboxInformation("Data berhasil divoid, tekan OK untuk melanjutkan.")
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()
        End Try
        Close()
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        ReallyVoid = False
        Close()
    End Sub

    Private Function beforeSave() As Boolean
        If KetVoid.Text.Contains("'") Then
            msgboxWarning("Huruf (') tidak dapat diterima")
            Return False
        End If

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function
End Class