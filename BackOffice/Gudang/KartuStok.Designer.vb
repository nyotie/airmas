﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class KartuStok
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MainControl = New DevExpress.XtraEditors.PanelControl()
        Me.RefreshData = New DevExpress.XtraEditors.SimpleButton()
        Me.DaftarGudang = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewDaftarGudang = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.butPrint = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.butHistory = New DevExpress.XtraEditors.SimpleButton()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.HistoryControl = New DevExpress.XtraEditors.PanelControl()
        Me.NamaBrgHistory = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.butBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.butCari = New DevExpress.XtraEditors.SimpleButton()
        Me.tglPencarian = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridStok = New DevExpress.XtraGrid.GridControl()
        Me.ViewStok = New DevExpress.XtraGrid.Views.Grid.GridView()
        CType(Me.MainControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainControl.SuspendLayout()
        CType(Me.DaftarGudang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarGudang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HistoryControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.HistoryControl.SuspendLayout()
        CType(Me.NamaBrgHistory.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tglPencarian.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tglPencarian.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridStok, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewStok, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainControl
        '
        Me.MainControl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MainControl.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.MainControl.Appearance.Options.UseBackColor = True
        Me.MainControl.Controls.Add(Me.RefreshData)
        Me.MainControl.Controls.Add(Me.DaftarGudang)
        Me.MainControl.Controls.Add(Me.butPrint)
        Me.MainControl.Controls.Add(Me.LabelControl3)
        Me.MainControl.Controls.Add(Me.butHistory)
        Me.MainControl.Controls.Add(Me.butClose)
        Me.MainControl.Location = New System.Drawing.Point(12, 381)
        Me.MainControl.Name = "MainControl"
        Me.MainControl.Size = New System.Drawing.Size(739, 48)
        Me.MainControl.TabIndex = 0
        '
        'RefreshData
        '
        Me.RefreshData.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefreshData.Appearance.Options.UseFont = True
        Me.RefreshData.Location = New System.Drawing.Point(206, 5)
        Me.RefreshData.Name = "RefreshData"
        Me.RefreshData.Size = New System.Drawing.Size(120, 39)
        Me.RefreshData.TabIndex = 1
        Me.RefreshData.Text = "Refresh Data"
        '
        'DaftarGudang
        '
        Me.DaftarGudang.EditValue = ""
        Me.DaftarGudang.Location = New System.Drawing.Point(81, 14)
        Me.DaftarGudang.Name = "DaftarGudang"
        Me.DaftarGudang.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarGudang.Properties.NullText = ""
        Me.DaftarGudang.Properties.View = Me.ViewDaftarGudang
        Me.DaftarGudang.Size = New System.Drawing.Size(119, 20)
        Me.DaftarGudang.TabIndex = 0
        '
        'ViewDaftarGudang
        '
        Me.ViewDaftarGudang.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarGudang.Name = "ViewDaftarGudang"
        Me.ViewDaftarGudang.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarGudang.OptionsView.ShowGroupPanel = False
        '
        'butPrint
        '
        Me.butPrint.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butPrint.Appearance.Options.UseFont = True
        Me.butPrint.Location = New System.Drawing.Point(458, 5)
        Me.butPrint.Name = "butPrint"
        Me.butPrint.Size = New System.Drawing.Size(109, 39)
        Me.butPrint.TabIndex = 3
        Me.butPrint.Text = "Print Tabel"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(17, 16)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Gudang :"
        '
        'butHistory
        '
        Me.butHistory.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butHistory.Appearance.Options.UseFont = True
        Me.butHistory.Location = New System.Drawing.Point(332, 5)
        Me.butHistory.Name = "butHistory"
        Me.butHistory.Size = New System.Drawing.Size(120, 39)
        Me.butHistory.TabIndex = 2
        Me.butHistory.Text = "Lihat History"
        '
        'butClose
        '
        Me.butClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butClose.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Appearance.Options.UseFont = True
        Me.butClose.Location = New System.Drawing.Point(643, 5)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(84, 39)
        Me.butClose.TabIndex = 4
        Me.butClose.Text = "Tutup"
        '
        'HistoryControl
        '
        Me.HistoryControl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.HistoryControl.Controls.Add(Me.NamaBrgHistory)
        Me.HistoryControl.Controls.Add(Me.LabelControl4)
        Me.HistoryControl.Controls.Add(Me.butBatal)
        Me.HistoryControl.Controls.Add(Me.butCari)
        Me.HistoryControl.Controls.Add(Me.tglPencarian)
        Me.HistoryControl.Controls.Add(Me.LabelControl1)
        Me.HistoryControl.Location = New System.Drawing.Point(12, 12)
        Me.HistoryControl.Name = "HistoryControl"
        Me.HistoryControl.Size = New System.Drawing.Size(739, 65)
        Me.HistoryControl.TabIndex = 2
        Me.HistoryControl.Visible = False
        '
        'NamaBrgHistory
        '
        Me.NamaBrgHistory.Location = New System.Drawing.Point(139, 36)
        Me.NamaBrgHistory.Name = "NamaBrgHistory"
        Me.NamaBrgHistory.Properties.ReadOnly = True
        Me.NamaBrgHistory.Size = New System.Drawing.Size(227, 20)
        Me.NamaBrgHistory.TabIndex = 1
        Me.NamaBrgHistory.TabStop = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(39, 38)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(94, 14)
        Me.LabelControl4.TabIndex = 6
        Me.LabelControl4.Text = "Nama barang :"
        '
        'butBatal
        '
        Me.butBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBatal.Appearance.Options.UseFont = True
        Me.butBatal.Location = New System.Drawing.Point(442, 14)
        Me.butBatal.Name = "butBatal"
        Me.butBatal.Size = New System.Drawing.Size(64, 39)
        Me.butBatal.TabIndex = 3
        Me.butBatal.Text = "Batal"
        '
        'butCari
        '
        Me.butCari.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCari.Appearance.Options.UseFont = True
        Me.butCari.Location = New System.Drawing.Point(372, 14)
        Me.butCari.Name = "butCari"
        Me.butCari.Size = New System.Drawing.Size(64, 39)
        Me.butCari.TabIndex = 2
        Me.butCari.Text = "Cari"
        '
        'tglPencarian
        '
        Me.tglPencarian.EditValue = Nothing
        Me.tglPencarian.Location = New System.Drawing.Point(139, 8)
        Me.tglPencarian.Name = "tglPencarian"
        Me.tglPencarian.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tglPencarian.Properties.Appearance.Options.UseFont = True
        Me.tglPencarian.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tglPencarian.Properties.Mask.EditMask = "dd MMM yyyy"
        Me.tglPencarian.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tglPencarian.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.tglPencarian.Size = New System.Drawing.Size(227, 21)
        Me.tglPencarian.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(8, 11)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(125, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Tanggal Pencarian :"
        '
        'GridStok
        '
        Me.GridStok.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridStok.Location = New System.Drawing.Point(12, 12)
        Me.GridStok.MainView = Me.ViewStok
        Me.GridStok.Name = "GridStok"
        Me.GridStok.Size = New System.Drawing.Size(739, 363)
        Me.GridStok.TabIndex = 1
        Me.GridStok.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewStok})
        '
        'ViewStok
        '
        Me.ViewStok.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 11.25!)
        Me.ViewStok.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewStok.Appearance.Row.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.ViewStok.Appearance.Row.Options.UseFont = True
        Me.ViewStok.ColumnPanelRowHeight = 40
        Me.ViewStok.GridControl = Me.GridStok
        Me.ViewStok.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways
        Me.ViewStok.Name = "ViewStok"
        Me.ViewStok.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewStok.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewStok.OptionsBehavior.Editable = False
        Me.ViewStok.OptionsCustomization.AllowColumnResizing = False
        Me.ViewStok.OptionsCustomization.AllowFilter = False
        Me.ViewStok.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewStok.OptionsView.EnableAppearanceEvenRow = True
        Me.ViewStok.OptionsView.ShowAutoFilterRow = True
        Me.ViewStok.OptionsView.ShowFooter = True
        Me.ViewStok.OptionsView.ShowGroupPanel = False
        '
        'KartuStok
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(763, 441)
        Me.Controls.Add(Me.MainControl)
        Me.Controls.Add(Me.HistoryControl)
        Me.Controls.Add(Me.GridStok)
        Me.Name = "KartuStok"
        Me.Text = "KartuStok"
        CType(Me.MainControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainControl.ResumeLayout(False)
        Me.MainControl.PerformLayout()
        CType(Me.DaftarGudang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarGudang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HistoryControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.HistoryControl.ResumeLayout(False)
        Me.HistoryControl.PerformLayout()
        CType(Me.NamaBrgHistory.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tglPencarian.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tglPencarian.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridStok, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewStok, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MainControl As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butPrint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butHistory As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DaftarGudang As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewDaftarGudang As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RefreshData As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents HistoryControl As DevExpress.XtraEditors.PanelControl
    Friend WithEvents NamaBrgHistory As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents butBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butCari As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tglPencarian As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridStok As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewStok As DevExpress.XtraGrid.Views.Grid.GridView
End Class
