﻿Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Columns

Public Class KartuStok
    Dim lucky_one As String

    Private Sub StokBrowser_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarGudang")
        xSet.Tables.Remove("DataStock")
        xSet.Tables.Remove("DataHistory")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub KartuStok_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Setup_Data()
        DaftarGudang.EditValue = xSet.Tables("DaftarGudang").Select("UTAMA=1")(0).Item("IDGUDANG")
        tujku_Stock()
        AddHandler ViewStok.DoubleClick, AddressOf butHistory_Click
    End Sub

    Private Sub RefreshData_Click(sender As Object, e As EventArgs) Handles RefreshData.Click
        GetDataKartuStok(False)
    End Sub

    Private Sub butHistory_Click(sender As Object, e As EventArgs) Handles butHistory.Click
        If butHistory.Enabled = False Then Return
        lucky_one = ViewStok.GetFocusedRowCellValue("IDBARANG")
        NamaBrgHistory.EditValue = ViewStok.GetFocusedRowCellValue("ALIAS")

        GetDataKartuStok(True)
    End Sub

    Private Sub butPrint_Click(sender As Object, e As EventArgs) Handles butPrint.Click
        ViewStok.ShowRibbonPrintPreview()
    End Sub

    Private Sub butCari_Click(sender As Object, e As EventArgs) Handles butCari.Click
        tujku_History()
    End Sub

    Private Sub butBatal_Click(sender As Object, e As EventArgs) Handles butBatal.Click
        GetDataKartuStok(False)
    End Sub

    Private Sub butClose_Click(sender As Object, e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------Daftar Cabang
        If Not xSet.Tables("DaftarGudang") Is Nothing Then xSet.Tables.Remove("DaftarGudang")
        SQLquery = "SELECT IDGUDANG, NAMA, UTAMA FROM MGUDANG WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarGudang")
        DaftarGudang.Properties.DataSource = xSet.Tables("DaftarGudang").DefaultView

        DaftarGudang.Properties.NullText = ""
        DaftarGudang.Properties.ValueMember = "IDGUDANG"
        DaftarGudang.Properties.DisplayMember = "NAMA"
        DaftarGudang.Properties.ShowClearButton = False
        DaftarGudang.Properties.PopulateViewColumns()

        ViewDaftarGudang.Columns("IDGUDANG").Visible = False

        For Each coll As DataColumn In xSet.Tables("DaftarGudang").Columns
            ViewDaftarGudang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

    End Sub

    Private Sub GetDataKartuStok(ByVal DoThis As Boolean)
        HistoryControl.Visible = DoThis
        GridStok.DataSource = Nothing
        ViewStok.PopulateColumns()

        '--- 0=Stock | 1=History
        If DoThis = True Then
            tglPencarian.EditValue = DateAdd(DateInterval.Day, -7, Today.Date)
            MainControl.Enabled = False
            MainControl.Visible = False
            GridStok.Location = New Point(12, 83)
            GridStok.Size = New Size(GridStok.Size.Width, GridStok.Size.Height - 17)

            tujku_History()
        Else
            MainControl.Enabled = True
            MainControl.Visible = True
            GridStok.Location = New Point(12, 12)
            GridStok.Size = New Size(GridStok.Size.Width, GridStok.Size.Height + 17)

            If Not xSet.Tables("DataStock") Is Nothing Then xSet.Tables.Remove("DataStock")
            tujku_Stock()
        End If
    End Sub

    Private Sub tujku_Stock()
        If DaftarGudang.EditValue = Nothing Then Return

        Try
            If xSet.Tables("DataStock") IsNot Nothing Then xSet.Tables.Remove("DataStock")
            SQLquery = String.Format("EXEC SP_DAFTAR_STOCK_ITEM '{0}'", DaftarGudang.EditValue)
            ExDb.ExecQuery(SQLquery, xSet, "DataStock")

            GridStok.DataSource = xSet.Tables("DataStock").DefaultView

            ViewStok.Columns("IDBARANG").Visible = False
            ViewStok.Columns("KATEGORI").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
            ViewStok.Columns("KATEGORI").SummaryItem.DisplayFormat = "{0:n0} item(s)"
            ViewStok.Columns("QTY_STOCK").Caption = "STOK"
            ViewStok.Columns("QTY_STOCK").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            ViewStok.Columns("QTY_STOCK").SummaryItem.DisplayFormat = "Total:{0:n0}"

            Dim xView As GridView = GridStok.FocusedView
            ViewStok.SortInfo.ClearAndAddRange(New GridColumnSortInfo() { _
               New GridColumnSortInfo(xView.Columns("KATEGORI"), DevExpress.Data.ColumnSortOrder.Ascending)
            }, 1)
            ViewStok.ExpandAllGroups()

            For Each coll As DataColumn In xSet.Tables("DataStock").Columns
                With ViewStok.Columns(coll.ColumnName)
                    .AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
                End With
            Next
            ViewStok.BestFitColumns()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub tujku_History()
        If xSet.Tables("DataHistory") IsNot Nothing Then xSet.Tables("DataHistory").Clear()
        SQLquery = String.Format("EXEC SP_DAFTAR_HISTORY_ITEM '{0}', '{1}', '{2}'", Format(tglPencarian.EditValue, "yyyy-MM-dd"), lucky_one, DaftarGudang.EditValue.ToString)
        ExDb.ExecQuery(SQLquery, xSet, "DataHistory")
        GridStok.DataSource = xSet.Tables("DataHistory").DefaultView

        ViewStok.Columns("TANGGAL").DisplayFormat.FormatType = FormatType.DateTime
        ViewStok.Columns("TANGGAL").DisplayFormat.FormatString = "dd/MMM/yyyy HH:mm"
        ViewStok.Columns("KREDIT").Caption = "Kredit"
        ViewStok.Columns("KREDIT").DisplayFormat.FormatType = FormatType.Numeric
        ViewStok.Columns("KREDIT").DisplayFormat.FormatString = "n0"
        ViewStok.Columns("DEBET").Caption = "Debet"
        ViewStok.Columns("DEBET").DisplayFormat.FormatType = FormatType.Numeric
        ViewStok.Columns("DEBET").DisplayFormat.FormatString = "n0"

        ViewStok.Columns("RUNNING_TOTAL").Caption = "Balance"
        ViewStok.Columns("RUNNING_TOTAL").DisplayFormat.FormatType = FormatType.Numeric
        ViewStok.Columns("RUNNING_TOTAL").DisplayFormat.FormatString = "n0"

        For Each coll As DataColumn In xSet.Tables("DataHistory").Columns
            If coll.Caption = "TANGGAL" Or coll.Caption = "NOTRX" Then Continue For
            ViewStok.Columns(coll.ColumnName).AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            ViewStok.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewStok.BestFitColumns()
    End Sub
End Class