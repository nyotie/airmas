﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.Icon32 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.ButLogout = New DevExpress.XtraBars.BarButtonItem()
        Me.ButGantiPassword = New DevExpress.XtraBars.BarButtonItem()
        Me.ButMasterBrg = New DevExpress.XtraBars.BarButtonItem()
        Me.ButMasterKatBrg = New DevExpress.XtraBars.BarButtonItem()
        Me.ButBeliPB = New DevExpress.XtraBars.BarButtonItem()
        Me.ProgPermissions = New DevExpress.XtraBars.BarButtonItem()
        Me.RepPermissions = New DevExpress.XtraBars.BarButtonItem()
        Me.CompanyProfiles = New DevExpress.XtraBars.BarButtonItem()
        Me.ButMasterUser = New DevExpress.XtraBars.BarButtonItem()
        Me.ButReport = New DevExpress.XtraBars.BarButtonItem()
        Me.ButToolsActivation = New DevExpress.XtraBars.BarButtonItem()
        Me.ButMasterKendaraan = New DevExpress.XtraBars.BarButtonItem()
        Me.ButMasterSubKatBrg = New DevExpress.XtraBars.BarButtonItem()
        Me.ConnStatus = New DevExpress.XtraBars.BarStaticItem()
        Me.ButBeliPO = New DevExpress.XtraBars.BarButtonItem()
        Me.ButSinkronisasi = New DevExpress.XtraBars.BarButtonItem()
        Me.ButAturKoneksi = New DevExpress.XtraBars.BarButtonItem()
        Me.ButMasterWilayah = New DevExpress.XtraBars.BarButtonItem()
        Me.ButCekKoneksi = New DevExpress.XtraBars.BarButtonItem()
        Me.ButAbout = New DevExpress.XtraBars.BarButtonItem()
        Me.ButMasterSupplier = New DevExpress.XtraBars.BarButtonItem()
        Me.ButMasterGudang = New DevExpress.XtraBars.BarButtonItem()
        Me.ButMasterSales = New DevExpress.XtraBars.BarButtonItem()
        Me.ButBeliRTR = New DevExpress.XtraBars.BarButtonItem()
        Me.ButMasterCust = New DevExpress.XtraBars.BarButtonItem()
        Me.ButJualSO = New DevExpress.XtraBars.BarButtonItem()
        Me.ButJualPJ = New DevExpress.XtraBars.BarButtonItem()
        Me.ButJualDO = New DevExpress.XtraBars.BarButtonItem()
        Me.ButProOut = New DevExpress.XtraBars.BarButtonItem()
        Me.ButProIn = New DevExpress.XtraBars.BarButtonItem()
        Me.ButInvStockCard = New DevExpress.XtraBars.BarButtonItem()
        Me.ButJualSOC = New DevExpress.XtraBars.BarButtonItem()
        Me.ImageMDI = New DevExpress.Utils.ImageCollection(Me.components)
        Me.PageHome = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageMaster = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup10 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageTransaksi = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup15 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup6 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup7 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageReport = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageTools = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup22 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup8 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup9 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.PageHelp = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup23 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.Icon48 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.RibbonPageGroup11 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.ButFinPLH = New DevExpress.XtraBars.BarButtonItem()
        Me.ButFinPLP = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Icon32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageMDI, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Icon48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ApplicationButtonText = Nothing
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.Images = Me.Icon32
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.ButLogout, Me.ButGantiPassword, Me.ButMasterBrg, Me.ButMasterKatBrg, Me.ButBeliPB, Me.ProgPermissions, Me.RepPermissions, Me.CompanyProfiles, Me.ButMasterUser, Me.ButReport, Me.ButToolsActivation, Me.ButMasterKendaraan, Me.ButMasterSubKatBrg, Me.ConnStatus, Me.ButBeliPO, Me.ButSinkronisasi, Me.ButAturKoneksi, Me.ButMasterWilayah, Me.ButCekKoneksi, Me.ButAbout, Me.ButMasterSupplier, Me.ButMasterGudang, Me.ButMasterSales, Me.ButBeliRTR, Me.ButMasterCust, Me.ButJualSO, Me.ButJualPJ, Me.ButJualDO, Me.ButProOut, Me.ButProIn, Me.ButInvStockCard, Me.ButJualSOC, Me.ButFinPLH, Me.ButFinPLP})
        Me.RibbonControl.LargeImages = Me.ImageMDI
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 67
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.PageHome, Me.PageMaster, Me.PageTransaksi, Me.PageReport, Me.PageTools, Me.PageHelp})
        Me.RibbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice
        Me.RibbonControl.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.[False]
        Me.RibbonControl.ShowToolbarCustomizeItem = False
        Me.RibbonControl.Size = New System.Drawing.Size(915, 131)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        Me.RibbonControl.Toolbar.ShowCustomizeItem = False
        '
        'Icon32
        '
        Me.Icon32.ImageSize = New System.Drawing.Size(32, 32)
        Me.Icon32.ImageStream = CType(resources.GetObject("Icon32.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.Icon32.Images.SetKeyName(0, "Online.png")
        Me.Icon32.Images.SetKeyName(1, "Local.png")
        '
        'ButLogout
        '
        Me.ButLogout.Caption = "Logout"
        Me.ButLogout.Id = 1
        Me.ButLogout.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButLogout.ItemAppearance.Normal.Options.UseFont = True
        Me.ButLogout.LargeImageIndex = 10
        Me.ButLogout.Name = "ButLogout"
        '
        'ButGantiPassword
        '
        Me.ButGantiPassword.Caption = "Ganti Password"
        Me.ButGantiPassword.Id = 2
        Me.ButGantiPassword.LargeImageIndex = 9
        Me.ButGantiPassword.Name = "ButGantiPassword"
        '
        'ButMasterBrg
        '
        Me.ButMasterBrg.Caption = "Barang"
        Me.ButMasterBrg.Id = 3
        Me.ButMasterBrg.LargeImageIndex = 18
        Me.ButMasterBrg.Name = "ButMasterBrg"
        '
        'ButMasterKatBrg
        '
        Me.ButMasterKatBrg.Caption = "Kategori Barang"
        Me.ButMasterKatBrg.Id = 4
        Me.ButMasterKatBrg.LargeImageIndex = 15
        Me.ButMasterKatBrg.Name = "ButMasterKatBrg"
        '
        'ButBeliPB
        '
        Me.ButBeliPB.Caption = "Penerimaan Barang"
        Me.ButBeliPB.Id = 5
        Me.ButBeliPB.LargeImageIndex = 14
        Me.ButBeliPB.Name = "ButBeliPB"
        '
        'ProgPermissions
        '
        Me.ProgPermissions.Caption = "Program Permissions"
        Me.ProgPermissions.Id = 12
        Me.ProgPermissions.LargeImageIndex = 9
        Me.ProgPermissions.Name = "ProgPermissions"
        '
        'RepPermissions
        '
        Me.RepPermissions.Caption = "Report Permissions"
        Me.RepPermissions.Id = 13
        Me.RepPermissions.Name = "RepPermissions"
        '
        'CompanyProfiles
        '
        Me.CompanyProfiles.Caption = "Profile Perusahaan"
        Me.CompanyProfiles.Id = 14
        Me.CompanyProfiles.LargeImageIndex = 13
        Me.CompanyProfiles.Name = "CompanyProfiles"
        '
        'ButMasterUser
        '
        Me.ButMasterUser.Caption = "User"
        Me.ButMasterUser.Id = 16
        Me.ButMasterUser.LargeImageIndex = 6
        Me.ButMasterUser.Name = "ButMasterUser"
        '
        'ButReport
        '
        Me.ButReport.Caption = "Laporan"
        Me.ButReport.Id = 19
        Me.ButReport.Name = "ButReport"
        '
        'ButToolsActivation
        '
        Me.ButToolsActivation.Caption = "Activation"
        Me.ButToolsActivation.Id = 20
        Me.ButToolsActivation.Name = "ButToolsActivation"
        '
        'ButMasterKendaraan
        '
        Me.ButMasterKendaraan.Caption = "Kendaraan"
        Me.ButMasterKendaraan.Id = 22
        Me.ButMasterKendaraan.LargeImageIndex = 61
        Me.ButMasterKendaraan.Name = "ButMasterKendaraan"
        '
        'ButMasterSubKatBrg
        '
        Me.ButMasterSubKatBrg.Caption = "Sub Kategori Barang"
        Me.ButMasterSubKatBrg.Id = 23
        Me.ButMasterSubKatBrg.LargeImageIndex = 35
        Me.ButMasterSubKatBrg.Name = "ButMasterSubKatBrg"
        '
        'ConnStatus
        '
        Me.ConnStatus.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.ConnStatus.Caption = "Lokal"
        Me.ConnStatus.Id = 24
        Me.ConnStatus.ImageIndex = 1
        Me.ConnStatus.ItemAppearance.Normal.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.ConnStatus.ItemAppearance.Normal.Options.UseFont = True
        Me.ConnStatus.Name = "ConnStatus"
        Me.ConnStatus.ShowImageInToolbar = False
        Me.ConnStatus.TextAlignment = System.Drawing.StringAlignment.Center
        Me.ConnStatus.Width = 100
        '
        'ButBeliPO
        '
        Me.ButBeliPO.Caption = "Purchase Order"
        Me.ButBeliPO.Id = 26
        Me.ButBeliPO.LargeImageIndex = 20
        Me.ButBeliPO.Name = "ButBeliPO"
        '
        'ButSinkronisasi
        '
        Me.ButSinkronisasi.Caption = "Sinkronisasi Data"
        Me.ButSinkronisasi.Id = 36
        Me.ButSinkronisasi.Name = "ButSinkronisasi"
        '
        'ButAturKoneksi
        '
        Me.ButAturKoneksi.Caption = "Koneksi"
        Me.ButAturKoneksi.Id = 38
        Me.ButAturKoneksi.LargeImageIndex = 58
        Me.ButAturKoneksi.Name = "ButAturKoneksi"
        '
        'ButMasterWilayah
        '
        Me.ButMasterWilayah.Caption = "Wilayah"
        Me.ButMasterWilayah.Id = 40
        Me.ButMasterWilayah.LargeImageIndex = 43
        Me.ButMasterWilayah.Name = "ButMasterWilayah"
        '
        'ButCekKoneksi
        '
        Me.ButCekKoneksi.Caption = "Cek Koneksi"
        Me.ButCekKoneksi.Id = 48
        Me.ButCekKoneksi.LargeImageIndex = 58
        Me.ButCekKoneksi.Name = "ButCekKoneksi"
        '
        'ButAbout
        '
        Me.ButAbout.Caption = "About"
        Me.ButAbout.Id = 50
        Me.ButAbout.Name = "ButAbout"
        '
        'ButMasterSupplier
        '
        Me.ButMasterSupplier.Caption = "Supplier"
        Me.ButMasterSupplier.Id = 53
        Me.ButMasterSupplier.LargeImageIndex = 50
        Me.ButMasterSupplier.Name = "ButMasterSupplier"
        '
        'ButMasterGudang
        '
        Me.ButMasterGudang.Caption = "Gudang"
        Me.ButMasterGudang.Id = 54
        Me.ButMasterGudang.LargeImageIndex = 42
        Me.ButMasterGudang.Name = "ButMasterGudang"
        '
        'ButMasterSales
        '
        Me.ButMasterSales.Caption = "Sales"
        Me.ButMasterSales.Id = 55
        Me.ButMasterSales.LargeImageIndex = 60
        Me.ButMasterSales.Name = "ButMasterSales"
        '
        'ButBeliRTR
        '
        Me.ButBeliRTR.Caption = "Retur Beli"
        Me.ButBeliRTR.Id = 56
        Me.ButBeliRTR.LargeImageIndex = 0
        Me.ButBeliRTR.Name = "ButBeliRTR"
        '
        'ButMasterCust
        '
        Me.ButMasterCust.Caption = "Customer"
        Me.ButMasterCust.Id = 57
        Me.ButMasterCust.LargeImageIndex = 59
        Me.ButMasterCust.Name = "ButMasterCust"
        '
        'ButJualSO
        '
        Me.ButJualSO.Caption = "Sales Order"
        Me.ButJualSO.Id = 58
        Me.ButJualSO.LargeImageIndex = 12
        Me.ButJualSO.Name = "ButJualSO"
        '
        'ButJualPJ
        '
        Me.ButJualPJ.Caption = "Penjualan"
        Me.ButJualPJ.Id = 59
        Me.ButJualPJ.LargeImageIndex = 19
        Me.ButJualPJ.Name = "ButJualPJ"
        '
        'ButJualDO
        '
        Me.ButJualDO.Caption = "Loading"
        Me.ButJualDO.Id = 60
        Me.ButJualDO.LargeImageIndex = 23
        Me.ButJualDO.Name = "ButJualDO"
        '
        'ButProOut
        '
        Me.ButProOut.Caption = "Permintaan Bahan"
        Me.ButProOut.Id = 61
        Me.ButProOut.LargeImageIndex = 27
        Me.ButProOut.Name = "ButProOut"
        '
        'ButProIn
        '
        Me.ButProIn.Caption = "Hasil Produksi"
        Me.ButProIn.Id = 62
        Me.ButProIn.LargeImageIndex = 39
        Me.ButProIn.Name = "ButProIn"
        '
        'ButInvStockCard
        '
        Me.ButInvStockCard.Caption = "Kartu Stok"
        Me.ButInvStockCard.Id = 63
        Me.ButInvStockCard.LargeImageIndex = 4
        Me.ButInvStockCard.Name = "ButInvStockCard"
        '
        'ButJualSOC
        '
        Me.ButJualSOC.Caption = "Sales Cash"
        Me.ButJualSOC.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.ButJualSOC.Id = 64
        Me.ButJualSOC.LargeImageIndex = 60
        Me.ButJualSOC.Name = "ButJualSOC"
        '
        'ImageMDI
        '
        Me.ImageMDI.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageMDI.ImageStream = CType(resources.GetObject("ImageMDI.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.ImageMDI.Images.SetKeyName(0, "1342151412_Book3.png")
        Me.ImageMDI.Images.SetKeyName(1, "1342151426_emblem-library.png")
        Me.ImageMDI.Images.SetKeyName(2, "1342151454_Book.png")
        Me.ImageMDI.Images.SetKeyName(3, "1342151463_-books.png")
        Me.ImageMDI.Images.SetKeyName(4, "1342153389_application-vnd.oasis.opendocument.spreadsheet.png")
        Me.ImageMDI.Images.SetKeyName(5, "web.png")
        Me.ImageMDI.Images.SetKeyName(6, "edit_group.png")
        Me.ImageMDI.Images.SetKeyName(7, "edit_user.png")
        Me.ImageMDI.Images.SetKeyName(8, "exit.png")
        Me.ImageMDI.Images.SetKeyName(9, "kopeteaway.png")
        Me.ImageMDI.Images.SetKeyName(10, "logout.png")
        Me.ImageMDI.Images.SetKeyName(11, "kexi.png")
        Me.ImageMDI.Images.SetKeyName(12, "templates.png")
        Me.ImageMDI.Images.SetKeyName(13, "gohome.png")
        Me.ImageMDI.Images.SetKeyName(14, "ark.png")
        Me.ImageMDI.Images.SetKeyName(15, "blockdevice.png")
        Me.ImageMDI.Images.SetKeyName(16, "folder-customer-icon.png")
        Me.ImageMDI.Images.SetKeyName(17, "supplier.png")
        Me.ImageMDI.Images.SetKeyName(18, "1375106577_Shipping6.png")
        Me.ImageMDI.Images.SetKeyName(19, "buy.png")
        Me.ImageMDI.Images.SetKeyName(20, "purchase-order-system-1-0-2-1491829449-600x600.png")
        Me.ImageMDI.Images.SetKeyName(21, "news_subscribe.png")
        Me.ImageMDI.Images.SetKeyName(22, "xcode.png")
        Me.ImageMDI.Images.SetKeyName(23, "delivery.png")
        Me.ImageMDI.Images.SetKeyName(24, "Portland-Office-Moving.png")
        Me.ImageMDI.Images.SetKeyName(25, "folder_sent_mail.png")
        Me.ImageMDI.Images.SetKeyName(26, "mail_reply.png")
        Me.ImageMDI.Images.SetKeyName(27, "kaddressbook.png")
        Me.ImageMDI.Images.SetKeyName(28, "windowlist.png")
        Me.ImageMDI.Images.SetKeyName(29, "designet.png")
        Me.ImageMDI.Images.SetKeyName(30, "contents.png")
        Me.ImageMDI.Images.SetKeyName(31, "ark.png")
        Me.ImageMDI.Images.SetKeyName(32, "calc.png")
        Me.ImageMDI.Images.SetKeyName(33, "home&food.png")
        Me.ImageMDI.Images.SetKeyName(34, "insert_table_col.png")
        Me.ImageMDI.Images.SetKeyName(35, "list.png")
        Me.ImageMDI.Images.SetKeyName(36, "advanced.png")
        Me.ImageMDI.Images.SetKeyName(37, "insert_table_row.png")
        Me.ImageMDI.Images.SetKeyName(38, "contents.png")
        Me.ImageMDI.Images.SetKeyName(39, "edit.png")
        Me.ImageMDI.Images.SetKeyName(40, "editcopy.png")
        Me.ImageMDI.Images.SetKeyName(41, "Adjustment.png")
        Me.ImageMDI.Images.SetKeyName(42, "Bank.png")
        Me.ImageMDI.Images.SetKeyName(43, "internet.png")
        Me.ImageMDI.Images.SetKeyName(44, "newfont.png")
        Me.ImageMDI.Images.SetKeyName(45, "agt_announcements.png")
        Me.ImageMDI.Images.SetKeyName(46, "money.png")
        Me.ImageMDI.Images.SetKeyName(47, "file_doc.png")
        Me.ImageMDI.Images.SetKeyName(48, "connect_to_network.png")
        Me.ImageMDI.Images.SetKeyName(49, "agt_update_recommended1.png")
        Me.ImageMDI.Images.SetKeyName(50, "SUPPLIER-sm.png")
        Me.ImageMDI.Images.SetKeyName(51, "1342151426_emblem-library.png")
        Me.ImageMDI.Images.SetKeyName(52, "1342153389_application-vnd.oasis.opendocument.spreadsheet.png")
        Me.ImageMDI.Images.SetKeyName(53, "1342153722_industry.png")
        Me.ImageMDI.Images.SetKeyName(54, "1342153729_industry.png")
        Me.ImageMDI.Images.SetKeyName(55, "easymoblog.png")
        Me.ImageMDI.Images.SetKeyName(56, "internet&networking.png")
        Me.ImageMDI.Images.SetKeyName(57, "news_subscribe.png")
        Me.ImageMDI.Images.SetKeyName(58, "connect_to_network.png")
        Me.ImageMDI.Images.SetKeyName(59, "edit_group.png")
        Me.ImageMDI.Images.SetKeyName(60, "Resell-icon.png")
        Me.ImageMDI.Images.SetKeyName(61, "Truck.png")
        '
        'PageHome
        '
        Me.PageHome.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1})
        Me.PageHome.Name = "PageHome"
        Me.PageHome.Text = "Home"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.ButLogout)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.ButGantiPassword)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.ButCekKoneksi)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.ShowCaptionButton = False
        Me.RibbonPageGroup1.Text = "Home"
        '
        'PageMaster
        '
        Me.PageMaster.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup10, Me.RibbonPageGroup2, Me.RibbonPageGroup4})
        Me.PageMaster.Name = "PageMaster"
        Me.PageMaster.Text = "Master"
        '
        'RibbonPageGroup10
        '
        Me.RibbonPageGroup10.ItemLinks.Add(Me.ButMasterUser)
        Me.RibbonPageGroup10.Name = "RibbonPageGroup10"
        Me.RibbonPageGroup10.ShowCaptionButton = False
        Me.RibbonPageGroup10.Text = "General"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.ButMasterKatBrg)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.ButMasterSubKatBrg)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.ButMasterBrg)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.ButMasterKendaraan)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.ButMasterGudang)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.ShowCaptionButton = False
        Me.RibbonPageGroup2.Text = "Master"
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.ItemLinks.Add(Me.ButMasterWilayah)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.ButMasterCust)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.ButMasterSales)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.ButMasterSupplier)
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        Me.RibbonPageGroup4.ShowCaptionButton = False
        Me.RibbonPageGroup4.Text = "Sales"
        '
        'PageTransaksi
        '
        Me.PageTransaksi.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup15, Me.RibbonPageGroup3, Me.RibbonPageGroup6, Me.RibbonPageGroup7, Me.RibbonPageGroup11})
        Me.PageTransaksi.Name = "PageTransaksi"
        Me.PageTransaksi.Text = "Transaksi"
        '
        'RibbonPageGroup15
        '
        Me.RibbonPageGroup15.ItemLinks.Add(Me.ButBeliPO)
        Me.RibbonPageGroup15.ItemLinks.Add(Me.ButBeliPB)
        Me.RibbonPageGroup15.ItemLinks.Add(Me.ButBeliRTR)
        Me.RibbonPageGroup15.Name = "RibbonPageGroup15"
        Me.RibbonPageGroup15.ShowCaptionButton = False
        Me.RibbonPageGroup15.Text = "Pembelian"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.ButJualSO)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.ButJualPJ)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.ButJualDO)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.ButJualSOC)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.ShowCaptionButton = False
        Me.RibbonPageGroup3.Text = "Penjualan"
        '
        'RibbonPageGroup6
        '
        Me.RibbonPageGroup6.ItemLinks.Add(Me.ButProOut)
        Me.RibbonPageGroup6.ItemLinks.Add(Me.ButProIn)
        Me.RibbonPageGroup6.Name = "RibbonPageGroup6"
        Me.RibbonPageGroup6.ShowCaptionButton = False
        Me.RibbonPageGroup6.Text = "Produksi"
        '
        'RibbonPageGroup7
        '
        Me.RibbonPageGroup7.ItemLinks.Add(Me.ButInvStockCard)
        Me.RibbonPageGroup7.Name = "RibbonPageGroup7"
        Me.RibbonPageGroup7.ShowCaptionButton = False
        Me.RibbonPageGroup7.Text = "Gudang"
        '
        'PageReport
        '
        Me.PageReport.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup5})
        Me.PageReport.Name = "PageReport"
        Me.PageReport.Text = "Laporan"
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.ButReport)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.ShowCaptionButton = False
        Me.RibbonPageGroup5.Text = "Report"
        '
        'PageTools
        '
        Me.PageTools.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup22, Me.RibbonPageGroup8, Me.RibbonPageGroup9})
        Me.PageTools.Name = "PageTools"
        Me.PageTools.Text = "Tools & Settings"
        '
        'RibbonPageGroup22
        '
        Me.RibbonPageGroup22.ItemLinks.Add(Me.CompanyProfiles)
        Me.RibbonPageGroup22.Name = "RibbonPageGroup22"
        Me.RibbonPageGroup22.ShowCaptionButton = False
        Me.RibbonPageGroup22.Text = "Branch"
        '
        'RibbonPageGroup8
        '
        Me.RibbonPageGroup8.ItemLinks.Add(Me.ProgPermissions)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.RepPermissions)
        Me.RibbonPageGroup8.Name = "RibbonPageGroup8"
        Me.RibbonPageGroup8.ShowCaptionButton = False
        Me.RibbonPageGroup8.Text = "Security"
        '
        'RibbonPageGroup9
        '
        Me.RibbonPageGroup9.ItemLinks.Add(Me.ButAturKoneksi)
        Me.RibbonPageGroup9.Name = "RibbonPageGroup9"
        Me.RibbonPageGroup9.ShowCaptionButton = False
        Me.RibbonPageGroup9.Text = "Setting"
        '
        'PageHelp
        '
        Me.PageHelp.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup23})
        Me.PageHelp.Name = "PageHelp"
        Me.PageHelp.Text = "Help"
        '
        'RibbonPageGroup23
        '
        Me.RibbonPageGroup23.ItemLinks.Add(Me.ButAbout)
        Me.RibbonPageGroup23.Name = "RibbonPageGroup23"
        Me.RibbonPageGroup23.ShowCaptionButton = False
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.ItemLinks.Add(Me.ConnStatus)
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 242)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(915, 31)
        '
        'Icon48
        '
        Me.Icon48.ImageSize = New System.Drawing.Size(48, 48)
        Me.Icon48.ImageStream = CType(resources.GetObject("Icon48.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.Icon48.Images.SetKeyName(0, "Logout.png")
        Me.Icon48.Images.SetKeyName(1, "Password.png")
        Me.Icon48.Images.SetKeyName(2, "Barang.png")
        Me.Icon48.Images.SetKeyName(3, "Design POS.png")
        Me.Icon48.Images.SetKeyName(4, "Kategori Barang.png")
        Me.Icon48.Images.SetKeyName(5, "Konsinyasi.png")
        Me.Icon48.Images.SetKeyName(6, "Kota.png")
        Me.Icon48.Images.SetKeyName(7, "Money Coin.png")
        Me.Icon48.Images.SetKeyName(8, "User.png")
        Me.Icon48.Images.SetKeyName(9, "DO.png")
        Me.Icon48.Images.SetKeyName(10, "Hasil Produksi.png")
        Me.Icon48.Images.SetKeyName(11, "Pemusnahan Retur.png")
        Me.Icon48.Images.SetKeyName(12, "Penjualan Retur.png")
        Me.Icon48.Images.SetKeyName(13, "Pindah Gudang.png")
        Me.Icon48.Images.SetKeyName(14, "ADJ.png")
        Me.Icon48.Images.SetKeyName(15, "Kartu Stok 2.png")
        Me.Icon48.Images.SetKeyName(16, "Kartu Stok.png")
        Me.Icon48.Images.SetKeyName(17, "LPB.png")
        Me.Icon48.Images.SetKeyName(18, "Opname.png")
        Me.Icon48.Images.SetKeyName(19, "Retur.png")
        Me.Icon48.Images.SetKeyName(20, "Report.png")
        Me.Icon48.Images.SetKeyName(21, "Cabang.png")
        Me.Icon48.Images.SetKeyName(22, "Company Profile.png")
        Me.Icon48.Images.SetKeyName(23, "Ganti Cabang.png")
        Me.Icon48.Images.SetKeyName(24, "Koneksi.png")
        Me.Icon48.Images.SetKeyName(25, "Program Permissions.png")
        Me.Icon48.Images.SetKeyName(26, "Report Permissions.png")
        Me.Icon48.Images.SetKeyName(27, "Sinkronisasi.png")
        Me.Icon48.Images.SetKeyName(28, "Re-Produksi.png")
        Me.Icon48.Images.SetKeyName(29, "Konversi ke Retur.png")
        Me.Icon48.Images.SetKeyName(30, "Security.png")
        Me.Icon48.Images.SetKeyName(31, "upload stok.png")
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.MdiParent = Me
        '
        'RibbonPageGroup11
        '
        Me.RibbonPageGroup11.ItemLinks.Add(Me.ButFinPLH)
        Me.RibbonPageGroup11.ItemLinks.Add(Me.ButFinPLP)
        Me.RibbonPageGroup11.Name = "RibbonPageGroup11"
        Me.RibbonPageGroup11.ShowCaptionButton = False
        Me.RibbonPageGroup11.Text = "Finance"
        '
        'ButFinPLH
        '
        Me.ButFinPLH.Caption = "Pelunasan Hutang"
        Me.ButFinPLH.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.ButFinPLH.Id = 65
        Me.ButFinPLH.Name = "ButFinPLH"
        '
        'ButFinPLP
        '
        Me.ButFinPLP.Caption = "Pelunasan Piutang"
        Me.ButFinPLP.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.ButFinPLP.Id = 66
        Me.ButFinPLP.Name = "ButFinPLP"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(915, 273)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "MainForm"
        Me.Ribbon = Me.RibbonControl
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "Airmas"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Icon32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageMDI, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Icon48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents PageHome As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents PageMaster As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents PageReport As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents PageTools As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup9 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup8 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ButLogout As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButGantiPassword As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButMasterBrg As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButMasterKatBrg As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButBeliPB As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ProgPermissions As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RepPermissions As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents CompanyProfiles As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup10 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ButMasterUser As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButToolsActivation As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButMasterKendaraan As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButMasterSubKatBrg As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ConnStatus As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents Icon32 As DevExpress.Utils.ImageCollection
    Friend WithEvents ButBeliPO As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PageTransaksi As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup15 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ButSinkronisasi As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButAturKoneksi As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup22 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ButMasterWilayah As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents Icon48 As DevExpress.Utils.ImageCollection
    Friend WithEvents ButCekKoneksi As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButAbout As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PageHelp As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup23 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ButMasterSupplier As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButMasterGudang As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButMasterSales As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButBeliRTR As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ButMasterCust As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ButJualSO As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButJualPJ As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButJualDO As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButProOut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup6 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ButProIn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButInvStockCard As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup7 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ImageMDI As DevExpress.Utils.ImageCollection
    Friend WithEvents ButJualSOC As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButFinPLH As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ButFinPLP As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup11 As DevExpress.XtraBars.Ribbon.RibbonPageGroup


End Class
