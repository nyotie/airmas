﻿Public Class ModProperties

    Private Sub ModProperties_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DataProperties")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ModProperties_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        SQLquery = String.Format("EXEC module_properties {0}, {1}", id_jenistrans, selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "DataProperties")
    End Sub

    Private Sub ModProperties_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        trans_creator.EditValue = xSet.Tables("DataProperties").Rows(0).Item("creator")
        trans_createdate.EditValue = Format(xSet.Tables("DataProperties").Rows(0).Item("createdate"), "dd MMMM yyyy, HH:mm")
        trans_editor.EditValue = xSet.Tables("DataProperties").Rows(0).Item("editor").ToString
        If Not trans_editor.Text = "" Then
            trans_editdate.EditValue = Format(xSet.Tables("DataProperties").Rows(0).Item("editdate"), "dd MMMM yyyy, HH:mm")
        End If
        trans_status.EditValue = xSet.Tables("DataProperties").Rows(0).Item("status")
        trans_ketvoid.EditValue = xSet.Tables("DataProperties").Rows(0).Item("ketvoid")
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub
End Class