﻿Imports DevExpress.Utils

Public Class ProIN
    Dim selected_cell As String
    Dim selected_cell2 As String

    Private Sub PengirimanCabang_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_PROD_IN")
        xSet.Tables.Remove("TD_PROD_IN")
        xSet.Tables.Remove("TD_PROD_IN2")
        xSet.Tables.Remove("DaftarGudang")
        xSet.Tables.Remove("DaftarProduksiOut")
        xSet.Tables.Remove("DaftarBahanKemasan")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub PengirimanCabang_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub PengirimanCabang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now
        Else
            SQLquery = String.Format("SELECT TANGGAL,SELECTOR,MEMO,ACC FROM TM_PROD_IN WHERE NOPROD_IN='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_PROD_IN")

            NoTrans.Text = selected_id
            TglTrans.EditValue = xSet.Tables("TM_PROD_IN").Rows(0).Item("TANGGAL")
            SelectorTrans.Text = xSet.Tables("TM_PROD_IN").Rows(0).Item("SELECTOR")
            Catatan.EditValue = xSet.Tables("TM_PROD_IN").Rows(0).Item("MEMO")
            ACCcheck.Checked = IIf(xSet.Tables("TM_PROD_IN").Rows(0).Item("ACC") = 0, False, True)
        End If
    End Sub

    Private Sub DaftarProdOut_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarProdOut.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell = Repository.EditValue
    End Sub

    Private Sub DaftarBahanKemasan_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarBahanKemasan.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell2 = Repository.EditValue
    End Sub

    Private Sub ViewTrans_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans.CellValueChanged
        Try
            Dim drow As DataRow = xSet.Tables("DaftarProduksiOut").Select(String.Format("KODEPRO='{0}'", selected_cell))(0)

            If e.Column.FieldName = "KODEPRO" Then
                For row As Integer = 0 To ViewTrans.RowCount - 1
                    If ViewTrans.GetRowCellValue(row, "KODEPRO") = selected_cell And Not ViewTrans.FocusedRowHandle = row Then
                        ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                        ViewTrans.FocusedRowHandle = row
                        Return
                    End If
                Next

                ViewTrans.SetFocusedRowCellValue("IDBARANG", drow.Item("IDBARANG"))
                ViewTrans.SetFocusedRowCellValue("BARANG", drow.Item("NAMA"))
                ViewTrans.SetFocusedRowCellValue("QTY", drow.Item("QTY"))
                ViewTrans.SetFocusedRowCellValue("HPP", drow.Item("HPP"))
                ViewTrans.SetFocusedRowCellValue("KETERANGAN", "")
                ViewTrans.SetFocusedRowCellValue("IDGUDANG", Nothing)
                ViewTrans.FocusedRowHandle = ViewTrans.RowCount
            End If
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("KODEPRO")) = True Then Throw New Exception
        Catch ex As Exception
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("KODEPRO")) = True Then
                ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                xSet.Tables("TD_PROD_IN").AcceptChanges()
            End If
        End Try
    End Sub

    Private Sub ViewTrans_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewTrans.FocusedRowChanged
        Try
            If e.FocusedRowHandle >= 0 Then
                selected_cell = ViewTrans.GetRowCellValue(e.FocusedRowHandle, "KODEPRO")
            End If
        Catch ex As Exception
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
        End Try
    End Sub

    Private Sub ViewTrans2_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans2.CellValueChanged
        Try
            Dim drow As DataRow = xSet.Tables("DaftarBahanKemasan").Select(String.Format("IDBARANG='{0}'", selected_cell2))(0)

            If e.Column.FieldName = "IDBARANG" Then
                For row As Integer = 0 To ViewTrans2.RowCount - 1
                    If ViewTrans2.GetRowCellValue(row, "IDBARANG") = selected_cell2 And Not ViewTrans2.FocusedRowHandle = row Then
                        ViewTrans2.DeleteRow(ViewTrans2.FocusedRowHandle)
                        ViewTrans2.FocusedRowHandle = row
                        Return
                    End If
                Next
                'IDBARANG, QTY, TD.HPP, KETERANGAN, TD.IDGUDANG
                'IDBARANG, NAMA, ALIAS, SATUAN1, SATUAN2, HPP
                ViewTrans2.SetFocusedRowCellValue("QTY", 0)
                ViewTrans2.SetFocusedRowCellValue("HPP", drow.Item("HPP"))
                ViewTrans2.SetFocusedRowCellValue("KETERANGAN", "")
                ViewTrans2.SetFocusedRowCellValue("IDGUDANG", Nothing)
                ViewTrans2.FocusedRowHandle = ViewTrans2.RowCount
            End If
            If IsDBNull(ViewTrans2.GetFocusedRowCellValue("IDBARANG")) = True Then Throw New Exception
        Catch ex As Exception
            If IsDBNull(ViewTrans2.GetFocusedRowCellValue("IDBARANG")) = True Then
                ViewTrans2.DeleteRow(ViewTrans2.FocusedRowHandle)
                xSet.Tables("TD_PROD_IN2").AcceptChanges()
            End If
        End Try
    End Sub

    Private Sub ViewTrans2_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewTrans2.FocusedRowChanged
        Try
            If e.FocusedRowHandle >= 0 Then
                selected_cell2 = ViewTrans2.GetRowCellValue(e.FocusedRowHandle, "IDBARANG")
            End If
        Catch ex As Exception
            ViewTrans2.DeleteRow(ViewTrans2.FocusedRowHandle)
        End Try
    End Sub

    Private Sub GridTransKeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
            xSet.Tables("TD_PROD_IN").AcceptChanges()
        End If
    End Sub

    Private Sub GridTrans2KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans2.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTrans2.DeleteRow(ViewTrans2.FocusedRowHandle)
            xSet.Tables("TD_PROD_IN2").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC PROD_IN_INS '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', """,
                                            NoTrans.Text, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"),
                                            SelectorTrans.Text.Replace("'", "''"), Catatan.Text.Replace("'", "''"), IIf(ACCcheck.Checked, 1, 0), staff_id)
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC PROD_IN_UPD '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', """,
                                            NoTrans.Text, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"),
                                            SelectorTrans.Text.Replace("'", "''"), Catatan.Text.Replace("'", "''"), IIf(ACCcheck.Checked, 1, 0), staff_id)
            End If

            '-----------------------------Daftar Hasil Produksi---------------------------------------------------------------------------------------
            Dim nourut As Integer = 1
            For Each drow As DataRow In xSet.Tables("TD_PROD_IN").Rows
                SQLquery += String.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', 0), ", NoTrans.Text, nourut,
                                          drow.Item("KODEPRO"), drow.Item("IDBARANG"), drow.Item("QTY"), drow.Item("HPP"), drow.Item("KETERANGAN"), drow.Item("IDGUDANG"))
                nourut += 1
            Next
            '-----------------------------Daftar Bahan Kemasan----------------------------------------------------------------------------------------
            For Each drow As DataRow In xSet.Tables("TD_PROD_IN2").Rows
                SQLquery += String.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', 1), ", NoTrans.Text, nourut,
                                          "-", drow.Item("IDBARANG"), drow.Item("QTY"), drow.Item("HPP"), drow.Item("KETERANGAN"), drow.Item("IDGUDANG"))
                nourut += 1
            Next
            '-----------------------------------------------------------------------------------------------------------------------------------------
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserProIN.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_PROD_IN").Clear()
        xSet.Tables("TD_PROD_IN").AcceptChanges()

        TglTrans.EditValue = Now
        SelectorTrans.Text = ""
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------Get Daftar Gudang
        If Not xSet.Tables("DaftarGudang") Is Nothing Then xSet.Tables("DaftarGudang").Clear()
        SQLquery = "SELECT IDGUDANG ID, NAMA FROM MGUDANG WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarGudang")

        '----------------------------------------------------Set Data Gudang ke RepoHasil
        DaftarGudang.DataSource = xSet.Tables("DaftarGudang").DefaultView
        DaftarGudang.NullText = ""
        DaftarGudang.ValueMember = "ID"
        DaftarGudang.DisplayMember = "NAMA"
        DaftarGudang.ShowClearButton = False
        DaftarGudang.PopulateViewColumns()
        ViewDaftarGudang.Columns("ID").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarGudang").Columns
            ViewDaftarGudang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Set Data Gudang ke RepoKemasan 
        DaftarGudangKemasan.DataSource = xSet.Tables("DaftarGudang").DefaultView
        DaftarGudangKemasan.NullText = ""
        DaftarGudangKemasan.ValueMember = "ID"
        DaftarGudangKemasan.DisplayMember = "NAMA"
        DaftarGudangKemasan.ShowClearButton = False
        DaftarGudangKemasan.PopulateViewColumns()
        ViewDaftarGudangKemasan.Columns("ID").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarGudang").Columns
            ViewDaftarGudangKemasan.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '---------------------------------------------------- Daftar Produksi out
        If Not xSet.Tables("DaftarProduksiOut") Is Nothing Then xSet.Tables("DaftarProduksiOut").Clear()
        SQLquery = String.Format("SELECT NOPROD_OUT KODEPRO, TANGGAL, TM.IDBARANG, MB.NAMA, TM.QTY, TM.HPP FROM TM_PROD_OUT TM  INNER JOIN MBARANG MB ON TM.IDBARANG=MB.IDBARANG " & _
                                 "WHERE VOID=0 AND ACC=1 OR NOPROD_OUT IN (SELECT NOPROD_OUT FROM TD_PROD_IN WHERE NOPROD_IN='{0}');", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarProduksiOut")
        DaftarProdOut.DataSource = xSet.Tables("DaftarProduksiOut").DefaultView

        DaftarProdOut.NullText = ""
        DaftarProdOut.ValueMember = "KODEPRO"
        DaftarProdOut.DisplayMember = "KODEPRO"
        DaftarProdOut.ShowClearButton = False
        DaftarProdOut.PopulateViewColumns()

        ViewDaftarProdOut.Columns("IDBARANG").Visible = False
        ViewDaftarProdOut.Columns("HPP").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarProduksiOut").Columns
            ViewDaftarProdOut.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '---------------------------------------------------- Daftar Bahan Kemasan
        If Not xSet.Tables("DaftarBahanKemasan") Is Nothing Then xSet.Tables("DaftarBahanKemasan").Clear()
        SQLquery = "SELECT IDBARANG, NAMA, ALIAS, SATUAN1, SATUAN2, HPP FROM MBARANG WHERE IDKATEGORI='K-002' AND INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarBahanKemasan")
        DaftarBahanKemasan.DataSource = xSet.Tables("DaftarBahanKemasan").DefaultView

        DaftarBahanKemasan.NullText = ""
        DaftarBahanKemasan.ValueMember = "IDBARANG"
        DaftarBahanKemasan.DisplayMember = "NAMA"
        DaftarBahanKemasan.ShowClearButton = False
        DaftarBahanKemasan.PopulateViewColumns()

        ViewDaftarBahanKemasan.Columns("IDBARANG").Visible = False
        ViewDaftarBahanKemasan.Columns("HPP").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarBahanKemasan").Columns
            ViewDaftarBahanKemasan.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar Produksi In
        SQLquery = String.Format("SELECT NOPROD_OUT KODEPRO,TD.IDBARANG,MB.NAMA BARANG,QTY,TD.HPP,KETERANGAN,TD.IDGUDANG FROM TD_PROD_IN TD " & _
                                 "INNER JOIN MBARANG MB ON MB.IDBARANG=TD.IDBARANG WHERE ISKEMASAN=0 AND NOPROD_IN='{0}' ORDER BY NO_URUT;", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_PROD_IN")
        GridTrans.DataSource = xSet.Tables("TD_PROD_IN").DefaultView

        ViewTrans.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        For Each coll As DataColumn In xSet.Tables("TD_PROD_IN").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("IDBARANG").Visible = False
        ViewTrans.Columns("IDBARANG").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("BARANG").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("HPP").OptionsColumn.AllowEdit = False

        ViewTrans.Columns("KODEPRO").ColumnEdit = DaftarProdOut
        ViewTrans.Columns("IDGUDANG").ColumnEdit = DaftarGudang

        ViewTrans.Columns("QTY").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("QTY").DisplayFormat.FormatString = "n2"
        ViewTrans.Columns("HPP").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("HPP").DisplayFormat.FormatString = "n2"

        ViewTrans.Columns("KODEPRO").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("KODEPRO").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewTrans.Columns("QTY").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("QTY").SummaryItem.DisplayFormat = "{0:n0}"

        ViewTrans.Columns("KODEPRO").Width = GridTrans.Width * 0.2
        ViewTrans.Columns("BARANG").Width = GridTrans.Width * 0.2
        ViewTrans.Columns("QTY").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("HPP").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("KETERANGAN").Width = GridTrans.Width * 0.3
        ViewTrans.Columns("IDGUDANG").Width = GridTrans.Width * 0.1

        '----------------------------------------------------Daftar Kebutuhan Bahan Kemas
        SQLquery = String.Format("SELECT TD.IDBARANG, QTY, TD.HPP, KETERANGAN, TD.IDGUDANG FROM TD_PROD_IN TD " & _
                                 "WHERE ISKEMASAN=1 AND NOPROD_IN='{0}' ORDER BY NO_URUT;", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_PROD_IN2")
        GridTrans2.DataSource = xSet.Tables("TD_PROD_IN2").DefaultView
        
        ViewTrans2.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        For Each coll As DataColumn In xSet.Tables("TD_PROD_IN2").Columns
            ViewTrans2.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans2.Columns("HPP").OptionsColumn.AllowEdit = False
        ViewTrans2.Columns("IDBARANG").ColumnEdit = DaftarBahanKemasan
        ViewTrans2.Columns("IDGUDANG").ColumnEdit = DaftarGudangKemasan

        ViewTrans2.Columns("QTY").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans2.Columns("QTY").DisplayFormat.FormatString = "n2"
        ViewTrans2.Columns("HPP").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans2.Columns("HPP").DisplayFormat.FormatString = "n2"

        ViewTrans2.Columns("IDBARANG").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans2.Columns("IDBARANG").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewTrans2.Columns("QTY").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans2.Columns("QTY").SummaryItem.DisplayFormat = "{0:n0}"

        ViewTrans2.Columns("IDBARANG").Width = GridTrans.Width * 0.3
        ViewTrans2.Columns("QTY").Width = GridTrans.Width * 0.1
        ViewTrans2.Columns("HPP").Width = GridTrans.Width * 0.1
        ViewTrans2.Columns("KETERANGAN").Width = GridTrans.Width * 0.4
        ViewTrans2.Columns("IDGUDANG").Width = GridTrans.Width * 0.1
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_PROD_IN").Rows.Count < 1 Then
            Return False
        End If

        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        For Each drow As DataRow In xSet.Tables("TD_PROD_IN").Rows
            If drow.Item("QTY") < 1 Then
                msgboxWarning("Jumlah barang tidak boleh kosong")
                Return False
            End If
            If IsDBNull(drow.Item("IDGUDANG")) Then
                msgboxWarning("Gudang pada daftar tidak boleh kosong")
                Return False
            End If
        Next

        For Each drow As DataRow In xSet.Tables("TD_PROD_IN2").Rows
            If drow.Item("QTY") < 1 Then
                msgboxWarning("Jumlah barang tidak boleh kosong")
                Return False
            End If
            If IsDBNull(drow.Item("IDGUDANG")) Then
                msgboxWarning("Gudang pada daftar tidak boleh kosong")
                Return False
            End If
        Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "PRI", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NOPROD_IN, 3)) AS KODE FROM TM_PROD_IN WHERE LEFT(NOPROD_IN, 10) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        Dim tgl As Date = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function
End Class