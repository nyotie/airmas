﻿Public Class xrProOUT

    Private Sub ThisPrintout_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        'XrPictureBox1.ImageUrl = String.Format("{0}\{1}", My.Application.Info.DirectoryPath, "CompanyLogo.png")

        CompanyName.Text = xSet.Tables("DataCompany").Rows(0).Item("NAMA")
        ThisBranchAddress.Text = xSet.Tables("DataCompany").Rows(0).Item("ALAMAT")
        ThisBranchPhone.Text = String.Format("Telp:{0} Fax:{1}", xSet.Tables("DataCompany").Rows(0).Item("TELP"), xSet.Tables("DataCompany").Rows(0).Item("FAX"))

        SQLquery = String.Format("SELECT TANGGAL, MB.NAMA BARANG, QTY, TM.HPP, TM.MEMO, TM.ACC, MU.NAMA PELAKU " & _
                                 "FROM TM_PROD_OUT TM INNER JOIN MBARANG MB ON TM.IDBARANG=MB.IDBARANG INNER JOIN MUSER MU ON TM.IDUSER=MU.IDUSER " & _
                                 "WHERE NOPROD_OUT='{0}'", param_notrans.Value)
        ExDb.ExecQuery(SQLquery, xSet, "Get_DetailPrintOut")

        NoTrans.Text = param_notrans.Value
        TglTrans.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("TANGGAL"), "dd-MM-yyyy HH:mm")
        NamaBarang.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("BARANG")
        JumlahBarang.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("QTY")
        KeteranganTransaksi.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("MEMO")
        Nama2.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("PELAKU")
        CheckACC.Checked = IIf(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("ACC") = 0, False, True)

        DA_PRODOUT.Connection.ConnectionString = conn_string_local
        DA_PRODOUT.Fill(Dataset_Printout1.PRT_PRODOUT, param_notrans.Value)
        xSet.Tables.Remove("Get_DetailPrintOut")
    End Sub
End Class