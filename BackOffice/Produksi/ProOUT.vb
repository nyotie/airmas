﻿Imports DevExpress.Utils

Public Class ProOUT
    Dim selected_cell As String
    Private HitungHPP As Decimal

    Private Sub PengirimanCabang_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_PROD_OUT")
        xSet.Tables.Remove("TD_PROD_OUT")
        xSet.Tables.Remove("DaftarGudang")
        xSet.Tables.Remove("DaftarBarangProduksi")
        xSet.Tables.Remove("DaftarBarangFormula")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub PengirimanCabang_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub PengirimanCabang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now
        Else
            SQLquery = String.Format("SELECT TANGGAL,IDBARANG,QTY,HPP,MEMO,ACC FROM TM_PROD_OUT WHERE NOPROD_OUT='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_PROD_OUT")

            NoTrans.Text = selected_id
            TglTrans.EditValue = xSet.Tables("TM_PROD_OUT").Rows(0).Item("TANGGAL")
            DaftarBarangP.Enabled = False
            DaftarBarangP.EditValue = xSet.Tables("TM_PROD_OUT").Rows(0).Item("IDBARANG")
            QttBarang.EditValue = xSet.Tables("TM_PROD_OUT").Rows(0).Item("QTY")
            Get_DaftarBarangFormula(xSet.Tables("TM_PROD_OUT").Rows(0).Item("IDBARANG"))
            DaftarBarangP.Enabled = True
            HitungHPP = xSet.Tables("TM_PROD_OUT").Rows(0).Item("HPP")
            Catatan.EditValue = xSet.Tables("TM_PROD_OUT").Rows(0).Item("MEMO")
            ACCcheck.Checked = IIf(xSet.Tables("TM_PROD_OUT").Rows(0).Item("ACC") = 0, False, True)
        End If
    End Sub

    Private Sub DaftarBarang_EditValueChanged(sender As Object, e As EventArgs) Handles DaftarBarangP.EditValueChanged
        If Not DaftarBarangP.Enabled Or DaftarBarangP.EditValue Is Nothing Then Return

        Get_DaftarBarangFormula(DaftarBarangP.EditValue)
        '---------------------------------------------------- Daftar Barang Formula
        If Not xSet.Tables("TD_PROD_OUT") Is Nothing Then xSet.Tables("TD_PROD_OUT").Clear()
        SQLquery = String.Format("SELECT DP.IDBARANG1 BARANG, DP.QTY QTYPCS, {1}*DP.QTY QTY, MB.HPP HRGPCS, {1}*DP.QTY*MB.HPP HARGA, '' KETERANGAN, NULL IDGUDANG FROM DPRODUKSI DP " & _
                                 "INNER JOIN MBARANG MB ON DP.IDBARANG1=MB.IDBARANG WHERE DP.IDBARANG='{0}' ORDER BY NO_URUT", DaftarBarangP.EditValue, QttBarang.EditValue)
        ExDb.ExecQuery(SQLquery, xSet, "TD_PROD_OUT")
    End Sub

    Private Sub QttBarang_Validated(sender As Object, e As EventArgs) Handles QttBarang.Validated
        If xSet.Tables("TD_PROD_OUT").Rows.Count < 1 Or Not DaftarBarangP.Enabled Then Return
        If QttBarang.EditValue Is Nothing Or QttBarang.Text = "" Then
            QttBarang.EditValue = 0
        End If

        For Each drow As DataRow In xSet.Tables("TD_PROD_OUT").Rows
            drow.Item("QTY") = QttBarang.EditValue * drow.Item("QTYPCS")
            drow.Item("HARGA") = drow.Item("QTY") * drow.Item("HRGPCS")
        Next
        xSet.Tables("TD_PROD_OUT").AcceptChanges()

        ViewTrans.UpdateSummary()
        HitungHPP = ViewTrans.Columns("HARGA").SummaryItem.SummaryValue / ViewTrans.Columns("QTY").SummaryItem.SummaryValue
    End Sub

    'Private Sub DaftarBrgFormula_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarBarangF.EditValueChanged
    '    Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
    '    If Repository.Text = vbNullString Then Return

    '    selected_cell = Repository.EditValue
    'End Sub

    Private Sub ViewTrans_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans.CellValueChanged
        'Try
        '    Dim drow As DataRow = xSet.Tables("DaftarBarangFormula").Select(String.Format("NOPJ='{0}'", selected_cell))(0)

        '    If e.Column.FieldName = "NOPJ" Then
        '        For row As Integer = 0 To ViewTrans.RowCount - 1
        '            If ViewTrans.GetRowCellValue(row, "NOPJ") = selected_cell And Not ViewTrans.FocusedRowHandle = row Then
        '                ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
        '                ViewTrans.FocusedRowHandle = row
        '                Return
        '            End If
        '        Next

        '        ViewTrans.SetFocusedRowCellValue("CUSTOMER", drow.Item("CUSTOMER"))
        '        ViewTrans.FocusedRowHandle = ViewTrans.RowCount
        '    End If
        '    If IsDBNull(ViewTrans.GetFocusedRowCellValue("NOPJ")) = True Then Throw New Exception
        'Catch ex As Exception
        '    If IsDBNull(ViewTrans.GetFocusedRowCellValue("NOPJ")) = True Then
        '        ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
        '        xSet.Tables("TD_PROD_OUT").AcceptChanges()
        '    End If
        'End Try

        If e.Column.FieldName = "QTY" And ViewTrans.FocusedColumn.FieldName = "QTY" Then
            If ViewTrans.GetFocusedRowCellValue("QTY") < 0 Then
                ViewTrans.SetFocusedRowCellValue("HARGA", 0)
                ViewTrans.SetFocusedRowCellValue("QTY", 0)
            Else
                ViewTrans.SetFocusedRowCellValue("HARGA", e.Value * ViewTrans.GetFocusedRowCellValue("HRGPCS"))
            End If
        End If

        ViewTrans.UpdateSummary()
        HitungHPP = ViewTrans.Columns("HARGA").SummaryItem.SummaryValue / ViewTrans.Columns("QTY").SummaryItem.SummaryValue
    End Sub

    'Private Sub ViewTrans_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewTrans.FocusedRowChanged
    '    Try
    '        If e.FocusedRowHandle >= 0 Then
    '            selected_cell = ViewTrans.GetRowCellValue(e.FocusedRowHandle, "NOPJ")
    '        End If
    '    Catch ex As Exception
    '        ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
    '    End Try
    'End Sub

    'Private Sub GridTransKeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans.KeyDown
    '    If e.KeyCode = Keys.Delete Then
    '        ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
    '        xSet.Tables("TD_PROD_OUT").AcceptChanges()
    '    End If
    'End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC PROD_OUT_INS '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', """,
                                            NoTrans.Text, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), DaftarBarangP.EditValue,
                                            QttBarang.EditValue, HitungHPP, Catatan.Text.Replace("'", "''"), IIf(ACCcheck.Checked, 1, 0), staff_id)
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC PROD_OUT_UPD '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', """,
                                            NoTrans.Text, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), DaftarBarangP.EditValue,
                                            QttBarang.EditValue, HitungHPP, Catatan.Text.Replace("'", "''"), IIf(ACCcheck.Checked, 1, 0), staff_id)
            End If

            Dim nourut As Integer = 1
            For Each drow As DataRow In xSet.Tables("TD_PROD_OUT").Rows
                SQLquery += String.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}'), ", NoTrans.Text, nourut,
                                          drow.Item("BARANG"), drow.Item("QTYPCS"), drow.Item("QTY"), drow.Item("HRGPCS"), drow.Item("KETERANGAN"), drow.Item("IDGUDANG"))
                nourut += 1
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserProOUT.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_PROD_OUT").Clear()
        xSet.Tables("TD_PROD_OUT").AcceptChanges()

        TglTrans.EditValue = Now
        DaftarBarangP.EditValue = Nothing
        QttBarang.Text = ""
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Get_DaftarBarangFormula(ByVal BrgPro As String)
        '---------------------------------------------------- Daftar Barang Formula
        If Not xSet.Tables("DaftarBarangFormula") Is Nothing Then xSet.Tables("DaftarBarangFormula").Clear()
        SQLquery = String.Format("SELECT DP.IDBARANG1 IDBARANG, MB.NAMA BARANG, MB.ALIAS, MB.HPP HARGA FROM DPRODUKSI DP " & _
                                 "INNER JOIN MBARANG MB ON DP.IDBARANG1=MB.IDBARANG WHERE DP.IDBARANG='{0}' ORDER BY NO_URUT;", BrgPro)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarBarangFormula")
        DaftarBarangF.DataSource = xSet.Tables("DaftarBarangFormula").DefaultView

        DaftarBarangF.NullText = ""
        DaftarBarangF.ValueMember = "IDBARANG"
        DaftarBarangF.DisplayMember = "BARANG"
        DaftarBarangF.ShowClearButton = False
        DaftarBarangF.PopulateViewColumns()

        ViewDaftarBarangF.Columns("IDBARANG").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarBarangFormula").Columns
            ViewDaftarBarangF.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------Daftar Gudang 
        If Not xSet.Tables("DaftarGudang") Is Nothing Then xSet.Tables("DaftarGudang").Clear()
        SQLquery = "SELECT IDGUDANG ID, NAMA FROM MGUDANG WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarGudang")
        DaftarGudang.DataSource = xSet.Tables("DaftarGudang").DefaultView

        DaftarGudang.NullText = ""
        DaftarGudang.ValueMember = "ID"
        DaftarGudang.DisplayMember = "NAMA"
        DaftarGudang.ShowClearButton = False
        DaftarGudang.PopulateViewColumns()

        ViewDaftarGudang.Columns("ID").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarGudang").Columns
            ViewDaftarGudang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar Barang Produksi
        If Not xSet.Tables("DaftarBarangProduksi") Is Nothing Then xSet.Tables("DaftarBarangProduksi").Clear()
        SQLquery = "SELECT MB.IDBARANG ID, MK.NAMA KATEGORI, MSK.NAMA SUBKATEGORI, MB.NAMA, MB.ALIAS KIMIA, SATUAN1, SATUAN2 " & _
                    "FROM MBARANG MB INNER JOIN MKATEGORI MK ON MB.IDKATEGORI=MK.IDKATEGORI INNER JOIN MSUBKATEGORI MSK ON MB.IDSUBKATEGORI=MSK.IDSUBKATEGORI " & _
                    "WHERE MK.DETAIL=0 AND MB.INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarBarangProduksi")
        DaftarBarangP.Properties.DataSource = xSet.Tables("DaftarBarangProduksi").DefaultView

        DaftarBarangP.Properties.NullText = ""
        DaftarBarangP.Properties.ValueMember = "ID"
        DaftarBarangP.Properties.DisplayMember = "NAMA"
        DaftarBarangP.Properties.ShowClearButton = False
        DaftarBarangP.Properties.PopulateViewColumns()

        For Each coll As DataColumn In xSet.Tables("DaftarBarangProduksi").Columns
            ViewDaftarBarangP.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar Produksi Out
        SQLquery = String.Format("SELECT IDBARANG BARANG, QTYPCS, QTY, HRGPCS, HARGA, KETERANGAN, IDGUDANG FROM TD_PROD_OUT WHERE NOPROD_OUT='{0}' ORDER BY NO_URUT", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_PROD_OUT")
        GridTrans.DataSource = xSet.Tables("TD_PROD_OUT").DefaultView

        'ViewTrans.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top

        For Each coll As DataColumn In xSet.Tables("TD_PROD_OUT").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("BARANG").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("QTYPCS").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("HRGPCS").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("HARGA").OptionsColumn.AllowEdit = False

        ViewTrans.Columns("BARANG").ColumnEdit = DaftarBarangF
        ViewTrans.Columns("IDGUDANG").ColumnEdit = DaftarGudang

        ViewTrans.Columns("QTYPCS").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("QTYPCS").DisplayFormat.FormatString = "n2"
        ViewTrans.Columns("QTY").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("QTY").DisplayFormat.FormatString = "n2"
        ViewTrans.Columns("HRGPCS").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("HRGPCS").DisplayFormat.FormatString = "n2"
        ViewTrans.Columns("HARGA").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("HARGA").DisplayFormat.FormatString = "n2"

        ViewTrans.Columns("BARANG").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("BARANG").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewTrans.Columns("QTY").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("QTY").SummaryItem.DisplayFormat = "{0:n0}"
        ViewTrans.Columns("HARGA").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("HARGA").SummaryItem.DisplayFormat = "{0:n0}"

        ViewTrans.Columns("BARANG").Width = GridTrans.Width * 0.2
        ViewTrans.Columns("QTYPCS").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("QTY").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("HRGPCS").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("HARGA").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("KETERANGAN").Width = GridTrans.Width * 0.3
        ViewTrans.Columns("IDGUDANG").Width = GridTrans.Width * 0.1
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_PROD_OUT").Rows.Count < 1 Then
            Return False
        End If

        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        If DaftarBarangP.EditValue = Nothing Then
            DaftarBarangP.Focus()
            msgboxWarning("Barang diproduksi tidak boleh kosong")
            Return False
        End If

        If QttBarang.EditValue < 0 Then
            DaftarBarangP.Focus()
            msgboxWarning("Jumlah barang diproduksi tidak boleh kurang dari 1")
            Return False
        End If

        For Each drow As DataRow In xSet.Tables("TD_PROD_OUT").Rows
            If drow.Item("QTY") < 1 Then
                msgboxWarning("Jumlah barang tidak boleh kosong")
                Return False
            End If
            If IsDBNull(drow.Item("IDGUDANG")) Then
                msgboxWarning("Gudang pada daftar tidak boleh kosong")
                Return False
            End If
        Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "PRO", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NOPROD_OUT, 3)) AS KODE FROM TM_PROD_OUT WHERE LEFT(NOPROD_OUT, 10) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        Dim tgl As Date = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function
End Class