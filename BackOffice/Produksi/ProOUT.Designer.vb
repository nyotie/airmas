﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProOUT
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Catatan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridTrans = New DevExpress.XtraGrid.GridControl()
        Me.ViewTrans = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DaftarBarangF = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.ViewDaftarBarangF = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DaftarGudang = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.ViewDaftarGudang = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DaftarBarangP = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewDaftarBarangP = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButClear = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.TglTrans = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.QttBarang = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ACCcheck = New DevExpress.XtraEditors.CheckEdit()
        Me.NoTrans = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridTrans, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewTrans, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarBarangF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarBarangF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarGudang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarGudang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarBarangP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarBarangP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglTrans.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QttBarang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ACCcheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NoTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Catatan
        '
        Me.Catatan.Location = New System.Drawing.Point(12, 76)
        Me.Catatan.Name = "Catatan"
        Me.Catatan.Properties.MaxLength = 200
        Me.Catatan.Size = New System.Drawing.Size(715, 20)
        Me.Catatan.TabIndex = 6
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(288, 12)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl2.TabIndex = 12
        Me.LabelControl2.Text = "Barang di produksi :"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(163, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl1.TabIndex = 13
        Me.LabelControl1.Text = "Tanggal :"
        '
        'GridTrans
        '
        Me.GridTrans.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridTrans.Location = New System.Drawing.Point(12, 102)
        Me.GridTrans.MainView = Me.ViewTrans
        Me.GridTrans.Name = "GridTrans"
        Me.GridTrans.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.DaftarBarangF, Me.DaftarGudang})
        Me.GridTrans.Size = New System.Drawing.Size(770, 260)
        Me.GridTrans.TabIndex = 7
        Me.GridTrans.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewTrans})
        '
        'ViewTrans
        '
        Me.ViewTrans.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ViewTrans.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewTrans.ColumnPanelRowHeight = 40
        Me.ViewTrans.GridControl = Me.GridTrans
        Me.ViewTrans.Name = "ViewTrans"
        Me.ViewTrans.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTrans.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTrans.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTrans.OptionsCustomization.AllowColumnMoving = False
        Me.ViewTrans.OptionsCustomization.AllowColumnResizing = False
        Me.ViewTrans.OptionsCustomization.AllowFilter = False
        Me.ViewTrans.OptionsCustomization.AllowGroup = False
        Me.ViewTrans.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewTrans.OptionsCustomization.AllowSort = False
        Me.ViewTrans.OptionsView.ShowFooter = True
        Me.ViewTrans.OptionsView.ShowGroupPanel = False
        '
        'DaftarBarangF
        '
        Me.DaftarBarangF.AutoHeight = False
        Me.DaftarBarangF.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarBarangF.Name = "DaftarBarangF"
        Me.DaftarBarangF.PopupFormSize = New System.Drawing.Size(700, 300)
        Me.DaftarBarangF.View = Me.ViewDaftarBarangF
        '
        'ViewDaftarBarangF
        '
        Me.ViewDaftarBarangF.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarBarangF.Name = "ViewDaftarBarangF"
        Me.ViewDaftarBarangF.OptionsCustomization.AllowColumnMoving = False
        Me.ViewDaftarBarangF.OptionsCustomization.AllowColumnResizing = False
        Me.ViewDaftarBarangF.OptionsCustomization.AllowFilter = False
        Me.ViewDaftarBarangF.OptionsCustomization.AllowGroup = False
        Me.ViewDaftarBarangF.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewDaftarBarangF.OptionsCustomization.AllowSort = False
        Me.ViewDaftarBarangF.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarBarangF.OptionsView.ShowGroupPanel = False
        '
        'DaftarGudang
        '
        Me.DaftarGudang.AutoHeight = False
        Me.DaftarGudang.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarGudang.Name = "DaftarGudang"
        Me.DaftarGudang.View = Me.ViewDaftarGudang
        '
        'ViewDaftarGudang
        '
        Me.ViewDaftarGudang.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarGudang.Name = "ViewDaftarGudang"
        Me.ViewDaftarGudang.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarGudang.OptionsView.ShowGroupPanel = False
        '
        'DaftarBarangP
        '
        Me.DaftarBarangP.Location = New System.Drawing.Point(288, 31)
        Me.DaftarBarangP.Name = "DaftarBarangP"
        Me.DaftarBarangP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarBarangP.Properties.NullText = ""
        Me.DaftarBarangP.Properties.View = Me.ViewDaftarBarangP
        Me.DaftarBarangP.Size = New System.Drawing.Size(119, 20)
        Me.DaftarBarangP.TabIndex = 3
        '
        'ViewDaftarBarangP
        '
        Me.ViewDaftarBarangP.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarBarangP.Name = "ViewDaftarBarangP"
        Me.ViewDaftarBarangP.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarBarangP.OptionsView.ShowGroupPanel = False
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Location = New System.Drawing.Point(198, 400)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(87, 29)
        Me.ButBatal.TabIndex = 11
        Me.ButBatal.Text = "&Batal"
        '
        'ButClear
        '
        Me.ButClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButClear.Appearance.Options.UseFont = True
        Me.ButClear.Location = New System.Drawing.Point(105, 400)
        Me.ButClear.Name = "ButClear"
        Me.ButClear.Size = New System.Drawing.Size(87, 29)
        Me.ButClear.TabIndex = 10
        Me.ButClear.Text = "&Clear"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Location = New System.Drawing.Point(12, 400)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(87, 29)
        Me.ButSimpan.TabIndex = 9
        Me.ButSimpan.Text = "&Simpan"
        '
        'LineShape1
        '
        Me.LineShape1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 12
        Me.LineShape1.X2 = 780
        Me.LineShape1.Y1 = 390
        Me.LineShape1.Y2 = 390
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(794, 441)
        Me.ShapeContainer1.TabIndex = 0
        Me.ShapeContainer1.TabStop = False
        '
        'TglTrans
        '
        Me.TglTrans.EditValue = Nothing
        Me.TglTrans.Location = New System.Drawing.Point(163, 31)
        Me.TglTrans.Name = "TglTrans"
        Me.TglTrans.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TglTrans.Properties.Mask.EditMask = "dd MMM yyyy, HH:mm"
        Me.TglTrans.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TglTrans.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TglTrans.Size = New System.Drawing.Size(119, 20)
        Me.TglTrans.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(12, 57)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl3.TabIndex = 12
        Me.LabelControl3.Text = "Catatan :"
        '
        'QttBarang
        '
        Me.QttBarang.EditValue = 0
        Me.QttBarang.Location = New System.Drawing.Point(413, 31)
        Me.QttBarang.Name = "QttBarang"
        Me.QttBarang.Properties.Appearance.Options.UseTextOptions = True
        Me.QttBarang.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.QttBarang.Properties.Mask.EditMask = "n2"
        Me.QttBarang.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.QttBarang.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.QttBarang.Properties.ValidateOnEnterKey = True
        Me.QttBarang.Size = New System.Drawing.Size(119, 20)
        Me.QttBarang.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(413, 12)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl4.TabIndex = 12
        Me.LabelControl4.Text = "Jumlah :"
        '
        'ACCcheck
        '
        Me.ACCcheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ACCcheck.Location = New System.Drawing.Point(12, 368)
        Me.ACCcheck.Name = "ACCcheck"
        Me.ACCcheck.Properties.Caption = "ACC"
        Me.ACCcheck.Size = New System.Drawing.Size(47, 19)
        Me.ACCcheck.TabIndex = 8
        '
        'NoTrans
        '
        Me.NoTrans.Location = New System.Drawing.Point(12, 31)
        Me.NoTrans.Name = "NoTrans"
        Me.NoTrans.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoTrans.Properties.Appearance.Options.UseFont = True
        Me.NoTrans.Properties.ReadOnly = True
        Me.NoTrans.Size = New System.Drawing.Size(145, 21)
        Me.NoTrans.TabIndex = 1
        Me.NoTrans.TabStop = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl8.TabIndex = 12
        Me.LabelControl8.Text = "Nomor transaksi :"
        '
        'ProOUT
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(794, 441)
        Me.Controls.Add(Me.NoTrans)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.ACCcheck)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.QttBarang)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.TglTrans)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButClear)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.Catatan)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.GridTrans)
        Me.Controls.Add(Me.DaftarBarangP)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.Name = "ProOUT"
        Me.ShowIcon = False
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridTrans, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewTrans, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarBarangF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarBarangF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarGudang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarGudang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarBarangP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarBarangP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglTrans.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QttBarang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ACCcheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NoTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Catatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridTrans As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewTrans As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DaftarBarangF As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents ViewDaftarBarangF As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DaftarBarangP As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewDaftarBarangP As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents TglTrans As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents QttBarang As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ACCcheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents NoTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DaftarGudang As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents ViewDaftarGudang As DevExpress.XtraGrid.Views.Grid.GridView
End Class
