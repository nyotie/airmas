﻿Public Class loginForm

    Private Sub loginForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        PictureBox1.ImageLocation = String.Format("{0}\{1}", My.Application.Info.DirectoryPath, My.Settings.login_sprite_name)
        LoginUsername.Text = ""
        LoginPassword.Text = ""
        status_login = False

        staff_id = 0
        staff_group = 0
        staff_name = ""

        LoginUsername.Focus()
    End Sub

    Private Sub LoginUsername_GotFocus(ByVal sender As Object, ByVal e As EventArgs) Handles LoginUsername.GotFocus
        LoginUsername.Select(0, LoginUsername.Text.Length)
    End Sub

    Private Sub LoginUsername_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles LoginUsername.KeyDown
        If Not (My.Computer.Name = "NYOTO-PC" Or My.Computer.Name = "WIN-P0SH11B6T0C") Then Exit Sub
        If e.KeyCode = Keys.F5 Then
            LoginUsername.Text = "admin"
            LoginPassword.Text = "123123"
            tryLogin()
        End If
    End Sub

    Private Sub LoginUsername_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles LoginUsername.KeyPress
        If e.KeyChar = ChrW(13) Then
            LoginPassword.Focus()
        End If
    End Sub

    Private Sub LoginPassword_GotFocus(ByVal sender As Object, ByVal e As EventArgs) Handles LoginPassword.GotFocus
        LoginPassword.Select(0, LoginPassword.Text.Length)
    End Sub

    Private Sub LoginPassword_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles LoginPassword.KeyPress
        If e.KeyChar = ChrW(13) Then
            butLogin.Focus()
            tryLogin()
        End If
    End Sub

    Private Sub tryLogin()
        ErrorProvider1.Clear()
        If LoginUsername.Text <> "" And LoginPassword.Text <> "" Then
            Try
                LoadSprite.ShowLoading()
                status_login = True

                If xSet.Tables("DataLoginUser") IsNot Nothing Then xSet.Tables("DataLoginUser").Clear()
                SQLquery = String.Format("SELECT IDUSER, NAMA, INACTIVE FROM MUSER a WHERE USERNAME='{0}' AND PWD='{1}'", LoginUsername.Text, Encryptor.EncryptData(LoginPassword.EditValue))
                ExDb.ExecQuery(SQLquery, xSet, "DataLoginUser")

                If xSet.Tables("DataLoginUser").Rows.Count < 1 Then
                    Throw New Exception
                Else
                    staff_id = xSet.Tables("DataLoginUser").Rows(0).Item("IDUSER")
                    staff_name = xSet.Tables("DataLoginUser").Rows(0).Item("NAMA")

                    If xSet.Tables("DataLoginUser").Rows(0).Item("INACTIVE") = 1 Then
                        ErrorProvider1.SetError(LoginUsername, "User sudah tidak aktif")
                        status_login = False
                        Return
                    End If
                End If
                LoadSprite.CloseLoading()

                If status_login = True Then Close()
            Catch ex As Exception
                ErrorProvider1.SetError(LoginUsername, "Username/password salah")
                status_login = False
                LoadSprite.CloseLoading()
            End Try
        End If
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butLogin.Click
        tryLogin()
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub
End Class