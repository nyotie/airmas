﻿Public Class xrPenerimaanBarang

    Private Sub ThisPrintout_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        'XrPictureBox1.ImageUrl = String.Format("{0}\{1}", My.Application.Info.DirectoryPath, "CompanyLogo.png")

        CompanyName.Text = xSet.Tables("DataCompany").Rows(0).Item("NAMA")
        ThisBranchAddress.Text = xSet.Tables("DataCompany").Rows(0).Item("ALAMAT")
        ThisBranchPhone.Text = String.Format("Telp:{0} Fax:{1}", xSet.Tables("DataCompany").Rows(0).Item("TELP"), xSet.Tables("DataCompany").Rows(0).Item("FAX"))

        SQLquery = String.Format("SELECT MS.NAMA SUPPLIER, MS.ALAMAT, TANGGAL, MEMO, ACC, MU.NAMA NAMA2 " & _
                                 "FROM TM_PB TM INNER JOIN MSUPPLIER MS ON TM.IDSUPPLIER=MS.IDSUPPLIER INNER JOIN MUSER MU ON TM.IDUSER=MU.IDUSER " & _
                                 "WHERE NOPB='{0}'", param_notrans.Value)
        ExDb.ExecQuery(SQLquery, xSet, "Get_DetailPrintOut")

        NoTrans.Text = param_notrans.Value
        TglTrans.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("TANGGAL"), "dd-MM-yyyy HH:mm")
        NamaTujuan.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("SUPPLIER")
        AlamatTujuan.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("ALAMAT")
        KeteranganTransaksi.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("MEMO")
        Nama2.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("NAMA2")
        CheckACC.Checked = IIf(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("ACC") = 0, False, True)

        DA_TDPB.Connection.ConnectionString = conn_string_local
        DA_TDPB.Fill(Dataset_Printout1.PRT_TDPB, param_notrans.Value)
        xSet.Tables.Remove("Get_DetailPrintOut")
    End Sub
End Class