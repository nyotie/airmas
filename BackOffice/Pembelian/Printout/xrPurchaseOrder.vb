﻿Public Class xrPurchaseOrder

    Private Sub ThisPrintout_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        'XrPictureBox1.ImageUrl = String.Format("{0}\{1}", My.Application.Info.DirectoryPath, "CompanyLogo.png")

        CompanyName.Text = xSet.Tables("DataCompany").Rows(0).Item("NAMA")
        ThisBranchAddress.Text = xSet.Tables("DataCompany").Rows(0).Item("ALAMAT")
        ThisBranchPhone.Text = String.Format("Telp:{0} Fax:{1}", xSet.Tables("DataCompany").Rows(0).Item("TELP"), xSet.Tables("DataCompany").Rows(0).Item("FAX"))

        SQLquery = String.Format("SELECT MS.NAMA SUPPLIER, MS.ALAMAT, TANGGAL, JTH_TEMPO, SUPP_PIC NAMA1, DISC, PPN, TOTAL, GRANDTOTAL, MEMO, ACC, MU.NAMA NAMA2 " & _
                                 "FROM TM_PO TM INNER JOIN MSUPPLIER MS ON TM.IDSUPPLIER=MS.IDSUPPLIER INNER JOIN MUSER MU ON TM.IDUSER=MU.IDUSER " & _
                                 "WHERE NOPO='{0}'", param_notrans.Value)
        ExDb.ExecQuery(SQLquery, xSet, "Get_DetailPrintOut")

        NoTrans.Text = param_notrans.Value
        TglTrans.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("TANGGAL"), "dd-MM-yyyy")
        NamaTujuan.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("SUPPLIER")
        AlamatTujuan.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("ALAMAT")
        Cell_disc.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("DISC"), "n0")
        Cell_ppn.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("PPN"), "n0")
        Cell_grandtotal.Text = Format(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("GRANDTOTAL"), "n0")
        KeteranganTransaksi.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("MEMO")
        Nama1.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("NAMA1")
        Nama2.Text = xSet.Tables("Get_DetailPrintOut").Rows(0).Item("NAMA2")
        CheckACC.Checked = IIf(xSet.Tables("Get_DetailPrintOut").Rows(0).Item("ACC") = 0, False, True)

        DA_TDPO.Connection.ConnectionString = conn_string_local
        DA_TDPO.Fill(Dataset_Printout1.PRT_TDPO, param_notrans.Value)
        xSet.Tables.Remove("Get_DetailPrintOut")
    End Sub
End Class