﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PurchaseOrder
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Catatan = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridTrans = New DevExpress.XtraGrid.GridControl()
        Me.ViewTrans = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DaftarBarang = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.ViewDaftarBarang = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DaftarSupplier = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.ViewDaftarSupplier = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ButBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.ButClear = New DevExpress.XtraEditors.SimpleButton()
        Me.ButSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.TglTrans = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.JthTempo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.PICName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.PPNCheck = New DevExpress.XtraEditors.CheckEdit()
        Me.DiscTrans = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.GTTrans = New DevExpress.XtraEditors.TextEdit()
        Me.PPNValue = New DevExpress.XtraEditors.TextEdit()
        Me.ACCcheck = New DevExpress.XtraEditors.CheckEdit()
        Me.NoTrans = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridTrans, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewTrans, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarBarang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarBarang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DaftarSupplier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewDaftarSupplier, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglTrans.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TglTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JthTempo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PICName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PPNCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DiscTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GTTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PPNValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ACCcheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NoTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Catatan
        '
        Me.Catatan.Location = New System.Drawing.Point(12, 76)
        Me.Catatan.Name = "Catatan"
        Me.Catatan.Properties.MaxLength = 200
        Me.Catatan.Size = New System.Drawing.Size(715, 20)
        Me.Catatan.TabIndex = 5
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(288, 12)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl2.TabIndex = 12
        Me.LabelControl2.Text = "Supplier :"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(163, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl1.TabIndex = 13
        Me.LabelControl1.Text = "Tanggal :"
        '
        'GridTrans
        '
        Me.GridTrans.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridTrans.Location = New System.Drawing.Point(12, 102)
        Me.GridTrans.MainView = Me.ViewTrans
        Me.GridTrans.Name = "GridTrans"
        Me.GridTrans.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.DaftarBarang})
        Me.GridTrans.Size = New System.Drawing.Size(770, 203)
        Me.GridTrans.TabIndex = 6
        Me.GridTrans.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.ViewTrans})
        '
        'ViewTrans
        '
        Me.ViewTrans.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ViewTrans.Appearance.HeaderPanel.Options.UseFont = True
        Me.ViewTrans.ColumnPanelRowHeight = 40
        Me.ViewTrans.GridControl = Me.GridTrans
        Me.ViewTrans.Name = "ViewTrans"
        Me.ViewTrans.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTrans.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTrans.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.ViewTrans.OptionsCustomization.AllowColumnMoving = False
        Me.ViewTrans.OptionsCustomization.AllowColumnResizing = False
        Me.ViewTrans.OptionsCustomization.AllowFilter = False
        Me.ViewTrans.OptionsCustomization.AllowGroup = False
        Me.ViewTrans.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewTrans.OptionsCustomization.AllowSort = False
        Me.ViewTrans.OptionsView.ShowFooter = True
        Me.ViewTrans.OptionsView.ShowGroupPanel = False
        '
        'DaftarBarang
        '
        Me.DaftarBarang.AutoHeight = False
        Me.DaftarBarang.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarBarang.Name = "DaftarBarang"
        Me.DaftarBarang.PopupFormSize = New System.Drawing.Size(700, 300)
        Me.DaftarBarang.View = Me.ViewDaftarBarang
        '
        'ViewDaftarBarang
        '
        Me.ViewDaftarBarang.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarBarang.Name = "ViewDaftarBarang"
        Me.ViewDaftarBarang.OptionsCustomization.AllowColumnMoving = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowColumnResizing = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowFilter = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowGroup = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowQuickHideColumns = False
        Me.ViewDaftarBarang.OptionsCustomization.AllowSort = False
        Me.ViewDaftarBarang.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarBarang.OptionsView.ShowGroupPanel = False
        '
        'DaftarSupplier
        '
        Me.DaftarSupplier.Location = New System.Drawing.Point(288, 31)
        Me.DaftarSupplier.Name = "DaftarSupplier"
        Me.DaftarSupplier.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DaftarSupplier.Properties.NullText = ""
        Me.DaftarSupplier.Properties.View = Me.ViewDaftarSupplier
        Me.DaftarSupplier.Size = New System.Drawing.Size(119, 20)
        Me.DaftarSupplier.TabIndex = 2
        '
        'ViewDaftarSupplier
        '
        Me.ViewDaftarSupplier.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.ViewDaftarSupplier.Name = "ViewDaftarSupplier"
        Me.ViewDaftarSupplier.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.ViewDaftarSupplier.OptionsView.ShowGroupPanel = False
        '
        'ButBatal
        '
        Me.ButBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButBatal.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButBatal.Appearance.Options.UseFont = True
        Me.ButBatal.Location = New System.Drawing.Point(198, 400)
        Me.ButBatal.Name = "ButBatal"
        Me.ButBatal.Size = New System.Drawing.Size(87, 29)
        Me.ButBatal.TabIndex = 12
        Me.ButBatal.Text = "&Batal"
        '
        'ButClear
        '
        Me.ButClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButClear.Appearance.Options.UseFont = True
        Me.ButClear.Location = New System.Drawing.Point(105, 400)
        Me.ButClear.Name = "ButClear"
        Me.ButClear.Size = New System.Drawing.Size(87, 29)
        Me.ButClear.TabIndex = 11
        Me.ButClear.Text = "&Clear"
        '
        'ButSimpan
        '
        Me.ButSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButSimpan.Appearance.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButSimpan.Appearance.Options.UseFont = True
        Me.ButSimpan.Location = New System.Drawing.Point(12, 400)
        Me.ButSimpan.Name = "ButSimpan"
        Me.ButSimpan.Size = New System.Drawing.Size(87, 29)
        Me.ButSimpan.TabIndex = 10
        Me.ButSimpan.Text = "&Simpan"
        '
        'LineShape1
        '
        Me.LineShape1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LineShape1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 12
        Me.LineShape1.X2 = 780
        Me.LineShape1.Y1 = 390
        Me.LineShape1.Y2 = 390
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(794, 441)
        Me.ShapeContainer1.TabIndex = 19
        Me.ShapeContainer1.TabStop = False
        '
        'TglTrans
        '
        Me.TglTrans.EditValue = Nothing
        Me.TglTrans.Location = New System.Drawing.Point(163, 31)
        Me.TglTrans.Name = "TglTrans"
        Me.TglTrans.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TglTrans.Properties.Mask.EditMask = "dd MMM yyyy, HH:mm"
        Me.TglTrans.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TglTrans.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TglTrans.Size = New System.Drawing.Size(119, 20)
        Me.TglTrans.TabIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(12, 57)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl3.TabIndex = 12
        Me.LabelControl3.Text = "Catatan :"
        '
        'JthTempo
        '
        Me.JthTempo.Location = New System.Drawing.Point(413, 31)
        Me.JthTempo.Name = "JthTempo"
        Me.JthTempo.Properties.Mask.EditMask = "[0-9]+"
        Me.JthTempo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.JthTempo.Size = New System.Drawing.Size(92, 20)
        Me.JthTempo.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(413, 12)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl4.TabIndex = 12
        Me.LabelControl4.Text = "Jatuh tempo :"
        '
        'PICName
        '
        Me.PICName.Location = New System.Drawing.Point(511, 31)
        Me.PICName.Name = "PICName"
        Me.PICName.Size = New System.Drawing.Size(216, 20)
        Me.PICName.TabIndex = 4
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(511, 12)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl5.TabIndex = 12
        Me.LabelControl5.Text = "Supplier PIC :"
        '
        'PPNCheck
        '
        Me.PPNCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PPNCheck.Location = New System.Drawing.Point(580, 337)
        Me.PPNCheck.Name = "PPNCheck"
        Me.PPNCheck.Properties.Caption = "PPN"
        Me.PPNCheck.Size = New System.Drawing.Size(47, 19)
        Me.PPNCheck.TabIndex = 9
        '
        'DiscTrans
        '
        Me.DiscTrans.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DiscTrans.EditValue = "0"
        Me.DiscTrans.Location = New System.Drawing.Point(659, 311)
        Me.DiscTrans.Name = "DiscTrans"
        Me.DiscTrans.Properties.Appearance.Options.UseTextOptions = True
        Me.DiscTrans.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.DiscTrans.Properties.Mask.EditMask = "n2"
        Me.DiscTrans.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.DiscTrans.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DiscTrans.Size = New System.Drawing.Size(123, 20)
        Me.DiscTrans.TabIndex = 8
        '
        'LabelControl6
        '
        Me.LabelControl6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl6.Location = New System.Drawing.Point(599, 314)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl6.TabIndex = 48
        Me.LabelControl6.Text = "Discount"
        '
        'LabelControl7
        '
        Me.LabelControl7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl7.Location = New System.Drawing.Point(599, 366)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl7.TabIndex = 47
        Me.LabelControl7.Text = "Grand total"
        '
        'GTTrans
        '
        Me.GTTrans.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GTTrans.EditValue = "0"
        Me.GTTrans.Location = New System.Drawing.Point(659, 363)
        Me.GTTrans.Name = "GTTrans"
        Me.GTTrans.Properties.Appearance.Options.UseTextOptions = True
        Me.GTTrans.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.GTTrans.Properties.Mask.EditMask = "n2"
        Me.GTTrans.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.GTTrans.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.GTTrans.Properties.ReadOnly = True
        Me.GTTrans.Size = New System.Drawing.Size(123, 20)
        Me.GTTrans.TabIndex = 49
        Me.GTTrans.TabStop = False
        '
        'PPNValue
        '
        Me.PPNValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PPNValue.EditValue = "0"
        Me.PPNValue.Location = New System.Drawing.Point(659, 337)
        Me.PPNValue.Name = "PPNValue"
        Me.PPNValue.Properties.Appearance.Options.UseTextOptions = True
        Me.PPNValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.PPNValue.Properties.Mask.EditMask = "n2"
        Me.PPNValue.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.PPNValue.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.PPNValue.Properties.ReadOnly = True
        Me.PPNValue.Size = New System.Drawing.Size(123, 20)
        Me.PPNValue.TabIndex = 50
        Me.PPNValue.TabStop = False
        '
        'ACCcheck
        '
        Me.ACCcheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ACCcheck.Location = New System.Drawing.Point(12, 311)
        Me.ACCcheck.Name = "ACCcheck"
        Me.ACCcheck.Properties.Caption = "ACC"
        Me.ACCcheck.Size = New System.Drawing.Size(47, 19)
        Me.ACCcheck.TabIndex = 7
        '
        'NoTrans
        '
        Me.NoTrans.Location = New System.Drawing.Point(12, 31)
        Me.NoTrans.Name = "NoTrans"
        Me.NoTrans.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoTrans.Properties.Appearance.Options.UseFont = True
        Me.NoTrans.Properties.ReadOnly = True
        Me.NoTrans.Size = New System.Drawing.Size(145, 21)
        Me.NoTrans.TabIndex = 0
        Me.NoTrans.TabStop = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl8.TabIndex = 12
        Me.LabelControl8.Text = "Nomor transaksi :"
        '
        'PurchaseOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(794, 441)
        Me.Controls.Add(Me.NoTrans)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.ACCcheck)
        Me.Controls.Add(Me.PPNCheck)
        Me.Controls.Add(Me.DiscTrans)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.GTTrans)
        Me.Controls.Add(Me.PPNValue)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.PICName)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.JthTempo)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.TglTrans)
        Me.Controls.Add(Me.ButBatal)
        Me.Controls.Add(Me.ButClear)
        Me.Controls.Add(Me.ButSimpan)
        Me.Controls.Add(Me.Catatan)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.GridTrans)
        Me.Controls.Add(Me.DaftarSupplier)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.Name = "PurchaseOrder"
        Me.ShowIcon = False
        CType(Me.Catatan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridTrans, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewTrans, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarBarang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarBarang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DaftarSupplier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewDaftarSupplier, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglTrans.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TglTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JthTempo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PICName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PPNCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DiscTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GTTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PPNValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ACCcheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NoTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Catatan As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridTrans As DevExpress.XtraGrid.GridControl
    Friend WithEvents ViewTrans As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DaftarBarang As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents ViewDaftarBarang As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DaftarSupplier As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents ViewDaftarSupplier As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ButBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ButSimpan As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents TglTrans As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents JthTempo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PICName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PPNCheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents DiscTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GTTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PPNValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ACCcheck As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents NoTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
End Class
