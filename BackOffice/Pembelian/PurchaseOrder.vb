﻿Imports DevExpress.Utils

Public Class PurchaseOrder
    Dim TotalTrans As Double
    Dim selected_cell As String

    Private Sub PengirimanCabang_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_PO")
        xSet.Tables.Remove("TD_PO")
        xSet.Tables.Remove("DaftarBarang")
        xSet.Tables.Remove("DaftarSupplier")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub PengirimanCabang_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub PengirimanCabang_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now
        Else
            SQLquery = String.Format("SELECT NOPO,IDSUPPLIER,TANGGAL,JTH_TEMPO,SUPP_PIC,DISC,STS_PPN,PPN,TOTAL,GRANDTOTAL,MEMO,ACC FROM TM_PO WHERE NOPO='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_PO")

            NoTrans.Text = xSet.Tables("TM_PO").Rows(0).Item("NOPO")
            TglTrans.EditValue = xSet.Tables("TM_PO").Rows(0).Item("TANGGAL")
            DaftarSupplier.EditValue = xSet.Tables("TM_PO").Rows(0).Item("IDSUPPLIER")
            JthTempo.EditValue = xSet.Tables("TM_PO").Rows(0).Item("JTH_TEMPO")
            PICName.EditValue = xSet.Tables("TM_PO").Rows(0).Item("SUPP_PIC")
            DiscTrans.EditValue = xSet.Tables("TM_PO").Rows(0).Item("DISC")
            PPNCheck.EditValue = xSet.Tables("TM_PO").Rows(0).Item("STS_PPN")
            PPNValue.EditValue = xSet.Tables("TM_PO").Rows(0).Item("PPN")
            GTTrans.EditValue = xSet.Tables("TM_PO").Rows(0).Item("GRANDTOTAL")
            Catatan.EditValue = xSet.Tables("TM_PO").Rows(0).Item("MEMO")
            ACCcheck.Checked = IIf(xSet.Tables("TM_PO").Rows(0).Item("ACC") = 0, False, True)
        End If

        DaftarSupplier.Properties.ShowClearButton = True
        DaftarSupplier.Properties.ShowAddNewButton = True
    End Sub

    Private Sub DaftarSupplier_AddNewValue(sender As Object, e As DevExpress.XtraEditors.Controls.AddNewValueEventArgs) Handles DaftarSupplier.AddNewValue
        selmaster_id = "0"
        MasterSupplier.ShowDialog()
        MasterSupplier = Nothing

        If Not xSet.Tables("DaftarSupplier") Is Nothing Then xSet.Tables("DaftarSupplier").Clear()
        SQLquery = "SELECT IDSUPPLIER ID, NAMA, ALAMAT, KOTA FROM MSUPPLIER WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarSupplier")
    End Sub

    Private Sub DaftarBarang_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarBarang.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell = Repository.EditValue
    End Sub

    Private Sub ViewTrans_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans.CellValueChanged
        Try
            Dim drow As DataRow = xSet.Tables("DaftarBarang").Select(String.Format("ID='{0}'", selected_cell))(0)

            If e.Column.FieldName = "Barang" Then
                For row As Integer = 0 To ViewTrans.RowCount - 1
                    If ViewTrans.GetRowCellValue(row, "Barang") = selected_cell And Not ViewTrans.FocusedRowHandle = row Then
                        ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                        ViewTrans.FocusedRowHandle = row
                        Return
                    End If
                Next

                ViewTrans.SetFocusedRowCellValue("Jumlah", 1)
                ViewTrans.SetFocusedRowCellValue("Harga", drow.Item("HARGA"))
                ViewTrans.SetFocusedRowCellValue("Total", drow.Item("HARGA"))
                ViewTrans.SetFocusedRowCellValue("Satuan", drow.Item("SATUAN2"))
                ViewTrans.SetFocusedRowCellValue("TglKirim", Today.Date)
                ViewTrans.FocusedRowHandle = ViewTrans.RowCount
            End If
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("Barang")) = True Then Throw New Exception

            If e.Column.FieldName = "Jumlah" And ViewTrans.FocusedColumn.FieldName = "Jumlah" Then
                If ViewTrans.GetFocusedRowCellValue("Jumlah") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("Jumlah", 0)
                    ViewTrans.SetFocusedRowCellValue("Total", 0)
                Else
                    ViewTrans.SetFocusedRowCellValue("Total", e.Value * ViewTrans.GetFocusedRowCellValue("Harga"))
                End If
            ElseIf e.Column.FieldName = "Harga" And ViewTrans.FocusedColumn.FieldName = "Harga" Then
                If ViewTrans.GetFocusedRowCellValue("Harga") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("Harga", 0)
                    ViewTrans.SetFocusedRowCellValue("Total", 0)
                Else
                    ViewTrans.SetFocusedRowCellValue("Total", e.Value * ViewTrans.GetFocusedRowCellValue("Jumlah"))
                End If
            End If
            Recalc_Total()
        Catch ex As Exception
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("Barang")) = True Then
                ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                xSet.Tables("TD_PO").AcceptChanges()
            End If
        End Try
    End Sub

    Private Sub ViewTrans_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewTrans.FocusedRowChanged
        Try
            If e.FocusedRowHandle >= 0 Then
                selected_cell = ViewTrans.GetRowCellValue(e.FocusedRowHandle, "Barang")
            End If
        Catch ex As Exception
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
        End Try
    End Sub

    Private Sub GridTransKeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
            xSet.Tables("TD_PO").AcceptChanges()

            Recalc_Total()
        End If
    End Sub

    Private Sub DiscTrans_EditValueChanged(sender As Object, e As EventArgs) Handles DiscTrans.EditValueChanged
        Recalc_Total()
    End Sub

    Private Sub PPNCheck_CheckedChanged(sender As Object, e As EventArgs) Handles PPNCheck.CheckedChanged
        Recalc_Total()
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC PO_INS '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', """,
                                            NoTrans.Text, DaftarSupplier.EditValue, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), JthTempo.EditValue, PICName.Text.Replace("'", "''"), DiscTrans.EditValue, PPNCheck.Checked, PPNValue.EditValue, TotalTrans, GTTrans.EditValue, Catatan.Text.Replace("'", "''"), IIf(ACCcheck.Checked, 1, 0), staff_id)
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC PO_UPD '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', """,
                                            NoTrans.Text, DaftarSupplier.EditValue, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), JthTempo.EditValue, PICName.Text.Replace("'", "''"), DiscTrans.EditValue, PPNCheck.Checked, PPNValue.EditValue, TotalTrans, GTTrans.EditValue, Catatan.Text.Replace("'", "''"), IIf(ACCcheck.Checked, 1, 0), staff_id)
            End If

            Dim nourut As Integer = 1
            For Each drow As DataRow In xSet.Tables("TD_PO").Rows
                SQLquery += String.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}'), ", NoTrans.Text, nourut, drow.Item("Barang"), drow.Item("Jumlah"), drow.Item("Harga"), Format(drow.Item("TglKirim"), "yyyy-MM-dd"))
                nourut += 1
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserPurchaseOrder.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            'Using printout As New xrPengirimanCabang
            '    printout.ShowRibbonPreviewDialog()
            'End Using

            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_PO").Clear()
        xSet.Tables("TD_PO").AcceptChanges()

        ViewTrans.UpdateSummary()
        TotalTrans = ViewTrans.Columns("Total").SummaryItem.SummaryValue

        TglTrans.EditValue = Now
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------Daftar Supplier
        If Not xSet.Tables("DaftarSupplier") Is Nothing Then xSet.Tables("DaftarSupplier").Clear()
        SQLquery = "SELECT IDSUPPLIER ID, NAMA, ALAMAT, KOTA FROM MSUPPLIER WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarSupplier")
        DaftarSupplier.Properties.DataSource = xSet.Tables("DaftarSupplier").DefaultView

        DaftarSupplier.Properties.NullText = ""
        DaftarSupplier.Properties.ValueMember = "ID"
        DaftarSupplier.Properties.DisplayMember = "NAMA"
        DaftarSupplier.Properties.ShowClearButton = False
        DaftarSupplier.Properties.PopulateViewColumns()

        For Each coll As DataColumn In xSet.Tables("DaftarSupplier").Columns
            ViewDaftarSupplier.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar Barang 
        If Not xSet.Tables("DaftarBarang") Is Nothing Then xSet.Tables("DaftarBarang").Clear()
        SQLquery = "SELECT MB.IDBARANG ID, MK.NAMA KATEGORI, MSK.NAMA SUBKATEGORI, MB.NAMA, MB.ALIAS KIMIA, SATUAN2, HARGABELI HARGA FROM MBARANG MB " & _
                    "INNER JOIN MKATEGORI MK ON MB.IDKATEGORI=MK.IDKATEGORI INNER JOIN MSUBKATEGORI MSK ON MB.IDSUBKATEGORI=MSK.IDSUBKATEGORI " & _
                    "WHERE MB.INACTIVE=0 AND MB.IDKATEGORI IN ('K-001','K-002');"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarBarang")
        DaftarBarang.DataSource = xSet.Tables("DaftarBarang").DefaultView

        DaftarBarang.NullText = ""
        DaftarBarang.ValueMember = "ID"
        DaftarBarang.DisplayMember = "NAMA"
        DaftarBarang.ShowClearButton = False
        DaftarBarang.PopulateViewColumns()

        ViewDaftarBarang.Columns("HARGA").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarBarang").Columns
            ViewDaftarBarang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------TD_PO
        SQLquery = String.Format("SELECT TD.IDBARANG Barang, QTY Jumlah, MB.SATUAN2 Satuan, HARGA Harga, TOTAL Total, TGL_KIRIM TglKirim FROM TD_PO TD INNER JOIN MBARANG MB ON TD.IDBARANG=MB.IDBARANG WHERE NOPO='{0}' ORDER BY NO_URUT", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_PO")
        GridTrans.DataSource = xSet.Tables("TD_PO").DefaultView

        ViewTrans.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top

        For Each coll As DataColumn In xSet.Tables("TD_PO").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("Total").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("Barang").ColumnEdit = DaftarBarang

        ViewTrans.Columns("Jumlah").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Jumlah").DisplayFormat.FormatString = "n2"
        ViewTrans.Columns("Harga").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Harga").DisplayFormat.FormatString = "n2"
        ViewTrans.Columns("Total").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Total").DisplayFormat.FormatString = "n2"
        ViewTrans.Columns("TglKirim").DisplayFormat.FormatType = FormatType.DateTime
        ViewTrans.Columns("TglKirim").DisplayFormat.FormatString = "dd/MMM/yyyy"

        ViewTrans.Columns("Barang").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("Barang").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewTrans.Columns("Jumlah").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("Jumlah").SummaryItem.DisplayFormat = "Total:{0:n2}"
        ViewTrans.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("Total").SummaryItem.DisplayFormat = "Total:{0:n2}"

        ViewTrans.Columns("Satuan").OptionsColumn.AllowEdit = False

        ViewTrans.Columns("Barang").Width = GridTrans.Width * 0.4
        ViewTrans.Columns("Jumlah").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("Harga").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("Total").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("TglKirim").Width = GridTrans.Width * 0.15
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_PO").Rows.Count < 1 Then
            Return False
        End If

        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        If DaftarSupplier.EditValue = Nothing Then
            DaftarSupplier.Focus()
            msgboxWarning("Supplier tidak boleh kosong")
            Return False
        End If

        If JthTempo.EditValue < 0 Then
            JthTempo.EditValue = 0
            msgboxWarning("Jatuh tempo tidak boleh kurang dari 0")
            Return False
        End If
        If DiscTrans.EditValue < 0 Then
            msgboxWarning("Diskon tidak boleh kurang dari 0")
            DiscTrans.EditValue = 0
            Return False
        End If

        For Each drow As DataRow In xSet.Tables("TD_PO").Rows
            If drow.Item("Jumlah") < 1 Then
                msgboxWarning("Jumlah barang tidak boleh kurang dari 0")
                Return False
            End If
            If drow.Item("Harga") < 1 Then
                msgboxWarning("Harga barang tidak boleh kurang dari 0")
                Return False
            End If
            If drow.Item("TglKirim") = Nothing Then
                msgboxWarning("Tanggal kirim tidak boleh kosong")
                Return False
            End If
        Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "PO", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NOPO, 3)) AS KODE FROM TM_PO WHERE LEFT(NOPO, 9) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        Dim tgl As Date

        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        tgl = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function

    Private Sub Recalc_Total()
        ViewTrans.UpdateSummary()
        If xSet.Tables("TD_PO") Is Nothing Then TotalTrans = 0 Else TotalTrans = ViewTrans.Columns("Total").SummaryItem.SummaryValue

        If PPNCheck.Checked = True Then
            PPNValue.EditValue = (TotalTrans - DiscTrans.EditValue) * 0.1
        Else
            PPNValue.EditValue = 0
        End If
        GTTrans.EditValue = TotalTrans - DiscTrans.EditValue + PPNValue.EditValue
    End Sub
End Class