﻿Imports DevExpress.Utils

Public Class PenerimaanBarang
    Dim selected_cell As String

    Private Sub ThisForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_PB")
        xSet.Tables.Remove("TD_PB")
        xSet.Tables.Remove("DaftarGudang")
        xSet.Tables.Remove("DaftarBarang")
        xSet.Tables.Remove("DaftarSupplier")
        xSet.Tables.Remove("DaftarPO")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub ThisForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now
        Else
            SQLquery = String.Format("SELECT NOPB,TANGGAL,NOPO,IDSUPPLIER,MEMO,INVOICE,FAKTUR,ACC FROM TM_PB WHERE NOPB='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_PB")

            NoTrans.Text = xSet.Tables("TM_PB").Rows(0).Item("NOPB")
            TxtInvoice.Text = xSet.Tables("TM_PB").Rows(0).Item("INVOICE")
            TxtFaktur.Text = xSet.Tables("TM_PB").Rows(0).Item("FAKTUR")
            TglTrans.EditValue = xSet.Tables("TM_PB").Rows(0).Item("TANGGAL")
            DaftarSupplier.EditValue = xSet.Tables("TM_PB").Rows(0).Item("IDSUPPLIER")

            If Not xSet.Tables("TM_PB").Rows(0).Item("NOPO").ToString = "" Then
                DaftarPO.Enabled = False
                DaftarSupplier.Enabled = False
                DaftarPO.EditValue = xSet.Tables("TM_PB").Rows(0).Item("NOPO")
            End If

            Catatan.EditValue = xSet.Tables("TM_PB").Rows(0).Item("MEMO")
            ACCcheck.Checked = xSet.Tables("TM_PB").Rows(0).Item("ACC")
        End If
    End Sub

    Private Sub DaftarPO_EditValueChanged(sender As Object, e As EventArgs) Handles DaftarPO.EditValueChanged
        If DaftarPO.Enabled = False Or DaftarPO.EditValue Is Nothing Then Return

        DaftarSupplier.EditValue = ViewDaftarPO.GetFocusedRowCellValue("IDSUPPLIER")
        DaftarSupplier.Enabled = False
        Try
            xSet.Tables("TD_PB").Clear()
            SQLquery = String.Format("SELECT a.IDBARANG Barang, ISNULL(a.QTY,0)-ISNULL(b.QTY,0) Jumlah, MB.SATUAN2 Satuan, HARGA, 0 GRANDTOTAL, '' AS Keterangan, NULL AS Gudang " & _
                                    "FROM TD_PO a LEFT JOIN (SELECT b.IDBARANG, SUM(b.QTY) QTY FROM TM_PB a INNER JOIN TD_PB b ON a.NOPB=b.NOPB " & _
                                    "WHERE a.NOPO='{0}' GROUP BY b.IDBARANG) b ON a.IDBARANG=b.IDBARANG INNER JOIN MBARANG MB ON a.IDBARANG=MB.IDBARANG " & _
                                    "WHERE a.NOPO='{0}' AND a.QTY-ISNULL(b.QTY,0)>0;",
                                    DaftarPO.EditValue)
            ExDb.ExecQuery(SQLquery, xSet, "TD_PB")
        Catch ex As Exception
            xSet.Tables("TD_PB").Clear()
        End Try
    End Sub

    Private Sub DaftarBarang_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarBarang.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell = Repository.EditValue
    End Sub

    Private Sub ViewTrans_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans.CellValueChanged
        Try
            Dim drow As DataRow = xSet.Tables("DaftarBarang").Select(String.Format("ID='{0}'", selected_cell))(0)

            If e.Column.FieldName = "Barang" Then
                'For row As Integer = 0 To ViewTrans.RowCount - 1
                '    If ViewTrans.GetRowCellValue(row, "Barang") = selected_cell And Not ViewTrans.FocusedRowHandle = row Then
                '        ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                '        ViewTrans.FocusedRowHandle = row
                '        Return
                '    End If
                'Next

                ViewTrans.SetFocusedRowCellValue("Jumlah", 1)
                ViewTrans.SetFocusedRowCellValue("Satuan", drow.Item("SATUAN2"))
                ViewTrans.SetFocusedRowCellValue("HARGA", drow.Item("HARGA"))
                ViewTrans.SetFocusedRowCellValue("Keterangan", "")
                ViewTrans.SetFocusedRowCellValue("Gudang", Nothing)
                ViewTrans.FocusedRowHandle = ViewTrans.RowCount
            End If
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("Barang")) = True Then Throw New Exception

            If e.Column.FieldName = "Jumlah" And ViewTrans.FocusedColumn.FieldName = "Jumlah" Then
                If ViewTrans.GetFocusedRowCellValue("Jumlah") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("Jumlah", 0)
                End If
            End If
        Catch ex As Exception
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("Barang")) = True Then
                ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                xSet.Tables("TD_PB").AcceptChanges()
            End If
        End Try
    End Sub

    Private Sub ViewTrans_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewTrans.FocusedRowChanged
        Try
            If e.FocusedRowHandle >= 0 Then
                selected_cell = ViewTrans.GetRowCellValue(e.FocusedRowHandle, "Barang")
            End If
        Catch ex As Exception
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
        End Try
    End Sub

    Private Sub GridTrans_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
            xSet.Tables("TD_PB").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC PB_INS '{0}', '{1}', '{2}', {3}, '{4}', '{5}', '{6}', '{7}','{8}','{9}', """,
                                            NoTrans.Text, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), DaftarSupplier.EditValue,
                                            IIf(DaftarPO.EditValue = Nothing, "NULL", "'" & DaftarPO.EditValue & "'"),
                                            Catatan.Text.Replace("'", "''"), ViewTrans.Columns("GRANDTOTAL").SummaryItem.SummaryValue, ACCcheck.Checked, staff_id, TxtInvoice.Text, TxtFaktur.Text)
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC PB_UPD '{0}', '{1}', '{2}', {3}, '{4}', '{5}', '{6}', '{7}','{8}','{9}', """,
                                            NoTrans.Text, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"), DaftarSupplier.EditValue,
                                            IIf(DaftarPO.EditValue = Nothing, "NULL", "'" & DaftarPO.EditValue & "'"),
                                            Catatan.Text.Replace("'", "''"), ViewTrans.Columns("GRANDTOTAL").SummaryItem.SummaryValue, ACCcheck.Checked, staff_id, TxtInvoice.Text, TxtFaktur.Text)
            End If

            Dim nourut As Integer = 1
            For Each drow As DataRow In xSet.Tables("TD_PB").Rows
                SQLquery += String.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}'), ",
                                          NoTrans.Text, nourut, drow.Item("Barang"), drow.Item("Jumlah"), drow.Item("HARGA"), drow.Item("Keterangan"), drow.Item("Gudang"))
                nourut += 1
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserPenerimaanBarang.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            'Using printout As New xrPengirimanCabang
            '    printout.ShowRibbonPreviewDialog()
            'End Using

            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_PB").Clear()
        xSet.Tables("TD_PB").AcceptChanges()
        DaftarPO.EditValue = Nothing
        DaftarSupplier.EditValue = Nothing

        TglTrans.EditValue = Now
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub Setup_Data()
        '----------------------------------------------------Daftar Supplier
        If Not xSet.Tables("DaftarSupplier") Is Nothing Then xSet.Tables("DaftarSupplier").Clear()
        SQLquery = "SELECT IDSUPPLIER ID, NAMA, ALAMAT, KOTA FROM MSUPPLIER WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarSupplier")
        DaftarSupplier.Properties.DataSource = xSet.Tables("DaftarSupplier").DefaultView

        DaftarSupplier.Properties.NullText = ""
        DaftarSupplier.Properties.ValueMember = "ID"
        DaftarSupplier.Properties.DisplayMember = "NAMA"
        DaftarSupplier.Properties.ShowClearButton = False
        DaftarSupplier.Properties.PopulateViewColumns()

        For Each coll As DataColumn In xSet.Tables("DaftarSupplier").Columns
            ViewDaftarSupplier.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar PO
        If Not xSet.Tables("DaftarPO") Is Nothing Then xSet.Tables("DaftarPO").Clear()
        SQLquery = String.Format("SELECT NOPO, PO.IDSUPPLIER, MS.NAMA, TANGGAL FROM TM_PO PO INNER JOIN MSUPPLIER MS ON PO.IDSUPPLIER=MS.IDSUPPLIER WHERE VOID=0 AND ACC=1 OR NOPO IN (SELECT NOPO FROM TM_PB WHERE NOPB='{0}');", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "DaftarPO")
        DaftarPO.Properties.DataSource = xSet.Tables("DaftarPO").DefaultView

        DaftarPO.Properties.NullText = ""
        DaftarPO.Properties.ValueMember = "NOPO"
        DaftarPO.Properties.DisplayMember = "NOPO"
        DaftarPO.Properties.ShowClearButton = False
        DaftarPO.Properties.PopulateViewColumns()

        ViewDaftarPO.Columns("IDSUPPLIER").Visible = False
        ViewDaftarPO.Columns("TANGGAL").DisplayFormat.FormatType = FormatType.DateTime
        ViewDaftarPO.Columns("TANGGAL").DisplayFormat.FormatString = "dd MMM yyyy"

        For Each coll As DataColumn In xSet.Tables("DaftarPO").Columns
            ViewDaftarPO.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar Stock 
        If Not xSet.Tables("DaftarBarang") Is Nothing Then xSet.Tables("DaftarBarang").Clear()
        SQLquery = "SELECT MB.IDBARANG ID, MK.NAMA KATEGORI, MSK.NAMA SUBKATEGORI, MB.NAMA, MB.ALIAS KIMIA, SATUAN2, HARGABELI HARGA FROM MBARANG MB " & _
                    "INNER JOIN MKATEGORI MK ON MB.IDKATEGORI=MK.IDKATEGORI INNER JOIN MSUBKATEGORI MSK ON MB.IDSUBKATEGORI=MSK.IDSUBKATEGORI " & _
                    "WHERE MB.INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarBarang")
        DaftarBarang.DataSource = xSet.Tables("DaftarBarang").DefaultView

        DaftarBarang.NullText = ""
        DaftarBarang.ValueMember = "ID"
        DaftarBarang.DisplayMember = "NAMA"
        DaftarBarang.ShowClearButton = False
        DaftarBarang.PopulateViewColumns()

        ViewDaftarBarang.Columns("HARGA").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarBarang").Columns
            ViewDaftarBarang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------Daftar Gudang 
        If Not xSet.Tables("DaftarGudang") Is Nothing Then xSet.Tables("DaftarGudang").Clear()
        SQLquery = "SELECT IDGUDANG ID, NAMA FROM MGUDANG WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarGudang")
        DaftarGudang.DataSource = xSet.Tables("DaftarGudang").DefaultView

        DaftarGudang.NullText = ""
        DaftarGudang.ValueMember = "ID"
        DaftarGudang.DisplayMember = "NAMA"
        DaftarGudang.ShowClearButton = False
        DaftarGudang.PopulateViewColumns()

        ViewDaftarGudang.Columns("ID").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarGudang").Columns
            ViewDaftarGudang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------TD_PB
        SQLquery = String.Format("SELECT TD.IDBARANG Barang, QTY Jumlah, MB.SATUAN2 Satuan, HARGA, 0 GRANDTOTAL, Keterangan, TD.IDGUDANG Gudang FROM TD_PB TD INNER JOIN MBARANG MB ON TD.IDBARANG=MB.IDBARANG WHERE NOPB='{0}' ORDER BY NO_URUT", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_PB")
        GridTrans.DataSource = xSet.Tables("TD_PB").DefaultView

        ViewTrans.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top

        For Each coll As DataColumn In xSet.Tables("TD_PB").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("HARGA").Visible = False
        ViewTrans.Columns("GRANDTOTAL").Visible = False
        ViewTrans.Columns("Satuan").OptionsColumn.AllowEdit = False

        ViewTrans.Columns("Barang").ColumnEdit = DaftarBarang
        ViewTrans.Columns("Gudang").ColumnEdit = DaftarGudang

        ViewTrans.Columns("Jumlah").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Jumlah").DisplayFormat.FormatString = "n2"
        ViewTrans.Columns("GRANDTOTAL").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("GRANDTOTAL").DisplayFormat.FormatString = "n2"

        ViewTrans.Columns("Barang").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("Barang").SummaryItem.DisplayFormat = "{0:n2} item(s)"
        ViewTrans.Columns("Jumlah").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("Jumlah").SummaryItem.DisplayFormat = "Total:{0:n2}"
        ViewTrans.Columns("GRANDTOTAL").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("GRANDTOTAL").SummaryItem.DisplayFormat = "{0:n0}"

        ViewTrans.Columns("Barang").Width = GridTrans.Width * 0.25
        ViewTrans.Columns("Jumlah").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("Satuan").Width = GridTrans.Width * 0.1
        ViewTrans.Columns("Keterangan").Width = GridTrans.Width * 0.35
        ViewTrans.Columns("Gudang").Width = GridTrans.Width * 0.15
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_PB").Rows.Count < 1 Then
            Return False
        End If

        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        If DaftarSupplier.EditValue = Nothing Then
            DaftarSupplier.Focus()
            msgboxWarning("Supplier tidak boleh kosong")
            Return False
        End If

        For Each drow As DataRow In xSet.Tables("TD_PB").Rows
            If drow.Item("Jumlah") < 1 Then
                msgboxWarning("Jumlah barang tidak boleh kosong")
                Return False
            End If
            If IsDBNull(drow.Item("Gudang")) Then
                msgboxWarning("Gudang pada daftar tidak boleh kosong")
                Return False
            End If

            drow.Item("GRANDTOTAL") = drow.Item("Jumlah") * drow.Item("HARGA")
        Next
        ViewTrans.UpdateSummary()

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "PB", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NOPB, 3)) AS KODE FROM TM_PB WHERE LEFT(NOPB, 9) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        Dim tgl As Date

        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        tgl = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function
End Class