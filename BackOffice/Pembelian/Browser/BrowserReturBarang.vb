﻿Imports DevExpress.Utils

Public Class BrowserReturBarang
    Public isChange As Boolean

    Private Sub ThisBrowser_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("DaftarBrowserReturBarang")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisBrowser_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        reset_buttons()

        butBaru.Focus()
    End Sub

    Private Sub ThisBrowser_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        isChange = True
        DateEdit1.EditValue = DateAdd(DateInterval.Day, -1, Today.Date)
        DateEdit2.EditValue = Today.Date

        refreshingGrid()
        AddHandler GridView1.DoubleClick, AddressOf butKoreksi_Click
    End Sub

    '--Refresh
    Private Sub butRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butRefresh.Click
        isChange = True
        refreshingGrid()
    End Sub

    '--New
    Private Sub butBaru_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBaru.Click
        selected_id = "0"

        openTheButton()
        refreshingGrid()
    End Sub

    '--Edit
    Private Sub butKoreksi_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butKoreksi.Click
        If butKoreksi.Enabled = False Or butKoreksi.Visible = False Then Return
        If GridView1.FocusedRowHandle < 0 Then Return

        If GridView1.GetFocusedRowCellValue("VOID") = True Then
            msgboxInformation("Transaksi sudah dibatalkan, tidak dapat dikoreksi")
            Return
        ElseIf Not GridView1.GetFocusedRowCellValue("ACC") = "OPEN" Then
            msgboxInformation("Transaksi sudah di ACC, tidak dapat dikoreksi")
            Return
        End If

        selected_id = GridView1.GetFocusedRowCellValue("KODE")

        openTheButton()
        refreshingGrid()
    End Sub

    '--Void
    Private Sub butVoid_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butVoid.Click
        If Not GridView1.RowCount > 0 Then Exit Sub
        If GridView1.GetFocusedRowCellValue("VOID") = True Then
            msgboxInformation("Transaksi sudah dibatalkan, tidak dapat dibatalkan")
            Return
        ElseIf Not GridView1.GetFocusedRowCellValue("ACC") = "OPEN" Then
            msgboxInformation("Transaksi sudah di ACC, tidak dapat dibatalkan")
            Return
        End If

        'If Not xSet.Tables("KODETERPAKAI") Is Nothing Then xSet.Tables("KODETERPAKAI").Clear()
        'SQLquery = String.Format("SELECT COUNT(*) ADA FROM TM_RTRPB WHERE NOPO='{0}'", GridView1.GetFocusedRowCellValue("KODE"))
        'ExDb.ExecQuery(SQLquery, xSet, "KODETERPAKAI")
        'If xSet.Tables("KODETERPAKAI").Rows(0).Item(0) > 0 Then
        '    'TIDAK BISA DI HAPUS
        '    If MsgBox("Nomor transaksi ini hanya bisa di BATALKAN, yakin ingin membatalkan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbYes Then
        '        SQLquery = String.Format("UPDATE TM_PO SET VOID=1, IDUSER={1}, LASTUPDATE=GETDATE() WHERE NOPO='{0}'; ", GridView1.GetFocusedRowCellValue("KODE"), staff_id)
        '        ExDb.ExecData(SQLquery)

        '        refreshingGrid()
        '    End If
        'Else
        'BISA DI HAPUS
        If MsgBox("Nomor transaksi ini bisa di HAPUS, yakin ingin menghapus?", MsgBoxStyle.YesNo, "Konfirmasi") = vbYes Then
            SQLquery = String.Format("DELETE FROM TM_RTRPB WHERE NORTRPB='{0}'; ", GridView1.GetFocusedRowCellValue("KODE"))
            ExDb.ExecData(SQLquery)

            isChange = True
            refreshingGrid()
        End If
        'End If

    End Sub

    '--Print
    Private Sub butView_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butView.Click
        If GridView1.FocusedRowHandle < 0 Then Exit Sub
        selected_id = GridView1.GetFocusedRowCellValue("KODE")

        Using printout As New xrReturBarang
            printout.param_notrans.Value = selected_id
            Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                tool.ShowRibbonPreviewDialog()
            End Using
        End Using
    End Sub

    '--Close Form
    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub reset_buttons()
        butBaru.Visible = True
        butKoreksi.Visible = True
        butVoid.Visible = True
        butView.Visible = True

        butBaru.Text = "Baru"
        butKoreksi.Text = "Koreksi"
        butVoid.Text = "Batal"
        butView.Text = "Cetak"

        butBaru.Enabled = True
        butKoreksi.Enabled = True
        butVoid.Enabled = True
        butView.Enabled = True

        butCheckShow.Enabled = True
    End Sub

    Private Sub openTheButton()
        ShowModule(ReturBarang, "Retur Beli")
        ReturBarang = Nothing
    End Sub

    Private Sub refreshingGrid()
        If isChange = False Then Exit Sub Else isChange = False

        Dim frow As Integer = GridView1.FocusedRowHandle
        If Not xSet.Tables("DaftarBrowserReturBarang") Is Nothing Then xSet.Tables.Remove("DaftarBrowserReturBarang")
        reset_buttons()
        SQLquery = "SELECT TM.NORTRPB KODE, TM.TANGGAL, MS.NAMA, CASE ACC WHEN 0 THEN 'OPEN' WHEN 1 THEN 'ACC' ELSE 'CLOSE' END AS ACC, VOID " & _
                   "FROM TM_RTRPB TM INNER JOIN MSUPPLIER MS ON TM.IDSUPPLIER=MS.IDSUPPLIER WHERE "
        If butCheckShow.Checked = False Then SQLquery += " TM.VOID=0 AND "
        SQLquery += String.Format(" CAST(TM.TANGGAL AS DATE) BETWEEN '{0}' AND '{1}' ORDER BY TM.TANGGAL", Format(DateEdit1.EditValue, "yyyy/MM/dd"), Format(DateEdit2.EditValue, "yyyy/MM/dd"))
        ExDb.ExecQuery(SQLquery, xSet, "DaftarBrowserReturBarang")

        GridControl1.DataSource = xSet.Tables("DaftarBrowserReturBarang").DefaultView

        For Each coll As DataColumn In xSet.Tables("DaftarBrowserReturBarang").Columns
            With GridView1.Columns(coll.ColumnName)
                .AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
                .AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            End With
        Next
        GridView1.Columns("TANGGAL").DisplayFormat.FormatType = FormatType.DateTime
        GridView1.Columns("TANGGAL").DisplayFormat.FormatString = "dd/MMM/yyyy HH:mm"

        GridView1.BestFitColumns()
        GridView1.FocusedRowHandle = frow
    End Sub

End Class