﻿Imports DevExpress.Utils

Public Class ReturBarang
    Dim TotalTrans As Integer
    Dim selected_cell As String

    Private Sub ThisForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        On Error Resume Next
        xSet.Tables.Remove("TM_RTRPB")
        xSet.Tables.Remove("TD_RTRPB")
        xSet.Tables.Remove("DaftarGudang")
        xSet.Tables.Remove("DaftarBarang")
        xSet.Tables.Remove("DaftarPB")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub ThisForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TglTrans.Focus()
        LoadSprite.CloseLoading()
    End Sub

    Private Sub ThisForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        LoadSprite.ShowLoading()
        Setup_Data()

        If selected_id = "0" Then
            CreateAutoNumber(NoTrans.Text)
            TglTrans.EditValue = Now

            GetDaftarPBAktif(DateEdit1.EditValue, DateEdit2.EditValue)
        Else
            SQLquery = String.Format("SELECT NORTRPB,TANGGAL,NOPB,TM.IDSUPPLIER,MS.NAMA,MEMO,ACC FROM TM_RTRPB TM INNER JOIN MSUPPLIER MS ON TM.IDSUPPLIER=MS.IDSUPPLIER WHERE NORTRPB='{0}'", selected_id)
            ExDb.ExecQuery(SQLquery, xSet, "TM_RTRPB")

            NoTrans.Text = xSet.Tables("TM_RTRPB").Rows(0).Item("NORTRPB")
            TglTrans.EditValue = xSet.Tables("TM_RTRPB").Rows(0).Item("TANGGAL")
            DataSupplier.EditValue = xSet.Tables("TM_RTRPB").Rows(0).Item("NAMA")

            If Not xSet.Tables("TM_RTRPB").Rows(0).Item("NOPB").ToString = "" Then
                DaftarPB.Enabled = False
                DaftarPB.EditValue = xSet.Tables("TM_RTRPB").Rows(0).Item("NOPB")
            End If

            Catatan.EditValue = xSet.Tables("TM_RTRPB").Rows(0).Item("MEMO")
            ACCcheck.Checked = xSet.Tables("TM_RTRPB").Rows(0).Item("ACC")
        End If
    End Sub

    Private Sub DateEdit1_EditValueChanging(sender As Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles DateEdit1.EditValueChanging
        If DateEdit1.EditValue Is Nothing Or DateEdit2.EditValue Is Nothing Then Return
        If xSet.Tables("TD_RTRPB").Rows.Count > 0 Then
            If MsgBox("Yakin ingin mengganti tanggal?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return
            xSet.Tables("TD_RTRPB").Clear()
        End If
        GetDaftarPBAktif(DateEdit1.EditValue, DateEdit2.EditValue)
    End Sub

    Private Sub DateEdit2_EditValueChanging(sender As Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles DateEdit2.EditValueChanging
        If DateEdit1.EditValue Is Nothing Or DateEdit2.EditValue Is Nothing Then Return
        If xSet.Tables("TD_RTRPB").Rows.Count > 0 Then
            If MsgBox("Yakin ingin mengganti tanggal?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then Return
            xSet.Tables("TD_RTRPB").Clear()
        End If
        GetDaftarPBAktif(DateEdit1.EditValue, DateEdit2.EditValue)
    End Sub

    Private Sub DaftarPB_EditValueChanged(sender As Object, e As EventArgs) Handles DaftarPB.EditValueChanged
        If DaftarPB.Enabled = False Then Return

        DataSupplier.EditValue = ViewDaftarPB.GetFocusedRowCellValue("NAMA")
        Try
            xSet.Tables("TD_RTRPB").Clear()
            SQLquery = String.Format("SELECT a.IDBARANG, NAMA Barang, ISNULL(a.QTY,0) Jumlah, HARGA, a.QTY*HARGA TOTAL, a.IDGUDANG AS Gudang " & _
                                    "FROM TD_PB a INNER JOIN MBARANG MB ON a.IDBARANG=MB.IDBARANG WHERE a.NOPB='{0}' ", DaftarPB.EditValue)
            ExDb.ExecQuery(SQLquery, xSet, "TD_RTRPB")
        Catch ex As Exception
            xSet.Tables("TD_RTRPB").Clear()
        End Try
    End Sub

    Private Sub DaftarBarang_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DaftarBarang.EditValueChanged
        Dim Repository As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
        If Repository.Text = vbNullString Then Return

        selected_cell = Repository.EditValue
    End Sub

    Private Sub ViewTrans_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles ViewTrans.CellValueChanged
        Try
            'Dim drow As DataRow = xSet.Tables("DaftarBarang").Select(String.Format("ID='{0}'", selected_cell))(0)

            'If e.Column.FieldName = "Barang" Then
            '    'For row As Integer = 0 To ViewTrans.RowCount - 1
            '    '    If ViewTrans.GetRowCellValue(row, "Barang") = selected_cell And Not ViewTrans.FocusedRowHandle = row Then
            '    '        ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
            '    '        ViewTrans.FocusedRowHandle = row
            '    '        Return
            '    '    End If
            '    'Next

            '    ViewTrans.SetFocusedRowCellValue("Jumlah", 1)
            '    ViewTrans.SetFocusedRowCellValue("HARGA", drow.Item("HARGA"))
            '    ViewTrans.SetFocusedRowCellValue("TOTAL", drow.Item("HARGA"))
            '    ViewTrans.SetFocusedRowCellValue("Gudang", Nothing)
            '    ViewTrans.FocusedRowHandle = ViewTrans.RowCount
            'End If
            'If IsDBNull(ViewTrans.GetFocusedRowCellValue("Barang")) = True Then Throw New Exception

            If e.Column.FieldName = "Jumlah" And ViewTrans.FocusedColumn.FieldName = "Jumlah" Then
                If ViewTrans.GetFocusedRowCellValue("Jumlah") < 0 Then
                    ViewTrans.SetFocusedRowCellValue("Jumlah", 0)
                    ViewTrans.SetFocusedRowCellValue("TOTAL", 0)
                Else
                    ViewTrans.SetFocusedRowCellValue("TOTAL", ViewTrans.GetFocusedRowCellValue("Jumlah") * ViewTrans.GetFocusedRowCellValue("HARGA"))
                End If
            End If
        Catch ex As Exception
            If IsDBNull(ViewTrans.GetFocusedRowCellValue("Barang")) = True Then
                ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
                xSet.Tables("TD_RTRPB").AcceptChanges()
            End If
        End Try
    End Sub

    Private Sub ViewTrans_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles ViewTrans.FocusedRowChanged
        Try
            If e.FocusedRowHandle >= 0 Then
                selected_cell = ViewTrans.GetRowCellValue(e.FocusedRowHandle, "Barang")
            End If
        Catch ex As Exception
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
        End Try
    End Sub

    Private Sub GridTrans_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles GridTrans.KeyDown
        If e.KeyCode = Keys.Delete Then
            ViewTrans.DeleteRow(ViewTrans.FocusedRowHandle)
            xSet.Tables("TD_RTRPB").AcceptChanges()
        End If
    End Sub

    Private Sub ButSimpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButSimpan.Click
        If Not beforeSave() Then Return
        LoadSprite.ShowLoading()
        ViewTrans.UpdateSummary()

        Try
            If selected_id = "0" Then
                CreateAutoNumber(NoTrans.Text)
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC RTRPB_INS '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', """,
                                            NoTrans.Text, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"),
                                            xSet.Tables("DaftarPB").Select("NOPB='" & DaftarPB.EditValue & "'")(0).Item("IDSUPPLIER"),
                                            DaftarPB.EditValue,
                                            Catatan.Text.Replace("'", "''"), ViewTrans.Columns("TOTAL").SummaryItem.SummaryValue, ACCcheck.Checked, staff_id)
            Else
                SQLquery = String.Format("SET QUOTED_IDENTIFIER OFF; EXEC RTRPB_UPD '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', """,
                                            NoTrans.Text, Format(TglTrans.EditValue, "yyyy-MM-dd HH:mm"),
                                            xSet.Tables("DaftarPB").Select("NOPB='" & DaftarPB.EditValue & "'")(0).Item("IDSUPPLIER"),
                                            DaftarPB.EditValue,
                                            Catatan.Text.Replace("'", "''"), ViewTrans.Columns("TOTAL").SummaryItem.SummaryValue, ACCcheck.Checked, staff_id)
            End If

            Dim nourut As Integer = 1
            For Each drow As DataRow In xSet.Tables("TD_RTRPB").Rows
                SQLquery += String.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}'), ", NoTrans.Text, nourut, drow.Item("IDBARANG"), drow.Item("Jumlah"), drow.Item("HARGA"), drow.Item("Gudang"))
                nourut += 1
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & """;"
            ExDb.ExecData(SQLquery)

            msgboxInformation("Data berhasil disimpan, tekan OK untuk melanjutkan.")
            BrowserReturBarang.isChange = True
        Catch ex As Exception
            msgboxErrorDev()
        Finally
            LoadSprite.CloseLoading()

            'Using printout As New xrPengirimanCabang
            '    printout.ShowRibbonPreviewDialog()
            'End Using

            Close()
        End Try
    End Sub

    Private Sub ButClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButClear.Click
        xSet.Tables("TD_RTRPB").Clear()
        xSet.Tables("TD_RTRPB").AcceptChanges()
        DataSupplier.EditValue = Nothing
        DaftarPB.EditValue = Nothing

        ViewTrans.UpdateSummary()
        TotalTrans = ViewTrans.Columns("Total").SummaryItem.SummaryValue

        TglTrans.EditValue = Now
        Catatan.Text = ""
    End Sub

    Private Sub ButBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButBatal.Click
        Close()
    End Sub

    Private Sub GetDaftarPBAktif(ByVal Tgl1 As Date, ByVal Tgl2 As Date)
        '----------------------------------------------------Daftar PB
        If Not xSet.Tables("DaftarPB") Is Nothing Then xSet.Tables("DaftarPB").Clear()
        SQLquery = String.Format("SELECT NOPB, PB.IDSUPPLIER, MS.NAMA, TANGGAL FROM TM_PB PB INNER JOIN MSUPPLIER MS ON PB.IDSUPPLIER=MS.IDSUPPLIER " & _
                                 "WHERE ACC=1 AND CAST(PB.TANGGAL AS DATE) BETWEEN '{1}' AND '{2}' OR NOPB IN (SELECT NOPB FROM TM_RTRPB WHERE VOID=0 AND NORTRPB <> '{0}');",
                                 selected_id, Format(Tgl1, "yyyy-MM-dd"), Format(Tgl2, "yyyy-MM-dd"))
        ExDb.ExecQuery(SQLquery, xSet, "DaftarPB")
        DaftarPB.Properties.DataSource = xSet.Tables("DaftarPB").DefaultView

        DaftarPB.Properties.NullText = ""
        DaftarPB.Properties.ValueMember = "NOPB"
        DaftarPB.Properties.DisplayMember = "NOPB"
        DaftarPB.Properties.ShowClearButton = False
        DaftarPB.Properties.PopulateViewColumns()

        ViewDaftarPB.Columns("IDSUPPLIER").Visible = False
        ViewDaftarPB.Columns("TANGGAL").DisplayFormat.FormatType = FormatType.DateTime
        ViewDaftarPB.Columns("TANGGAL").DisplayFormat.FormatString = "dd MMM yyyy"

        For Each coll As DataColumn In xSet.Tables("DaftarPB").Columns
            ViewDaftarPB.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
    End Sub

    Private Sub Setup_Data()

        '----------------------------------------------------Daftar Stock 
        'If Not xSet.Tables("DaftarBarang") Is Nothing Then xSet.Tables("DaftarBarang").Clear()
        'SQLquery = "SELECT MB.IDBARANG ID, MK.NAMA KATEGORI, MSK.NAMA SUBKATEGORI, MB.NAMA, MB.ALIAS KIMIA, SATUAN2, HARGABELI HARGA FROM MBARANG MB " & _
        '            "INNER JOIN MKATEGORI MK ON MB.IDKATEGORI=MK.IDKATEGORI INNER JOIN MSUBKATEGORI MSK ON MB.IDSUBKATEGORI=MSK.IDSUBKATEGORI " & _
        '            "WHERE MB.INACTIVE=0;"
        'ExDb.ExecQuery(SQLquery, xSet, "DaftarBarang")
        'DaftarBarang.DataSource = xSet.Tables("DaftarBarang").DefaultView

        'DaftarBarang.NullText = ""
        'DaftarBarang.ValueMember = "ID"
        'DaftarBarang.DisplayMember = "NAMA"
        'DaftarBarang.ShowClearButton = False
        'DaftarBarang.PopulateViewColumns()

        'ViewDaftarBarang.Columns("HARGA").Visible = False
        'For Each coll As DataColumn In xSet.Tables("DaftarBarang").Columns
        '    ViewDaftarBarang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        'Next

        '----------------------------------------------------Daftar Gudang 
        If Not xSet.Tables("DaftarGudang") Is Nothing Then xSet.Tables("DaftarGudang").Clear()
        SQLquery = "SELECT IDGUDANG ID, NAMA FROM MGUDANG WHERE INACTIVE=0;"
        ExDb.ExecQuery(SQLquery, xSet, "DaftarGudang")
        DaftarGudang.DataSource = xSet.Tables("DaftarGudang").DefaultView

        DaftarGudang.NullText = ""
        DaftarGudang.ValueMember = "ID"
        DaftarGudang.DisplayMember = "NAMA"
        DaftarGudang.ShowClearButton = False
        DaftarGudang.PopulateViewColumns()

        ViewDaftarGudang.Columns("ID").Visible = False
        For Each coll As DataColumn In xSet.Tables("DaftarGudang").Columns
            ViewDaftarGudang.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        '----------------------------------------------------TD_RTRPB
        SQLquery = String.Format("SELECT TD.IDBARANG, MB.NAMA Barang, QTY Jumlah, HARGA, TOTAL, TD.IDGUDANG Gudang FROM TD_RTRPB TD INNER JOIN MBARANG MB ON TD.IDBARANG=MB.IDBARANG WHERE NORTRPB='{0}' ORDER BY NO_URUT", selected_id)
        ExDb.ExecQuery(SQLquery, xSet, "TD_RTRPB")
        GridTrans.DataSource = xSet.Tables("TD_RTRPB").DefaultView

        'ViewTrans.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top

        For Each coll As DataColumn In xSet.Tables("TD_RTRPB").Columns
            ViewTrans.Columns(coll.ColumnName).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next

        ViewTrans.Columns("IDBARANG").Visible = False
        ViewTrans.Columns("IDBARANG").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("Barang").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("HARGA").OptionsColumn.AllowEdit = False
        ViewTrans.Columns("TOTAL").OptionsColumn.AllowEdit = False

        'ViewTrans.Columns("Barang").ColumnEdit = DaftarBarang
        ViewTrans.Columns("Gudang").ColumnEdit = DaftarGudang

        ViewTrans.Columns("Jumlah").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("Jumlah").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("HARGA").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("HARGA").DisplayFormat.FormatString = "n0"
        ViewTrans.Columns("TOTAL").DisplayFormat.FormatType = FormatType.Numeric
        ViewTrans.Columns("TOTAL").DisplayFormat.FormatString = "n0"

        ViewTrans.Columns("Barang").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
        ViewTrans.Columns("Barang").SummaryItem.DisplayFormat = "{0:n0} item(s)"
        ViewTrans.Columns("Jumlah").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("Jumlah").SummaryItem.DisplayFormat = "Total:{0:n0}"
        ViewTrans.Columns("TOTAL").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        ViewTrans.Columns("TOTAL").SummaryItem.DisplayFormat = "Total:{0:n0}"

        ViewTrans.Columns("Barang").Width = GridTrans.Width * 0.3
        ViewTrans.Columns("Jumlah").Width = GridTrans.Width * 0.15
        ViewTrans.Columns("TOTAL").Width = GridTrans.Width * 0.4
        ViewTrans.Columns("Gudang").Width = GridTrans.Width * 0.15

        '----------------------------------------------------Tanggal untuk daftar pb
        DateEdit1.EditValue = Today.Date
        DateEdit2.EditValue = Today.Date
    End Sub

    Private Function beforeSave() As Boolean
        If xSet.Tables("TD_RTRPB").Rows.Count < 1 Then
            Return False
        End If

        If DaftarPB.EditValue = Nothing Then
            DaftarPB.Focus()
            msgboxWarning("PB tidak boleh kosong")
            Return False
        End If

        If TglTrans.EditValue = Nothing Then
            TglTrans.Focus()
            msgboxWarning("Tanggal tidak boleh kosong")
            Return False
        End If

        If xSet.Tables("TD_RTRPB").Select("Jumlah>0").Length <= 0 Then
            msgboxWarning("Jumlah tidak boleh kosong")
            Return False
        End If

        For Each drow As DataRow In xSet.Tables("TD_RTRPB").Select("Jumlah>0")
            If drow.Item("Jumlah") < 0 Then
                msgboxWarning("Jumlah barang tidak boleh kurang dari 0")
                Return False
            End If
            If IsDBNull(drow.Item("Gudang")) Then
                msgboxWarning("Gudang pada daftar tidak boleh kosong")
                Return False
            End If
        Next

        If MsgBox("Yakin ingin menyimpan?", MsgBoxStyle.YesNo, "Konfirmasi") = vbNo Then
            Return False
        End If

        Return True
    End Function

    Private Sub CreateAutoNumber(ByRef KodeBaru As String)
        Dim no_bukti, kode_bukti, urutan_bukti As String
        Dim kode As String = String.Format("{0}/{1}/", "RTR", Format(AmbilTanggalServer(), "yy/MM"))
        SQLquery = String.Format("SELECT MAX(RIGHT(NORTRPB, 3)) AS KODE FROM TM_RTRPB WHERE LEFT(NORTRPB, 10) ='{0}'", UCase(kode))
        ExDb.ExecQuery(SQLquery, xSet, "CreateAutoNumber")

        If IsDBNull(xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")) = True Then
            no_bukti = 0
        Else
            no_bukti = xSet.Tables("CreateAutoNumber").Rows(xSet.Tables("CreateAutoNumber").Rows.Count - 1).Item("KODE")
        End If

        no_bukti += 1
        urutan_bukti = Mid("000", 1, Len("000") - Len(no_bukti)) & no_bukti
        kode_bukti = UCase(kode) & urutan_bukti
        KodeBaru = kode_bukti

        xSet.Tables("CreateAutoNumber").Clear()
    End Sub

    Private Function AmbilTanggalServer() As Date
        Dim tgl As Date

        If Not xSet.Tables("TglSekarang") Is Nothing Then xSet.Tables("TglSekarang").Clear()
        SQLquery = "SELECT GETDATE();"
        ExDb.ExecQuery(SQLquery, xSet, "TglSekarang")
        tgl = xSet.Tables("TglSekarang").Rows(0).Item(0)

        Return tgl
    End Function
End Class