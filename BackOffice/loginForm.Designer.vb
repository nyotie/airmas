﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class loginForm
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LoginUsername = New DevExpress.XtraEditors.TextEdit()
        Me.LoginPassword = New DevExpress.XtraEditors.TextEdit()
        Me.butLogin = New DevExpress.XtraEditors.SimpleButton()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.LoginUsername.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LoginPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LoginUsername
        '
        Me.LoginUsername.Location = New System.Drawing.Point(225, 77)
        Me.LoginUsername.Name = "LoginUsername"
        Me.LoginUsername.Size = New System.Drawing.Size(178, 20)
        Me.LoginUsername.TabIndex = 2
        '
        'LoginPassword
        '
        Me.LoginPassword.Location = New System.Drawing.Point(225, 118)
        Me.LoginPassword.Name = "LoginPassword"
        Me.LoginPassword.Properties.MaxLength = 10
        Me.LoginPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.LoginPassword.Size = New System.Drawing.Size(178, 20)
        Me.LoginPassword.TabIndex = 3
        '
        'butLogin
        '
        Me.butLogin.Location = New System.Drawing.Point(225, 144)
        Me.butLogin.Name = "butLogin"
        Me.butLogin.Size = New System.Drawing.Size(75, 23)
        Me.butLogin.TabIndex = 4
        Me.butLogin.Text = "Login"
        '
        'butClose
        '
        Me.butClose.Location = New System.Drawing.Point(306, 144)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(75, 23)
        Me.butClose.TabIndex = 5
        Me.butClose.Text = "Tutup"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(-1, -1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(450, 200)
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(225, 99)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl2.TabIndex = 12
        Me.LabelControl2.Text = "Password:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(225, 58)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl1.TabIndex = 11
        Me.LabelControl1.Text = "Username :"
        '
        'loginForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(448, 198)
        Me.ControlBox = False
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.LoginUsername)
        Me.Controls.Add(Me.LoginPassword)
        Me.Controls.Add(Me.butLogin)
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "loginForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TopMost = True
        CType(Me.LoginUsername.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LoginPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LoginUsername As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LoginPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents butLogin As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
End Class
