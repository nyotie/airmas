﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ModProperties
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.trans_creator = New DevExpress.XtraEditors.TextEdit()
        Me.trans_createdate = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.trans_editor = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.trans_editdate = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.trans_status = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.trans_ketvoid = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.trans_creator.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trans_createdate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trans_editor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trans_editdate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trans_status.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trans_ketvoid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl3.TabIndex = 21
        Me.LabelControl3.Text = "Creator :"
        '
        'trans_creator
        '
        Me.trans_creator.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.trans_creator.Location = New System.Drawing.Point(12, 30)
        Me.trans_creator.Name = "trans_creator"
        Me.trans_creator.Properties.ReadOnly = True
        Me.trans_creator.Size = New System.Drawing.Size(193, 20)
        Me.trans_creator.TabIndex = 2
        Me.trans_creator.TabStop = False
        '
        'trans_createdate
        '
        Me.trans_createdate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.trans_createdate.Location = New System.Drawing.Point(12, 74)
        Me.trans_createdate.Name = "trans_createdate"
        Me.trans_createdate.Properties.ReadOnly = True
        Me.trans_createdate.Size = New System.Drawing.Size(193, 20)
        Me.trans_createdate.TabIndex = 3
        Me.trans_createdate.TabStop = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 56)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl1.TabIndex = 21
        Me.LabelControl1.Text = "Create date :"
        '
        'trans_editor
        '
        Me.trans_editor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.trans_editor.Location = New System.Drawing.Point(12, 118)
        Me.trans_editor.Name = "trans_editor"
        Me.trans_editor.Properties.ReadOnly = True
        Me.trans_editor.Size = New System.Drawing.Size(193, 20)
        Me.trans_editor.TabIndex = 4
        Me.trans_editor.TabStop = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(12, 100)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl2.TabIndex = 21
        Me.LabelControl2.Text = "Last editor :"
        '
        'trans_editdate
        '
        Me.trans_editdate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.trans_editdate.Location = New System.Drawing.Point(12, 162)
        Me.trans_editdate.Name = "trans_editdate"
        Me.trans_editdate.Properties.ReadOnly = True
        Me.trans_editdate.Size = New System.Drawing.Size(193, 20)
        Me.trans_editdate.TabIndex = 5
        Me.trans_editdate.TabStop = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(12, 144)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl4.TabIndex = 21
        Me.LabelControl4.Text = "Last edit date :"
        '
        'trans_status
        '
        Me.trans_status.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.trans_status.Location = New System.Drawing.Point(12, 206)
        Me.trans_status.Name = "trans_status"
        Me.trans_status.Properties.ReadOnly = True
        Me.trans_status.Size = New System.Drawing.Size(193, 20)
        Me.trans_status.TabIndex = 6
        Me.trans_status.TabStop = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(12, 188)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl5.TabIndex = 21
        Me.LabelControl5.Text = "Status :"
        '
        'trans_ketvoid
        '
        Me.trans_ketvoid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.trans_ketvoid.Location = New System.Drawing.Point(12, 252)
        Me.trans_ketvoid.Name = "trans_ketvoid"
        Me.trans_ketvoid.Properties.ReadOnly = True
        Me.trans_ketvoid.Size = New System.Drawing.Size(193, 95)
        Me.trans_ketvoid.TabIndex = 7
        Me.trans_ketvoid.TabStop = False
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(12, 232)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl6.TabIndex = 21
        Me.LabelControl6.Text = "Void notes :"
        '
        'butClose
        '
        Me.butClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butClose.Location = New System.Drawing.Point(113, 353)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(92, 23)
        Me.butClose.TabIndex = 1
        Me.butClose.Text = "Close"
        '
        'ModProperties
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(217, 388)
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.trans_ketvoid)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.trans_status)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.trans_editdate)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.trans_editor)
        Me.Controls.Add(Me.trans_createdate)
        Me.Controls.Add(Me.trans_creator)
        Me.Name = "ModProperties"
        Me.Text = "ModProperties"
        CType(Me.trans_creator.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trans_createdate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trans_editor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trans_editdate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trans_status.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trans_ketvoid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents trans_creator As DevExpress.XtraEditors.TextEdit
    Friend WithEvents trans_createdate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents trans_editor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents trans_editdate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents trans_status As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents trans_ketvoid As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
End Class
