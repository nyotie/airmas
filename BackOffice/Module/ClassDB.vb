﻿Imports System.Data.SqlClient

Public Class ClassDB

    Dim xCom As SqlCommand
    Dim xAdap As SqlDataAdapter
    Dim xTrans As SqlTransaction

    Public Function ExecQuery(ByVal pSQL As String, ByVal pDataSet As DataSet, ByVal pTableName As String) As DataSet
        Dim connection_string As String = conn_string_local

        Try
            Using xCon = New SqlConnection(connection_string)
                xAdap = New SqlDataAdapter(pSQL, xCon)
                xAdap.Fill(pDataSet, pTableName)
                xAdap.Dispose()
            End Using
            SQLquery = ""

            Return pDataSet
        Catch exx As SqlException When exx.Message.Contains("The server was not found or was not accessible.")
            Return Nothing
        Catch ex As SqlException
            Return Nothing
        End Try
    End Function

    Public Sub ExecData(ByVal pSQL As String)
        Dim connection_string As String = conn_string_local

        Using xCon = New SqlConnection(connection_string)
            Try
                xCom = New SqlCommand(pSQL, xCon)
                xCon.Open()
                xTrans = xCon.BeginTransaction
                xCom.Transaction = xTrans
                xCom.ExecuteNonQuery()
                xTrans.Commit()
                xCon.Close()
                xCom.Dispose()
            Catch exx As SqlException When exx.Message.Contains("The server was not found or was not accessible.")
                xTrans.Rollback()
                MsgBox(String.Format("Error On : {0}{1}{2}", exx.Source, Chr(13), exx.Message), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Catch ex As SqlException
                xTrans.Rollback()
                MsgBox(String.Format("Error On : {0}{1}{2}", ex.Source, Chr(13), ex.Message), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                xCon.Close()
            End Try
        End Using
        SQLquery = ""
    End Sub
End Class