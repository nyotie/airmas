﻿Public Class FileSettingsHelper
    '***********************************************************************************
    ' API Function
    '***********************************************************************************
    Private Declare Unicode Function WriteINIFile Lib "kernel32" Alias "WritePrivateProfileStringW" (ByVal pAppName As String, ByVal pKeyName As String, ByVal pString As String, ByVal pFileName As String) As Int32
    Private Declare Unicode Function GetINIFile Lib "kernel32" Alias "GetPrivateProfileStringW" (ByVal pAppName As String, ByVal pKeyName As String, ByVal pDefault As String, ByVal pReturned As String, ByVal pSize As Int32, ByVal pFileName As String) As Int32

    '***********************************************************************************
    '   WriteSettings - Copyright © 2012-2013 PhoenixSoft 
    '***********************************************************************************
    Public Shared Sub WriteSettings(ByVal pFile As String, ByVal pSection As String, ByVal pName As String, ByVal pValue As String)
        WriteINIFile(pSection, pName, pValue, pFile)
    End Sub

    '***********************************************************************************
    '   ReadSettings - Copyright © 2012-2013 PhoenixSoft 
    '***********************************************************************************
    Public Shared Function ReadSettings(ByVal pFile As String, ByVal pSection As String, ByVal pName As String, ByVal pDefault As String) As String
        Dim Values As String = Space$(1024)
        Dim LenParamVal As Long = GetINIFile(pSection, pName, pDefault, Values, Len(Values), pFile)

        Return Left$(Values, LenParamVal)
    End Function
End Class