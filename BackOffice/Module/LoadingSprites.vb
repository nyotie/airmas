﻿Imports DevExpress.Utils

Public Class LoadingSprites
    Public Property Count As Integer = 0
    Private LoadingSprite As WaitDialogForm

    Public Sub ShowLoading()
        LoadingSprite = New WaitDialogForm("Tunggu sebentar..", "Persiapan")
        LoadingSprite.Show()
        Count += 1
    End Sub

    Public Sub ShowLoading(ByVal Info As String, ByVal title As String)
        LoadingSprite = New WaitDialogForm(Info, title)
        LoadingSprite.Show()
        Count += 1
    End Sub

    Public Sub ShowLoading(ByVal Info As String, ByVal title As String, ByVal ParentForm As Form)
        LoadingSprite = New WaitDialogForm(Info, title) With {.Owner = ParentForm}
        LoadingSprite.Show()
        Count += 1
    End Sub

    Public Sub CloseLoading()
        If Count <= 0 Then Return
        If LoadingSprite.Created Then
            LoadingSprite.Close()
            LoadingSprite.Dispose()
            Count -= 1
        End If
    End Sub
End Class
