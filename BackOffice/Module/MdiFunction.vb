﻿Module MdiFunction

    Public Sub ShowLookUpModule(ByVal pForm As Form, ByVal pFormInduk As Form, ByVal pCaption As String)
        If Not pForm.IsDisposed And pForm.CanFocus Then
            pForm.BringToFront()
        Else
            pForm.FormBorderStyle = FormBorderStyle.FixedSingle
            pForm.WindowState = FormWindowState.Maximized
            pForm.KeyPreview = True
            pForm.Text = pCaption
            pForm.MinimizeBox = False
            pForm.MaximizeBox = False
            pForm.MdiParent = pFormInduk
            pForm.Show()
        End If
    End Sub

    Public Sub ShowModule(ByVal pForm As Form, ByVal pCaption As String)
        pForm.ControlBox = False
        pForm.KeyPreview = True
        pForm.StartPosition = FormStartPosition.CenterScreen
        pForm.FormBorderStyle = FormBorderStyle.FixedSingle
        pForm.Text = pCaption
        pForm.ShowDialog()
    End Sub

    Public Function ReplaceString(ByVal pType As Integer, ByVal pStr As String)
        Select Case pType
            Case 1  'Repclace ' -> ''
                Return Replace(pStr, "'", "''")
            Case Else
                Return Replace(pStr, "'", "''")
        End Select
    End Function

    Public Function ReplaceUnString(ByVal pType As Integer, ByVal pStr As String)
        Select Case pType
            Case 1  'Repclace '' -> '
                Return Replace(pStr, "''", "'")
            Case Else
                Return Replace(pStr, "''", "'")
        End Select
    End Function

    Public Sub NumericText(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Select Case e.KeyChar
            Case Chr(8), Chr(48), Chr(49), Chr(50), Chr(51), Chr(52), Chr(53), Chr(54), Chr(55), Chr(56), Chr(57)
                e.Handled = False
            Case Else
                e.Handled = True
        End Select
    End Sub

    Public Function TranslateDay(ByVal day As String) As String
        Dim hari As String = ""
        If day = "Sunday" Then
            hari = "Minggu"
        ElseIf day = "Monday" Then
            hari = "Senin"
        ElseIf day = "Tuesday" Then
            hari = "Selasa"
        ElseIf day = "Wednesday" Then
            hari = "Rabu"
        ElseIf day = "Thursday" Then
            hari = "Kamis"
        ElseIf day = "Friday" Then
            hari = "Jum'at"
        ElseIf day = "Saturday" Then
            hari = "Sabtu"
        End If
        TranslateDay = hari
    End Function

    Public Function angka2word(ByVal ANGKA As String) As String
        Dim word As String
        Dim satuan, puluhan, ratusan As Integer
        Dim c As Long = 0
        Dim tigadigit(5) As String

        If IsNumeric(ANGKA) = False Then
            MsgBox("not valid number")
            Return ANGKA
            Exit Function
        End If

        Dim word0 As String = ""
        tigadigit(0) = " "
        tigadigit(1) = "Ribu "
        tigadigit(2) = "Juta "
        tigadigit(3) = "Milyar "
        tigadigit(4) = "Trilyun "

        Do While Len(RTrim(ANGKA)) > 0

            word = ""
            '3 digit awal
            If Len(RTrim(ANGKA)) > 0 Then
                satuan = CDbl(Right(ANGKA, 1))
                ANGKA = Left(ANGKA, Len(ANGKA) - 1)
            Else
                satuan = 0
            End If
            If Len(RTrim(ANGKA)) > 0 Then
                puluhan = CDbl(Right(ANGKA, 1))
                ANGKA = Left(ANGKA, Len(ANGKA) - 1)
            Else
                puluhan = 0
            End If

            'angka satuan dan puluhan
            If puluhan = 1 Then
                Select Case satuan
                    Case 0
                        word = "Sepuluh " & word
                    Case 1
                        word = "Sebelas " & word
                    Case 2
                        word = "Dua Belas " & word
                    Case 3
                        word = "Tiga Belas " & word
                    Case 4
                        word = "Empat Belas " & word
                    Case 5
                        word = "Lima Belas " & word
                    Case 6
                        word = "Enam Belas " & word
                    Case 7
                        word = "Tujuh Belas " & word
                    Case 8
                        word = "Delapan Belas " & word
                    Case 9
                        word = "Sembilan Belas " & word
                End Select
            ElseIf puluhan = 0 Then
                Select Case satuan
                    Case 0
                        If Len(ANGKA) = 0 Then
                            word = "nol"
                        End If
                    Case 1
                        word = "Satu " & word
                    Case 2
                        word = "Dua " & word
                    Case 3
                        word = "Tiga " & word
                    Case 4
                        word = "Empat " & word
                    Case 5
                        word = "Lima " & word
                    Case 6
                        word = "Enam " & word
                    Case 7
                        word = "Tujuh " & word
                    Case 8
                        word = "Delapan " & word
                    Case 9
                        word = "Sembilan " & word
                End Select
            Else
                Select Case puluhan
                    Case 2
                        word = "Dua Puluh " & word
                    Case 3
                        word = "Tiga Puluh " & word
                    Case 4
                        word = "Empat Puluh " & word
                    Case 5
                        word = "Lima Puluh " & word
                    Case 6
                        word = "Enam Puluh " & word
                    Case 7
                        word = "Tujuh Puluh " & word
                    Case 8
                        word = "Delapan Puluh " & word
                    Case 9
                        word = "Sembilan Puluh " & word
                End Select
                Select Case satuan
                    Case 0
                        word = word & " "
                    Case 1
                        word = word & "Satu "
                    Case 2
                        word = word & "Dua "
                    Case 3
                        word = word & "Tiga "
                    Case 4
                        word = word & "Empat "
                    Case 5
                        word = word & "Lima "
                    Case 6
                        word = word & "Enam "
                    Case 7
                        word = word & "Tujuh "
                    Case 8
                        word = word & "Delapan "
                    Case 9
                        word = word & "Sembilan "
                End Select
            End If
            'ratusan
            If Len(RTrim(ANGKA)) > 0 Then
                ratusan = CDbl(Right(ANGKA, 1))
                ANGKA = Left(ANGKA, Len(ANGKA) - 1)
            Else
                ratusan = 0
            End If
            Select Case ratusan
                Case 0
                Case 1
                    word = "Seratus " & word
                Case 2
                    word = "Dua Ratus " & word
                Case 3
                    word = "Tiga Ratus " & word
                Case 4
                    word = "Empat Ratus " & word
                Case 5
                    word = "Lima Ratus " & word
                Case 6
                    word = "Enam Ratus " & word
                Case 7
                    word = "Tujuh Ratus " & word
                Case 8
                    word = "Delapan Ratus " & word
                Case 9
                    word = "Sembilan Ratus " & word
            End Select
            '----------------------------------
            If Trim(word) <> "" Then
                word = word & tigadigit(c)
            End If
            word0 = word & word0
            c = c + 1
        Loop
        angka2word = word0 & "Rupiah"
    End Function
End Module
