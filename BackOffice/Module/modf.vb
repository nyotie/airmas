﻿Imports System.Data.SqlClient

Module modf
    'WaitFormDialog
    Public LoadSprite As New LoadingSprites

    'Encryption
    Public Const EncryptKey As String = "01encryptitk02"
    Public Encryptor As New TripleDES(EncryptKey)

    'Variabel SQL
    Public xCon As SqlClient.SqlConnection
    Public ExDb As New ClassDB
    Public xSet As New DataSet
    Public SQLquery As String
    Public sp_Query, param_Query As String

    'Connection String
    Public conn_string_local As String
    Public conn_string_online As String

    Public LocalStatus As Boolean

    'Variabel login
    Public staff_id, staff_group, shiftLogin As Integer
    Public staff_name As String
    Public status_login As Boolean = True

    'Variabel permissions tambahan
    Public GlobalEditVisibility As Boolean
    Public GlobalPrintVisibility As Boolean = True

    'Variabel Browsers
    Public selpur_id, selinven_id As Integer
    Public id_module, id_jenistrans As Integer
    Public nameCheck As String

    Public selmaster_id As String
    Public selected_id, selected_no As String

    Public Sub Main()
        Dim appProc As Process() = Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(Process.GetCurrentProcess.MainModule.ModuleName))

        If appProc.Length > 1 Then
            MessageBox.Show("Applikasi sudah berjalan di Desktop.")
        Else
            Try
                My.Application.ChangeCulture("en-US")

                If Not OpenConnection() Then
                    Application.Run(New FrmSettingKoneksi)
                    Return
                End If

                DevExpress.UserSkins.BonusSkins.Register()
                DevExpress.Skins.SkinManager.Default.RegisterAssembly(GetType(DevExpress.UserSkins.BonusSkins).Assembly)
                DevExpress.Skins.SkinManager.Default.RegisterAssembly(GetType(DevExpress.UserSkins.OfficeSkins).Assembly)
                DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("DevExpress Style")

                Application.Run(New MainForm)
            Catch ex As Exception
                Return
            End Try
        End If
    End Sub

    Public Sub msgboxInformation(ByVal xtext As String)
        MsgBox(xtext, MsgBoxStyle.Information, "Perhatian")
    End Sub

    Public Sub msgboxErrorDev()
        MsgBox("Terdapat error pada saat menyimpan/mengambil data, mintalah IT Person perusahaan anda untuk mencari tahu permasalahan yang terjadi.", MsgBoxStyle.Critical, "PERHATIAN!")
    End Sub

    Public Sub msgboxWarning(ByVal xtext As String)
        MsgBox(xtext, MsgBoxStyle.Exclamation, "Perhatian")
    End Sub

    Public Function isDoubleData(ByVal query1 As String) As Boolean
        If Not xSet.Tables("nameCheck") Is Nothing Then xSet.Tables("nameCheck").Clear()
        ExDb.ExecQuery(query1, xSet, "nameCheck")
        If xSet.Tables("nameCheck").Rows.Count > 0 Then
            If selmaster_id = "0" Or Not selmaster_id = xSet.Tables("nameCheck").Rows(0).Item(0) Then
                Return False
            End If
        End If

        Return True
    End Function

    '------------------------------------------------------------GET DATA

    Public Sub Get_idmodule(ByVal nama_module As String)
        If nama_module = "Master Security Groups" Then
            id_module = 1
        ElseIf nama_module = "Master User" Then
            id_module = 2
            'ElseIf nama_module = "Master Supplier" Then
            '    id_module = 3
        ElseIf nama_module = "Master Kategori Barang" Then
            id_module = 4
        ElseIf nama_module = "Master Barang" Then
            id_module = 5
        ElseIf nama_module = "Master Kota" Then
            id_module = 6
        ElseIf nama_module = "Master Konsinyasi" Then
            id_module = 7
        ElseIf nama_module = "Master Pembayaran" Then
            id_module = 8
        ElseIf nama_module = "Design Menu POS" Then
            id_module = 9
        ElseIf nama_module = "Produksi Pusat" Then
            id_module = 15
        ElseIf nama_module = "Pengiriman Cabang" Then
            id_module = 16
        ElseIf nama_module = "Pengiriman Client" Then
            id_module = 17
        ElseIf nama_module = "Penjualan Retur" Then
            id_module = 18
        ElseIf nama_module = "Pemusnahan Retur" Then
            id_module = 19
        ElseIf nama_module = "Kartu Stok Pusat" Then
            id_module = 20
        ElseIf nama_module = "Adjustment Pusat" Then
            id_module = 21
        ElseIf nama_module = "Opname Pusat" Then
            id_module = 22
        ElseIf nama_module = "Produksi Cabang" Then
            id_module = 23
        ElseIf nama_module = "Penerimaan Cabang" Then
            id_module = 24
        ElseIf nama_module = "Kartu Stok Cabang" Then
            id_module = 25
        ElseIf nama_module = "Retur Cabang" Then
            id_module = 26
        ElseIf nama_module = "Adjustment Cabang" Then
            id_module = 27
        ElseIf nama_module = "Opname Cabang" Then
            id_module = 28
        ElseIf nama_module = "Penerimaan Rekanan" Then
            id_module = 29
        ElseIf nama_module = "Retur Rekanan" Then
            id_module = 30
        ElseIf nama_module = "Penerimaan Client" Then
            id_module = 31
        ElseIf nama_module = "Kartu Stok Client" Then
            id_module = 32
        ElseIf nama_module = "Sinkronisasi Data" Then
            id_module = 33
        ElseIf nama_module = "Profile Perusahaan" Then
            id_module = 34
        ElseIf nama_module = "Master Cabang" Then
            id_module = 35
        ElseIf nama_module = "Program Permissions" Then
            id_module = 36
        ElseIf nama_module = "Report Permissions" Then
            id_module = 37
        ElseIf nama_module = "Ganti Cabang" Then
            id_module = 38
        ElseIf nama_module = "Koneksi" Then
            id_module = 39
        ElseIf nama_module = "Daftar Report" Then
            id_module = 40
        ElseIf nama_module = "Konversi ke Retur" Then
            id_module = 41
        ElseIf nama_module = "Penerimaan Retur" Then
            id_module = 42
        ElseIf nama_module = "Reproduksi" Then
            id_module = 43
        ElseIf nama_module = "Opname Retur" Then
            id_module = 44
        ElseIf nama_module = "Retur Client" Then
            id_module = 45
        ElseIf nama_module = "Upload Stok" Then
            id_module = 46
        ElseIf nama_module = "Invoice Client" Then
            id_module = 47
        ElseIf nama_module = "Invoice Rekanan" Then
            id_module = 48
        End If
    End Sub

    Public Sub Get_idtrans(ByVal nama_trans As String)
        If nama_trans = "Hasil Produksi Pusat" Then
            id_jenistrans = 1
        ElseIf nama_trans = "Hasil Produksi Cabang" Then
            id_jenistrans = 2
        ElseIf nama_trans = "Pengiriman Cabang" Then
            id_jenistrans = 3
        ElseIf nama_trans = "Pengiriman Client" Then
            id_jenistrans = 4
        ElseIf nama_trans = "Penerimaan Cabang" Then
            id_jenistrans = 5
        ElseIf nama_trans = "Penerimaan Rekanan" Then
            id_jenistrans = 6
        ElseIf nama_trans = "Konversi ke Retur" Then
            id_jenistrans = 7
        ElseIf nama_trans = "Pemusnahan Retur" Then
            id_jenistrans = 8
        ElseIf nama_trans = "Retur Cabang" Then
            id_jenistrans = 9
        ElseIf nama_trans = "Penerimaan Retur" Then
            id_jenistrans = 10
        ElseIf nama_trans = "Retur Rekanan" Then
            id_jenistrans = 11
        ElseIf nama_trans = "Retur Client" Then
            id_jenistrans = 12
        ElseIf nama_trans = "Penerimaan Client" Then
            id_jenistrans = 13
        ElseIf nama_trans = "Adjustment" Then
            id_jenistrans = 14
        ElseIf nama_trans = "Opname" Then
            id_jenistrans = 15
        ElseIf nama_trans = "Opname Retur" Then
            id_jenistrans = 16
        ElseIf nama_trans = "Penjualan Retur" Then
            id_jenistrans = 17
        ElseIf nama_trans = "Reproduksi" Then
            id_jenistrans = 18
        ElseIf nama_trans = "Penjualan POS" Then
            id_jenistrans = 19
        ElseIf nama_trans = "Closing POS" Then
            id_jenistrans = 20
        ElseIf nama_trans = "Pengembalian Pengiriman Client" Then
            id_jenistrans = 21
        ElseIf nama_trans = "Invoice Client" Then
            id_jenistrans = 22
        ElseIf nama_trans = "Invoice Rekanan" Then
            id_jenistrans = 23
        End If

    End Sub

    Public Sub Get_User_Permissions()
        If Not xSet.Tables("User_ProgPermisisons") Is Nothing Then xSet.Tables("User_ProgPermisisons").Clear()
        SQLquery = "EXEC Get_Data_Permissions_Program_User " & staff_group
        ExDb.ExecQuery(SQLquery, xSet, "User_ProgPermisisons")
    End Sub

    Public Function Get_DataCompanyAndBranch()
        Try
CompanyDataChecker: LoadSprite.ShowLoading("Looking for lemon", "Loading")
            If Not xSet.Tables("DataCompany") Is Nothing Then xSet.Tables("DataCompany").Clear()
            SQLquery = "SELECT NAMA,ALAMAT,KOTA,TELP,FAX,KODEPOS,EMAIL,NPWP FROM MCOMPANY WHERE IDCOMPANY=1"
            ExDb.ExecQuery(SQLquery, xSet, "DataCompany")
            LoadSprite.CloseLoading()
            If xSet.Tables("DataCompany").Rows.Count = 0 Then
                selmaster_id = 0
                CompanyProfile.ShowDialog()
                GoTo CompanyDataChecker
            End If
        Catch ex As Exception
            LoadSprite.CloseLoading()
            msgboxWarning("Terdapat error pada saat mengambil data perusahaan!")
            Return False
        End Try

        Return True
    End Function

    '------------------------------------------------------------CONNECTION SETTINGS

    Public Function OpenConnection() As Boolean
        Try
            LoadSprite.ShowLoading("Mencoba koneksi ke Database", "Testing Koneksi")
            '-------------------------------LOCAL-------------------------------
            If Not ConnectionChecker("Local Database", conn_string_local) Then
                LoadSprite.CloseLoading()
                msgboxWarning("Gagal terhubung dengan Server!")
                LocalStatus = False
                Return False
            Else
                LocalStatus = True
            End If
            LoadSprite.CloseLoading()

            If Get_DataCompanyAndBranch() = False Then Return False
        Catch ex As Exception
            LoadSprite.CloseLoading()
            msgboxWarning("Terdapat masalah pada pengaturan setting. Form pengaturan akan terbuka setelah pesan ini ditutup.")
            Return False
        End Try

        Return True
    End Function

    Public Function ConnectionChecker(ByVal LineConnection As String, ByRef the_packet As String) As Boolean
        Dim ConnectionString As String
        Dim DbName As String = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", LineConnection, "DatabaseName", "")
        If DbName = "" Then Return False

        Dim Usr As String = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", LineConnection, "User", "")
        Dim Host As String = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", LineConnection, "Host", "")
        Dim Pwd As String = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", LineConnection, "Password", "")
        Dim AutenSQL As String = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", LineConnection, "SQL", "")

        If Not Pwd = "" Then
            Pwd = Encryptor.DecryptData(Pwd)
        End If

        If AutenSQL = 1 Then
            ConnectionString = String.Format("Server = {0}; Initial Catalog = {1}; Integrated Security = True", Host, DbName)
        Else
            ConnectionString = String.Format("Timeout=30 ; Server = {0}; Initial Catalog = {1}; Uid = {2}; Pwd = {3}", Host, DbName, Usr, Pwd)
        End If

        xCon = New SqlConnection(ConnectionString)
        Try
            xCon.Open()
            If xCon.State = ConnectionState.Open Then
                xCon.Close()
            Else
                Return False
            End If

            the_packet = ConnectionString
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Module